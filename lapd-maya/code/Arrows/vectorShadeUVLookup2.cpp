/* ==========================================================================
// Arrows : vectorShadeUVLookup2.cpp
//-
// ==========================================================================
// Copyright (C) 2008 Jim Bamber
//
// ==========================================================================
//+
*/
#include "vectorShadeUVLookup2.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "arrowsCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
//#include <maya/MFnPlugin.h> -- this turns the piece of code into the plug-in main
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnStringData.h>
#include <maya/MVector.h>

//#include <H5public.h>
#include <H5Apublic.h>
#include <H5Fpublic.h>
#include <H5Gpublic.h>
#include <H5Ppublic.h>
#include <H5Spublic.h>
#include <H5Tpublic.h>


// 2012-03-03 JB: commented this out as part of the Microsoft "safe" functions nonsense
//#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1  // causes functions like strcpy() to be replaced with more secure versions

// Static data
MTypeId vectorShadeUVLookup2::id( 0x00652 );

// Attributes
MObject vectorShadeUVLookup2::aUVCoord;
MObject vectorShadeUVLookup2::aCoordToUse;
MObject vectorShadeUVLookup2::aTableMode;

MObject vectorShadeUVLookup2::aFilename;
MObject vectorShadeUVLookup2::aGroupName;
MObject vectorShadeUVLookup2::aTableName;
 
MObject vectorShadeUVLookup2::aTableLength;
MObject vectorShadeUVLookup2::aStartIndex;
MObject vectorShadeUVLookup2::aEndIndex;
MObject vectorShadeUVLookup2::aStartValue;
MObject vectorShadeUVLookup2::aEndValue;
 
MObject vectorShadeUVLookup2::aVectorShade;

//MString vectorShadeUVLookup2::mFilename;
//MString vectorShadeUVLookup2::mGroupName;
//MString vectorShadeUVLookup2::mTableName;
//MVectorArray vectorShadeUVLookup2::mVectorShadingTable;


/* ==========================================================================
// lookup()
*/
MFloatVector vectorShadeUVLookup2::lookup(
	float2& uvCoord,
	int coordToUse )
{
	//arrowsCommon::logEntry( (char*) "--> vectorShadeUVLookup2::lookup()" );
	MFloatVector resultVector( 0.0, 0.0, 0.0 );

	int nVectors = mVectorShadingTable.length();
	if ( nVectors > 0 )
	{
		if ( coordToUse < 0 ) coordToUse = 0;
		if ( coordToUse > 1 ) coordToUse = 1;
		float value = uvCoord[coordToUse];
		int index = (int)floor( value * (float)(nVectors-1) );
		if ( index > (nVectors-1) ) index = nVectors - 1;
		if ( index < 0 ) index = 0;
		MVector vectorElement( mVectorShadingTable[index] );
		resultVector = MFloatVector( vectorElement );
	}

	//arrowsCommon::logEntry( (char*) "<-- vectorShadeUVLookup2::lookup()" );
	return resultVector;
}


/* ==========================================================================
// compute()
//
// This function gets called by Maya to evaluate the texture.
//
*/
MStatus vectorShadeUVLookup2::compute(const MPlug& plug, MDataBlock& block) 
{
	arrowsCommon::logEntry( arrowsCommon::eVectorShadeUVLookup );
	MStatus stat;

	// looking for vectorShade
    if ( (plug != aVectorShade) && (plug.parent() != aVectorShade) )
	{
		arrowsCommon::logEntry(
			(char*) "Abnormal exit from vectorShadeUVLookup2::compute(), Maya not asking for vector shade" );
		return MS::kUnknownParameter;
	}

	MDataHandle hUVCoord( block.inputValue(aUVCoord, &stat) );			diLOG_MSTATUS( stat );
	MDataHandle hCoordToUse( block.inputValue(aCoordToUse, &stat) );	diLOG_MSTATUS( stat );
	MDataHandle hTableMode( block.inputValue(aTableMode, &stat) );		diLOG_MSTATUS( stat );

	MDataHandle hFilename( block.inputValue(aFilename, &stat) );        diLOG_MSTATUS( stat );
	MDataHandle hGroupName( block.inputValue(aGroupName, &stat) );      diLOG_MSTATUS( stat );
	MDataHandle hTableName( block.inputValue(aTableName, &stat) );      diLOG_MSTATUS( stat );

	MDataHandle hTableLength( block.inputValue(aTableLength, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle hStartIndex( block.inputValue(aStartIndex, &stat) );    diLOG_MSTATUS( stat );
	MDataHandle hEndIndex( block.inputValue(aEndIndex, &stat) );        diLOG_MSTATUS( stat );
	MDataHandle hStartValue( block.inputValue(aStartValue, &stat) );    diLOG_MSTATUS( stat );
	MDataHandle hEndValue( block.inputValue(aEndValue, &stat) );        diLOG_MSTATUS( stat );

	float2& uvCoord = hUVCoord.asFloat2();
	int coordToUse = hCoordToUse.asInt();
	int tableMode = hTableMode.asInt();

	MString filename = hFilename.asString();
	MString groupName = hGroupName.asString();
	MString tableName = hTableName.asString();

	int tableLength = hTableLength.asInt();
	int startIndex = hStartIndex.asInt();
	int endIndex = hEndIndex.asInt();
	float3& startValue = hStartValue.asFloat3();
	float3& endValue = hEndValue.asFloat3();

	// tableMode == Read
	//   -if tableMode, filename, group, or table have changed, read in the texture table from the HDF5 file
	if ( tableMode == 0 )
	{
		if ( (tableMode != mTableMode) ||
			 (filename != mFilename) || (groupName != mGroupName) || (tableName != mTableName) )
		{
			mTableMode = tableMode;
			mFilename = filename;
			mGroupName = groupName;
			mTableName = tableName;
			cerr << "Calling readHDF5File() from compute()" << endl;
			readHDF5File();
		}
	}

	// tableMode == Generate
	//   -if tableMode, startIndex, endIndex, startValue or endValue have changed, generate the texture table from these values
	if ( tableMode == 1 )
	{
		if ( (tableMode != mTableMode) ||
			 (startIndex != mStartIndex) || (endIndex != mEndIndex) ||
			 (startValue[0] != mStartValue[0]) || (endValue[0] != mEndValue[0]) ||
			 (startValue[1] != mStartValue[1]) || (endValue[1] != mEndValue[1]) ||
			 (startValue[2] != mStartValue[2]) || (endValue[2] != mEndValue[2]) )
		{
			mTableMode = tableMode;
			mStartIndex = startIndex;
			mEndIndex = endIndex;
			mStartValue[0] = startValue[0];
			mStartValue[1] = startValue[1];
			mStartValue[2] = startValue[2];  
			mEndValue[0] = endValue[0];
			mEndValue[1] = endValue[1];
			mEndValue[2] = endValue[2];  
			cerr << "Calling generateTable() from compute()" << endl;
			generateTable();
		}
	}

	//MVectorArray vectorShadingTable;
	//for ( int i=0; i<10; i++ )
	//{
	//	vectorShadingTable.append( MVector(0.2, 0.2, (float)i/10.0) );
	//}

	//arrowsCommon::logEntry( (char*) "About to do vector shade lookup" );
	MFloatVector resultVectorShade;
	resultVectorShade = lookup(	uvCoord, coordToUse );

	// Set output color attribute
	MDataHandle hVectorShade( block.outputValue(aVectorShade) );
//	MFloatVector& vectorShade( hVectorShade.asFloatVector() );
//	vectorShade = resultVectorShade;
	hVectorShade.asFloatVector() = resultVectorShade;
	hVectorShade.setClean();
	block.setClean( plug );

	//arrowsCommon::logEntry( (char*) "<-- vectorShadeUVLookup2::compute()" );
    return MS::kSuccess;
}


/* ==========================================================================
// initialize()
//
// initializes attribute information
*/
MStatus vectorShadeUVLookup2::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData vectorArrayData;		
	MFnStringData stringData;
	MStatus stat, stat2;

	arrowsCommon::logEntry( (char*) "--> vectorShadeUVLookup2::initialize()" );


	// Create input attributes:
	//
	// aUVCoord: an implicit shading network attribute
    MObject child1( nAttr.create("uCoord", "u", MFnNumericData::kFloat) );
    MObject child2( nAttr.create("vCoord", "v", MFnNumericData::kFloat) );
    aUVCoord = nAttr.create( "uvCoord", "uv", child1, child2);
    MAKE_INPUT( nAttr );
    //diLOG_MSTATUS( nAttr.setHidden(true) );

	// aCoordToUse
	aCoordToUse = enumAttr.create( "coordToUse", "coordToUse", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("U", 0) );
	diLOG_MSTATUS( enumAttr.addField("V", 1) );
	MAKE_INPUT( enumAttr );

	// aTableMode
	aTableMode = enumAttr.create( "tableMode", "tableMode", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("Read", 0) );
	diLOG_MSTATUS( enumAttr.addField("Generate", 1) );
	MAKE_INPUT( enumAttr );

	// aFilename
    aFilename = tAttr.create( "filename", "filename", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );

	// aGroupName
    aGroupName = tAttr.create( "groupName", "groupName", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );

	// aTableName
    aTableName = tAttr.create( "tableName", "tableName", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );

	// aTableLength
	aTableLength = nAttr.create( "tableLength", "tableLength", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(256) );
	MAKE_INPUT( nAttr );

	// aStartIndex
	aStartIndex = nAttr.create( "startIndex", "startIndex", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	diLOG_MSTATUS( nAttr.setMin(0) );
	MAKE_INPUT( nAttr );

	// aEndIndex
	aEndIndex = nAttr.create( "endIndex", "endIndex", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	diLOG_MSTATUS( nAttr.setMin(0) );
	MAKE_INPUT( nAttr );

	// aStartValue
	aStartValue = nAttr.create( "startValue", "startValue", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0, 1.0, 1.0) );
	MAKE_INPUT( nAttr );

	// aEndValue
	aEndValue = nAttr.create( "endValue", "endValue", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );


    // Create output attributes
	//
	// aVectorShade
    aVectorShade = nAttr.createColor( "vectorShade", "vectorShade", &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( nAttr );


    // Add the attributes
	//
    diLOG_MSTATUS( addAttribute(aUVCoord) );
    diLOG_MSTATUS( addAttribute(aCoordToUse) );
    diLOG_MSTATUS( addAttribute(aTableMode) );
    diLOG_MSTATUS( addAttribute(aFilename) );
    diLOG_MSTATUS( addAttribute(aGroupName) );
    diLOG_MSTATUS( addAttribute(aTableName) );
    diLOG_MSTATUS( addAttribute(aTableLength) );
	diLOG_MSTATUS( addAttribute(aStartIndex) );
	diLOG_MSTATUS( addAttribute(aEndIndex) );
	diLOG_MSTATUS( addAttribute(aStartValue) );
	diLOG_MSTATUS( addAttribute(aEndValue) );

    diLOG_MSTATUS( addAttribute(aVectorShade) );


    // All inputs affect the output shade
	//
    diLOG_MSTATUS( attributeAffects(aUVCoord, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aCoordToUse, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aTableMode, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aFilename, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aGroupName, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aTableName, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aTableLength, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aStartIndex, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aEndIndex, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aStartValue, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aEndValue, aVectorShade) );

	arrowsCommon::logEntry( (char*) "<-- vectorShadeUVLookup2::initialize()" );
    return MS::kSuccess;
}


/* ==========================================================================
// readHDF5File()
*/
void vectorShadeUVLookup2::readHDF5File()
{
	cerr << "vectorShadeUVLookup2::readHDF5File()" << endl;
	//arrowsCommon::logEntry( (char*) "--> vectorShadeUVLookup2::readHDF5File()" );
	mVectorShadingTable.clear();

	// Open the file.
	//
	if ( mFilename == "" ) return;
	const char *filenameAsChar = mFilename.asChar();
	hid_t file_id = H5Fopen( filenameAsChar, H5F_ACC_RDONLY, H5P_DEFAULT );	  diLOG_HDF5STAT( file_id );
	if ( file_id < 0 ) return;


	// This is the basic scheme for reading data:
	/*
	Define memory dataspace of read. (Optional if dataspace is H5S_SELECT_ALL)
	Open the dataset.
	Get the dataset dataspace. (If using H5S_SELECT_ALL above)

	Else define dataset dataspace of read. Define the memory datatype. (Optional)
	Define the memory buffer.
	Open the dataset.
	Read data.

	Close the datatype, dataspace, and property list. (As necessary)
	Close the dataset.
	*/

	// Open the dataset specified by groupName and tableName.
	//
	if ( mGroupName == "" )
	{
		diLOG_HDF5STAT( H5Fclose(file_id) );
		return;
	}
	const char *groupNameAsChar = mGroupName.asChar();
	hid_t group_id = H5Gopen2( file_id, groupNameAsChar, H5P_DEFAULT );   diLOG_HDF5STAT( group_id );

	if ( mTableName == "" )
	{
		diLOG_HDF5STAT( H5Gclose(group_id) );
		diLOG_HDF5STAT( H5Fclose(file_id) );
		return;
	}
	const char *tableNameAsChar = mTableName.asChar();
	hid_t dataset_id = H5Dopen2( group_id, tableNameAsChar, H5P_DEFAULT );   diLOG_HDF5STAT( dataset_id );

	// Check that the data type is 3-vector.
	//
	hid_t data_type_id = H5Aopen_name( dataset_id, "Type" );   diLOG_HDF5STAT( data_type_id );
	hid_t data_type_type_id = H5Aget_type( data_type_id );	   diLOG_HDF5STAT( data_type_type_id );

	char data_type[120];
	diLOG_HDF5STAT( H5Aread(data_type_id, data_type_type_id, data_type) );
	//sprintf( HDF5common::logString, "data_type: %s", data_type );
	//HDF5common::logEntry( HDF5common::logString );

	diLOG_HDF5STAT( H5Tclose(data_type_type_id) );
	diLOG_HDF5STAT( H5Aclose(data_type_id) );
	
	if ( MString(data_type) != "3-vector" )
	{
		diLOG_HDF5STAT( H5Dclose(dataset_id) );
		diLOG_HDF5STAT( H5Gclose(group_id) );
		diLOG_HDF5STAT( H5Fclose(file_id) );
		return;
	}

	hid_t space_id   = H5Dget_space( dataset_id );			   diLOG_HDF5STAT( space_id );
	hsize_t current_dims[1];
	hsize_t max_dims[1];
	int ndims = H5Sget_simple_extent_dims( space_id, current_dims, max_dims );   diLOG_HDF5STAT( ndims );
	unsigned long npoints = (unsigned long)current_dims[0];

	// Define the data structure and allocate memory.
	//
	typedef struct {
	  float table_x;
	  float table_y;
	  float table_z;
	} table_element;

	hid_t table_type_id = H5Tcreate( H5T_COMPOUND, sizeof(table_element) );   diLOG_HDF5STAT( table_type_id );
	diLOG_HDF5STAT( H5Tinsert(table_type_id, "table_x", HOFFSET(table_element, table_x), H5T_NATIVE_FLOAT) );
	diLOG_HDF5STAT( H5Tinsert(table_type_id, "table_y", HOFFSET(table_element, table_y), H5T_NATIVE_FLOAT) );
	diLOG_HDF5STAT( H5Tinsert(table_type_id, "table_z", HOFFSET(table_element, table_z), H5T_NATIVE_FLOAT) );

	table_element *mem_buffer = (table_element *)malloc( npoints*sizeof(table_element) );

	// Read the dataset.
	//
	diLOG_HDF5STAT( H5Dread(dataset_id, table_type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, mem_buffer) );
	float table_x;
	float table_y;
	float table_z;
	//float table_x = (*mem_buffer).table_x;
	//float table_y = (*mem_buffer).table_y;
	//float table_z = (*mem_buffer).table_z;
	//sprintf( HDF5common::logString, "table_(x,y,z): %f %f %f", table_x, table_y, table_z );
	//HDF5common::logEntry( HDF5common::logString );

	// Build the array attribute.
	//
	MVector vectorElement;
    table_x = (*mem_buffer).table_x;
    table_y = (*mem_buffer).table_y;
    table_z = (*mem_buffer).table_z;
	for (int i=0; i<(int)npoints; i++)
	{
		vectorElement = MVector( table_x, table_y, table_z );
		mVectorShadingTable.append( vectorElement );
        table_x = (*mem_buffer++).table_x;
        table_y = (*mem_buffer).table_y;
        table_z = (*mem_buffer).table_z;
	}

	// Close the ids, etc.
	//
	diLOG_HDF5STAT( H5Tclose(table_type_id) );
	diLOG_HDF5STAT( H5Sclose(space_id) );
	diLOG_HDF5STAT( H5Dclose(dataset_id) );
	diLOG_HDF5STAT( H5Fclose(file_id) );

	//cerr << mVectorShadingTable.length() << endl;
	//free( mem_buffer );  // made Maya crash!
	//cerr << "Just freed mem_buffer" << endl; 


	//arrowsCommon::logEntry( (char*) "<-- vectorShadeUVLookup2::readHDF5File()" );
	return;
}


/* ==========================================================================
// generateTable()
*/
void vectorShadeUVLookup2::generateTable()
{
	//arrowsCommon::logEntry( (char*) "--> vectorShadeUVLookup2::generateTable()" );
	mVectorShadingTable.clear();
	MVector vectorElement;
	double table_x;
	double table_y;
	double table_z;


	// Table is flat (= startValue) from 0 to startIndex-1
	//
	table_x = (double)mStartValue[0];
	table_y = (double)mStartValue[1];
	table_z = (double)mStartValue[2];

	for (int i=0; i<mStartIndex; i++)
	{
		vectorElement = MVector( table_x, table_y, table_z );
		mVectorShadingTable.append( vectorElement );
	}


	// Table is a linear ramp (= startValue -> endValue) from startIndex to endIndex-1
	//
	double m[3];
	int rampLength = mEndIndex - mStartIndex;
	if ( rampLength > 0 )
	{
		m[0] = ( (double)(mEndValue[0]-mStartValue[0]) ) / ( (double)rampLength );
		m[1] = ( (double)(mEndValue[1]-mStartValue[1]) ) / ( (double)rampLength );
		m[2] = ( (double)(mEndValue[2]-mStartValue[2]) ) / ( (double)rampLength );
	}
	else
	{
		m[0] = 0.0;
		m[1] = 0.0;
		m[2] = 0.0;
	}

	for (int i=0; i<rampLength; i++)
	{
		table_x = mStartValue[0] + (double)i * m[0];
		table_y = mStartValue[1] + (double)i * m[1];
		table_z = mStartValue[2] + (double)i * m[2];

		vectorElement = MVector( table_x, table_y, table_z );
		mVectorShadingTable.append( vectorElement );
	}


	// Table is flat (= endValue) from endIndex to tableLength-1
	//
	table_x = (double)mEndValue[0];
	table_y = (double)mEndValue[1];
	table_z = (double)mEndValue[2];

	for (int i=mEndIndex; i<mTableLength; i++)
	{
		vectorElement = MVector( table_x, table_y, table_z );
		mVectorShadingTable.append( vectorElement );
	}


	//arrowsCommon::logEntry( (char*) "<-- vectorShadeUVLookup2::generateTable()" );
	return;
}


/* ==========================================================================
// postConstructor()
*/
void vectorShadeUVLookup2::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType vectorShadeUVLookup2::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// vectorShadeUVLookup2()
*/
vectorShadeUVLookup2::vectorShadeUVLookup2()
{
	mTableMode = 1;  // Generate

	mFilename = "";
	mGroupName = "";
	mTableName = "";

	mTableLength = 256;
	mStartIndex = 0;
	mEndIndex = 0;
	mStartValue[0] = 1.0;
	mStartValue[1] = 1.0;
	mStartValue[2] = 1.0;
	mEndValue[0] = 0.8;
	mEndValue[1] = 0.8;
	mEndValue[2] = 0.8;

	cerr << "calling generateTable() from constructor" << endl;
	generateTable();

	//mVectorShadingTable.clear();
	//for ( int i=0; i<mTableLength; i++ )
	//{
	//	mVectorShadingTable.append( MVector(0.0, 0.0, 0.0) );
	//}
}


/* ==========================================================================
// ~vectorShadeUVLookup2()
*/
vectorShadeUVLookup2::~vectorShadeUVLookup2()
{
}


/* ==========================================================================
// creator()
//
// creates an instance of the node
*/
void * vectorShadeUVLookup2::creator()
{
    return new vectorShadeUVLookup2();
}


