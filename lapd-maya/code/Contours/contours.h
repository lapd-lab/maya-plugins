/* ============================================================================
 * Contours : contours.h
 /
 / Copyright (C) 2008 Jim Bamber
 /
 */
#ifndef CONTOURS_H
#define CONTOURS_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class contours : public MPxNode
{
    public:

    contours();
	virtual void postConstructor();
    virtual ~contours();

    static  void *  creator();
    static  MStatus initialize();
    
    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	// This creates a single mesh which is a collection of contours
    static MStatus createMesh(
		float3& translate,
		float3& rotate,
		float3& scale,
		int rotateOrder,
		MDoubleArray& contourValues,
		int nx, int ny, int nz,
		double dx, double dy, double dz,
		int widthSampling, int heightSampling,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MDoubleArray& scalarDataGrid,
		MObject& newOutputData );

	// This adds a single contour (i.e. single contour value and single plane) to the arrays
	static MStatus addContour(
		MPointArray& contourPlane,
		double contourValue,
		int widthSampling, int heightSampling,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MDoubleArray& scalarDataGrid,
		MPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );

	// This adds a single line segment of a single contour
	static MStatus addLineSegment(
		double contourValue,
		MVector& planeNormal,
		MPointArray& sampledGridCellVertices,
		MDoubleArray& sampledGridCellValues,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MDoubleArray& scalarDataGrid,
		MPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );

	static MPoint intersect(
		MPointArray& sampledGridCellVertices,
		MDoubleArray& sampledGridCellValues,
		int highIndex,
		int lowIndex,
		double contourValue );

	static MStatus lineSegment(
		MPoint& point1,
		MPoint& point2,
		MVector& planeNormal,
		MPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );


	// Input attributes
	static MObject aTranslate;			// Translation vector for the original display plane (float3).
	static MObject aRotate;				// Rotation angles (in degrees) for the original display plane (float3).
	static MObject aScale;				// Scale values for the original display plane (float3).
	static MObject aRotateOrder;		// The order of rotation (enum: xyz, yzx, zxy, xzy, yxz, zyx).
	static MObject aMinContour;			// Minimum contour value (double).
	static MObject aMaxContour;			// Maximum contour value (double).
	static MObject aContourIntervals;	// Number of contour intervals -- one more contour line than this (int).
	static MObject aNx;					// Number of copies of the display plane in the x direction (int).
	static MObject aNy;					// Number of copies of the display plane in the y direction (int).
	static MObject aNz;					// Number of copies of the display plane in the z direction (int).
	static MObject aDx;					// Offset of the display plane in the x direction (double).
	static MObject aDy;					// Offset of the display plane in the y direction (double).
	static MObject aDz;					// Offset of the display plane in the z direction (double).
	static MObject aWidthSampling;		// Number of samples to take across the width of the plane (int).
	static MObject aHeightSampling;		// Number of samples to take across the height of the plane (int).
										// xy: width == +x, height == +y
										// xz: width == -z, height == -x
										// yz: width == -z, height == +y

										// Spatial grid is assumed to be uniform => is not used as an input.
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aScalarDataGrid;		// This supplies the scalars for the secondary mesh (double array).
	static MObject aDataMin;			// Minimum data value (double).
	static MObject aDataMax;			// Maximum data value (double).


	// Output attributes
	static MObject aOutputMesh;			// The newly created mesh of line segments forming the contours.


	// Convenience variables
	static double bevelWidth;


};


#endif

