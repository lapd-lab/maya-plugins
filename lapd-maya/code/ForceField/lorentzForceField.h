/* ============================================================================
// forceField : lorentzForceField.h
//
// Copyright (C) 2006 Jim Bamber 
// 
//  Description
//	The lorentzForceField node implements F = q(E+vxB/c) in cgs units.
//
*/

#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MDataBlock.h>
#include <maya/MPxFieldNode.h>
#include <maya/MFloatPoint.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MVectorArray.h>

#if defined(OSMac_MachO_)
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


class lorentzForceField: public MPxFieldNode
{
public:
	// Public methods.
	//
	lorentzForceField() {};
	virtual ~lorentzForceField() {};
	virtual void postConstructor();

	static void		*creator();
	static MStatus	initialize();

    MPxNode::SchedulingType schedulingType() const;

	virtual MStatus	compute( const MPlug& plug, MDataBlock& block );
	//virtual void 	draw (  M3dView  & view, const  MDagPath  & path,  M3dView::DisplayStyle  style, M3dView:: DisplayStatus );

    virtual MStatus getForceAtPoint(const MVectorArray& point,
                            const MVectorArray& velocity,
                            const MDoubleArray& mass,
                            MVectorArray& force,
                            double deltaTime);

	virtual MStatus iconSizeAndOrigin(	GLuint& width,
						GLuint& height,
						GLuint& xbo,
						GLuint& ybo   );

	virtual MStatus iconBitmap(GLubyte* bitmap);


	// Public attributes.
	//
	static MObject  aParticleType;		// electron or ion
	static MObject  aChargeMultiple;	// only applicable to ions, multiplies proton charge of 4.8032e-10 esu
	static MObject  aMassMultiple;		// only applicable to ions, multiplies proton mass of 1.6726e-24 g
	//
	// Note that mass is actually handled as an inherited per-particle attribute by Maya, and it defaults to 1 gram.
	// The smallest mass that the Maya-inherited attribute can take is about 1e-9 grams.  To get around this problem, I
	// divide the calculated output force by the calculated particle mass.  This scales the force so as to provide the
	// correct acceleration for the 1 gram default mass, and it is the resulting acceleration that we really care about.
	//
	static MObject  aSpatialGrid;		// the spatial grid of the vector field (below)
	static MObject  aDims;				// dimensions of the spatial grid
	static MObject  aGridMin;			// minimum point of spatial grid, in cm
	static MObject  aGridMax;			// maximum point of spatial grid, in cm
	static MObject  aEFieldData;		// electric field vector data grid from HDF5 file, in V/cm
	static MObject  aBFieldData;		// magnetic field vector data grid from HDF5 file, in Gauss
	static MObject  aBackgroundEField;	// background electric field vector, in V/cm
	static MObject  aBackgroundBField;	// background magnetic field vector, in Gauss

	// Other data members
	//
	static MTypeId	id;


private:
	// Private attributes.
	//
	int		xlim[3][2];		// integer bound for point
	double	xarg[3];		// fractional part


	// Private methods to help compute output force.
	//
	void	calculateForceArray( MDataBlock& block,
							const MVectorArray &points,
							const MVectorArray &velocities,
							//const MDoubleArray &masses,
							MVectorArray &forceArray );

	static void calculateFields(
		MVector& point,
		int3& dims,
		float3& gridMax,
		float3& gridMin,
		MVectorArray& EFieldData,
		MVectorArray& BFieldData,
		MVector& backgroundEField,
		MVector& backgroundBField,
		double halfWindow,
		MVector& EField,
		MVector& BField );

	static MVector calculateForce(
		MVector& velocity,
		MVector& EField,
		MVector& BField,
		double EForceFactor,
		double BForceFactor );

	void	ownerPosition( MDataBlock& block, MVectorArray &vArray );
	MStatus	getWorldPosition( MVector &vector );
	MStatus	getWorldPosition( MDataBlock& block, MVector &vector );

	// Private methods to get inherited attribute values.
	//
	double	getMagnitudeValue( MDataBlock& block );
	double	getAttenuationValue( MDataBlock& block );
	double	getMaxDistanceValue( MDataBlock& block );
	bool	getUseMaxDistanceValue( MDataBlock& block );
	bool	getApplyPerVertexValue( MDataBlock& block );

	MStatus	getOwnerCentroidValue( MDataBlock& block, MVector &vector );

	// Private methods to get local attributes values.
	//
	short		 getParticleType( MDataBlock& block );
	int			 getChargeMultiple( MDataBlock& block );
	int			 getMassMultiple( MDataBlock& block );
	MVectorArray getSpatialGrid( MDataBlock& block );
	int3&		 getDims( MDataBlock& block );
	float3&		 getGridMin( MDataBlock& block );
	float3&		 getGridMax( MDataBlock& block );
	MVectorArray getEFieldData( MDataBlock& block );
	MVectorArray getBFieldData( MDataBlock& block );
	MVector		 getBackgroundEField( MDataBlock& block );
	MVector		 getBackgroundBField( MDataBlock& block );

};


// Inline methods
//
/* ============================================================================
// getMagnitudeValue()
*/
inline double lorentzForceField::getMagnitudeValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mMagnitude, &status );   CHECK_MSTATUS( status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


/* ============================================================================
// getAttenuationValue()
*/
inline double lorentzForceField::getAttenuationValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mAttenuation, &status );   CHECK_MSTATUS( status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


/* ============================================================================
// getMaxDistanceValue()
*/
inline double lorentzForceField::getMaxDistanceValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mMaxDistance, &status );   CHECK_MSTATUS( status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


/* ============================================================================
// getUseMaxDistanceValue()
*/
inline bool lorentzForceField::getUseMaxDistanceValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mUseMaxDistance, &status );   CHECK_MSTATUS( status );

	bool value = false;
	if( status == MS::kSuccess )
		value = hValue.asBool();

	return( value );
}


/* ============================================================================
// getApplyPerVertexValue()
*/
inline bool lorentzForceField::getApplyPerVertexValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mApplyPerVertex, &status );   CHECK_MSTATUS( status );

	bool value = false;
	if( status == MS::kSuccess )
		value = hValue.asBool();

	return( value );
}


/* ============================================================================
// getParticleType()
*/
inline short lorentzForceField::getParticleType( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aParticleType, &status );   CHECK_MSTATUS( status );

	short value=0;
	if ( status == MS::kSuccess )
	{
		value = hValue.asShort();
	}

	return( value );
}


/* ============================================================================
// getChargeMultiple()
*/
inline int lorentzForceField::getChargeMultiple( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aChargeMultiple, &status );   CHECK_MSTATUS( status );

	int value=0;
	if ( status == MS::kSuccess )
	{
		value = hValue.asInt();
	}

	return( value );
}


/* ============================================================================
// getMassMultiple()
*/
inline int lorentzForceField::getMassMultiple( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aMassMultiple, &status );   CHECK_MSTATUS( status );

	int value=0;
	if ( status == MS::kSuccess )
	{
		value = hValue.asInt();
	}

	return( value );
}


/* ============================================================================
// getSpatialGrid()
*/
inline MVectorArray lorentzForceField::getSpatialGrid( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aSpatialGrid, &status );   CHECK_MSTATUS( status );

	MVectorArray value;
	if ( status == MS::kSuccess )
	{
        //STV
		//MFnVectorArrayData fnValue = hValue.data();
		MFnVectorArrayData fnValue ( hValue.data());
		value = fnValue.array();
	}

	return( value );
}


/* ============================================================================
// getDims()
*/
inline int3& lorentzForceField::getDims( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aDims, &status );   CHECK_MSTATUS( status );
	int3& value = hValue.asInt3();

	return( value );
}


/* ============================================================================
// getGridMin()
*/
inline float3& lorentzForceField::getGridMin( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aGridMin, &status );   CHECK_MSTATUS( status );
	float3& value = hValue.asFloat3();

	return( value );
}


/* ============================================================================
// getGridMax()
*/
inline float3& lorentzForceField::getGridMax( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aGridMax, &status );   CHECK_MSTATUS( status );
	float3& value = hValue.asFloat3();

	return( value );
}


/* ============================================================================
// getEFieldData()
*/
inline MVectorArray lorentzForceField::getEFieldData( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aEFieldData, &status );   CHECK_MSTATUS( status );

	MVectorArray value;
	if ( status == MS::kSuccess )
	{
        //STV
        //MFnVectorArrayData fnValue = hValue.data();
		MFnVectorArrayData fnValue ( hValue.data());
		value = fnValue.array();
	}

	return( value );
}


/* ============================================================================
// getBFieldData()
*/
inline MVectorArray lorentzForceField::getBFieldData( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aBFieldData, &status );   CHECK_MSTATUS( status );

	MVectorArray value;
	if ( status == MS::kSuccess )
	{
        //STV
        //MFnVectorArrayData fnValue = hValue.data();
		MFnVectorArrayData fnValue ( hValue.data());
		value = fnValue.array();
	}

	return( value );
}


/* ============================================================================
// getBackgroundEField()
*/
inline MVector lorentzForceField::getBackgroundEField( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aBackgroundEField, &status );   CHECK_MSTATUS( status );

	MVector value = MVector( 0.0, 0.0, 0.0 );
	if( status == MS::kSuccess )
		value = hValue.asVector();

	return( value );
}


/* ============================================================================
// getBackgroundBField()
*/
inline MVector lorentzForceField::getBackgroundBField( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aBackgroundBField, &status );   CHECK_MSTATUS( status );

	MVector value = MVector( 0.0, 0.0, 0.0 );
	if( status == MS::kSuccess )
		value = hValue.asVector();

	return( value );
}


/* ============================================================================
// getOwnerCentroidValue()
*/
inline MStatus lorentzForceField::getOwnerCentroidValue(MDataBlock& block, MVector &vector)
{
	MStatus status;

	MDataHandle hValueX = block.inputValue( mOwnerCentroidX, &status );   CHECK_MSTATUS( status );
	MDataHandle hValueY = block.inputValue( mOwnerCentroidY, &status );   CHECK_MSTATUS( status );
	MDataHandle hValueZ = block.inputValue( mOwnerCentroidZ, &status );   CHECK_MSTATUS( status );

	if( status == MS::kSuccess )
	{
		vector[0] = hValueX.asDouble();
		vector[1] = hValueY.asDouble();
		vector[2] = hValueZ.asDouble();
	}

	return( status );
}

