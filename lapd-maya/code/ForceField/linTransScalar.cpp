/* ============================================================================
// forceField : linTransScalar.cpp
//-
// ==========================================================================
// Copyright (C) 2012 Jim Bamber
//
// ==========================================================================
//+
*/
#include "linTransScalar.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MVector.h>


// Static data
MTypeId linTransScalar::id( 0x00462 );

// Attributes
MObject linTransScalar::aM;
MObject linTransScalar::aB;
MObject linTransScalar::aInput;

MObject linTransScalar::aOutputScalar;
MObject linTransScalar::aOutputRGB;


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the UV-mapped tubes.
//
*/
MStatus linTransScalar::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> linTransScalar::compute()" );

	if ( (plug != aOutputScalar) && (plug != aOutputRGB) )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from linTransScalar::compute(), Maya not asking for outputScalar or outputRGB" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hM = block.inputValue( aM, &stat );   diLOG_MSTATUS( stat );
	float m = hM.asFloat();

	MDataHandle hB = block.inputValue( aB, &stat );   diLOG_MSTATUS( stat );
	float b = hB.asFloat();

	MDataHandle hInput = block.inputValue( aInput, &stat );   diLOG_MSTATUS( stat );
	float input = hInput.asFloat();


	// Now do the output attributes.
	//
	MDataHandle hOutputScalar = block.outputValue( aOutputScalar, &stat );   diLOG_MSTATUS( stat );
	float& outputScalar = hOutputScalar.asFloat();

	MDataHandle hOutputRGB = block.outputValue( aOutputRGB, &stat );   diLOG_MSTATUS( stat );
	MFloatVector& outputRGB = hOutputRGB.asFloatVector();


	// Do the linear transformation
	//
	float output_scalar = m * input + b;
	outputScalar = output_scalar;  // Not totally sure if this is necessary

	MFloatVector output_RGB;
	output_RGB.x = output_scalar;
	output_RGB.y = output_scalar;
	output_RGB.z = output_scalar;
	outputRGB = output_RGB;  // Not totally sure if this is necessary


	// Set the outputs as clean.
	//block.setClean( plug );
	hOutputScalar.setClean();
	hOutputRGB.setClean();

	//forceFieldCommon::logEntry( (char*) "<-- linTransScalar::compute()" );
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information and the unit circle values
*/
MStatus linTransScalar::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> linTransScalar::initialize()" );


	// Create input attributes:
	//
	// m
	aM = nAttr.create( "m", "m", MFnNumericData::kFloat, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aM) );

	// b
	aB = nAttr.create( "b", "b", MFnNumericData::kFloat, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aB) );

	// input
	aInput = nAttr.create( "input", "input", MFnNumericData::kFloat, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aInput) );


	// Create output attributes
	//
	// outputScalar
	aOutputScalar = nAttr.create( "outputScalar", "outputScalar", MFnNumericData::kFloat, 0, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aOutputScalar) );

	// outputRGB
	aOutputRGB = nAttr.createColor( "outputRGB", "outputRGB", &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aOutputRGB) );


	// All inputs affect all outputs
	//
	// outputScalar
    diLOG_MSTATUS( attributeAffects(aM, aOutputScalar) );
    diLOG_MSTATUS( attributeAffects(aB, aOutputScalar) );
    diLOG_MSTATUS( attributeAffects(aInput, aOutputScalar) );

	// outputRGB
    diLOG_MSTATUS( attributeAffects(aM, aOutputRGB) );
    diLOG_MSTATUS( attributeAffects(aB, aOutputRGB) );
    diLOG_MSTATUS( attributeAffects(aInput, aOutputRGB) );

	forceFieldCommon::logEntry( (char*) "<-- linTransScalar::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
// linTransScalar()
*/
linTransScalar::linTransScalar()
{
}


/* ============================================================================
// postConstructor()
*/
void linTransScalar::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType linTransScalar::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * linTransScalar::creator()
{
    return new linTransScalar();
}


/* ============================================================================
// ~linTransScalar()
*/
linTransScalar::~linTransScalar()
{
}


