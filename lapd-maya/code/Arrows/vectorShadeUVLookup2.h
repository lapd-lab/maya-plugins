/* ==========================================================================
// Arrows : vectorShadeUVLookup2.h
//
// Copyright (C) 2012 Jim Bamber 
//
// This version of the UV lookup reads in the table internally if any of the input filename, group name,
// or table name do not match stored values.  This avoids passing vector array data, which seems to give
// Maya problems when rendering.
//
*/
#ifndef VECTORSHADEUVLOOKUP2_H
#define VECTORSHADEUVLOOKUP2_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MVectorArray.h>


class vectorShadeUVLookup2 : public MPxNode
{
    public:

    vectorShadeUVLookup2();
	virtual void postConstructor();
    virtual ~vectorShadeUVLookup2();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    MFloatVector lookup(
		float2& uvCoord,
		int coordToUse );

	void readHDF5File();

	void generateTable();

	// Input attributes
	static MObject aUVCoord;				// The UV value of the current triangle(?), point(?) being shaded (float2)
	static MObject aCoordToUse;				// Which of the two to use (U or V) (enum)
	static MObject aTableMode;				// How to produce table (Read or Generate) (enum)

	static MObject aFilename;               // HDF5 texture table filename
	static MObject aGroupName;              // Group name within HDF5 texture table file
	static MObject aTableName;              // Table name within group

	static MObject aTableLength;			// Number of elements in generated texture table (int)
	static MObject aStartIndex;				// From index=0 to startIndex-1, elements will be set to startValue (int)
	static MObject aEndIndex;				// From index=startIndex to endIndex-1, elements will be ramped from startValue to endValue (int)
											// From index=endIndex to tableLength-1, elements will be set to endValue
	static MObject aStartValue;				// From index=0 to startIndex, elements will be set to startValue (float3)
	static MObject aEndValue;				// From index=startIndex to endIndex, elements will be ramped from startValue to endValue (float3)

	// Output attributes
    static MObject aVectorShade;			// Output color or transparency (MFloatVector)

	// Internal variables
	int mTableMode;

	MString mFilename;
	MString mGroupName;
	MString mTableName;

	int mTableLength;
	int mStartIndex;
	int mEndIndex;
	float3 mStartValue;
	float3 mEndValue;

	MVectorArray mVectorShadingTable;
};


#endif

