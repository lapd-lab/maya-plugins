/* ==========================================================================
// Texture 3D : vectorFade.h
//
// Copyright (C) 2006 Jim Bamber 
//
*/
#ifndef VECTORFADE_H
#define VECTORFADE_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnVectorArrayData.h>


class vectorFade : public MPxNode
{
    public:

    vectorFade();
	virtual void postConstructor();
    virtual ~vectorFade();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MFloatVector calculateFade( MFloatVector mFade, MFloatVector mVectorBackground, MFloatVector mVectorForeground );

	// Input attributes
	static MObject fade;
	static MObject vectorBackground;
	static MObject vectorForeground;

	// Output attributes
    static MObject vectorShade;
};


#endif

