/* ============================================================================
// forceField : squares.h
//
// Copyright (C) 2008 Jim Bamber 
// 2008-07-17: Changed to produce one mesh from multiple curves.  Building vertex and connection arrays
//   manually instead of calling addPolygon().
//
*/
#ifndef SQUARES_H
#define SQUARES_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class squares : public MPxNode
{
    public:

    squares();
	virtual void postConstructor();
    virtual ~squares();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MStatus createMesh(
		int curveCount,
		MIntArray startIndices,
		MIntArray CVCounts,
		MPointArray controlVertexList,
		MVectorArray tangentList,
		double tubeRadius,
		bool randomize,
		//int curveIndex,
		int start,
		int step,
		double UVScale,
		bool xySquares,
		bool xzSquares,
		bool yzSquares,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MDoubleArray& scalarDataGrid,
		double multiplier,
		MObject& newOutputData );

	static MStatus addSquare(
		//MFnMesh& fnMesh,
		//MObject& newOutputData,
		MPoint controlVertex,
		MVector tangent,
		double scalarDatum,
		double tubeRadius,
		bool randomize,
		bool xySquares,
		bool xzSquares,
		bool yzSquares,
		MPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );

	static MStatus assignUVs(
		double UVScale,
		MFnMesh& fnMesh,
		MObject& newOutputData );

	// Input attributes
	static MObject aCurveCount;				// Number of curves in the output array.
	static MObject aStartIndices;			// Used to index into the control vertex and tangent arrays (int array).
	static MObject aCVCounts;				// Number of CV's for each curve (int array), for example:
											//   index = startIndices[curveNum] + cvIndex, where cvIndex = {0:CVCounts[curveNum]-1}
	static MObject aControlVertexList;		// Control vertices for curves (point array).
	static MObject aTangentList;			// Tangents for curves (vector array).
	static MObject aTubeRadius;				// Radius of field line tubes in cm (double).
	static MObject aRandomize;				// Causes squares to be rotated randomly about the tangent (boolean).
	//static MObject aCurveIndex;				// Index of curve to use as basis for output mesh (int, 0-based).
	static MObject aStart;					// Start CV for placement of squares (int).
	static MObject aStep;					// Size of step through CV's for placement of squares (int).
	static MObject aUVScale;				// U or V value of 1.0 equals this value in cm, negative value means
											//   scale to the size of the square (double).
	static MObject aXYSquares;				// Show squares perpendicular to field line tangent (boolean).
	static MObject aXZSquares;				// Show squares parallel to field line tangent (originally in x-z plane) (boolean).
	static MObject aYZSquares;				// Show squares parallel to field line tangent (originally in y-z plane) (boolean).

											// Spatial grid is assumed to be uniform => is not used as an input.
	static MObject aDims;					// Dimensions of the spatial grid (int3).
	static MObject aGridMin;				// Minimum point of spatial grid (float3).
	static MObject aGridMax;				// Maximum point of spatial grid (float3).
	static MObject aScalarDataGrid;			// This supplies the scalars for the secondary mesh (scalar array).
	static MObject aMultiplier;				// A convenience input to scale up the secondary data (double).

	// Output attributes
	static MObject aSecondaryMesh;		// The newly created mesh.
};


#endif

