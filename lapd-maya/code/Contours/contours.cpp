/* ============================================================================
 * Contours : contours.cpp
 *
 * Copyright (C) 2008 Jim Bamber
 *
 */
#include "contours.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "contoursCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MFloatArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshEdge.h>
#include <maya/MQuaternion.h>


// Static data
MTypeId contours::id( 0x00700 );
double contours::bevelWidth(0.05);  // 0.5 mm

// Attributes
MObject contours::aTranslate;
MObject contours::aRotate;
MObject contours::aScale;
MObject contours::aRotateOrder;
MObject contours::aMinContour;
MObject contours::aMaxContour;
MObject contours::aContourIntervals;
MObject contours::aNx;	
MObject contours::aNy;	
MObject contours::aNz;	
MObject contours::aDx;	
MObject contours::aDy;	
MObject contours::aDz;	
MObject contours::aWidthSampling;
MObject contours::aHeightSampling;
MObject contours::aDims;
MObject contours::aGridMin;
MObject contours::aGridMax;
MObject contours::aScalarDataGrid;
MObject contours::aDataMin;
MObject contours::aDataMax;

MObject contours::aOutputMesh;


/* ============================================================================
 * createMesh()
 *
 * This creates a single mesh which is a collection of contours
 *
 */
MStatus contours::createMesh(
	float3& translate,
	float3& rotate,
	float3& scale,
	int rotateOrder,
	MDoubleArray& contourValues,
	int nx, int ny, int nz,
	double dx, double dy, double dz,
	int widthSampling, int heightSampling,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MDoubleArray& scalarDataGrid,
	MObject& newOutputData )
{
	//contoursCommon::logEntry( "--> contours::createMesh()" );
	MStatus stat;
	MFnMesh fnMesh;

	// Mesh arrays
	//
	// vertices -- point (vertex) array. This should include all the vertices in the mesh, and no extras. For example,
	// a cube could have the vertices: { (-1,-1,-1), (1,-1,-1), (1,-1,1), (-1,-1,1), (-1,1,-1), (-1,1,1), (1,1,1), (1,1,-1) }
	//
	// polygonVertexCounts -- array of vertex counts for each polygon. For example the cube would have 6 faces, each of which
	// had 4 verts, so the polygonVertexCounts would be {4,4,4,4,4,4}.
	//
	// polygonVertexIndices -- array of vertices for each polygon. For example, in the cube, we have 4 vertices for
	// every face, so we list the vertices for face0, face1, etc consecutively in the array. These are specified by indexes
	// in the vertexArray: e.g for the cube: { 0, 1, 2, 3, 4, 5, 6, 7, 3, 2, 6, 5, 0, 3, 5, 4, 0, 4, 7, 1, 1, 7, 6, 2 }.  The
	// connections between the vertices are implied by the order in the list.
	//
	MPointArray vertices;
	MIntArray polygonVertexCounts;
	MIntArray polygonVertexIndices;


	// Store the contour plane corner vertices
	// -this is based on the translate, rotate, and scale attributes
	//
	MVectorArray originalPlaneVectors;
	float width = scale[0];
	float height = scale[1];
	originalPlaneVectors.append( MVector(-0.5*width,  0.5*height, 0.0) );
	originalPlaneVectors.append( MVector( 0.5*width,  0.5*height, 0.0) );
	originalPlaneVectors.append( MVector( 0.5*width, -0.5*height, 0.0) );
	originalPlaneVectors.append( MVector(-0.5*width, -0.5*height, 0.0) );

	double rotXYZ[3];
	float pi = (float)3.1415927;
	for ( int i=0; i<3; i++ ) rotXYZ[i] = rotate[i] * pi/180.0;
	MTransformationMatrix::RotationOrder order;
	if ( rotateOrder == 0 ) order = MTransformationMatrix::kXYZ;
	if ( rotateOrder == 1 ) order = MTransformationMatrix::kYZX;
	if ( rotateOrder == 2 ) order = MTransformationMatrix::kZXY;
	if ( rotateOrder == 3 ) order = MTransformationMatrix::kXZY;
	if ( rotateOrder == 4 ) order = MTransformationMatrix::kYXZ;
	if ( rotateOrder == 5 ) order = MTransformationMatrix::kZYX;
	for ( int i=0; i<4; i++ )
	{
		MVector vector( originalPlaneVectors[i] );
		originalPlaneVectors[i] = vector.rotateBy( rotXYZ, order );
	}

	MVector translationVector( translate[0], translate[1], translate[2] );
	MPointArray originalContourPlane;
	for ( int i=0; i<4; i++ )
	{
		MPoint point( originalPlaneVectors[i] + translationVector );
		originalContourPlane.append( point );
	}
	MPointArray contourPlane( originalContourPlane );


	// Load up the mesh-building arrays (vertices and polygon connections)
	// -outer loops: display plane repetitions
	// -inner loop: contour values within one display plane
	//
	for ( int ix=0; ix<nx; ix++ )
	{
		//sprintf( contoursCommon::logString, "ix: %d", ix );
		//contoursCommon::logEntry( contoursCommon::logString );
		double xOffset = (double)ix*dx;
		for ( int iy=0; iy<ny; iy++ )
		{
			double yOffset = (double)iy*dy;
			for ( int iz=0; iz<nz; iz++ )
			{
				double zOffset = (double)iz*dz;
				MVector offset( xOffset, yOffset, zOffset );
				for ( int i=0; i<4; i++ ) contourPlane[i] = originalContourPlane[i] + offset;

				for ( int iContour=0; iContour<(int)(contourValues.length()); iContour++ )
				{
					double contourValue = contourValues[iContour];
					addContour(
						contourPlane,
						contourValue,
						widthSampling, heightSampling,
						dims,
						gridMin,
						gridMax,
						scalarDataGrid,
						vertices,
						polygonVertexCounts,
						polygonVertexIndices );
				}
			}
		}
	}

	int vertexCount = vertices.length();
	int polygonCount = polygonVertexCounts.length();
	//sprintf( contoursCommon::logString, "vertexCount: %d  polygonCount: %d", vertexCount, polygonCount );
	//contoursCommon::logEntry( contoursCommon::logString );

	// Single call here to create the mesh
	fnMesh.create(
		vertexCount,
		polygonCount, 
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		newOutputData, 
		&stat );   diLOG_MSTATUS( stat );

	//diLOG_MSTATUS( fnMesh.cleanupEdgeSmoothing() );

	//diLOG_MSTATUS( fnMesh.setVertexNormals(normals, vertexIndices) );

	//contoursCommon::logEntry( "<-- contours::createMesh()" );
	return MStatus::kSuccess;
}


/* ============================================================================
 * addContour()
 *
 * This adds a single contour (i.e. single contour value and single plane) to the arrays
 *
 */
MStatus contours::addContour(
	MPointArray& contourPlane,
	double contourValue,
	int widthSampling, int heightSampling,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MDoubleArray& scalarDataGrid,
	MPointArray& vertices,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices )
{
	MStatus stat;
	double halfWindow = 2.0;


	// What are the data structures?
	// rawVertices -- grid vertices, array extent: (widthSampling+1) * (heightSampling+1)
	// rawValues -- data values, array extent: (widthSampling+1) * (heightSampling+1)
	// sampledGridCellVertices -- locations of the vertices of a single grid cell, array extent: 4
	// sampledGridCellValues -- sampled values at the corners of a single grid cell, array extent: 4
	//
	MPointArray rawVertices;
	MDoubleArray rawValues;
	MPointArray sampledGridCellVertices;
	MDoubleArray sampledGridCellValues;


	// Set up some variables for the sampling
    //
	MPoint startPoint = contourPlane[0];
	MPoint point;
	MVector widthDelta = (contourPlane[1]-contourPlane[0]) / (double)widthSampling;
	MVector heightDelta = (contourPlane[3]-contourPlane[0]) / (double)heightSampling;
	MVector planeNormal = heightDelta ^ widthDelta;


	// First, do the raw sampling
    //
	double maxValue = 0.0;
	for ( int iHeight = 0;  iHeight < heightSampling+1;  iHeight++ )
	{
		MVector heightOffset = (double)iHeight*heightDelta;
		for ( int iWidth = 0;  iWidth < widthSampling+1;  iWidth++ )
		{
			MVector widthOffset = (double)iWidth*widthDelta;
			point = startPoint + heightOffset + widthOffset;
			rawVertices.append( point );
			double scalar =
				lapdCommon::interpolateScalar( point, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
			rawValues.append( scalar );
			if ( scalar > maxValue ) maxValue = scalar;
		}
	}


	// Step through grid squares, taking vertices and values from raw arrays, calculate line segment(s)
	// for each grid square.  Grid square vertex indexing is:
	// 0 1
	// 3 2
	//
	int rawHeightIndex, rawIndex;
	for ( int iHeight = 0;  iHeight < heightSampling;  iHeight++ )
	{
		rawHeightIndex = iHeight * (widthSampling+1);
		for ( int iWidth = 0;  iWidth < widthSampling;  iWidth++ )
		{
			rawIndex = rawHeightIndex + iWidth;

			sampledGridCellVertices.clear();
			sampledGridCellVertices.append( rawVertices[ rawIndex                  +0 ] );
			sampledGridCellVertices.append( rawVertices[ rawIndex                  +1 ] );
			sampledGridCellVertices.append( rawVertices[ rawIndex+(widthSampling+1)+1 ] );
			sampledGridCellVertices.append( rawVertices[ rawIndex+(widthSampling+1)+0 ] );

			sampledGridCellValues.clear();
			sampledGridCellValues.append( rawValues[ rawIndex                  +0 ] );
			sampledGridCellValues.append( rawValues[ rawIndex                  +1 ] );
			sampledGridCellValues.append( rawValues[ rawIndex+(widthSampling+1)+1 ] );
			sampledGridCellValues.append( rawValues[ rawIndex+(widthSampling+1)+0 ] );

			addLineSegment(
				contourValue,
				planeNormal,
				sampledGridCellVertices,
				sampledGridCellValues,
				dims,
				gridMin,
				gridMax,
				scalarDataGrid,
				vertices,
				polygonVertexCounts,
				polygonVertexIndices );
		}
	}

	return MStatus::kSuccess;
}


/* ============================================================================
 * addLineSegment()
 *
 * This adds a single line segment of a single contour
 *
 */
MStatus contours::addLineSegment(
	double contourValue,
	MVector& planeNormal,
	MPointArray& sampledGridCellVertices,
	MDoubleArray& sampledGridCellValues,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MDoubleArray& scalarDataGrid,
	MPointArray& vertices,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices )
{
	// Figure out the corner flag
	// 0 1
	// 3 2
	//
	int cornerFlag = 0;
	if ( sampledGridCellValues[0] > contourValue ) cornerFlag = cornerFlag + 1;
	if ( sampledGridCellValues[1] > contourValue ) cornerFlag = cornerFlag + 2;
	if ( sampledGridCellValues[2] > contourValue ) cornerFlag = cornerFlag + 4;
	if ( sampledGridCellValues[3] > contourValue ) cornerFlag = cornerFlag + 8;


	// Now deal with each case in turn
	//
	MPoint point1, point2;
	if ( cornerFlag == 0 )
	{
		// L L
		// L L
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 1 )
	{
		// H L
		// L L
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 0, 1, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 0, 3, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 2 )
	{
		// L H
		// L L
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 1, 0, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 1, 2, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 3 )
	{
		// H H
		// L L
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 0, 3, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 1, 2, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 4 )
	{
		// L L
		// L H
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 2, 1, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 2, 3, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 5 )
	{
		// H L
		// L H
		// High points on the diagonal: needs more work
		double halfWindow = 2.0;

		MPoint point( sampledGridCellVertices[0] );
		for ( int i=1; i<4; i++ ) point = point + sampledGridCellVertices[i];
		for ( int i=0; i<3; i++ ) point[i] = 0.25 * point[i];

		double midValue =
			lapdCommon::interpolateScalar( point, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
		if ( midValue > contourValue )
		{
			// lines descend to the right
			point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 0, 1, contourValue );
			point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 2, 1, contourValue );
			lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );

			point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 0, 3, contourValue );
			point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 2, 3, contourValue );
			lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		}
		else
		{
			// lines descend to the left
			point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 0, 1, contourValue );
			point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 0, 3, contourValue );
			lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );

			point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 2, 1, contourValue );
			point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 2, 3, contourValue );
			lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		}
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 6 )
	{
		// L H
		// L H
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 1, 0, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 2, 3, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 7 )
	{
		// H H
		// L H
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 0, 3, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 2, 3, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 8 )
	{
		// L L
		// H L
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 3, 0, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 3, 2, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 9 )
	{
		// H L
		// H L
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 0, 1, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 3, 2, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 10 )
	{
		// L H
		// H L
		// High points on the diagonal: needs more work
		double halfWindow = 2.0;

		MPoint point( sampledGridCellVertices[0] );
		for ( int i=1; i<4; i++ ) point = point + sampledGridCellVertices[i];
		for ( int i=0; i<3; i++ ) point[i] = 0.25 * point[i];

		double midValue =
			lapdCommon::interpolateScalar( point, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
		if ( midValue > contourValue )
		{
			// lines descend to the left
			point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 1, 0, contourValue );
			point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 3, 0, contourValue );
			lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );

			point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 1, 2, contourValue );
			point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 3, 2, contourValue );
			lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		}
		else
		{
			// lines descend to the right
			point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 1, 0, contourValue );
			point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 1, 2, contourValue );
			lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );

			point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 3, 0, contourValue );
			point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 3, 2, contourValue );
			lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		}
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 11 )
	{
		// H H
		// H L
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 1, 2, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 3, 2, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 12 )
	{
		// L L
		// H H
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 3, 0, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 2, 1, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 13 )
	{
		// H L
		// H H
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 0, 1, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 2, 1, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 14 )
	{
		// L H
		// H H
		point1 = intersect( sampledGridCellVertices, sampledGridCellValues, 1, 0, contourValue );
		point2 = intersect( sampledGridCellVertices, sampledGridCellValues, 3, 0, contourValue );
		lineSegment( point1, point2, planeNormal, vertices, polygonVertexCounts, polygonVertexIndices );
		return MStatus::kSuccess;
	}

	if ( cornerFlag == 15 )
	{
		// H H
		// H H
		return MStatus::kSuccess;
	}

	return MStatus::kSuccess;
}


/* ============================================================================
 * intersect()
 *
 * Finds the point along a line where one would find the contour value, assuming the value changes
 * linearly along the line.
 *
 */
MPoint contours::intersect(
	MPointArray& sampledGridCellVertices,
	MDoubleArray& sampledGridCellValues,
	int highIndex,
	int lowIndex,
	double contourValue )
{
	MPoint intersectionPoint;

	MPoint highPoint( sampledGridCellVertices[highIndex] );
	MPoint lowPoint( sampledGridCellVertices[lowIndex] );

	double highValue = sampledGridCellValues[ highIndex ];
	double lowValue = sampledGridCellValues[ lowIndex ];
	double fraction;

	if ( highValue == lowValue ) fraction = 0.5;
	else fraction = (contourValue-lowValue) / (highValue-lowValue);

	intersectionPoint = lowPoint + fraction * (highPoint-lowPoint);

	return intersectionPoint;
}


/* ============================================================================
 * lineSegment()
 *
 * Adds the contour line segment vertices and connectivity information into the appropriate arrays.
 * Each line segment is a six-sided, lozenge-shaped object, similar to those used in digital displays.
 *
 */
MStatus contours::lineSegment(
	MPoint& point1,
	MPoint& point2,
	MVector& planeNormal,
	MPointArray& vertices,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices )
{
	polygonVertexCounts.append( 6 );

	MVector parallel( point2 - point1 );
	MVector perpendicular( parallel ^ planeNormal );

	parallel.normalize();
	perpendicular.normalize();
	parallel = parallel * bevelWidth;
	perpendicular = perpendicular * bevelWidth;

	int nVertices = (int)vertices.length();
	vertices.append( point1 );
	vertices.append( point1 + parallel + perpendicular );
	vertices.append( point2 - parallel + perpendicular );
	vertices.append( point2 );
	vertices.append( point2 - parallel - perpendicular );
	vertices.append( point1 + parallel - perpendicular );

	polygonVertexIndices.append( nVertices );
	polygonVertexIndices.append( nVertices + 1 );
	polygonVertexIndices.append( nVertices + 2 );
	polygonVertexIndices.append( nVertices + 3 );
	polygonVertexIndices.append( nVertices + 4 );
	polygonVertexIndices.append( nVertices + 5 );

	return MStatus::kSuccess;
}


/* ============================================================================
 * compute()
 *
 * This function gets called by Maya to calculate the curves.
 *
 */
MStatus contours::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//contoursCommon::logEntry( "--> contours::compute()" );
	contoursCommon::logEntry( contoursCommon::eContours );

	if ( (plug != aOutputMesh) )
	{
		contoursCommon::logEntry( (char*) "Abnormal exit from contours::compute(), Maya not asking for output mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hTranslate( block.inputValue(aTranslate, &stat) );   diLOG_MSTATUS( stat );
	float3& translate = hTranslate.asFloat3();

	MDataHandle hRotate( block.inputValue(aRotate, &stat) );   diLOG_MSTATUS( stat );
	float3& rotate = hRotate.asFloat3();

	MDataHandle hScale( block.inputValue(aScale, &stat) );   diLOG_MSTATUS( stat );
	float3& scale = hScale.asFloat3();

	MDataHandle hRotateOrder( block.inputValue(aRotateOrder, &stat) );   diLOG_MSTATUS( stat );
	int rotateOrder = hRotateOrder.asInt();

	MDataHandle hMinContour( block.inputValue(aMinContour, &stat) );   diLOG_MSTATUS( stat );
	double minContour = hMinContour.asDouble();

	MDataHandle hMaxContour( block.inputValue(aMaxContour, &stat) );   diLOG_MSTATUS( stat );
	double maxContour = hMaxContour.asDouble();

	MDataHandle hContourIntervals( block.inputValue(aContourIntervals, &stat) );   diLOG_MSTATUS( stat );
	int contourIntervals = hContourIntervals.asInt();

	MDataHandle hNx( block.inputValue(aNx, &stat) );   diLOG_MSTATUS( stat );
	int nx = hNx.asInt();

	MDataHandle hNy( block.inputValue(aNy, &stat) );   diLOG_MSTATUS( stat );
	int ny = hNy.asInt();

	MDataHandle hNz( block.inputValue(aNz, &stat) );   diLOG_MSTATUS( stat );
	int nz = hNz.asInt();

	MDataHandle hDx( block.inputValue(aDx, &stat) );   diLOG_MSTATUS( stat );
	double dx = hDx.asDouble();

	MDataHandle hDy( block.inputValue(aDy, &stat) );   diLOG_MSTATUS( stat );
	double dy = hDy.asDouble();

	MDataHandle hDz( block.inputValue(aDz, &stat) );   diLOG_MSTATUS( stat );
	double dz = hDz.asDouble();

	MDataHandle hWidthSampling( block.inputValue(aWidthSampling, &stat) );   diLOG_MSTATUS( stat );
	int widthSampling = hWidthSampling.asInt();

	MDataHandle hHeightSampling( block.inputValue(aHeightSampling, &stat) );   diLOG_MSTATUS( stat );
	int heightSampling = hHeightSampling.asInt();

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hScalarDataGrid( block.inputValue(aScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnDoubleArrayData fnScalarDataGrid( hScalarDataGrid.data() );
	MDoubleArray scalarDataGrid( fnScalarDataGrid.array() );

	MDataHandle hDataMin( block.inputValue(aDataMin, &stat) );   diLOG_MSTATUS( stat );
	double dataMin = hDataMin.asDouble();

	MDataHandle hDataMax( block.inputValue(aDataMax, &stat) );   diLOG_MSTATUS( stat );
	double dataMax = hDataMax.asDouble();


	// Now do the output attributes.
	//
	MDataHandle hOutputMesh( block.outputValue(aOutputMesh, &stat) );   diLOG_MSTATUS( stat );


	// Create the data object which underlies the output mesh
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );

	if ( dataMax > dataMin )
	{
		// Figure out the contour values
		if ( minContour < dataMin ) minContour = dataMin;
		if ( maxContour > dataMax ) maxContour = dataMax;
		MDoubleArray contourValues;
		double currentContour = minContour;
		double contourInterval = ( maxContour - minContour ) / (double)contourIntervals;
		for ( int i=0; i<=contourIntervals; i++ )
		{
			contourValues.append( currentContour );
			currentContour = currentContour + contourInterval;
		}

		// Make the call to create the secondary mesh
		diLOG_MSTATUS(
			createMesh(
				translate,
				rotate,
				scale,
				rotateOrder,
				contourValues,
				nx, ny, nz,
				dx, dy, dz,
				widthSampling, heightSampling,
				dims,
				gridMin,
				gridMax,
				scalarDataGrid,
				newOutputData ) );
	}


	// Set the outputs as clean.
	hOutputMesh.set( newOutputData );
	block.setClean( plug );

	//contoursCommon::logEntry( "<-- contours::compute()" );
	return MStatus::kSuccess;
}


/* ============================================================================
 * initialize()
 *
 * initializes attribute information and the unit circle values
 */
MStatus contours::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	contoursCommon::logEntry( (char*) "--> contours::initialize()" );


	// Create input attributes:
	//
	// translate
	aTranslate = nAttr.create( "translate", "translate", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aTranslate) );

	// rotate
	aRotate = nAttr.create( "rotate", "rotate", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aRotate) );

	// scale
	aScale = nAttr.create( "scale", "scale", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0, 1.0, 1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aScale) );

	// rotate order
    aRotateOrder = enumAttr.create( "rotateOrder", "rotateOrder", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("xyz", 0) );
	diLOG_MSTATUS( enumAttr.addField("yzx", 1) );
	diLOG_MSTATUS( enumAttr.addField("zxy", 2) );
	diLOG_MSTATUS( enumAttr.addField("xzy", 3) );
	diLOG_MSTATUS( enumAttr.addField("yxz", 4) );
	diLOG_MSTATUS( enumAttr.addField("zyx", 5) );
	MAKE_INPUT( enumAttr );
	diLOG_MSTATUS( addAttribute(aRotateOrder) );

	// min contour value
	aMinContour = nAttr.create( "minContour", "minContour", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aMinContour) );

	// max contour value
	aMaxContour = nAttr.create( "maxContour", "maxContour", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aMaxContour) );

	// number of contour intervals
	aContourIntervals = nAttr.create( "contourIntervals", "contourIntervals", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(5) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	diLOG_MSTATUS( nAttr.setMax(500) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aContourIntervals) );

	// nx
	aNx = nAttr.create( "nx", "nx", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aNx) );

	// ny
	aNy = nAttr.create( "ny", "ny", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aNy) );

	// nz
	aNz = nAttr.create( "nz", "nz", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aNz) );

	// dx
	aDx = nAttr.create( "dx", "dx", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDx) );

	// dy
	aDy = nAttr.create( "dy", "dy", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDy) );

	// dz
	aDz = nAttr.create( "dz", "dz", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDz) );

	// width sampling
	aWidthSampling = nAttr.create( "widthSampling", "widthSampling", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(100) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aWidthSampling) );

	// height sampling
	aHeightSampling = nAttr.create( "heightSampling", "heightSampling", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(100) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aHeightSampling) );

	// grid dimensions
	aDims = nAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gridMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gridMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// scalar data grid
	aScalarDataGrid = tAttr.create( "scalarDataGrid", "scalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aScalarDataGrid) );

	// data min
	aDataMin = nAttr.create( "dataMin", "dataMin", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDataMin) );

	// data max
	aDataMax = nAttr.create( "dataMax", "dataMax", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDataMax) );


	// Create output attributes
	//
	// output mesh
	aOutputMesh = tAttr.create( "outputMesh", "outputMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aOutputMesh) );


	// Attribute dependencies
	//
    diLOG_MSTATUS( attributeAffects(aTranslate, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aRotate, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aScale, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aRotateOrder, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aMinContour, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aMaxContour, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aContourIntervals, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aNx, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aNy, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aNz, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDx, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDy, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDz, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aWidthSampling, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aHeightSampling, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDims, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aScalarDataGrid, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMin, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMax, aOutputMesh) );


	contoursCommon::logEntry( (char*) "<-- contours::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
 * contours()
 */
contours::contours()
{
}


/* ============================================================================
 * postConstructor()
 */
void contours::postConstructor( )
{
}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType contours::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
 * creator()
 *
 * creates an instance of the node
 */
void * contours::creator()
{
    return new contours();
}


/* ============================================================================
 * ~contours()
 */
contours::~contours()
{
}

