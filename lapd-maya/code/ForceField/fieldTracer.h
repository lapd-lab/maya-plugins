/* ============================================================================
// forceField : fieldTracer.h
//
// Copyright (C) 2006 Jim Bamber 
// 
//  Description
//	The fieldTracer node implements field line tracing based on the input vector data grid.
//
*/

#include <maya/MVector.h>
#include <maya/MObject.h>
#include <maya/MDataBlock.h>
#include <maya/MPxFieldNode.h>
#include <maya/MFloatPoint.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MVectorArray.h>


#if defined(OSMac_MachO_)
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif


class fieldTracer: public MPxFieldNode
{
public:
	// Public methods.
	//
	fieldTracer() {};
	virtual ~fieldTracer() {};
	virtual void postConstructor();

	static void		*creator();
	static MStatus	initialize();

    MPxNode::SchedulingType schedulingType() const;

	virtual MStatus	compute( const MPlug& plug, MDataBlock& block );
	//virtual void 	draw (  M3dView  & view, const  MDagPath  & path,  M3dView::DisplayStyle  style, M3dView:: DisplayStatus );

    virtual MStatus getForceAtPoint(const MVectorArray& point,
                            const MVectorArray& velocity,
                            const MDoubleArray& mass,
                            MVectorArray& force,
                            double deltaTime);

	virtual MStatus iconSizeAndOrigin(	GLuint& width,
						GLuint& height,
						GLuint& xbo,
						GLuint& ybo   );

	virtual MStatus iconBitmap(GLubyte* bitmap);


	// Public attributes.
	//
	static MObject  aSpatialGrid;		// the spatial grid of the vector field (below).
	static MObject  aDims;				// dimensions of the spatial grid.
	static MObject  aGridMin;			// minimum point of spatial grid.
	static MObject  aGridMax;			// maximum point of spatial grid.
	static MObject  aVectorDataGrid;	// this supplies the vectors of the basic force field.
	static MObject  aTargetSpeed;		// maintain a target speed when tracing the field line
	static MObject  aReverseField;		// boolean flag to reverse the vector field, this
										//   allows tracing of field lines in both directions

	// Other data members
	//
	static MTypeId	id;


private:
	// Private attributes.
	//
	int		xlim[3][2];		// integer bound for point
	double	xarg[3];		// fractional part


	// Private methods to help compute output force.
	//
	void	calculateForceArray( MDataBlock& block,
							const MVectorArray &points,
							const MVectorArray &velocities,
							const MDoubleArray &masses,
							MVectorArray &forceArray );

	static MVector calculateForce(
		MVector& point,
		MVector& velocity,
		int3& dims,
		float3& gridMax,
		float3& gridMin,
		double magnitude,
		MVectorArray& vectorDataGrid,
		double halfWindow,
		double targetSpeed,
		bool reverseField );

	void	ownerPosition( MDataBlock& block, MVectorArray &vArray );
	MStatus	getWorldPosition( MVector &vector );
	MStatus	getWorldPosition( MDataBlock& block, MVector &vector );
	//void	noiseFunction( double *inputNoise, double *out );
	//double  frand( register int s );   // get random number from seed
	//double  hermite( double p0, double p1, double r0, double r1, double t );
	//void    interpolate( double f[4], register int i, register int n );

	// Private methods to get inherited attribute values.
	//
	double	getMagnitudeValue( MDataBlock& block );
	double	getAttenuationValue( MDataBlock& block );
	double	getMaxDistanceValue( MDataBlock& block );
	bool	getUseMaxDistanceValue( MDataBlock& block );
	bool	getApplyPerVertexValue( MDataBlock& block );

	MStatus	getOwnerCentroidValue( MDataBlock& block, MVector &vector );

	// Private methods to get local attributes values.
	//
	MVectorArray getSpatialGrid( MDataBlock& block );
	int3&		 getDims( MDataBlock& block );
	float3&		 getGridMin( MDataBlock& block );
	float3&		 getGridMax( MDataBlock& block );
	MVectorArray getVectorDataGrid( MDataBlock& block );
	double		 getTargetSpeed( MDataBlock& block );
	bool		 getReverseField( MDataBlock& block );

};


// Inline methods
//
/* ============================================================================
// getMagnitudeValue()
*/
inline double fieldTracer::getMagnitudeValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mMagnitude, &status );   CHECK_MSTATUS( status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


/* ============================================================================
// getAttenuationValue()
*/
inline double fieldTracer::getAttenuationValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mAttenuation, &status );   CHECK_MSTATUS( status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


/* ============================================================================
// getMaxDistanceValue()
*/
inline double fieldTracer::getMaxDistanceValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mMaxDistance, &status );   CHECK_MSTATUS( status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


/* ============================================================================
// getUseMaxDistanceValue()
*/
inline bool fieldTracer::getUseMaxDistanceValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mUseMaxDistance, &status );   CHECK_MSTATUS( status );

	bool value = false;
	if( status == MS::kSuccess )
		value = hValue.asBool();

	return( value );
}


/* ============================================================================
// getApplyPerVertexValue()
*/
inline bool fieldTracer::getApplyPerVertexValue( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( mApplyPerVertex, &status );   CHECK_MSTATUS( status );

	bool value = false;
	if( status == MS::kSuccess )
		value = hValue.asBool();

	return( value );
}


/* ============================================================================
// getSpatialGrid()
*/
inline MVectorArray fieldTracer::getSpatialGrid( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aSpatialGrid, &status );   CHECK_MSTATUS( status );

	MVectorArray value;
	if ( status == MS::kSuccess )
	{
        //STV
        //		MFnVectorArrayData fnValue = hValue.data();
		MFnVectorArrayData fnValue ( hValue.data());
		value = fnValue.array();
	}

	return( value );
}


/* ============================================================================
// getDims()
*/
inline int3& fieldTracer::getDims( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aDims, &status );   CHECK_MSTATUS( status );
	int3& value = hValue.asInt3();

	return( value );
}


/* ============================================================================
// getGridMin()
*/
inline float3& fieldTracer::getGridMin( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aGridMin, &status );   CHECK_MSTATUS( status );
	float3& value = hValue.asFloat3();

	return( value );
}


/* ============================================================================
// getGridMax()
*/
inline float3& fieldTracer::getGridMax( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aGridMax, &status );   CHECK_MSTATUS( status );
	float3& value = hValue.asFloat3();

	return( value );
}


/* ============================================================================
// getVectorDataGrid()
*/
inline MVectorArray fieldTracer::getVectorDataGrid( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aVectorDataGrid, &status );   CHECK_MSTATUS( status );

	MVectorArray value;
	if ( status == MS::kSuccess )
	{
		//STV
        //	MFnVectorArrayData fnValue = hValue.data();
		MFnVectorArrayData fnValue ( hValue.data());
		value = fnValue.array();
	}

	return( value );
}


/* ============================================================================
// getTargetSpeed()
*/
inline double fieldTracer::getTargetSpeed( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aTargetSpeed, &status );   CHECK_MSTATUS( status );

	double value = 0.0;
	if( status == MS::kSuccess )
		value = hValue.asDouble();

	return( value );
}


/* ============================================================================
// getReverseField()
*/
inline bool fieldTracer::getReverseField( MDataBlock& block )
{
	MStatus status;

	MDataHandle hValue = block.inputValue( aReverseField, &status );   CHECK_MSTATUS( status );

	bool value = false;
	if( status == MS::kSuccess )
		value = hValue.asBool();

	return( value );
}


/* ============================================================================
// getOwnerCentroidValue()
*/
inline MStatus fieldTracer::getOwnerCentroidValue(MDataBlock& block, MVector &vector)
{
	MStatus status;

	MDataHandle hValueX = block.inputValue( mOwnerCentroidX, &status );   CHECK_MSTATUS( status );
	MDataHandle hValueY = block.inputValue( mOwnerCentroidY, &status );   CHECK_MSTATUS( status );
	MDataHandle hValueZ = block.inputValue( mOwnerCentroidZ, &status );   CHECK_MSTATUS( status );

	if( status == MS::kSuccess )
	{
		vector[0] = hValueX.asDouble();
		vector[1] = hValueY.asDouble();
		vector[2] = hValueZ.asDouble();
	}

	return( status );
}

