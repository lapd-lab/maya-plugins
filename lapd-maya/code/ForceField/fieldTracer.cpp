/* ============================================================================
// forceField : fieldTracer.cpp
//
// Copyright (C) 2006 Jim Bamber 
// 
//  Description
//	The fieldTracer node implements field line tracing based on the input vector data grid.
//
*/

#include "fieldTracer.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>

#include <maya/MTime.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MMatrix.h>
#include <maya/MArrayDataBuilder.h> //STV
//RELEASE NOTE FROM MAYA 2015
//Transitive Header Includes
//Some of the header files included by other API header files have been replaced with forward declarations. For example, MArrayDataBuilder.h used to include MDataHandle.h. That include has been removed and replaced with a forward declaration of MDataHandle, which is all that MArrayDataBuilder.h really requires.
//If you have code which has been relying on these transitive header inclusions, then you will have to modify your code directly to include the headers it needs.

#include <maya/MFnDependencyNode.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnMatrixData.h>



MObject fieldTracer::aSpatialGrid;
MObject fieldTracer::aDims;
MObject fieldTracer::aGridMin;
MObject fieldTracer::aGridMax;
MObject fieldTracer::aVectorDataGrid;
MObject fieldTracer::aTargetSpeed;
MObject fieldTracer::aReverseField;

MTypeId fieldTracer::id( 0x00450 );


/* ============================================================================
// creator()
*/
void *fieldTracer::creator()
{
    return new fieldTracer;
}


/* ============================================================================
// postConstructor()
*/
void fieldTracer::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType fieldTracer::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// initialize()
//
//	Descriptions:
//		Initialize the node, attributes.
//
*/
MStatus fieldTracer::initialize()
{
	MStatus stat, stat2;

	MFnNumericAttribute numAttr;
	MFnTypedAttribute typedAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData vectorArrayData;

	forceFieldCommon::logEntry( (char*) "--> fieldTracer::initialize()" );


	// create the field basic attributes.
	//
    aSpatialGrid = typedAttr.create( "spatialGrid", "grid", MFnData::kVectorArray, vectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( typedAttr );
	diLOG_MSTATUS( addAttribute(aSpatialGrid) );

	aDims = numAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	numAttr.setDefault( 0, 0, 0 );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	aGridMin = numAttr.create( "gridMin", "gMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	numAttr.setDefault( 0.0, 0.0, 0.0 );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	aGridMax = numAttr.create( "gridMax", "gMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	numAttr.setDefault( 0.0, 0.0, 0.0 );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

    aVectorDataGrid = typedAttr.create( "vectorDataGrid", "vData", MFnData::kVectorArray, vectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( typedAttr );
	diLOG_MSTATUS( addAttribute(aVectorDataGrid) );

	aTargetSpeed = numAttr.create( "targetSpeed", "target", MFnNumericData::kDouble, 0.0, &stat );   diLOG_MSTATUS( stat );
	numAttr.setDefault( 1.0 );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aTargetSpeed) );

	aReverseField = numAttr.create( "reverseField", "reverse", MFnNumericData::kBoolean, false, &stat );   diLOG_MSTATUS( stat );
	numAttr.setDefault( false );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aReverseField) );

	// No dependencies are set -- perhaps they are taken care of by the parent class.

	forceFieldCommon::logEntry( (char*) "<-- fieldTracer::initialize()" );
	return( MS::kSuccess );
}


/* ============================================================================
// compute()
//
//	Descriptions:
//		compute output force.
//
*/
MStatus fieldTracer::compute(const MPlug& plug, MDataBlock& block)
{
	MStatus status;

	//forceFieldCommon::logEntry( (char*) "--> fieldTracer::compute()" );
	forceFieldCommon::logEntry( forceFieldCommon::eFieldTracer );
	if( !(plug == mOutputForce) )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from fieldTracer::compute(), Maya not asking for output force" );
		return MS::kUnknownParameter;
	}

	// get the logical index of the element this plug refers to.
	//
	int multiIndex = plug.logicalIndex( &status );
	diLOG_MSTATUS( status );

	// Get input data handle, use outputArrayValue since we do not
	// want to evaluate both inputs, only the one related to the
	// requested multiIndex. Evaluating both inputs at once would cause
	// a dependency graph loop.
	//
	MArrayDataHandle hInputArray( block.outputArrayValue(mInputData, &status) );
	diLOG_MSTATUS( status );

	diLOG_MSTATUS( hInputArray.jumpToElement(multiIndex) );

	// get children of aInputData.
	//
	MDataHandle hCompound( hInputArray.inputValue(&status) );
	diLOG_MSTATUS( status );

	MDataHandle hPosition( hCompound.child(mInputPositions) );
	MObject dPosition( hPosition.data() );
	MFnVectorArrayData fnPosition( dPosition );
	MVectorArray points( fnPosition.array(&status) );
	diLOG_MSTATUS( status );

	MDataHandle hVelocity( hCompound.child(mInputVelocities) );
	MObject dVelocity( hVelocity.data() );
	MFnVectorArrayData fnVelocity( dVelocity );
	MVectorArray velocities( fnVelocity.array(&status) );
	diLOG_MSTATUS( status );

	MDataHandle hMass( hCompound.child(mInputMass) );
	MObject dMass( hMass.data() );
	MFnDoubleArrayData fnMass( dMass );
	MDoubleArray masses( fnMass.array(&status) );
	diLOG_MSTATUS( status );

	// Compute the output force.
	//
	//forceFieldCommon::logEntry( (char*) "About to calculate output force array" );
	MVectorArray forceArray;
	calculateForceArray( block, points, velocities, masses, forceArray );

	// get output data handle
	//
	MArrayDataHandle hOutArray( block.outputArrayValue(mOutputForce, &status) );   diLOG_MSTATUS( status );
    MArrayDataBuilder bOutArray( hOutArray.builder(&status) );   diLOG_MSTATUS( status );
 //   MArrayDataBuilder bOutArray = hOutArray.builder(&status) ;   diLOG_MSTATUS( status );
	// get output force array from block.
	//
	MDataHandle hOut( bOutArray.addElement(multiIndex, &status) );   diLOG_MSTATUS( status );
	MFnVectorArrayData fnOutputForce;
	MObject dOutputForce( fnOutputForce.create(forceArray, &status) );   diLOG_MSTATUS( status );

	// update data block with new output force data.
	//
	hOut.set( dOutputForce );
	block.setClean( plug );

	//forceFieldCommon::logEntry( (char*) "<-- fieldTracer::compute()" );
	return( MS::kSuccess );
}


/* ============================================================================
// calculateForceArray()
//
//	Descriptions:
//		Compute output force.
//
*/
void fieldTracer::calculateForceArray
	(
		MDataBlock &block,					// block containing field params
		const MVectorArray &points,			// current positions of objects
		const MVectorArray &velocities,		// current velocities of objects
		const MDoubleArray &/*masses*/,		// masses of objects
		MVectorArray &forceArray			// output force array
	)
{
	// points and velocities should have the same length. If not return.
	//
	if( points.length() != velocities.length() )
	{
		forceFieldCommon::logEntry(
			(char*) "Abnormal exit from fieldTracer::calculateForceArray(), length mismatch between points and velocities arrays" );
		return;
	}

	// clear the output force array.
	//
	forceArray.clear();

	// get field parameters.
	//
	double magnitude = getMagnitudeValue( block );
	double attenuation = getAttenuationValue( block );
	int3& dims = getDims( block );
	float3& gridMin = getGridMin( block );
	float3& gridMax = getGridMax( block );
	MVectorArray vectorDataGrid( getVectorDataGrid(block) );
	double halfWindow = 2.0;

	// get owner's data. posArray may have only one point which is the centroid
	// (if this has owner) or field position(if without owner). Or it may have
	// a list of points if with owner and applyPerVertex.
	//
	//MVectorArray posArray;
	//posArray.clear();
	//ownerPosition( block, posArray );

	//int fieldPosCount = posArray.length();
	int pointCount = points.length();
	MVector point;
	MVector velocity;
	double targetSpeed = getTargetSpeed( block );
	bool reverseField = getReverseField( block );

	// With this model, where the force field is supplied, max distance attribute doesn't do anything.
	// Also attenuation is used as if it were drag.
	//
	for (int ptIndex = 0; ptIndex < pointCount; ptIndex ++ )
	{
		point = points[ptIndex];
		velocity = velocities[ptIndex];
/* stv dec2015		if ( ptIndex == 0 )
		{
			double speed = velocity.length();
			//sprintf( forceFieldCommon::logString, "speed: %d", speed );
			//forceFieldCommon::logEntry( forceFieldCommon::logString );
		}
*/
		MVector forceV;
		if ( ( ( point[0] >= gridMin[0] ) && ( point[0] < gridMax[0]-0.001 ) ) &&
			 ( ( point[1] >= gridMin[1] ) && ( point[1] < gridMax[1]-0.001 ) ) &&
			 ( ( point[2] >= gridMin[2] ) && ( point[2] < gridMax[2]-0.001 ) ) )
		{
			forceV = calculateForce(
				point, velocity, dims, gridMax, gridMin, magnitude, vectorDataGrid,
				halfWindow, targetSpeed, reverseField );
		}
		else
		{
			forceV = velocity * (-attenuation);   // Apply "attenuation" outside the data volume as if it were "drag"
		}

		forceArray.append( forceV );
	}
}


/* ============================================================================
// calculateForce()
*/
MVector fieldTracer::calculateForce(
	MVector& point, MVector& velocity, int3& dims, float3& gridMax, float3& gridMin,
	double magnitude, MVectorArray& vectorDataGrid, double halfWindow,
	double targetSpeed, bool reverseField )
{
	MVector weightedVector( 0.0, 0.0, 0.0 );
	double dir = 1.0;
	if ( reverseField ) dir = -1.0;

	int nx = dims[0];
	int ny = dims[1];
	int nz = dims[2];

	// distances between grid points
	float xmin = gridMin[0];
	float ymin = gridMin[1];
	float zmin = gridMin[2];
	float dx = (gridMax[0]-xmin) / (nx-1);
	float dy = (gridMax[1]-ymin) / (ny-1);
	float dz = (gridMax[2]-zmin) / (nz-1);

	// central point
	float x0 = (float)(point[0]);
	float y0 = (float)(point[1]);
	float z0 = (float)(point[2]);

	// floating point indices of central point
	float fx0 = (x0-xmin) / dx;
	float fy0 = (y0-ymin) / dy;
	float fz0 = (z0-zmin) / dz;

	// number of steps to either side, based on smoothing radius
	int ixStart = (int)floor( fx0 - halfWindow );  if ( ixStart < 0  ) ixStart = 0;
	int iyStart = (int)floor( fy0 - halfWindow );  if ( iyStart < 0  ) iyStart = 0;
	int izStart = (int)floor( fz0 - halfWindow );  if ( izStart < 0  ) izStart = 0;
	int ixEnd   = (int)ceil( fx0 + halfWindow );   if ( ixEnd > nx-1 ) ixEnd   = nx-1;
	int iyEnd   = (int)ceil( fy0 + halfWindow );   if ( iyEnd > ny-1 ) iyEnd   = ny-1;
	int izEnd   = (int)ceil( fz0 + halfWindow );   if ( izEnd > nz-1 ) izEnd   = nz-1;

	// loop through the bounding "cube", total up values within the window
	int nplane = ny * nx;
	int iplane, irow, index;
	int iz, iy, ix;
	float rz, ry, rx;
	float rz2, ry2, rx2;
	float rz2_ry2;
	float r2;
	float limit = (float)(halfWindow * halfWindow);
	float weight;
	float totalWeight = 0.0;

	for ( iz=izStart; iz<=izEnd; iz++ )
	{
		rz = (float)iz - fz0;
		rz2 = rz * rz;
		iplane = iz * nplane;
		for ( iy=iyStart; iy<=iyEnd; iy++ )
		{
			ry = (float)iy - fy0;
			ry2 = ry * ry;
			rz2_ry2 = rz2 + ry2;
			irow = iplane + iy * nx;
			for ( ix=ixStart; ix<=ixEnd; ix++ )
			{
				rx = (float)ix - fx0;
				rx2 = rx * rx;
				r2 = rz2_ry2 + rx2;
				index = irow + ix;
				if ( r2 < limit )
				{
					weight = limit - r2;
					weightedVector = weightedVector + weight*vectorDataGrid[index];
					totalWeight = totalWeight + weight;
				} // end if within the smoothing radius
			} // end x loop
		} // end y loop
	} // end z loop

	// Calculate force vector, use "magnitude" as a multiplicative factor
	weightedVector = weightedVector/totalWeight;
	MVector forceVector;

	double eps = 1.0e-12;
	double speed = velocity.length();
	double fieldStrength = weightedVector.length();

	if ( (speed < eps) && (fieldStrength < eps) )
	{ // particle not moving and no field, resulting force is zero
		forceVector = MVector( 0.0, 0.0, 0.0 );
	}

	if ( (speed < eps) && (fieldStrength >= eps) )
	{ // particle not moving but there is a field, just give it a kick in the direction of the field vector
		forceVector = magnitude * dir * weightedVector / fieldStrength;
	}

	if ( (speed >= eps) && (fieldStrength < eps) )
	{ // particle moving but no field, just control speed
		//forceVector = magnitude * ( (targetSpeed-speed)/targetSpeed ) * velocity;
		forceVector = ( (targetSpeed-speed)/targetSpeed ) * velocity;
	}

	if ( (speed >= eps) && (fieldStrength >= eps) )
	{ // particle moving and there is a field, do turning and control speed

		// this part handles the turning to follow the local data vector
		MVector differenceVector( (dir * weightedVector / fieldStrength) - (velocity / speed) );
		forceVector = magnitude * speed * differenceVector;

		// this part tries to keep the speed constant
		//forceVector = forceVector + magnitude * ( (targetSpeed-speed)/targetSpeed ) * velocity;
		forceVector = forceVector + ( (targetSpeed-speed)/targetSpeed ) * velocity;
	}

	return forceVector;
}


/* ============================================================================
// ownerPosition()
//
//	Descriptions:
//		If this field has an owner, get the owner's position array or
//		centroid, then assign it to the ownerPosArray.
//		If it does not have owner, get the field position in the world
//		space, and assign it to the given array, ownerPosArray.
//
*/
void fieldTracer::ownerPosition
	(
		MDataBlock& block,
		MVectorArray &ownerPosArray
	)
{
	MStatus status;

	if( getApplyPerVertexValue(block) )
	{
		MDataHandle hOwnerPos( block.inputValue(mOwnerPosData, &status) );
		if( status == MS::kSuccess )
		{
			MObject dOwnerPos( hOwnerPos.data() );
			MFnVectorArrayData fnOwnerPos( dOwnerPos );
			MVectorArray posArray( fnOwnerPos.array(&status) );
			if( status == MS::kSuccess )
			{
				// assign vectors from block to ownerPosArray.
				//
				for( unsigned int i = 0; i < posArray.length(); i ++ )
					ownerPosArray.append( posArray[i] );
			}
			else
			{
				MVector worldPos( 0.0, 0.0, 0.0 );
				//status = getWorldPosition( block, worldPos );
				status = getWorldPosition( worldPos );
				ownerPosArray.append( worldPos );
			}
		}
		else
		{
			// get the field position in the world space
			// and add it into ownerPosArray.
			//
			MVector worldPos( 0.0, 0.0, 0.0 );
			//status = getWorldPosition( block, worldPos );
			status = getWorldPosition( worldPos );
			ownerPosArray.append( worldPos );
		}
	}
	else
	{
		MVector centroidV( 0.0, 0.0, 0.0 );
		status = getOwnerCentroidValue( block, centroidV );
		if( status == MS::kSuccess )
		{
			// assign centroid vector to ownerPosArray.
			//
			ownerPosArray.append( centroidV );
		}
		else
		{
			// get the field position in the world space.
			//
			MVector worldPos( 0.0, 0.0, 0.0 );
			//status = getWorldPosition( block, worldPos );
			status = getWorldPosition( worldPos );
			ownerPosArray.append( worldPos );
		}
	}
}


/* ============================================================================
// getWorldPosition()
//
//	Descriptions:
//		get the field position in the world space.
//		The position value is from inherited attribute, aWorldMatrix.
//
*/
MStatus fieldTracer::getWorldPosition( MVector &vector )
{
	MStatus status;

	MObject thisNode( thisMObject() );
	MFnDependencyNode fnThisNode( thisNode );

	// get worldMatrix attribute.
	//
	MObject worldMatrixAttr( fnThisNode.attribute("worldMatrix") );

	// build worldMatrix plug, and specify which element the plug refers to.
	// We use the first element(the first dagPath of this field).
	//
	MPlug matrixPlug( thisNode, worldMatrixAttr );
	matrixPlug = matrixPlug.elementByLogicalIndex( 0 );

	// Get the value of the 'worldMatrix' attribute
	//
	MObject matrixObject;
	status = matrixPlug.getValue( matrixObject );
	if( !status )
	{
		status.perror("fieldTracer::getWorldPosition(): get matrixObject");
		forceFieldCommon::logEntry( (char*) "Abnormal exit from fieldTracer::getWorldPosition(): get matrixObject" );
		diLOG_MSTATUS( status );
		return( status );
	}

	MFnMatrixData worldMatrixData( matrixObject, &status );
	if( !status )
	{
		status.perror("fieldTracer::getWorldPosition(): get worldMatrixData");
		forceFieldCommon::logEntry( (char*) "Abnormal exit from fieldTracer::getWorldPosition(): get worldMatrixData" );
		diLOG_MSTATUS( status );
		return( status );
	}

	MMatrix worldMatrix( worldMatrixData.matrix(&status) );
	if( !status )
	{
		status.perror("fieldTracer::getWorldPosition(): get worldMatrix");
		forceFieldCommon::logEntry( (char*) "Abnormal exit from fieldTracer::getWorldPosition(): get worldMatrix" );
		diLOG_MSTATUS( status );
		return( status );
	}

	// assign the translate to the given vector.
	//
	vector[0] = worldMatrix( 3, 0 );
	vector[1] = worldMatrix( 3, 1 );
	vector[2] = worldMatrix( 3, 2 );

    return( status );
}


/* ============================================================================
// getWorldPosition()
//
//	Descriptions:
//		Find the field position in the world space.
//
*/
MStatus fieldTracer::getWorldPosition( MDataBlock& block, MVector &vector )
{
    MStatus status;

	MObject thisNode( thisMObject() );
	MFnDependencyNode fnThisNode( thisNode );

	// get worldMatrix attribute.
	//
	MObject worldMatrixAttr( fnThisNode.attribute("worldMatrix") );

	// build worldMatrix plug, and specify which element the plug refers to.
	// We use the first element(the first dagPath of this field).
	//
	MPlug matrixPlug( thisNode, worldMatrixAttr );
	matrixPlug = matrixPlug.elementByLogicalIndex( 0 );

    //MDataHandle hWMatrix = block.inputValue( worldMatrix, &status );
    MDataHandle hWMatrix( block.inputValue(matrixPlug, &status) );   diLOG_MSTATUS( status );

    if( status == MS::kSuccess )
    {
        MMatrix wMatrix( hWMatrix.asMatrix() );
        vector[0] = wMatrix(3, 0);
        vector[1] = wMatrix(3, 1);
        vector[2] = wMatrix(3, 2);
    }
    return( status );
}


/* ============================================================================
// getForceAtPoint()
//
//    This method is not required to be overridden, it is only necessary
//    for compatibility with the MFnField function set.
//
*/
MStatus fieldTracer::getForceAtPoint(const MVectorArray&	points,
                                  const MVectorArray&	velocities,
                                  const MDoubleArray&	masses,
                                  MVectorArray&	forceArray,
                                  double	/*deltaTime*/)
{
	MDataBlock block( forceCache() );

	calculateForceArray( block, points, velocities, masses, forceArray );

    return MS::kSuccess;
}


/* ============================================================================
// iconSizeAndOrigin()
*/
MStatus fieldTracer::iconSizeAndOrigin(	GLuint& width,
					GLuint& height,
					GLuint& xbo,
					GLuint& ybo   )
//
//	This method is not required to be overridden.  It should be overridden
//	if the plug-in has custom icon.
//
//	The width and height have to be a multiple of 32 on Windows and 16 on 
//	other platform.
//
//	Define an 8x8 icon at the lower left corner of the 32x32 grid. 
//	(xbo, ybo) of (4,4) makes sure the icon is center at origin.
//
{
	width = 32;
	height = 32;
	xbo = 4;
	ybo = 4;
	return MS::kSuccess;
}


/* ============================================================================
// iconBitMap()
//
//	This method is not required to be overridden.  It should be overridden
//	if the plug-in has custom icon.
//
*/
MStatus fieldTracer::iconBitmap(GLubyte* bitmap)
//	Define an 8x8 icon at the lower left corner of the 32x32 grid. 
//	(xbo, ybo) of (4,4) makes sure the icon is center at origin.
{
	bitmap[0] = 0x18;
	bitmap[4] = 0x66;
	bitmap[8] = 0xC3;
	bitmap[12] = 0x81;
	bitmap[16] = 0x81;
	bitmap[20] = 0xC3;
	bitmap[24] = 0x66;
	bitmap[28] = 0x18;

	return MS::kSuccess;
}


/* ============================================================================
// noiseFunction()
//
//	Descriptions:
//		A noise function.
//
*/
/*
void fieldTracer::noiseFunction( double *inNoise, double *out )
{
	xlim[0][0] = (int)floor( inNoise[0] );
	xlim[0][1] = xlim[0][0] + 1;
	xlim[1][0] = (int)floor( inNoise[1] );
	xlim[1][1] = xlim[1][0] + 1;
	xlim[2][0] = (int)floor( inNoise[2] );
	xlim[2][1] = xlim[2][0] + 1;

	xarg[0] = inNoise[0] - xlim[0][0];
	xarg[1] = inNoise[1] - xlim[1][0];
	xarg[2] = inNoise[2] - xlim[2][0];

	interpolate( out, 0, 3 ) ;
}
*/


/* ============================================================================
// draw()
//
//	Descriptions:
//		Draw a set of rings to symbolie the field. This does not override default icon, you can do that by implementing the iconBitmap() function
//
*/
/*
void fieldTracer::draw( M3dView& view, const MDagPath& path, M3dView::DisplayStyle style, M3dView:: DisplayStatus )
{
	 view.beginGL();
	 for (int j = 0; j < SEGMENTS; j++ )
	 {
		glPushMatrix();
		glRotatef( GLfloat(360 * j / SEGMENTS), 0.0, 1.0, 0.0 );
		glTranslatef( 1.5, 0.0, 0.0 );

		 for (int i = 0; i < EDGES; i++ )
		 {
			glBegin(GL_LINE_STRIP);
			float p0 = float( TORUS_2PI * i / EDGES );
			float p1 = float( TORUS_2PI * (i+1) / EDGES );
			glVertex2f( cos(p0), sin(p0) );
			glVertex2f( cos(p1), sin(p1) );
			glEnd();
		 }
		glPopMatrix();
	 }
	 view.endGL ();
}
*/


/* ============================================================================
// frand()
*/
/*
double fieldTracer::frand( register int s )   // get random number from seed
{
	s = s << 13^s;
	return(1. - ((s*(s*s*15731+789221)+1376312589)&0x7fffffff)/1073741824.);
}
*/


/* ============================================================================
// hermite()
*/
/*
double fieldTracer::hermite( double p0, double p1, double r0, double r1, double t )
{
	register double t2, t3, _3t2, _2t3 ;
	t2 = t * t;
	t3 = t2 * t;
	_3t2 = 3. * t2;
	_2t3 = 2. * t3 ;

	return(p0*(_2t3-_3t2+1) + p1*(-_2t3+_3t2) + r0*(t3-2.*t2+t) + r1*(t3-t2));
}
*/


/* ============================================================================
// interpolate()
//
//	f[] returned tangent and value *
//	i   location ?
//	n   order
//
*/
/*
void fieldTracer::interpolate( double f[4], register int i, register int n )
{
	double f0[4], f1[4] ;  //results for first and second halves

	if( n == 0 )	// at 0, return lattice value
	{
		f[0] = rand3a( xlim[0][i&1], xlim[1][i>>1&1], xlim[2][i>>2] );
		f[1] = rand3b( xlim[0][i&1], xlim[1][i>>1&1], xlim[2][i>>2] );
		f[2] = rand3c( xlim[0][i&1], xlim[1][i>>1&1], xlim[2][i>>2] );
		f[3] = rand3d( xlim[0][i&1], xlim[1][i>>1&1], xlim[2][i>>2] );
		return;
	}

	n--;
	interpolate( f0, i, n );		// compute first half
	interpolate( f1, i| 1<<n, n );	// compute second half

	// use linear interpolation for slopes
	//
	f[0] = (1. - xarg[n]) * f0[0] + xarg[n] * f1[0];
	f[1] = (1. - xarg[n]) * f0[1] + xarg[n] * f1[1];
	f[2] = (1. - xarg[n]) * f0[2] + xarg[n] * f1[2];

	// use hermite interpolation for values
	//
	f[3] = hermite( f0[3], f1[3], f0[n], f1[n], xarg[n] );
}
*/
