/* ============================================================================
// forceField : secondaryData2.cpp
//-
// ==========================================================================
// Copyright (C) 2012 Jim Bamber
//
// Cloned from C:\Program Files\Autodesk\Maya2011\devkit\plug-ins\lavaShader.h
//
// ==========================================================================
//+
//-
// ==========================================================================
// Copyright 1995,2006,2008 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk
// license agreement provided at the time of installation or download,
// or which otherwise accompanies this software in either electronic
// or hard copy form.
// ==========================================================================
//+
*/
#include <maya/MIOStream.h> //STV Dec 2015
#include "secondaryData2.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"
#include <maya/MFnStringData.h>
#include <H5public.h>
#include <H5Apublic.h>
#include <H5Fpublic.h>
#include <H5Gpublic.h>
#include <H5Ppublic.h>
#include <H5Spublic.h>
#include <H5Tpublic.h>

/*
// Local functions
float Noise(float, float, float);
void  Noise_init();
static float Omega(int i, int j, int k, float t[3]);
static float omega(float);
static double turbulence(double u,double v,double w,int octaves);

#define PI                  3.14159265358979323846

#ifdef FLOOR
#undef FLOOR
#endif
#define FLOOR(x)            ((int)floorf(x))
#define TABLELEN            512
#define TLD2                256    // TABLELEN 

// Local variables
static int                  Phi[TABLELEN];
static char                 fPhi[TABLELEN];
static float                G[TABLELEN][3];
*/


// Static data
MTypeId secondaryData2::id( 0x00469 );

// Attributes

/*
MObject  secondaryData2::aColorBase;
MObject  secondaryData2::aColorFlame;
MObject  secondaryData2::aDeform;
MObject  secondaryData2::aWarp;
MObject  secondaryData2::aSpeed;
MObject  secondaryData2::aTurbulence;
MObject  secondaryData2::aPower;
MObject  secondaryData2::aFrame;
*/
MObject  secondaryData2::aPointWorld;
MObject  secondaryData2::aPlaceMat;

MObject secondaryData2::aFilename;             
MObject secondaryData2::aGroupName;            

MObject secondaryData2::aTimeIndex;				

MObject secondaryData2::aParameter1Index;		
MObject secondaryData2::aParameter2Index;		
MObject secondaryData2::aParameter3Index;		
MObject secondaryData2::aParameter4Index;		

MObject secondaryData2::aScaledDatum;

//
// DESCRIPTION:
///////////////////////////////////////////////////////
void secondaryData2::postConstructor()
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType secondaryData2::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


//
// DESCRIPTION:
///////////////////////////////////////////////////////
secondaryData2::secondaryData2()
{
	mFilename = ""; 
	mGroupName = "";

	int mTimeIndex = 0;

	int mParameter1Index = 0;
	int mParameter2Index = 0;
	int mParameter3Index = 0;
	int mParameter4Index = 0;

	mGridDims[0] = 0;					
	mGridDims[1] = 0;					
	mGridDims[2] = 0;					
	mGridMin[0] = 0.0;					
	mGridMin[1] = 0.0;					
	mGridMin[2] = 0.0;					
	mGridMax[0] = 0.0;					
	mGridMax[1] = 0.0;					
	mGridMax[2] = 0.0;					
	mScalarDataGrid.clear();			
	mDataMin = 0.0;					
	mDataMax = 0.0;					
}

//
// DESCRIPTION:
///////////////////////////////////////////////////////
secondaryData2::~secondaryData2()
{
}

//
// DESCRIPTION:
///////////////////////////////////////////////////////
void * secondaryData2::creator()
{
    return new secondaryData2();
}

//
// DESCRIPTION:
///////////////////////////////////////////////////////
MStatus secondaryData2::initialize()
{
    MFnMatrixAttribute mAttr;
    MFnNumericAttribute nAttr; 
	MFnTypedAttribute tAttr;
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnStringData stringData;
	MStatus stat, stat2;

	// Input attributes

	/*
    aColorBase = nAttr.createColor("ColorBase", "cb");
    MAKE_INPUT(nAttr);

    aColorFlame = nAttr.createColor("ColorFlame", "cf");
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(1., 1., 1.) );

    aDeform = nAttr.create( "Deformation", "d", MFnNumericData::kLong);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(1) );
    diLOG_MSTATUS (nAttr.setMin(1) );
    diLOG_MSTATUS ( nAttr.setMax(10) );

    aWarp = nAttr.create( "Warp", "w", MFnNumericData::kFloat);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(0.1f) );
    diLOG_MSTATUS ( nAttr.setMin(0.0f) );
    diLOG_MSTATUS ( nAttr.setMax(10.0f) );

    aSpeed = nAttr.create( "Speed", "ws", MFnNumericData::kFloat);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(0.1f) );
    diLOG_MSTATUS (nAttr.setMin(0.0f) );
    diLOG_MSTATUS (nAttr.setMax(1.0f) );

    aTurbulence = nAttr.create( "Turbulence", "t", MFnNumericData::kLong);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(2) );
    diLOG_MSTATUS ( nAttr.setMin(1) );
    diLOG_MSTATUS ( nAttr.setMax(10) );

    aPower = nAttr.create( "Power", "pow", MFnNumericData::kFloat);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(1.0f) );
    diLOG_MSTATUS ( nAttr.setMin(0.0f) );
    diLOG_MSTATUS ( nAttr.setMax(100.0f) );

    aFrame = nAttr.create( "Frame", "f", MFnNumericData::kFloat);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(1.0f) );
    diLOG_MSTATUS ( nAttr.setMin(0.0f) );
    diLOG_MSTATUS ( nAttr.setMax(1000.0f) );
	*/

	// aFilename
    aFilename = tAttr.create( "filename", "filename", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
    diLOG_MSTATUS ( addAttribute(aFilename) );

	// aGroupName
    aGroupName = tAttr.create( "groupName", "groupName", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
    diLOG_MSTATUS ( addAttribute(aGroupName) );

	// aTimeIndex
	aTimeIndex = nAttr.create( "timeIndex", "timeIndex", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	diLOG_MSTATUS( nAttr.setMin(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS ( addAttribute(aTimeIndex) );

	// aParameter1Index
	aParameter1Index = nAttr.create( "parameter1Index", "parameter1Index", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	diLOG_MSTATUS( nAttr.setMin(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS ( addAttribute(aParameter1Index) );

	// aParameter2Index
	aParameter2Index = nAttr.create( "parameter2Index", "parameter2Index", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	diLOG_MSTATUS( nAttr.setMin(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS ( addAttribute(aParameter2Index) );

	// aParameter3Index
	aParameter3Index = nAttr.create( "parameter3Index", "parameter3Index", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	diLOG_MSTATUS( nAttr.setMin(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS ( addAttribute(aParameter3Index) );

	// aParameter4Index
	aParameter4Index = nAttr.create( "parameter4Index", "parameter4Index", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	diLOG_MSTATUS( nAttr.setMin(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS ( addAttribute(aParameter4Index) );

	// Implicit shading network attributes

    aPlaceMat = mAttr.create("placementMatrix", "pm", MFnMatrixAttribute::kFloat);
    MAKE_INPUT(mAttr);
    diLOG_MSTATUS ( addAttribute(aPlaceMat) );

    aPointWorld = nAttr.createPoint("pointWorld", "pw");
	MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setHidden(true) );
    diLOG_MSTATUS ( addAttribute(aPointWorld) );


	// Create output attributes

    aScaledDatum = nAttr.create( "scaledDatum", "scaledDatum", MFnNumericData::kFloat);
	MAKE_OUTPUT(nAttr);
    diLOG_MSTATUS ( addAttribute(aScaledDatum) );

	/*
	// Add attributes to the node database.
	
    diLOG_MSTATUS ( addAttribute(aColorBase) );
    diLOG_MSTATUS ( addAttribute(aColorFlame) );
    diLOG_MSTATUS ( addAttribute(aDeform) );
    diLOG_MSTATUS ( addAttribute(aWarp) );
    diLOG_MSTATUS ( addAttribute(aSpeed) );
    diLOG_MSTATUS ( addAttribute(aTurbulence) );
    diLOG_MSTATUS ( addAttribute(aPower) );
    diLOG_MSTATUS ( addAttribute(aFrame) );
	*/

    // All inputs affect all outputs
	/*
    diLOG_MSTATUS ( attributeAffects (aColorBase, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aColorFlame, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aDeform, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aWarp, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aSpeed, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aTurbulence, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aPower, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aFrame, aOutAlpha) );
	*/

	diLOG_MSTATUS( attributeAffects(aFilename, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aGroupName, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aTimeIndex, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aParameter1Index, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aParameter2Index, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aParameter3Index, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aParameter4Index, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aPointWorld, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aPlaceMat, aScaledDatum) );

    return MS::kSuccess;
}


///////////////////////////////////////////////////////
// DESCRIPTION:
// This function gets called by Maya to evaluate the texture.
//
///////////////////////////////////////////////////////

MStatus secondaryData2::compute(const MPlug& plug, MDataBlock& block)
{
	MStatus stat;

    if ( (plug != aScaledDatum) && (plug.parent() != aScaledDatum) )
       return MS::kUnknownParameter;

    const float3& worldPos = block.inputValue(aPointWorld).asFloat3();
    const MFloatMatrix& placementMatrix( block.inputValue(aPlaceMat).asFloatMatrix() );
	/*
    const MFloatVector& cBase = block.inputValue(aColorBase).asFloatVector();
    const MFloatVector& cFlame =block.inputValue(aColorFlame).asFloatVector();

    const int   deform = block.inputValue( aDeform ).asLong();
    const float warp   = block.inputValue( aWarp ).asFloat();
    const float speed  = block.inputValue( aSpeed ).asFloat();
    const int   turbValue   = block.inputValue( aTurbulence ).asLong();
    const float power  = block.inputValue( aPower ).asFloat();
    const float frame  = block.inputValue( aFrame ).asFloat();
	*/

	MFloatPoint q(worldPos[0], worldPos[1], worldPos[2]);
    q *= placementMatrix;									// Convert into solid space
	MPoint renderLocation( q );

	/*
    float u, v, w;
    u = q.x; v = q.y; w = q.z;

    float dist = speed * frame;
    float au, av, aw;
    au = u + dist;
    av = v + dist;
    aw = w + dist;

	// Calculate 3 noise values
    float ascale = (float) turbulence( au, av, aw,(int)deform);
    float bscale = (float) turbulence( au,-av, aw,(int)deform);
    float cscale = (float) turbulence(-au, av,-aw,(int)deform);
    float dscale = warp;

	// Add this noise as a vector to the texture coordinates
	// (since we are only calculating one value, the
	// displacement will be alont the 1 1 1 vector ... this
	// displacement generates the "flicker" movement as the
	// value moves around the texture coordinate

    u += ascale * dscale;
    v += bscale * dscale;
    w += cscale * dscale;

	// Calculate a turbulence value for this point

    float scalar = (float) (turbulence(u,v,w,(int)turbValue) + 0.5);

	// convert scalar into a point on the color curve

    if (power != 1) scalar = powf (scalar, power);
	*/

	MDataHandle hFilename( block.inputValue(aFilename, &stat) );   diLOG_MSTATUS( stat );
	MString filename = hFilename.asString();

	MDataHandle hGroupName( block.inputValue(aGroupName, &stat) );   diLOG_MSTATUS( stat );
	MString groupName = hGroupName.asString();

	MDataHandle hTimeIndex( block.inputValue(aTimeIndex, &stat) );   diLOG_MSTATUS( stat );
	int timeIndex = hTimeIndex.asInt();

	MDataHandle hParameter1Index( block.inputValue(aParameter1Index, &stat) );   diLOG_MSTATUS( stat );
	int parameter1Index = hParameter1Index.asInt();

	MDataHandle hParameter2Index( block.inputValue(aParameter2Index, &stat) );   diLOG_MSTATUS( stat );
	int parameter2Index = hParameter2Index.asInt();

	MDataHandle hParameter3Index( block.inputValue(aParameter3Index, &stat) );   diLOG_MSTATUS( stat );
	int parameter3Index = hParameter3Index.asInt();

	MDataHandle hParameter4Index( block.inputValue(aParameter4Index, &stat) );   diLOG_MSTATUS( stat );
	int parameter4Index = hParameter4Index.asInt();

	// Read in data from HDF5 viz file and scale it, if any of the data related parameters have changed
	//
	if ( (filename != mFilename) ||
		 (groupName != mGroupName) ||
		 (timeIndex != mTimeIndex) ||
		 (parameter1Index != mParameter1Index) ||
		 (parameter2Index != mParameter1Index) ||
		 (parameter3Index != mParameter1Index) ||
		 (parameter4Index != mParameter1Index) )
	{
		mFilename = filename;
		mGroupName = groupName;
		mTimeIndex = timeIndex;
		mParameter1Index = parameter1Index;
		mParameter2Index = parameter2Index;
		mParameter3Index = parameter3Index;
		mParameter4Index = parameter4Index;

		readHDF5VizFile();
	}

	//double slope = 1.0;
	//if ( mDataMax != mDataMin ) slope = 1.0 / (mDataMax-mDataMin);
	double halfWindow = 2.0;

	double scaledScalar = lapdCommon::interpolateScalar( renderLocation, mGridDims, mGridMax, mGridMin, mScaledScalarDataGrid, halfWindow );
	//double scaledScalar = slope * (scalarDatum-mDataMin);
	//if ( scaledScalar < 0.0 ) scaledScalar=0.0;  if ( scaledScalar > 1.0 ) scaledScalar = 1.0;

	/*
    MDataHandle outHandle = block.outputValue( aOutColor );
    MFloatVector & outColor = outHandle.asFloatVector();
    outColor = ((cFlame-cBase)*scalar) + cBase;
    outHandle.setClean();
	*/

    MDataHandle outHandle( block.outputValue(aScaledDatum) );
    outHandle.asFloat() = (float)scaledScalar;
    outHandle.setClean();

    return MS::kSuccess;
}


/* ============================================================================
// readHDF5VizFile()
*/
void secondaryData2::readHDF5VizFile()
{
	MStatus stat;

	// Open the file
	//
	if ( mFilename == "" )
	{
        cerr << "Abnormal exit from secondaryData2::readHDF5VizFile(), filename is blank" << endl;
		return;
	}

	// Open the file.
	//
	const char *filenameAsChar = mFilename.asChar();
	hid_t file_id = H5Fopen( filenameAsChar, H5F_ACC_RDONLY, H5P_DEFAULT );	  diLOG_HDF5STAT( file_id );
	if ( file_id < 0 )
	{
//STV Dec 2015		cerr << "Could not open " << mFilename << endl; // Need libsdc++ library for this to work!
        cerr << "Could not open " << mFilename << endl;
		return;
	}


	// ----------------------------------------------------------------------------------------
	// Do the spatial grid first.
	// Spatial grid should be an array of length nz*ny*nx with ix varying fastest (C-style)
	// See the HDF5 User's Guide to see how to read out the array, see p. 203 for use of malloc
	/*
	To read from a dataset:
	NO -- Define memory dataspace of read. (Optional if dataspace is H5S_SELECT_ALL)
	DONE -- Open the dataset.
	DONE -- Get the dataset dataspace. (If using H5S_SELECT_ALL above)

	NO -- Else define dataset dataspace of read. DONE -- Define the memory datatype. (Optional)
	DONE -- Define the memory buffer.
	ALREADY DONE -- Open the dataset.
	DONE -- Read data.

	DONE -- Close the datatype, dataspace, and property list. (As necessary)
	DONE -- Close the dataset.
	*/

	// Open the "Spatial grid" dataset.
	//
	hid_t grid_dataset_id = H5Dopen2( file_id, "Spatial grid", H5P_DEFAULT );   diLOG_HDF5STAT( grid_dataset_id );
	hid_t grid_space_id   = H5Dget_space( grid_dataset_id );			 diLOG_HDF5STAT( grid_space_id );
	hsize_t grid_current_dims[3];
	hsize_t grid_max_dims[3];
	int grid_ndims = H5Sget_simple_extent_dims( grid_space_id, grid_current_dims, grid_max_dims );   diLOG_HDF5STAT( grid_ndims );

	mGridDims[0] = (int)grid_current_dims[2];
	mGridDims[1] = (int)grid_current_dims[1];
	mGridDims[2] = (int)grid_current_dims[0];
	unsigned long grid_npoints = (unsigned long)( grid_current_dims[0] * grid_current_dims[1] * grid_current_dims[2] );

	// Define the data structure and allocate memory.
	//
	typedef struct {
	  float x;
	  float y;
	  float z;
	} vector3;

	hid_t v3_type_id = H5Tcreate( H5T_COMPOUND, sizeof(vector3) );   diLOG_HDF5STAT( v3_type_id );
	diLOG_HDF5STAT( H5Tinsert(v3_type_id, "X", HOFFSET(vector3, x), H5T_NATIVE_FLOAT) );
	diLOG_HDF5STAT( H5Tinsert(v3_type_id, "Y", HOFFSET(vector3, y), H5T_NATIVE_FLOAT) );
	diLOG_HDF5STAT( H5Tinsert(v3_type_id, "Z", HOFFSET(vector3, z), H5T_NATIVE_FLOAT) );

	vector3 *grid_mem_buffer = (vector3 *)malloc( grid_npoints*sizeof(vector3) );

	// Read the dataset.
	//
	diLOG_HDF5STAT( H5Dread(grid_dataset_id, v3_type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, grid_mem_buffer) );
	float Grid_x = (*grid_mem_buffer).x;
	float Grid_y = (*grid_mem_buffer).y;
	float Grid_z = (*grid_mem_buffer).z;
	//sprintf( HDF5common::logString, "Grid_(x,y,z): %f %f %f", Grid_x, Grid_y, Grid_z );
	//HDF5common::logEntry( HDF5common::logString );

	// Find the grid min and max.
	//
	float max_x, max_y, max_z;
	float min_x, min_y, min_z;
    Grid_x = (*grid_mem_buffer).x;
    Grid_y = (*grid_mem_buffer).y;
    Grid_z = (*grid_mem_buffer).z;
	for (int i=0; i<(int)grid_npoints; i++)
	{

		if ( i==1 )
		{
			max_x = Grid_x;  max_y = Grid_y;  max_z = Grid_z;
			min_x = Grid_x;  min_y = Grid_y;  min_z = Grid_z;
		}

		if ( Grid_x > max_x ) max_x = Grid_x;
		if ( Grid_y > max_y ) max_y = Grid_y;
		if ( Grid_z > max_z ) max_z = Grid_z;
		if ( Grid_x < min_x ) min_x = Grid_x;
		if ( Grid_y < min_y ) min_y = Grid_y;
		if ( Grid_z < min_z ) min_z = Grid_z;

        Grid_x = (*grid_mem_buffer++).x;
        Grid_y = (*grid_mem_buffer).y;
        Grid_z = (*grid_mem_buffer).z;
	}

	mGridMax[0] = max_x;
	mGridMax[1] = max_y;
	mGridMax[2] = max_z;
	mGridMin[0] = min_x;
	mGridMin[1] = min_y;
	mGridMin[2] = min_z;

	// Close the ids (but not the file id).
	//
	diLOG_HDF5STAT( H5Tclose(v3_type_id) );
	diLOG_HDF5STAT( H5Sclose(grid_space_id) );
	diLOG_HDF5STAT( H5Dclose(grid_dataset_id) );

	//For some reason, freeing the mem_buffer causes a crash, maybe because it's on the heap (?)
	//free( (void *)mem_buffer );


	// ----------------------------------------------------------------------------------------
	// Now do the scalar data grid.
	// Read the parameter count attribute.
	//
	int parameter_count;
	hid_t attr_group_id = H5Gopen2( file_id, "/Attributes", H5P_DEFAULT );	diLOG_HDF5STAT( attr_group_id );
	hid_t pcount_id = H5Aopen_name( attr_group_id, "Parameter count" );     diLOG_HDF5STAT( pcount_id );
	hid_t pcount_type_id = H5Aget_type( pcount_id );					    diLOG_HDF5STAT( pcount_type_id );

	diLOG_HDF5STAT( H5Aread(pcount_id, pcount_type_id, &parameter_count) );
	//sprintf( HDF5common::logString, "Parameter count: %d", parameter_count );
	//HDF5common::logEntry( HDF5common::logString );

	diLOG_HDF5STAT( H5Tclose(pcount_type_id) );
	diLOG_HDF5STAT( H5Aclose(pcount_id) );
	diLOG_HDF5STAT( H5Gclose(attr_group_id) );


	// Data vector should be array of floats of length nz*ny*nx with ix varying fastest (C-style)
	/*
	To read from a dataset:
	DONE -- Define memory dataspace of read. (Optional if dataspace is H5S_SELECT_ALL)
	DONE -- Open the dataset.
	DONE -- Get the dataset dataspace. (If using H5S_SELECT_ALL above)

	DONE -- Else define dataset dataspace of read. DONE -- Define the memory datatype. (Optional)
	DONE -- Define the memory buffer.
	ALREADY DONE -- Open the dataset.
	DONE -- Read data.

	DONE -- Close the datatype, dataspace, and property list. (As necessary)
	DONE -- Close the dataset.
	*/

	// Open the containing group.
	//
	if ( mGroupName == "" )
	{
		cerr << "Abnormal exit from secondaryData2::readHDF5VizFile(), group name is blank" << endl;
		return;
	}
	const char *groupAsChar = mGroupName.asChar();
	hid_t group_id = H5Gopen2( file_id, groupAsChar, H5P_DEFAULT );   diLOG_HDF5STAT( group_id );

	// Check that the data type is scalar.
	//
	hid_t data_type_id = H5Aopen_name( group_id, "Type" );   diLOG_HDF5STAT( data_type_id );
	hid_t data_type_type_id = H5Aget_type( data_type_id );	 diLOG_HDF5STAT( data_type_type_id );

	char data_type[120];
	diLOG_HDF5STAT( H5Aread(data_type_id, data_type_type_id, data_type) );
	diLOG_HDF5STAT( H5Tclose(data_type_type_id) );
	diLOG_HDF5STAT( H5Aclose(data_type_id) );

	if ( MString(data_type) != "Scalar" ) 
	{
        cerr << "Abnormal exit from secondaryData2::readHDF5VizFile(), data type is not scalar" << endl;
		diLOG_HDF5STAT( H5Gclose(group_id) );
		diLOG_HDF5STAT( H5Fclose(file_id) );
		return;
	}

	// Read the symbol attributes.
	//
	hid_t symbol_id = H5Aopen_name( group_id, "Symbol" );   diLOG_HDF5STAT( symbol_id );
	hid_t symbol_type_id = H5Aget_type( symbol_id );		diLOG_HDF5STAT( symbol_type_id );

	char data_symbol[120];
	diLOG_HDF5STAT( H5Aread(symbol_id, symbol_type_id, data_symbol) );
	diLOG_HDF5STAT( H5Tclose(symbol_type_id) );
	diLOG_HDF5STAT( H5Aclose(symbol_id) );

	// Construct the dataset name.
	//
	MString datasetName(data_symbol);
	if ( parameter_count >= 1 )	datasetName = datasetName + "." + mParameter1Index;
	if ( parameter_count >= 2 )	datasetName = datasetName + "." + mParameter2Index;
	if ( parameter_count >= 3 )	datasetName = datasetName + "." + mParameter3Index;
	if ( parameter_count == 4 )	datasetName = datasetName + "." + mParameter4Index;
	const char *datasetAsChar = datasetName.asChar();
	//sprintf( HDF5common::logString, "Dataset name: %s", datasetAsChar );
	//HDF5common::logEntry( HDF5common::logString );

	// Open the dataset.
	//
	hid_t dataset_id = H5Dopen2( group_id, datasetAsChar, H5P_DEFAULT );	diLOG_HDF5STAT( dataset_id );
	hid_t space_id = H5Dget_space( dataset_id );							diLOG_HDF5STAT( space_id );
	hsize_t current_dims[4];
	hsize_t max_dims[4];
	int ndims = H5Sget_simple_extent_dims( space_id, current_dims, max_dims );   diLOG_HDF5STAT( ndims );
	unsigned long nt = (unsigned long)current_dims[0];
	unsigned long nz = (unsigned long)current_dims[1];
	unsigned long ny = (unsigned long)current_dims[2];
	unsigned long nx = (unsigned long)current_dims[3];
	unsigned long npoints = nz * ny * nx;
	//sprintf( HDF5common::logString, "nt: %d  nz: %d  ny: %d  nx: %d", nt, nz, ny, nx );
	//HDF5common::logEntry( HDF5common::logString );

	// Prepare to read the dataset.  First allocate memory.
	//
	float *mem_buffer = (float *)malloc( npoints*sizeof(float) );
	//sprintf( HDF5common::logString, "npoints: %d", npoints );
	//HDF5common::logEntry( HDF5common::logString );

	// Define memory dataspace.
	//
	hsize_t mem_dims[4];
	mem_dims[0] = 1;
	mem_dims[1] = nz;
	mem_dims[2] = ny;
	mem_dims[3] = nx;
	hid_t memspace_id = H5Screate_simple( 4, mem_dims, mem_dims );   diLOG_HDF5STAT( memspace_id );

	hsize_t start[4];
	hsize_t end[4];
	diLOG_HDF5STAT( H5Sget_select_bounds(space_id, start, end) );
	//sprintf( HDF5common::logString, "start[0:3]: %d %d %d %d", start[0], start[1], start[2], start[3] );
	//HDF5common::logEntry( HDF5common::logString );
	//sprintf( HDF5common::logString, "end[0:3]: %d %d %d %d", end[0], end[1], end[2], end[3] );
	//HDF5common::logEntry( HDF5common::logString );

	// Define filespace hyperslab.
	//
	start[0] = mTimeIndex;
	start[1] = 0;
	start[2] = 0;
	start[3] = 0;

	hsize_t stride[4];
	stride[0] = 1;
	stride[1] = 1;
	stride[2] = 1;
	stride[3] = 1;

	hsize_t count[4];
	count[0] = 1;
	count[1] = nz;
	count[2] = ny;
	count[3] = nx;

	hsize_t block[4];
	block[0] = 1;
	block[1] = 1;
	block[2] = 1;
	block[3] = 1;

	diLOG_HDF5STAT( H5Sselect_hyperslab(space_id, H5S_SELECT_SET, start, stride, count, block) );
	htri_t valid_select = H5Sselect_valid( space_id );   diLOG_HDF5STAT( valid_select );
	diLOG_BOOLSTAT( (valid_select > 0) );
	diLOG_HDF5STAT( H5Sget_select_bounds(space_id, start, end) );
	//sprintf( HDF5common::logString, "start[0:3]: %d %d %d %d", start[0], start[1], start[2], start[3] );
	//HDF5common::logEntry( HDF5common::logString );
	//sprintf( HDF5common::logString, "end[0:3]: %d %d %d %d", end[0], end[1], end[2], end[3] );
	//HDF5common::logEntry( HDF5common::logString );

	// Read the data.
	//
	//HDF5common::logEntry( (char*) "About to call H5Dread()" );
	diLOG_HDF5STAT( H5Dread(dataset_id, H5T_NATIVE_FLOAT, memspace_id, space_id, H5P_DEFAULT, mem_buffer) );
	float Data_item = (*mem_buffer);
	//sprintf( HDF5common::logString, "Data_item: %f", Data_item );
	//HDF5common::logEntry( HDF5common::logString );

	// Close the ids.
	//
	diLOG_HDF5STAT( H5Sclose(space_id) );
	diLOG_HDF5STAT( H5Sclose(memspace_id) );
	diLOG_HDF5STAT( H5Dclose(dataset_id) );
	diLOG_HDF5STAT( H5Gclose(group_id) );
	diLOG_HDF5STAT( H5Fclose(file_id) );

	// Build the scalar data array.
	//
	double element;
	float floatElement;

	mScalarDataGrid.clear();
    floatElement = (*mem_buffer);
	for (int i=0; i<(int)npoints; i++)
	{
		element = (double)floatElement;
		if ( i==0 )
		{
			mDataMax = floatElement;
			mDataMin = floatElement;
		}
		if ( floatElement > mDataMax ) mDataMax = floatElement;
		if ( floatElement < mDataMin ) mDataMin = floatElement;
		mScalarDataGrid.append( element );
        floatElement = (*mem_buffer++);
	}

	//For some reason, freeing the mem_buffer causes a crash, maybe because it's on the heap (?)
	//free( (void *)mem_buffer );

	// Now scale the array.
	//
	double slope = 1.0;
	if ( mDataMax != mDataMin ) slope = 1.0 / (mDataMax-mDataMin);

	mScaledScalarDataGrid.clear();
	for (int i=0; i<(int)npoints; i++)
	{
		double scaledScalar = slope * (mScalarDataGrid[i]-mDataMin);
		if ( scaledScalar < 0.0 ) scaledScalar=0.0;  if ( scaledScalar > 1.0 ) scaledScalar = 1.0;
		mScaledScalarDataGrid.append( scaledScalar );
	}

	return;
}


