/* ============================================================================
// forceField : curves.cpp
//-
// ==========================================================================
// Copyright (C) 2007 Jim Bamber
//
// ==========================================================================
//+
*/
#include "curves.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MArrayDataBuilder.h> //STV


// Static data
MTypeId curves::id( 0x00453 );

// Attributes
//MObject curves::aInputCurve;
MObject curves::aInputMesh;
MObject curves::aStepDistance;
MObject curves::aMaxSteps;
MObject curves::aReverseField;
MObject curves::aSpatialGrid;
MObject curves::aDims;
MObject curves::aGridMin;
MObject curves::aGridMax;
MObject curves::aVectorDataGrid;

MObject curves::aCurveCount;
MObject curves::aOutputCurves;
MObject curves::aStartIndices;
MObject curves::aCVCounts;
MObject curves::aControlVertexList;
MObject curves::aTangentList;


/* ============================================================================
// postConstructor()
*/
void curves::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType curves::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// curves()
*/
curves::curves()
{
}


/* ============================================================================
// ~curves()
*/
curves::~curves()
{
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * curves::creator()
{
    return new curves();
}


/* ============================================================================
// initialize()
//
// initializes attribute information
*/
MStatus curves::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> curves::initialize()" );


	// Create input attributes:
	//
	// input mesh
	aInputMesh = tAttr.create( "inputMesh", "mesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aInputMesh) );

	// step distance
	aStepDistance = nAttr.create( "stepDistance", "step", MFnNumericData::kFloat, 0, &stat ); diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aStepDistance) );

	// max steps
	aMaxSteps = nAttr.create( "maxSteps", "max", MFnNumericData::kInt, 0, &stat ); diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1000) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aMaxSteps) );

	// reverse field
	aReverseField = nAttr.create( "reverseField", "reverse", MFnNumericData::kBoolean, false, &stat );   diLOG_MSTATUS( stat );
	nAttr.setDefault( false );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aReverseField) );

	// spatial grid
    aSpatialGrid = tAttr.create( "spatialGrid", "grid", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aSpatialGrid) );

	// grid dimensions
	aDims = nAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// vector data grid
    aVectorDataGrid = tAttr.create( "vectorDataGrid", "vData", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aVectorDataGrid) );


	// Create output attributes
	//
	// curve count
	aCurveCount = nAttr.create( "curveCount", "count", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_OUTPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveCount) );

	// output curves
	aOutputCurves = tAttr.create( "outputCurves", "curves", MFnNurbsCurveData::kNurbsCurve, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
	diLOG_MSTATUS( tAttr.setArray(true) );
	diLOG_MSTATUS( tAttr.setUsesArrayDataBuilder(true) );
	diLOG_MSTATUS( addAttribute(aOutputCurves) );

	// start indices
	aStartIndices = tAttr.create( "startIndices", "starts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_OUTPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aStartIndices) );

	// CV counts
	aCVCounts = tAttr.create( "CVCounts", "counts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_OUTPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aCVCounts) );

	// control vertices
	aControlVertexList = tAttr.create( "controlVertexList", "cvs", MFnData::kPointArray, fnPointArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_OUTPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aControlVertexList) );

	// tangents
	aTangentList = tAttr.create( "tangentList", "tangents", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_OUTPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aTangentList) );


	// All inputs affect all outputs
	//
	// curve count
    diLOG_MSTATUS( attributeAffects(aInputMesh, aCurveCount) );
    diLOG_MSTATUS( attributeAffects(aStepDistance, aCurveCount) );
    diLOG_MSTATUS( attributeAffects(aMaxSteps, aCurveCount) );
    diLOG_MSTATUS( attributeAffects(aReverseField, aCurveCount) );
    diLOG_MSTATUS( attributeAffects(aSpatialGrid, aCurveCount) );
    diLOG_MSTATUS( attributeAffects(aDims, aCurveCount) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aCurveCount) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aCurveCount) );
    diLOG_MSTATUS( attributeAffects(aVectorDataGrid, aCurveCount) );

	// output curves
    diLOG_MSTATUS( attributeAffects(aInputMesh, aOutputCurves) );
    diLOG_MSTATUS( attributeAffects(aStepDistance, aOutputCurves) );
    diLOG_MSTATUS( attributeAffects(aMaxSteps, aOutputCurves) );
    diLOG_MSTATUS( attributeAffects(aReverseField, aOutputCurves) );
    diLOG_MSTATUS( attributeAffects(aSpatialGrid, aOutputCurves) );
    diLOG_MSTATUS( attributeAffects(aDims, aOutputCurves) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aOutputCurves) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aOutputCurves) );
    diLOG_MSTATUS( attributeAffects(aVectorDataGrid, aOutputCurves) );

	// start indices
    diLOG_MSTATUS( attributeAffects(aInputMesh, aStartIndices) );
    diLOG_MSTATUS( attributeAffects(aStepDistance, aStartIndices) );
    diLOG_MSTATUS( attributeAffects(aMaxSteps, aStartIndices) );
    diLOG_MSTATUS( attributeAffects(aReverseField, aStartIndices) );
    diLOG_MSTATUS( attributeAffects(aSpatialGrid, aStartIndices) );
    diLOG_MSTATUS( attributeAffects(aDims, aStartIndices) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aStartIndices) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aStartIndices) );
    diLOG_MSTATUS( attributeAffects(aVectorDataGrid, aStartIndices) );

	// CV counts
    diLOG_MSTATUS( attributeAffects(aInputMesh, aCVCounts) );
    diLOG_MSTATUS( attributeAffects(aStepDistance, aCVCounts) );
    diLOG_MSTATUS( attributeAffects(aMaxSteps, aCVCounts) );
    diLOG_MSTATUS( attributeAffects(aReverseField, aCVCounts) );
    diLOG_MSTATUS( attributeAffects(aSpatialGrid, aCVCounts) );
    diLOG_MSTATUS( attributeAffects(aDims, aCVCounts) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aCVCounts) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aCVCounts) );
    diLOG_MSTATUS( attributeAffects(aVectorDataGrid, aCVCounts) );

	// control vertices
    diLOG_MSTATUS( attributeAffects(aInputMesh, aControlVertexList) );
    diLOG_MSTATUS( attributeAffects(aStepDistance, aControlVertexList) );
    diLOG_MSTATUS( attributeAffects(aMaxSteps, aControlVertexList) );
    diLOG_MSTATUS( attributeAffects(aReverseField, aControlVertexList) );
    diLOG_MSTATUS( attributeAffects(aSpatialGrid, aControlVertexList) );
    diLOG_MSTATUS( attributeAffects(aDims, aControlVertexList) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aControlVertexList) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aControlVertexList) );
    diLOG_MSTATUS( attributeAffects(aVectorDataGrid, aControlVertexList) );

	// tangents
    diLOG_MSTATUS( attributeAffects(aInputMesh, aTangentList) );
    diLOG_MSTATUS( attributeAffects(aStepDistance, aTangentList) );
    diLOG_MSTATUS( attributeAffects(aMaxSteps, aTangentList) );
    diLOG_MSTATUS( attributeAffects(aReverseField, aTangentList) );
    diLOG_MSTATUS( attributeAffects(aSpatialGrid, aTangentList) );
    diLOG_MSTATUS( attributeAffects(aDims, aTangentList) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aTangentList) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aTangentList) );
    diLOG_MSTATUS( attributeAffects(aVectorDataGrid, aTangentList) );

	forceFieldCommon::logEntry( (char*) "<-- curves::initialize()" );
    return MS::kSuccess;
}


/* ============================================================================
// calculateCurve()
*/
int curves::calculateCurve(
	MPoint &startPoint,
	float stepDistance,
	int maxSteps,
	bool reverseField,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MVectorArray& vectorDataGrid,
	MPointArray& cvs,
	MDoubleArray& knots,
	MVectorArray& tangents)
{
	int CVCount = 0;
	double halfWindow = 2.0;
	bool insideDataVolume = true;
	MVector startVector;

	cvs.clear();
	knots.clear();
	tangents.clear();

	cvs.append( startPoint );
	startVector = lapdCommon::interpolateVector( startPoint, dims, gridMax, gridMin, vectorDataGrid, halfWindow );
	diLOG_MSTATUS( startVector.normalize() );
	tangents.append( startVector );  // keep tangents the same direction as the actual field
	if ( reverseField ) startVector = -startVector;
	CVCount = cvs.length();

	if ( startPoint.x < gridMin[0] ) insideDataVolume = false;
	if ( startPoint.x > gridMax[0] ) insideDataVolume = false;
	if ( startPoint.y < gridMin[1] ) insideDataVolume = false;
	if ( startPoint.y > gridMax[1] ) insideDataVolume = false;
	if ( startPoint.z < gridMin[2] ) insideDataVolume = false;
	if ( startPoint.z > gridMax[2] ) insideDataVolume = false;

	float h = stepDistance;
	MPoint currentPoint( startPoint );
    MPoint tempPoint = startPoint; // STV
	MVector currentVector;
	MVector vector1, vector2, vector3, vector4;
	MVector k1, k2, k3, k4;

	while ( (CVCount < maxSteps) && insideDataVolume )
	{
		// This is the original method
		//
		/*
		MPoint point1, point2;
		MVector vector1, vector2;
		point1 = point;
		vector1 = lapdCommon::interpolateVector( point1, dims, gridMax, gridMin, vectorDataGrid, halfWindow );
		diLOG_MSTATUS( vector1.normalize() );
		if ( reverseField ) vector1 = -vector1;

		point2 = point1 + stepDistance*vector1;
		vector2 = lapdCommon::interpolateVector( point2, dims, gridMax, gridMin, vectorDataGrid, halfWindow );
		diLOG_MSTATUS( vector2.normalize() );
		if ( reverseField ) vector2 = -vector2;

		vector = 0.5 * (vector1+vector2);
		point = point1 + stepDistance*vector;
		*/


		// Fourth order Runge-Kutta method
		//   -from Wikipedia
		//
		// One member of the family of Runge�Kutta methods is so commonly used that it is often referred to as "RK4", "classical Runge-Kutta method" 
		// or simply as "the Runge�Kutta method".  Let an initial value problem be specified as follows.
		//
		//		y' = f(t, y),  y(t_0) = y_0 
		//
		// In words, what this means is that the rate at which y changes is a function of y and of t (time). At the start, time is t0 and y is y0.
		// The RK4 method for this problem is given by the following equations:
		//
		//		y_{n+1} = y_n + 1/6 (k_1 + 2k_2 + 2k_3 + k_4)
		//		t_{n+1} = t_n + h
		//
		// where y_{n+1} is the RK4 approximation of y(t_{n+1}), and
		//
		//		k_1 = hf(t_n, y_n)
		//		k_2 = hf(t_n + 1/2 h, y_n + 1/2 k_1)
		//		k_3 = hf(t_n + 1/2 h, y_n + 1/2 k_2)
		//		k_4 = hf(t_n + h , y_n + k_3) 
		//
		// Thus, the next value y_{n+1} is determined by the present value (y_n) plus the weighted average of 4 deltas, where each delta is the 
		// product of the size of the interval (h = ?t) and an estimated slope: h (slope) = h f(t, y) = ?t (dy / dt) = ?y.
		//
		// k_1 is the delta based on the slope at the beginning of the interval, using y_n, ( Euler's method ) ;
		// k_2 is the delta based on the slope at the midpoint of the interval, using y_n + 1/2 k_1 ;
		// k_3 is again the delta based on the slope at the midpoint, but now using y_n + 1/2 k_2 ;
		// k_4 is the delta based on the slope at the end of the interval, using y_n + k_3 .
		//
		// In averaging the four deltas, greater weight is given to the deltas at the midpoint:
		//
		//		delta = 1/6 (k_1 + 2k_2 + 2k_3 + k_4).
		//
		// The RK4 method is a fourth-order method, meaning that the error per step is on the order of h^5, while the total accumulated error 
		// has order h^4.
		//

		vector1 = lapdCommon::interpolateVector( currentPoint, dims, gridMax, gridMin, vectorDataGrid, halfWindow );
		diLOG_MSTATUS( vector1.normalize() );
		if ( reverseField ) vector1 = -vector1;
		k1 = h * vector1;

        tempPoint = (currentPoint+0.5*k1) ; //STV
		vector2 = lapdCommon::interpolateVector( tempPoint, dims, gridMax, gridMin, vectorDataGrid, halfWindow );
		diLOG_MSTATUS( vector2.normalize() );
		if ( reverseField ) vector2 = -vector2;
		k2 = h * vector2;
        
        tempPoint = (currentPoint+0.5*k2) ; //STV
		vector3 = lapdCommon::interpolateVector( tempPoint, dims, gridMax, gridMin, vectorDataGrid, halfWindow );
		diLOG_MSTATUS( vector3.normalize() );
		if ( reverseField ) vector3 = -vector3;
		k3 = h * vector3;
        
        tempPoint = (currentPoint+k3) ; // STV
		vector4 = lapdCommon::interpolateVector( tempPoint, dims, gridMax, gridMin, vectorDataGrid, halfWindow );
		diLOG_MSTATUS( vector4.normalize() );
		if ( reverseField ) vector4 = -vector4;
		k4 = h * vector4;


		currentVector = 0.16666666666666666666666666666667 * ( k1 + 2*k2 + 2*k3 + k4 );
		currentPoint = currentPoint + currentVector;

		cvs.append( currentPoint );
		diLOG_MSTATUS( currentVector.normalize() );
		if ( !reverseField ) tangents.append( currentVector );
		else tangents.append( -currentVector );  // keep tangents pointing in the same direction as the original field
		CVCount = cvs.length();

		if ( currentPoint.x < gridMin[0] ) insideDataVolume = false;
		if ( currentPoint.x > gridMax[0] ) insideDataVolume = false;
		if ( currentPoint.y < gridMin[1] ) insideDataVolume = false;
		if ( currentPoint.y > gridMax[1] ) insideDataVolume = false;
		if ( currentPoint.z < gridMin[2] ) insideDataVolume = false;
		if ( currentPoint.z > gridMax[2] ) insideDataVolume = false;
	}

	knots.append( 0 );
	knots.append( 0 );
	knots.append( 0 );
	for ( double cvIndex=1; cvIndex<=(double)CVCount-4; cvIndex++ ) knots.append( cvIndex );
	knots.append( (double)CVCount-3 );
	knots.append( (double)CVCount-3 );
	knots.append( (double)CVCount-3 );

	return CVCount;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the curves.
//
*/
MStatus curves::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> curves::compute()" );
	forceFieldCommon::logEntry( forceFieldCommon::eCurves );

	if ( (plug != aCurveCount) &&
		 (plug != aOutputCurves) &&
		 (plug != aStartIndices) &&
		 (plug != aCVCounts) &&
		 (plug != aControlVertexList) &&
		 (plug != aTangentList) )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from curves::compute(), Maya not asking for one of the expected outputs" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hInputMesh( block.inputValue(aInputMesh, &stat) );   diLOG_MSTATUS( stat );
	MObject inMesh( hInputMesh.asMeshTransformed() );
	MFnMesh fnInputMesh( inMesh );

	MDataHandle hStepDistance( block.inputValue(aStepDistance, &stat) );   diLOG_MSTATUS( stat );
	double stepDistance = hStepDistance.asFloat();

	MDataHandle hMaxSteps( block.inputValue(aMaxSteps, &stat) );   diLOG_MSTATUS( stat );
	int maxSteps = hMaxSteps.asInt();

	MDataHandle hReverseField( block.inputValue(aReverseField, &stat) );   diLOG_MSTATUS( stat );
	bool reverseField = hReverseField.asBool();

	MDataHandle hSpatialGrid( block.inputValue(aSpatialGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnSpatialGrid( hSpatialGrid.data() );
	MVectorArray spatialGrid( fnSpatialGrid.array() );

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hVectorDataGrid( block.inputValue(aVectorDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnVectorDataGrid( hVectorDataGrid.data() );
	MVectorArray vectorDataGrid( fnVectorDataGrid.array() );


	// Now do the output attributes, starting with the easy one: curveCount.
	//
	MDataHandle hCurveCount( block.outputValue(aCurveCount, &stat) );   diLOG_MSTATUS( stat );
	MPointArray meshPoints;
	diLOG_MSTATUS( fnInputMesh.getPoints(meshPoints, MSpace::kWorld) );
	int curveCount = meshPoints.length();
	hCurveCount.set( curveCount );
	hCurveCount.setClean();


	// The array outputs are harder to handle.  The general approach is
	// to create an array data build that is preallocated to hold just
	// the number of curves we plan on creating.  When this builder
	// is set in to the MArrayDataHandle at the end of the compute
	// method, the new array will replace the existing array in the
	// scene.
	// 
	// If the number of elements of the multi does not change between
	// compute cycles, then one can reuse the space allocated on a
	// previous cycle by extracting the existing builder from the
	// MArrayDataHandle:
	//		MArrayDataBuilder builder( outputArray.builder(&stat) );
	// This later form of the builder will allow you to rewrite elements
	// of the array, and to grow it, but the array can only be shrunk by
	// explicitly removing elements with the method
	//		MArrayDataBuilder::removeElement(unsigned index);
	//
	MArrayDataHandle hOutputCurves( block.outputArrayValue(aOutputCurves, &stat) );   diLOG_MSTATUS( stat );
	MArrayDataBuilder buildOutputCurves( &block, aOutputCurves, curveCount, &stat );   diLOG_MSTATUS( stat );

	MDataHandle hStartIndices( block.outputValue(aStartIndices, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnStartIndices( hStartIndices.data() );
	//MIntArray startIndices = fnStartIndices.array();
	//startIndices.clear();
	MIntArray startIndices;

	MDataHandle hCVCounts( block.outputValue(aCVCounts, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnCVCounts( hCVCounts.data() );
	//MIntArray CVCounts = fnCVCounts.array();
	//CVCounts.clear();
	MIntArray CVCounts;

	MDataHandle hControlVertexList( block.outputValue(aControlVertexList, &stat) );   diLOG_MSTATUS( stat );
	MFnPointArrayData fnControlVertexList( hControlVertexList.data() );
	//MPointArray controlVertexList = fnControlVertexList.array();
	//controlVertexList.clear();
	MPointArray controlVertexList;

	MDataHandle hTangentList( block.outputValue(aTangentList, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnTangentList( hTangentList.data() );
	//MVectorArray tangentList = fnTangentList.array();
	//tangentList.clear();
	MVectorArray tangentList;

	//forceFieldCommon::logEntry( (char*) "About to calculate curves" );
	for ( int curveNum = 0; curveNum < curveCount; curveNum++ )
	{
		MDataHandle hOutCurve( buildOutputCurves.addElement(curveNum) );
		MFnNurbsCurveData dataCreator;
		MObject outCurveData( dataCreator.create() );
		MFnNurbsCurve fnOutCurve( outCurveData );

		MPoint startPoint = meshPoints[curveNum];
		MPointArray cvs;
		MDoubleArray knots;
		MVectorArray tangents;
		int CVCount = calculateCurve(
			startPoint, (float)stepDistance, maxSteps, reverseField, dims, gridMin, gridMax, vectorDataGrid, cvs, knots, tangents );

		if ( CVCount >= 4 )
		{
			fnOutCurve.create( cvs, knots, 3, MFnNurbsCurve::kOpen, false, false, outCurveData, &stat );
			diLOG_MSTATUS( stat );
		}

		hOutCurve.set( outCurveData );
		if ( curveNum == 0 ) startIndices.append( 0 );
		else startIndices.append( startIndices[curveNum-1] + CVCounts[curveNum-1] );
		CVCounts.append( CVCount );
		for ( int i=0; i<CVCount; i++ )
		{
			controlVertexList.append( cvs[i] );
			int index = controlVertexList.length() - 1;
			tangentList.append( tangents[i] );
		}
	}


	// Set the builder back into the output array.  This statement
	// is always required, no matter what constructor was used to
	// create the builder.
	diLOG_MSTATUS( hOutputCurves.set(buildOutputCurves) );
	fnStartIndices.set( startIndices );
	fnCVCounts.set( CVCounts );
	fnControlVertexList.set( controlVertexList );
	fnTangentList.set( tangentList );

	// Since we compute all the elements of the array, instead of
	// just marking the plug we were asked to compute as clean, mark
	// every element of the array as clean to prevent further calls
	// to this compute method during this DG evaluation cycle.
	diLOG_MSTATUS( hOutputCurves.setAllClean() );
	hStartIndices.setClean();
	hCVCounts.setClean();
	hControlVertexList.setClean();
	hTangentList.setClean();

	//forceFieldCommon::logEntry( (char*) "<-- curves::compute()" );
	return stat;
}
