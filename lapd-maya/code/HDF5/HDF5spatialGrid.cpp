/* ==========================================================================
// HDF5 : HDF5spatialGrid.cpp
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "HDF5common.h"
#include "errorHandling.h"
#include "HDF5spatialGrid.h"
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MArrayDataBuilder.h>
#include <maya/MGlobal.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatVector.h>

#include <H5public.h>
#include <H5Fpublic.h>
#include <H5Ppublic.h>
#include <H5Spublic.h>
#include <H5Tpublic.h>

MTypeId  HDF5spatialGrid::id( 0x00333 );

MObject  HDF5spatialGrid::filename;        
MObject  HDF5spatialGrid::grid;
MObject  HDF5spatialGrid::dims;        
MObject  HDF5spatialGrid::max;        
MObject  HDF5spatialGrid::min;        


/* ==========================================================================
// compute()
*/
MStatus HDF5spatialGrid::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;
	//HDF5common::logEntry( (char*) "--> HDF5spatialGrid::compute()" );
	HDF5common::logEntry( HDF5common::eHDF5spatialGrid );

	if ( ( plug == grid ) || ( plug.parent() == grid ) ||
		 ( plug == dims ) || ( plug.parent() == dims ) ||
		 ( plug == max ) || ( plug.parent() == max ) ||
		 ( plug == min ) || ( plug.parent() == min ) )
	{
		stat = computeKernel( plug, data );
		data.setClean( plug );
	} 
	else 
		stat  = MS::kUnknownParameter;

	//HDF5common::logEntry( (char*) "<-- HDF5spatialGrid::compute()" );
	return stat;
}


/* ==========================================================================
// computeKernel()
*/
MStatus HDF5spatialGrid::computeKernel( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	MDataHandle gridHandle( data.outputValue(grid, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle dimsHandle( data.outputValue(dims, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle maxHandle( data.outputValue(max, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle minHandle( data.outputValue(min, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle filenameHandle( data.inputValue(filename, &stat) );  diLOG_MSTATUS( stat );

	//HDF5common::logEntry( (char*) "--> HDF5spatialGrid::computeKernel()" );


	MString filename( filenameHandle.asString() );
	if ( filename == "" )
	{
		HDF5common::logEntry( (char*) "Abnormal exit from HDF5spatialGrid::computeKernel(), filename is blank" );
		return MS::kSuccess;
	}

	// Open the file.
	//
	const char *filenameAsChar = filename.asChar();
	hid_t file_id = H5Fopen( filenameAsChar, H5F_ACC_RDONLY, H5P_DEFAULT );	  diLOG_HDF5STAT( file_id );
	if ( file_id < 0 )
	{
		sprintf_s( HDF5common::logString, DI_LOG_STRING_LENGTH, "Could not open %s", filenameAsChar );
		HDF5common::logEntry( HDF5common::logString );
		return MS::kFailure;
	}


	// Spatial grid should be an array of length nz*ny*nx with ix varying fastest (C-style)
	// See the HDF5 User's Guide to see how to read out the array, see p. 203 for use of malloc
	/*
	To read from a dataset:
	NO -- Define memory dataspace of read. (Optional if dataspace is H5S_SELECT_ALL)
	DONE -- Open the dataset.
	DONE -- Get the dataset dataspace. (If using H5S_SELECT_ALL above)

	NO -- Else define dataset dataspace of read. DONE -- Define the memory datatype. (Optional)
	DONE -- Define the memory buffer.
	ALREADY DONE -- Open the dataset.
	DONE -- Read data.

	DONE -- Close the datatype, dataspace, and property list. (As necessary)
	DONE -- Close the dataset.
	*/

	// Open the "Spatial grid" dataset.
	//
	hid_t dataset_id = H5Dopen2( file_id, "Spatial grid", H5P_DEFAULT );   diLOG_HDF5STAT( dataset_id );
	hid_t space_id   = H5Dget_space( dataset_id );			 diLOG_HDF5STAT( space_id );
	hsize_t current_dims[3];
	hsize_t max_dims[3];
	int ndims = H5Sget_simple_extent_dims( space_id, current_dims, max_dims );   diLOG_HDF5STAT( ndims );

	int3& mDims = dimsHandle.asInt3();
	mDims[0] = (int)current_dims[2];
	mDims[1] = (int)current_dims[1];
	mDims[2] = (int)current_dims[0];
	dimsHandle.setClean();
	//dimsHandle.set( (int)current_dims[0], (int)current_dims[1], (int)current_dims[2] );
	unsigned long npoints = (unsigned long)( current_dims[0] * current_dims[1] * current_dims[2] );

	// Define the data structure and allocate memory.
	//
	typedef struct {
	  float x;
	  float y;
	  float z;
	} vector3;

	hid_t v3_type_id = H5Tcreate( H5T_COMPOUND, sizeof(vector3) );   diLOG_HDF5STAT( v3_type_id );
	diLOG_HDF5STAT( H5Tinsert(v3_type_id, "X", HOFFSET(vector3, x), H5T_NATIVE_FLOAT) );
	diLOG_HDF5STAT( H5Tinsert(v3_type_id, "Y", HOFFSET(vector3, y), H5T_NATIVE_FLOAT) );
	diLOG_HDF5STAT( H5Tinsert(v3_type_id, "Z", HOFFSET(vector3, z), H5T_NATIVE_FLOAT) );

	vector3 *mem_buffer = (vector3 *)malloc( npoints*sizeof(vector3) );

	// Read the dataset.
	//
	diLOG_HDF5STAT( H5Dread(dataset_id, v3_type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, mem_buffer) );
	float Grid_x = (*mem_buffer).x;
	float Grid_y = (*mem_buffer).y;
	float Grid_z = (*mem_buffer).z;
	//sprintf( HDF5common::logString, "Grid_(x,y,z): %f %f %f", Grid_x, Grid_y, Grid_z );
	//HDF5common::logEntry( HDF5common::logString );

	// Build the array attribute.
	//
	float max_x, max_y, max_z;
	float min_x, min_y, min_z;
	MFnVectorArrayData mGrid( gridHandle.data() );
	MVectorArray vectorArray;
	MVector vectorElement;
    Grid_x = (*mem_buffer).x;
    Grid_y = (*mem_buffer).y;
    Grid_z = (*mem_buffer).z;
	for (int i=0; i<(int)npoints; i++)
	{

		if ( i==1 )
		{
			max_x = Grid_x;  max_y = Grid_y;  max_z = Grid_z;
			min_x = Grid_x;  min_y = Grid_y;  min_z = Grid_z;
		}

		if ( Grid_x > max_x ) max_x = Grid_x;
		if ( Grid_y > max_y ) max_y = Grid_y;
		if ( Grid_z > max_z ) max_z = Grid_z;
		if ( Grid_x < min_x ) min_x = Grid_x;
		if ( Grid_y < min_y ) min_y = Grid_y;
		if ( Grid_z < min_z ) min_z = Grid_z;

		vectorElement = MVector( Grid_x, Grid_y, Grid_z );
		vectorArray.append( vectorElement );
        Grid_x = (*mem_buffer++).x;
        Grid_y = (*mem_buffer).y;
        Grid_z = (*mem_buffer).z;
	}
	diLOG_MSTATUS( mGrid.set(vectorArray) );
	gridHandle.setClean();

	//maxHandle.set( max_x, max_y, max_z );
	float3& mMax = maxHandle.asFloat3();
	mMax[0] = max_x;
	mMax[1] = max_y;
	mMax[2] = max_z;
	maxHandle.setClean();
	//minHandle.set( min_x, min_y, min_z );
	float3& mMin = minHandle.asFloat3();
	mMin[0] = min_x;
	mMin[1] = min_y;
	mMin[2] = min_z;
	minHandle.setClean();

	// Close the ids.
	//
	diLOG_HDF5STAT( H5Tclose(v3_type_id) );
	diLOG_HDF5STAT( H5Sclose(space_id) );
	diLOG_HDF5STAT( H5Dclose(dataset_id) );
	diLOG_HDF5STAT( H5Fclose(file_id) );

	//For some reason, freeing the mem_buffer causes a crash, maybe because it's on the heap (?)
	//free( (void *)mem_buffer );

	//HDF5common::logEntry( (char*) "<-- HDF5spatialGrid::computeKernel()" );
	return MS::kSuccess;
}


/* ==========================================================================
// HDF5spatialGrid()
*/
HDF5spatialGrid::HDF5spatialGrid()
{
}


/* ==========================================================================
// postConstructor()
*/
void HDF5spatialGrid::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType HDF5spatialGrid::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// ~HDF5spatialGrid()
*/
HDF5spatialGrid::~HDF5spatialGrid()
{
}


/* ==========================================================================
// creator()
*/
void *HDF5spatialGrid::creator()
{
	return new HDF5spatialGrid();
}


/* ==========================================================================
// initialize()
*/
MStatus HDF5spatialGrid::initialize()
{
	MStatus				 stat, stat2;
	MFnTypedAttribute	 typedAttr;
	MFnNumericAttribute	 numericAttr;
	MFnCompoundAttribute compAttr;
	MFnStringData		 stringData;
	MFnVectorArrayData	 vectorArrayData;

	HDF5common::logEntry( (char*) "--> HDF5spatialGrid::initialize()" );


    filename = typedAttr.create( "filename", "file", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

    grid = typedAttr.create( "grid", "grid", MFnData::kVectorArray, vectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

	dims = numericAttr.create( "dimensions", "dims", MFnNumericData::k3Int, 1, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( numericAttr.setDefault(1, 1, 1) );

    max = numericAttr.create( "maximums", "max", MFnNumericData::k3Float, 1, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( numericAttr.setDefault(0.0, 0.0, 0.0) );

    min = numericAttr.create( "minimums", "min", MFnNumericData::k3Float, 1, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( numericAttr.setDefault(0.0, 0.0, 0.0) );

	diLOG_MSTATUS( addAttribute(filename) );
	diLOG_MSTATUS( addAttribute(grid) );
	diLOG_MSTATUS( addAttribute(dims) );
	diLOG_MSTATUS( addAttribute(max) );
	diLOG_MSTATUS( addAttribute(min) );

	diLOG_MSTATUS( attributeAffects(filename, grid) );
	diLOG_MSTATUS( attributeAffects(filename, dims) );
	diLOG_MSTATUS( attributeAffects(filename, max) );
	diLOG_MSTATUS( attributeAffects(filename, min) );

	HDF5common::logEntry( (char*) "--> HDF5spatialGrid::initialize()" );
	return MS::kSuccess;
}
