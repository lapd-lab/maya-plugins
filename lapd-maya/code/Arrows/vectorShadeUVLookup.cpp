/* ==========================================================================
// Arrows : vectorShadeUVLookup.cpp
//-
// ==========================================================================
// Copyright (C) 2008 Jim Bamber
//
// ==========================================================================
//+
*/
#include "vectorShadeUVLookup.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "arrowsCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
//#include <maya/MFnPlugin.h> -- this turns the piece of code into the plug-in main
#include <maya/MFnVectorArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>


// 2012-03-03 JB: commented this out as part of the Microsoft "safe" functions nonsense
//#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1  // causes functions like strcpy() to be replaced with more secure versions

// Static data
MTypeId vectorShadeUVLookup::id( 0x00651 );

// Attributes
MObject vectorShadeUVLookup::aUVCoord;
MObject vectorShadeUVLookup::aCoordToUse;
MObject vectorShadeUVLookup::aVectorShadingTable;
 
MObject vectorShadeUVLookup::aVectorShade;


/* ==========================================================================
// lookup()
*/
MFloatVector vectorShadeUVLookup::lookup(
	float2& uvCoord,
	int coordToUse,
	MVectorArray vectorShadingTable )
{
	//arrowsCommon::logEntry( (char*) "--> vectorShadeUVLookup::lookup()" );
	MFloatVector resultVector( 0.0, 0.0, 0.0 );

	int nVectors = vectorShadingTable.length();
	if ( nVectors > 0 )
	{
		if ( coordToUse < 0 ) coordToUse = 0;
		if ( coordToUse > 1 ) coordToUse = 1;
		float value = uvCoord[coordToUse];
		int index = (int)floor( value * (float)(nVectors-1) );
		if ( index > (nVectors-1) ) index = nVectors - 1;
		if ( index < 0 ) index = 0;
		MVector vectorElement( vectorShadingTable[index] );
		resultVector = MFloatVector( vectorElement );
	}

	//arrowsCommon::logEntry( (char*) "<-- vectorShadeUVLookup::lookup()" );
	return resultVector;
}


/* ==========================================================================
// compute()
//
// This function gets called by Maya to evaluate the texture.
//
*/
MStatus vectorShadeUVLookup::compute(const MPlug& plug, MDataBlock& block) 
{
	arrowsCommon::logEntry( arrowsCommon::eVectorShadeUVLookup );
	MStatus stat;

	// looking for vectorShade
    if ( (plug != aVectorShade) && (plug.parent() != aVectorShade) )
	{
		arrowsCommon::logEntry(
			(char*) "Abnormal exit from vectorShadeUVLookup::compute(), Maya not asking for vector shade" );
		return MS::kUnknownParameter;
	}

	MDataHandle hUVCoord( block.inputValue(aUVCoord, &stat) );					      diLOG_MSTATUS( stat );
	MDataHandle hCoordToUse( block.inputValue(aCoordToUse, &stat) );				  diLOG_MSTATUS( stat );
	MDataHandle hVectorShadingTable( block.inputValue(aVectorShadingTable, &stat) );  diLOG_MSTATUS( stat );

	float2& uvCoord = hUVCoord.asFloat2();
	//float2 uvCoord;
	//uvCoord[0] = 0.0;
	//uvCoord[1] = 0.0;
	int coordToUse = hCoordToUse.asInt();
	//int coordToUse = 1;
	MFnVectorArrayData fnVectorShadingTable( hVectorShadingTable.data() );
	//cout << "MVectorArray vectorShadingTable( fnVectorShadingTable.array() )";
	MVectorArray vectorShadingTable( fnVectorShadingTable.array() );
	//cout << " --> succeeded" << endl;
	//MVectorArray vectorShadingTable;
	//for ( int i=0; i<10; i++ )
	//{
	//	vectorShadingTable.append( MVector(0.2, 0.2, (float)i/10.0) );
	//}

	//arrowsCommon::logEntry( (char*) "About to do vector shade lookup" );
	MFloatVector resultVectorShade;
	resultVectorShade = lookup(	uvCoord, coordToUse, vectorShadingTable );

	// Set output color attribute
	MDataHandle hVectorShade( block.outputValue(aVectorShade) );
//	MFloatVector& vectorShade( hVectorShade.asFloatVector() );
//	vectorShade = resultVectorShade;
	hVectorShade.asFloatVector() = resultVectorShade;
	hVectorShade.setClean();
	block.setClean( plug );

	//arrowsCommon::logEntry( (char*) "<-- vectorShadeUVLookup::compute()" );
    return MS::kSuccess;
}


/* ==========================================================================
// initialize()
//
// initializes attribute information
*/
MStatus vectorShadeUVLookup::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData vectorArrayData;		
	MStatus stat, stat2;

	arrowsCommon::logEntry( (char*) "--> vectorShadeUVLookup::initialize()" );


	// Create input attributes:
	//
	// aUVCoord: an implicit shading network attribute
    MObject child1( nAttr.create("uCoord", "u", MFnNumericData::kFloat) );
    MObject child2( nAttr.create("vCoord", "v", MFnNumericData::kFloat) );
    aUVCoord = nAttr.create( "uvCoord", "uv", child1, child2);
    MAKE_INPUT( nAttr );
    //diLOG_MSTATUS( nAttr.setHidden(true) );

	// aCoordToUse
	aCoordToUse = enumAttr.create( "coordToUse", "coordToUse", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("U", 0) );
	diLOG_MSTATUS( enumAttr.addField("V", 1) );
	MAKE_INPUT( enumAttr );

	// aVectorShadingTable
	aVectorShadingTable = tAttr.create(
	  "vectorShadingTable", "vectorShadingTable", MFnData::kVectorArray, vectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );


    // Create output attributes
	//
	// aVectorShade
    aVectorShade = nAttr.createColor( "vectorShade", "vectorShade", &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( nAttr );


    // Add the attributes
	//
    diLOG_MSTATUS( addAttribute(aUVCoord) );
    diLOG_MSTATUS( addAttribute(aCoordToUse) );
    diLOG_MSTATUS( addAttribute(aVectorShadingTable) );

    diLOG_MSTATUS( addAttribute(aVectorShade) );


    // All inputs affect the output shade
	//
    diLOG_MSTATUS( attributeAffects(aUVCoord, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aCoordToUse, aVectorShade) );
    diLOG_MSTATUS( attributeAffects(aVectorShadingTable, aVectorShade) );

	arrowsCommon::logEntry( (char*) "<-- vectorShadeUVLookup::initialize()" );
    return MS::kSuccess;
}


/* ==========================================================================
// postConstructor()
*/
void vectorShadeUVLookup::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType vectorShadeUVLookup::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// vectorShadeUVLookup()
*/
vectorShadeUVLookup::vectorShadeUVLookup()
{
}


/* ==========================================================================
// ~vectorShadeUVLookup()
*/
vectorShadeUVLookup::~vectorShadeUVLookup()
{
}


/* ==========================================================================
// creator()
//
// creates an instance of the node
*/
void * vectorShadeUVLookup::creator()
{
    return new vectorShadeUVLookup();
}


