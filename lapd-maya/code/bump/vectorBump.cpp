/* ============================================================================
// bump : vectorBump.cpp
//-
// ==========================================================================
// Copyright (C) 2007 Jim Bamber
//
// ==========================================================================
//+
*/
#include "vectorBump.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "bumpCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>


// Static data
MTypeId vectorBump::id( 0x00500 );

// Attributes
MObject vectorBump::aInputMesh;
MObject vectorBump::aWorldMatrix;
MObject vectorBump::aBumpScale;
MObject vectorBump::aDims;
MObject vectorBump::aGridMin;
MObject vectorBump::aGridMax;
MObject vectorBump::aVectorDataGrid;

MObject vectorBump::aOutputMesh;


/* ============================================================================
 * postConstructor()
 */
void vectorBump::postConstructor( )
{
}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType vectorBump::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
 * vectorBump()
 */
vectorBump::vectorBump()
{
}


/* ============================================================================
 * ~vectorBump()
 */
vectorBump::~vectorBump()
{
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * vectorBump::creator()
{
    return new vectorBump();
}


/* ============================================================================
// initialize()
//
// initializes attribute information
*/
MStatus vectorBump::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	bumpCommon::logEntry( (char*) "--> vectorBump::initialize()" );


	// Create input attributes:
	//
	// input mesh
	aInputMesh = tAttr.create( "inputMesh", "inMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aInputMesh) );

	// world matrix
	aWorldMatrix = mAttr.create( "worldMatrix", "wm", MFnMatrixAttribute::kDouble, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aWorldMatrix) );

	// bump scale
	aBumpScale = nAttr.create( "bumpScale", "scale", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aBumpScale) );

	// grid dimensions
	aDims = nAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// vector data grid
    aVectorDataGrid = tAttr.create( "vectorDataGrid", "vData", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aVectorDataGrid) );


	// Create output attributes
	//
	// output mesh
	aOutputMesh = tAttr.create( "outputMesh", "outMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aOutputMesh) );


	// All inputs affect all outputs
	//
	// output mesh
    diLOG_MSTATUS( attributeAffects(aInputMesh, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aWorldMatrix, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aBumpScale, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDims, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aVectorDataGrid, aOutputMesh) );

	bumpCommon::logEntry( (char*) "<-- vectorBump::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
 * calculateBump()
 */
MPointArray vectorBump::calculateBump(
	MPointArray& inMeshPoints,
	MMatrix& worldMatrix,
	double bumpScale,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MVectorArray& vectorDataGrid )
{
	MPoint point, newPoint, worldPoint;
	MVector vector;
	MPointArray outMeshPoints;
	double halfWindow = 2.0;

	int pointCount = inMeshPoints.length();
	for ( int i = 0; i < pointCount; i++ )
	{
		point = inMeshPoints[i];
		worldPoint = point * worldMatrix;
		vector = lapdCommon::interpolateVector( worldPoint, dims, gridMax, gridMin, vectorDataGrid, halfWindow );
		vector = bumpScale * vector;
		newPoint = point + MPoint( vector );
		outMeshPoints.append( newPoint );
	}

	return outMeshPoints;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the curves.
//
*/
MStatus vectorBump::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//bumpCommon::logEntry( "--> vectorBump::compute()" );
	bumpCommon::logEntry( bumpCommon::eVectorBump );

	if ( (plug != aOutputMesh) )
	{
		bumpCommon::logEntry( (char*) "Abnormal exit from vectorBump::compute(), Maya not asking for output mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hInputMesh( block.inputValue(aInputMesh, &stat) );   diLOG_MSTATUS( stat );
	MObject inMesh( hInputMesh.asMeshTransformed() );
	MFnMesh fnInputMesh( inMesh );

	MDataHandle hWorldMatrix( block.inputValue(aWorldMatrix, &stat) );   diLOG_MSTATUS( stat );
	MMatrix worldMatrix = hWorldMatrix.asMatrix();

	MDataHandle hBumpScale( block.inputValue(aBumpScale, &stat) );   diLOG_MSTATUS( stat );
	double bumpScale = hBumpScale.asDouble();

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hVectorDataGrid( block.inputValue(aVectorDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnVectorDataGrid( hVectorDataGrid.data() );
	MVectorArray vectorDataGrid( fnVectorDataGrid.array() );


	// Now do the output attributes.
	//
	MDataHandle hOutputMesh( block.outputValue(aOutputMesh, &stat) );   diLOG_MSTATUS( stat );


	// Get the input mesh points and calculate output mesh points.
	MPointArray inMeshPoints;
	diLOG_MSTATUS( fnInputMesh.getPoints(inMeshPoints, MSpace::kWorld) );

	//bumpCommon::logEntry( "About to calculate vector bump" );
	MPointArray outMeshPoints;
	outMeshPoints =
		calculateBump( inMeshPoints, worldMatrix, bumpScale, dims, gridMin, gridMax, vectorDataGrid );


	// Create the output mesh.
	MFnMeshData dataCreator;
	MObject newOutputData( dataCreator.create(&stat) );   diLOG_MSTATUS( stat );

	int numVertices = outMeshPoints.length();
	int numPolygons = fnInputMesh.numPolygons( &stat );   diLOG_MSTATUS( stat );
	MIntArray polygonCounts;
	MIntArray polygonConnects;
	diLOG_MSTATUS( fnInputMesh.getVertices(polygonCounts, polygonConnects) );

	MFnMesh fnMesh;
	MObject newMesh;
	newMesh = fnMesh.create(
		numVertices, numPolygons, outMeshPoints, polygonCounts, polygonConnects, newOutputData, &stat );   diLOG_MSTATUS( stat );


	// Set the outputs as clean.
	hOutputMesh.set( newOutputData );
	block.setClean( plug );

	//bumpCommon::logEntry( "<-- vectorBump::compute()" );
	return stat;
}
