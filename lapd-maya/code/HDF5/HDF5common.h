/* ==========================================================================
// HDF5 : HDF5common.h
//
// Copyright (C) 2011 Jim Bamber 
//
*/
#ifndef HDF5COMMON_H
#define HDF5COMMON_H

#include <maya/MVectorArray.h>
#include <maya/MPoint.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>

#include "../lapdCommon/defines.h"


class HDF5common
{
    public:

	enum nodeNameEnum
	{
		eOther,
		eHDF5file,
		eHDF5scalarDataGrid,
		eHDF5spatialGrid,
		eHDF5textureFile,
		eHDF5vectorDataGrid,
		eHDF5vectorShadingTable
	};

	static void logEntry(
		char *logString );

	static void logEntry(
		nodeNameEnum nodeName );

	static char logString[DI_LOG_STRING_LENGTH];

	private:

	static char logFilename[DI_LOG_FILENAME_LENGTH];
	static FILE* logFile;
    static int currentDay;
    static time_t currentRawTime;
	static int nodeCount;             // must match the number of entries in the nodeNameEnum
	static int callCounters[7];       // ""
	static char* nodeNameStrings[7];  // ""
	static int logTimestep;

};


#endif

