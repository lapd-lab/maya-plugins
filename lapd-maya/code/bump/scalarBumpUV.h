/* ============================================================================
// bump : scalarBumpUV.h
//
// Copyright (C) 2013 Jim Bamber 
//
*/
#ifndef SCALARBUMPUV_H
#define SCALARBUMPUV_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MDoubleArray.h>
#include <maya/MFloatVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MColorArray.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnMesh.h>


class scalarBumpUV : public MPxNode
{
    public:

    scalarBumpUV();
	virtual void postConstructor();
    virtual ~scalarBumpUV();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	static int uvIdCount;

    static MStatus calculateBump(
		MPointArray& inVertices,		/* | */
		int3& dims,						/* | */
		float3& gridMin,				/* | */
		float3& gridMax,				/* | */
		double dataMin,					/* | */
		double dataMax,					/* | */
		MDoubleArray& scalarDataGrid,	/* `-> argument group 1: used to interpolate and scale the scalar datum */
		/*.................*/
		MVector& bumpNormal,			/* | */
		double bumpScale,				/* | */
		short centering,				/* | */
		double centerValue,				/* | */
		MPointArray& outVertices,		/* `-> argument group 2: used to calculate the bump offset */
		/*.................*/
		MIntArray& uvIdArray );			/* `-> argument group 3: used to calculate the color value */

	static MStatus setUVs(
		MFnMesh& fnMesh );

	static MStatus assignUVs(
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices,
		MIntArray& uvIdArray,
		MFnMesh& fnMesh );


	// Input attributes
	static MObject aInputMesh;			// The base object to deform.
	static MObject aInputMeshNormal;	// The initial normal for the input mesh (float3).
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aDataMin;			// Minimum data value in data grid.
	static MObject aDataMax;			// Maximum data value in data grid.
	static MObject aScalarDataGrid;		// This supplies scalar values which are used to bump the surface.
										// in the normal direction.
	static MObject aBumpRotate;			// The bump normal is rotated by these angles from the y-axis (float3).
	static MObject aBumpRotateOrder;	// The order of rotation (enum: xyz, yzx, zxy, xzy, yxz, zyx).
	static MObject aBumpScale;			// A multiplying factor to control bump height.
	static MObject aCentering;			// Dropdown for centering type (none, on average, manual).
	static MObject aCenterValue;		// Bump about this center value (manual mode only).

	// Output attributes
	static MObject aOutputMesh;			// The deformed object.
};


#endif

