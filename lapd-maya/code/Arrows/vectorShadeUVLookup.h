/* ==========================================================================
// Arrows : vectorShadeUVLookup.h
//
// Copyright (C) 2008 Jim Bamber 
//
*/
#ifndef VECTORSHADEUVLOOKUP_H
#define VECTORSHADEUVLOOKUP_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnVectorArrayData.h>


class vectorShadeUVLookup : public MPxNode
{
    public:

    vectorShadeUVLookup();
	virtual void postConstructor();
    virtual ~vectorShadeUVLookup();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MFloatVector lookup(
		float2& uvCoord,
		int coordToUse,
		MVectorArray vectorTable );

	// Input attributes
	static MObject aUVCoord;				// The UV value of the current triangle(?), point(?) being shaded (float2)
	static MObject aCoordToUse;				// Which of the two to use (U or V) (enum)
	static MObject aVectorShadingTable;		// Color or transparency lookup table (MVectorArray)

	// Output attributes
    static MObject aVectorShade;			// Output color or transparency (MFloatVector)
};


#endif

