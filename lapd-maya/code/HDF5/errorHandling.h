/* ==========================================================================
// HDF5 : errorHandling.h
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#ifndef ERRORHANDLING_H
#define ERRORHANDLING_H

//#define STATCHECK( stat, string ) \
//	if ( !stat ) \
//	{ \
//		MGlobal::displayError( string ); \
//		return MS::kFailure; \
//	}

#define BOOLCHECK( x )								\
{													\
	bool _bool_stat = ( x );						\
	if ( !_bool_stat )								\
	{												\
		cerr << "\nError detected in " << __FILE__	\
			 <<	" at line "	<< __LINE__ << endl;	\
	}												\
}

#define HDF5CHECK( x )									\
{														\
	int _hdf5_stat = ( x );								\
	if ( _hdf5_stat < 0 )								\
	{													\
		cerr << "\nHDF5 error detected in " << __FILE__	\
			 <<	" at line "	<< __LINE__ << endl;		\
	}													\
}

#endif
