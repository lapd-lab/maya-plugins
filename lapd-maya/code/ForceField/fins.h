/* ============================================================================
// forceField : fins.h
//
// Copyright (C) 2008 Jim Bamber 
//
*/
#ifndef FINS_H
#define FINS_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class fins : public MPxNode
{
    public:

    fins();
	virtual void postConstructor();
    virtual ~fins();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MStatus createMesh(
		int curveCount,
		MIntArray startIndices,
		MIntArray CVCounts,
		MPointArray controlVertexList,
		MVectorArray tangentList,
		bool reverseTangents,
		double tubeRadius,
		double startDegrees,
		int finCount,
		short UVMapping,
		int repetitionWidth,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MDoubleArray& scalarDataGrid,
		double multiplier,
		MObject& newOutputData );

	static MStatus addFin(
		//MFnMesh& fnMesh,
		//MObject& newOutputData,
		MPointArray curveVertices,
		MVectorArray curveTangents,
		MDoubleArray scalarData,
		double tubeRadius,
		double finDegrees,
		MPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );

	static MStatus assignUVs(
		short UVMapping,
		int repetitionWidth,
		MIntArray CVCounts,
		int finCount,
		MFnMesh& fnMesh,
		MObject& newOutputData );

	// Input attributes
	static MObject aCurveCount;			// Number of curves in the output array.
	static MObject aStartIndices;		// Used to index into the control vertex and tangent arrays (int array).
	static MObject aCVCounts;			// Number of CV's for each curve (int array), for example:
										//   index = startIndices[curveNum] + cvIndex, where cvIndex = {0:CVCounts[curveNum]-1}
	static MObject aControlVertexList;	// Control vertices for curves (point array).
	static MObject aTangentList;		// Tangents for curves (vector array).
	static MObject aReverseTangents;	// Specifies whether to multiply tangents by -1 (boolean).
	static MObject aTubeRadius;			// Radius of field line tubes in cm (double).
	static MObject aStartDegrees;		// First fin placed this many degrees around the field line tube (double).
	static MObject aFinCount;			// Number of fins around the tube (int).
	static MObject aUVMapping;			// An enum with values {0=Cartesian, 1=radial}.  This controls how UV values are assigned.
	static MObject aRepetitionWidth;	// Number of CV's contained within a single UV repetition.

										// Spatial grid is assumed to be uniform => is not used as an input.
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aScalarDataGrid;		// This supplies the scalars for the secondary mesh (scalar array).
	static MObject aMultiplier;			// A convenience input to scale up the secondary data (double).

	// Output attributes
	static MObject aSecondaryMesh;		// The newly created mesh.
};


#endif

