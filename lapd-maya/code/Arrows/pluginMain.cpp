/* ==========================================================================
// arrows : pluginMain.cpp
//
// Copyright (C) 2008 Jim Bamber 
// 
*/
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "arrows.h"
#include "arrowsUV.h"
#include "vectorShadeUVLookup.h"
#include "vectorShadeUVLookup2.h"
#include <maya/MFnPlugin.h>
#include <maya/MPxNode.h>
#include <maya/MGlobal.h>



// 2012-03-03 JB: commented this out as part of the Microsoft "safe" functions nonsense
//#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1  // causes functions like strcpy() to be replaced with more secure versions


/* ==========================================================================
// initializePlugin()
*/
MStatus initializePlugin( MObject obj )
{
    //const MString UserClassify( "texture/2d" );

	MFnPlugin plugin( obj, "Jim Bamber", "1.0", "Any");
	MGlobal::displayInfo( " " );
	MGlobal::displayInfo( "plugin: Arrows  " + lapdCommon::version );
	MGlobal::displayInfo( "============================" );

	MGlobal::displayInfo( "plugin.registerNode(arrows)" );
	diLOG_MSTATUS ( plugin.registerNode( "arrows", arrows::id, 
                         &arrows::creator, &arrows::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(arrowsUV)" );
	diLOG_MSTATUS ( plugin.registerNode( "arrowsUV", arrowsUV::id, 
                         &arrowsUV::creator, &arrowsUV::initialize ) );

	//diLOG_MSTATUS ( plugin.registerNode( "vectorShadeUVLookup", vectorShadeUVLookup::id, 
    //                     &vectorShadeUVLookup::creator, &vectorShadeUVLookup::initialize,
    //                     MPxNode::kDependNode, &UserClassify ) );

	MGlobal::displayInfo( "plugin.registerNode(vectorShadeUVLookup)" );
	diLOG_MSTATUS ( plugin.registerNode( "vectorShadeUVLookup", vectorShadeUVLookup::id, 
                         &vectorShadeUVLookup::creator, &vectorShadeUVLookup::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(vectorShadeUVLookup2)" );
	diLOG_MSTATUS ( plugin.registerNode( "vectorShadeUVLookup2", vectorShadeUVLookup2::id, 
                         &vectorShadeUVLookup2::creator, &vectorShadeUVLookup2::initialize ) );

    return MS::kSuccess;
}


/* ==========================================================================
// uninitializePlugin()
*/
MStatus uninitializePlugin( MObject obj )
{
    MFnPlugin plugin( obj );
	diLOG_MSTATUS ( plugin.deregisterNode( arrows::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( arrowsUV::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( vectorShadeUVLookup::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( vectorShadeUVLookup2::id ) );

    return MS::kSuccess;
}


