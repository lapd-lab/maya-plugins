/* ==========================================================================
// Texture 3D : defaultVectorShadingTable.h
//
// Copyright (C) 2006 Jim Bamber 
//
*/
#ifndef DEFAULTVECTORSHADINGTABLE_H
#define DEFAULTVECTORSHADINGTABLE_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>


class defaultVectorShadingTable : public MPxNode
{
    public:

    defaultVectorShadingTable();
	virtual void    postConstructor();
    virtual ~defaultVectorShadingTable();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

    static  void *  creator();
    static  MStatus initialize();

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	static MVectorArray calculateTable(
		int mVectorCount,
		int mStartIndex,
		int mEndIndex,
		MVector& mStartValue,
		MVector& mEndValue );

	// Input attributes
	static MObject vectorCount;
	static MObject startIndex;
	static MObject endIndex;
	static MObject startValue;
	static MObject endValue;

	// Output attributes
    static MObject vectorShadingTable;
};


#endif

