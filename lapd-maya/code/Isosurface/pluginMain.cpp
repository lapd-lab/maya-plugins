/* ============================================================================
// Isosurface : pluginMain.cpp
//
// Copyright (C) 2008 Jim Bamber 
// 
*/
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "isoSurface.h"
#include "resample.h"
#include "isoSurfaceUV.h"
#include <maya/MFnPlugin.h>
#include <maya/MPxNode.h>
#include <maya/MGlobal.h>


/* ============================================================================
 * initializePlugin()
 */
MStatus initializePlugin( MObject obj )
{
    MFnPlugin plugin( obj, "Jim Bamber", "1.0", "Any");
	MGlobal::displayInfo( " " );
	MGlobal::displayInfo( "plugin: Isosurface  " + lapdCommon::version );
	MGlobal::displayInfo( "================================" );

	MGlobal::displayInfo( "plugin.registerNode(isoSurface)" );
	diLOG_MSTATUS ( plugin.registerNode( "isoSurface", isoSurface::id, 
                         &isoSurface::creator, &isoSurface::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(resample)" );
	diLOG_MSTATUS ( plugin.registerNode( "resample", resample::id, 
                         &resample::creator, &resample::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(isoSurfaceUV)" );
	diLOG_MSTATUS ( plugin.registerNode( "isoSurfaceUV", isoSurfaceUV::id, 
                         &isoSurfaceUV::creator, &isoSurfaceUV::initialize ) );

    return MS::kSuccess;
}


/* ============================================================================
 * uninitializePlugin()
 */
MStatus uninitializePlugin( MObject obj )
{
    MFnPlugin plugin( obj );
	diLOG_MSTATUS ( plugin.deregisterNode( isoSurface::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( resample::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( isoSurfaceUV::id ) );

    return MS::kSuccess;
}


