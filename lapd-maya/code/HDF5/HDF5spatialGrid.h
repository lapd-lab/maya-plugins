/* ==========================================================================
// HDF5 : HDF5spatialGrid.h
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#ifndef HDF5SPATIALGRID_H
#define HDF5SPATIALGRID_H

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class HDF5spatialGrid : public MPxNode
{
public:
    MPxNode::SchedulingType schedulingType() const;

	virtual MStatus compute( const MPlug& plug, MDataBlock& data );

    HDF5spatialGrid();
	virtual void postConstructor();
    virtual ~HDF5spatialGrid();

	static  void *creator();
	static  MStatus initialize();

public:
	static MObject filename;
	static MObject grid;
	static MObject dims;
	static MObject max;
	static MObject min;

	static	MTypeId	id;

private:
	MStatus computeKernel( const MPlug& plug, MDataBlock& data );
};

#endif
