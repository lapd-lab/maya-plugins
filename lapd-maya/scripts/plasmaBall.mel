// Script to add "fur" to a field lines scene.
// -------------------------------------------
//
// This uses the squares node.
//


// Parameter section.  Adjust these as necessary.
//
float $tubeRadius = 0.4;    // The tube radius is set in the field lines UI window.  Units are cm.
int $randomize = 1;         // Boolean flag to determine whether squares are rotated randomly about the field line axis.  0 = no, 1 = yes.
int $startCVFwd = 0;        // Index of control vertex where first square will be placed on the forward curves.
int $startCVBak = 0;        // Index of control vertex where first square will be placed on the backward curves.
int $stepCV = 1;            // Squares will be placed at CV's with this step distance.  Units are number of CV's.
float $UVScale = -1.0;      // Negative UV scale means to fit the image to the size of the square.  I recommend not changing this.
float $multiplier = 500.0;  // If your input data is not in cm, use this multiplier to correct.
int $furFrameOffset = 0;    // Used to animate the timestep.  It must match the one in the field lines UI window.
float $glow = 0.4;          // Glow intensity of the "fur" in the rendered image.  Ranges from 0.0 to 1.0.                      
string $furFilePath =
  "/Users/vincena/data/maya/mb02xz_Gfug_3dvphase2_J.hdf5";  // HDF file path for secondary data (scalar).

string $furDataGroup = "B amplitudes";  // Data group within the HDF secondary data file.

string $furColorPath =
  "/Users/vincena/Documents/maya/scripts/fur_images/Plasma ball 02 color.JPG";  // File path for image to control the color.

string $furTransparencyPath =
  "/Users/vincena/Documents/maya/scripts/fur_images/Plasma ball 02 transparency.JPG";  // File path for image to control the transparency.



// Create the transforms and the meshes (these are receptacles to hold the generated meshes).
//
createNode transform -n furSquaresFwdMesh1;
createNode mesh -n furSquaresFwdShape1 -p furSquaresFwdMesh1;
createNode transform -n furSquaresBakMesh1;
createNode mesh -n furSquaresBakShape1 -p furSquaresBakMesh1;


// Create the shading nodes (these are used to apply the images to the squares).
//
shadingNode -asShader blinn -name furBlinn;
setAttr "furBlinn.reflectivity" 0;
setAttr "furBlinn.specularColor" -type double3 0 0 0 ;
setAttr "furBlinn.glowIntensity" $glow;
sets -renderable true -noSurfaceShader true -empty -name furBlinnSG;
connectAttr -f furBlinn.outColor furBlinnSG.surfaceShader;
connectAttr -f furSquaresFwdShape1.instObjGroups[0] furBlinnSG.dagSetMembers[0];
connectAttr -f furSquaresBakShape1.instObjGroups[0] furBlinnSG.dagSetMembers[1];


// Set up the forward and backward "squares" nodes.
//
// Forward
createNode squares -name furSquaresFwd1;
connectAttr flCurves.curveCount furSquaresFwd1.curveCount;
connectAttr flCurves.startIndices furSquaresFwd1.startIndices;
connectAttr flCurves.CVCounts furSquaresFwd1.CVCounts;
connectAttr flCurves.controlVertexList furSquaresFwd1.controlVertexList;
connectAttr flCurves.tangentList furSquaresFwd1.tangentList;
setAttr furSquaresFwd1.tubeRadius $tubeRadius;
setAttr furSquaresFwd1.randomize $randomize;
setAttr furSquaresFwd1.start $startCVFwd;
setAttr furSquaresFwd1.step $stepCV;
setAttr furSquaresFwd1.UVScale $UVScale;
connectAttr flSpatialGrid.dims furSquaresFwd1.dims;
connectAttr flSpatialGrid.min furSquaresFwd1.gridMin;
connectAttr flSpatialGrid.max furSquaresFwd1.gridMax;

createNode HDF5scalarDataGrid -name furDataGrid;
setAttr furDataGrid.filename -type "string" $furFilePath;
setAttr furDataGrid.group -type "string" $furDataGroup;
string $furExpression = "furDataGrid.timeIndex = frame + " + $furFrameOffset + ";";
expression -s $furExpression -o furDataGrid -ae 1 -uc all -name furFrameExpression;
connectAttr furDataGrid.scalarDataGrid furSquaresFwd1.scalarDataGrid;
setAttr furSquaresFwd1.multiplier $multiplier;

connectAttr furSquaresFwd1.secondaryMesh furSquaresFwdShape1.inMesh;

// Backward
createNode squares -name furSquaresBak1;
connectAttr flBackCurves.curveCount furSquaresBak1.curveCount;
connectAttr flBackCurves.startIndices furSquaresBak1.startIndices;
connectAttr flBackCurves.CVCounts furSquaresBak1.CVCounts;
connectAttr flBackCurves.controlVertexList furSquaresBak1.controlVertexList;
connectAttr flBackCurves.tangentList furSquaresBak1.tangentList;
setAttr furSquaresBak1.tubeRadius $tubeRadius;
setAttr furSquaresBak1.randomize $randomize;
setAttr furSquaresBak1.start $startCVBak;
setAttr furSquaresBak1.step $stepCV;
setAttr furSquaresBak1.UVScale $UVScale;
connectAttr flSpatialGrid.dims furSquaresBak1.dims;
connectAttr flSpatialGrid.min furSquaresBak1.gridMin;
connectAttr flSpatialGrid.max furSquaresBak1.gridMax;

connectAttr furDataGrid.scalarDataGrid furSquaresBak1.scalarDataGrid;
setAttr furSquaresBak1.multiplier $multiplier;

connectAttr furSquaresBak1.secondaryMesh furSquaresBakShape1.inMesh;


// Connect up the image files.
//
// Color
shadingNode -asTexture file -name furColorFile;
shadingNode -asUtility place2dTexture -name place2dFurColor;
connectAttr -f place2dFurColor.coverage furColorFile.coverage;
connectAttr -f place2dFurColor.translateFrame furColorFile.translateFrame;
connectAttr -f place2dFurColor.rotateFrame furColorFile.rotateFrame;
connectAttr -f place2dFurColor.mirrorU furColorFile.mirrorU;
connectAttr -f place2dFurColor.mirrorV furColorFile.mirrorV;
connectAttr -f place2dFurColor.stagger furColorFile.stagger;
connectAttr -f place2dFurColor.wrapU furColorFile.wrapU;
connectAttr -f place2dFurColor.wrapV furColorFile.wrapV;
connectAttr -f place2dFurColor.repeatUV furColorFile.repeatUV;
connectAttr -f place2dFurColor.offset furColorFile.offset;
connectAttr -f place2dFurColor.rotateUV furColorFile.rotateUV;
connectAttr -f place2dFurColor.noiseUV furColorFile.noiseUV;
connectAttr -f place2dFurColor.vertexUvOne furColorFile.vertexUvOne;
connectAttr -f place2dFurColor.vertexUvTwo furColorFile.vertexUvTwo;
connectAttr -f place2dFurColor.vertexUvThree furColorFile.vertexUvThree;
connectAttr -f place2dFurColor.vertexCameraOne furColorFile.vertexCameraOne;
connectAttr place2dFurColor.outUV furColorFile.uv;
connectAttr place2dFurColor.outUvFilterSize furColorFile.uvFilterSize;
setAttr furColorFile.fileTextureName -type "string" $furColorPath;
connectAttr -force furColorFile.outColor furBlinn.color;

// Transparency
shadingNode -asTexture file -name furTransparencyFile;
shadingNode -asUtility place2dTexture -name place2dFurTransparency;
connectAttr -f place2dFurTransparency.coverage furTransparencyFile.coverage;
connectAttr -f place2dFurTransparency.translateFrame furTransparencyFile.translateFrame;
connectAttr -f place2dFurTransparency.rotateFrame furTransparencyFile.rotateFrame;
connectAttr -f place2dFurTransparency.mirrorU furTransparencyFile.mirrorU;
connectAttr -f place2dFurTransparency.mirrorV furTransparencyFile.mirrorV;
connectAttr -f place2dFurTransparency.stagger furTransparencyFile.stagger;
connectAttr -f place2dFurTransparency.wrapU furTransparencyFile.wrapU;
connectAttr -f place2dFurTransparency.wrapV furTransparencyFile.wrapV;
connectAttr -f place2dFurTransparency.repeatUV furTransparencyFile.repeatUV;
connectAttr -f place2dFurTransparency.offset furTransparencyFile.offset;
connectAttr -f place2dFurTransparency.rotateUV furTransparencyFile.rotateUV;
connectAttr -f place2dFurTransparency.noiseUV furTransparencyFile.noiseUV;
connectAttr -f place2dFurTransparency.vertexUvOne furTransparencyFile.vertexUvOne;
connectAttr -f place2dFurTransparency.vertexUvTwo furTransparencyFile.vertexUvTwo;
connectAttr -f place2dFurTransparency.vertexUvThree furTransparencyFile.vertexUvThree;
connectAttr -f place2dFurTransparency.vertexCameraOne furTransparencyFile.vertexCameraOne;
connectAttr place2dFurTransparency.outUV furTransparencyFile.uv;
connectAttr place2dFurTransparency.outUvFilterSize furTransparencyFile.uvFilterSize;
setAttr furTransparencyFile.fileTextureName -type "string" $furTransparencyPath;
connectAttr -force furTransparencyFile.outTransparency furBlinn.transparency;


