// Script to add "fur" to a field lines scene.
// -------------------------------------------
//
// This uses the envelopes node.
//


// Parameter section.  Adjust these as necessary.
//
float $tubeRadius = 0.4;    // The tube radius is set in the field lines UI window.  Units are cm.
int $segmentCount = 16;     // Number of straight line segments making up each circle.
float $multiplier = 500.0;  // If your input data is not in cm, use this multiplier to correct.
int $furFrameOffset = 0;    // Used to animate the timestep.  It must match the one in the field lines UI window.
float $glow = 0.4;          // Glow intensity of the "fur" in the rendered image.  Ranges from 0.0 to 1.0.                      
string $furFilePath =
  "C:/UCLA development/maya/Sandbox/HDF5 sandbox/test_2007-23-02.hdf5";  // HDF file path for secondary data (scalar).

string $furDataGroup = "B amplitudes";  // Data group within the HDF secondary data file.


// Create the transforms and the meshes (these are receptacles to hold the generated meshes).
//
createNode transform -n furEnvelopesFwdMesh1;
createNode mesh -n furEnvelopesFwdShape1 -p furEnvelopesFwdMesh1;
createNode transform -n furEnvelopesBakMesh1;
createNode mesh -n furEnvelopesBakShape1 -p furEnvelopesBakMesh1;


// Create the shading nodes (these are used to apply the images to the envelopes).
//
shadingNode -asShader blinn -name furEnvelopesBlinn;
setAttr "furEnvelopesBlinn.reflectivity" 0;
setAttr "furEnvelopesBlinn.specularColor" -type double3 0 0 0 ;
setAttr "furEnvelopesBlinn.glowIntensity" $glow;
setAttr "furEnvelopesBlinn.color" -type double3 0.900588 0.37219 0.91;
setAttr "furEnvelopesBlinn.transparency" -type double3 0.796 0.796 0.796;
sets -renderable true -noSurfaceShader true -empty -name furEnvelopesBlinnSG;
connectAttr -f furEnvelopesBlinn.outColor furEnvelopesBlinnSG.surfaceShader;
connectAttr -f furEnvelopesFwdShape1.instObjGroups[0] furEnvelopesBlinnSG.dagSetMembers[0];
connectAttr -f furEnvelopesBakShape1.instObjGroups[0] furEnvelopesBlinnSG.dagSetMembers[1];


// Set up the forward and backward "envelopes" nodes.
//
// Forward
createNode envelopes -name furEnvelopesFwd1;
connectAttr flCurves.curveCount furEnvelopesFwd1.curveCount;
connectAttr flCurves.startIndices furEnvelopesFwd1.startIndices;
connectAttr flCurves.CVCounts furEnvelopesFwd1.CVCounts;
connectAttr flCurves.controlVertexList furEnvelopesFwd1.controlVertexList;
connectAttr flCurves.tangentList furEnvelopesFwd1.tangentList;
setAttr furEnvelopesFwd1.tubeRadius $tubeRadius;
setAttr furEnvelopesFwd1.segmentCount $segmentCount;
connectAttr flSpatialGrid.dims furEnvelopesFwd1.dims;
connectAttr flSpatialGrid.min furEnvelopesFwd1.gridMin;
connectAttr flSpatialGrid.max furEnvelopesFwd1.gridMax;

createNode HDF5scalarDataGrid -name furDataGrid;
setAttr furDataGrid.filename -type "string" $furFilePath;
setAttr furDataGrid.group -type "string" $furDataGroup;
string $furExpression = "furDataGrid.timeIndex = frame + " + $furFrameOffset + ";";
expression -s $furExpression -o furDataGrid -ae 1 -uc all -name furFrameExpression;
connectAttr furDataGrid.scalarDataGrid furEnvelopesFwd1.scalarDataGrid;
setAttr furEnvelopesFwd1.multiplier $multiplier;

connectAttr furEnvelopesFwd1.secondaryMesh furEnvelopesFwdShape1.inMesh;

// Backward
createNode envelopes -name furEnvelopesBak1;
connectAttr flBackCurves.curveCount furEnvelopesBak1.curveCount;
connectAttr flBackCurves.startIndices furEnvelopesBak1.startIndices;
connectAttr flBackCurves.CVCounts furEnvelopesBak1.CVCounts;
connectAttr flBackCurves.controlVertexList furEnvelopesBak1.controlVertexList;
connectAttr flBackCurves.tangentList furEnvelopesBak1.tangentList;
setAttr furEnvelopesBak1.tubeRadius $tubeRadius;
setAttr furEnvelopesBak1.segmentCount $segmentCount;
connectAttr flSpatialGrid.dims furEnvelopesBak1.dims;
connectAttr flSpatialGrid.min furEnvelopesBak1.gridMin;
connectAttr flSpatialGrid.max furEnvelopesBak1.gridMax;

connectAttr furDataGrid.scalarDataGrid furEnvelopesBak1.scalarDataGrid;
setAttr furEnvelopesBak1.multiplier $multiplier;

connectAttr furEnvelopesBak1.secondaryMesh furEnvelopesBakShape1.inMesh;


// Don't need these...

// Connect up the image files.
//
// Color
shadingNode -asTexture file -name furColorFile;
shadingNode -asUtility place2dTexture -name place2dFurColor;
connectAttr -f place2dFurColor.coverage furColorFile.coverage;
connectAttr -f place2dFurColor.translateFrame furColorFile.translateFrame;
connectAttr -f place2dFurColor.rotateFrame furColorFile.rotateFrame;
connectAttr -f place2dFurColor.mirrorU furColorFile.mirrorU;
connectAttr -f place2dFurColor.mirrorV furColorFile.mirrorV;
connectAttr -f place2dFurColor.stagger furColorFile.stagger;
connectAttr -f place2dFurColor.wrapU furColorFile.wrapU;
connectAttr -f place2dFurColor.wrapV furColorFile.wrapV;
connectAttr -f place2dFurColor.repeatUV furColorFile.repeatUV;
connectAttr -f place2dFurColor.offset furColorFile.offset;
connectAttr -f place2dFurColor.rotateUV furColorFile.rotateUV;
connectAttr -f place2dFurColor.noiseUV furColorFile.noiseUV;
connectAttr -f place2dFurColor.vertexUvOne furColorFile.vertexUvOne;
connectAttr -f place2dFurColor.vertexUvTwo furColorFile.vertexUvTwo;
connectAttr -f place2dFurColor.vertexUvThree furColorFile.vertexUvThree;
connectAttr -f place2dFurColor.vertexCameraOne furColorFile.vertexCameraOne;
connectAttr place2dFurColor.outUV furColorFile.uv;
connectAttr place2dFurColor.outUvFilterSize furColorFile.uvFilterSize;
setAttr furColorFile.fileTextureName -type "string" $furColorPath;
connectAttr -force furColorFile.outColor furEnvelopesBlinn.color;

// Transparency
shadingNode -asTexture file -name furTransparencyFile;
shadingNode -asUtility place2dTexture -name place2dFurTransparency;
connectAttr -f place2dFurTransparency.coverage furTransparencyFile.coverage;
connectAttr -f place2dFurTransparency.translateFrame furTransparencyFile.translateFrame;
connectAttr -f place2dFurTransparency.rotateFrame furTransparencyFile.rotateFrame;
connectAttr -f place2dFurTransparency.mirrorU furTransparencyFile.mirrorU;
connectAttr -f place2dFurTransparency.mirrorV furTransparencyFile.mirrorV;
connectAttr -f place2dFurTransparency.stagger furTransparencyFile.stagger;
connectAttr -f place2dFurTransparency.wrapU furTransparencyFile.wrapU;
connectAttr -f place2dFurTransparency.wrapV furTransparencyFile.wrapV;
connectAttr -f place2dFurTransparency.repeatUV furTransparencyFile.repeatUV;
connectAttr -f place2dFurTransparency.offset furTransparencyFile.offset;
connectAttr -f place2dFurTransparency.rotateUV furTransparencyFile.rotateUV;
connectAttr -f place2dFurTransparency.noiseUV furTransparencyFile.noiseUV;
connectAttr -f place2dFurTransparency.vertexUvOne furTransparencyFile.vertexUvOne;
connectAttr -f place2dFurTransparency.vertexUvTwo furTransparencyFile.vertexUvTwo;
connectAttr -f place2dFurTransparency.vertexUvThree furTransparencyFile.vertexUvThree;
connectAttr -f place2dFurTransparency.vertexCameraOne furTransparencyFile.vertexCameraOne;
connectAttr place2dFurTransparency.outUV furTransparencyFile.uv;
connectAttr place2dFurTransparency.outUvFilterSize furTransparencyFile.uvFilterSize;
setAttr furTransparencyFile.fileTextureName -type "string" $furTransparencyPath;
connectAttr -force furTransparencyFile.outTransparency furEnvelopesBlinn.transparency;


