; 3dv_to_mayahdf5
; As the name suggests, this program will translate
; a Vincena 3DV-format binary datafile into
; The Vincena-Bamber standard HDF5 format designed
; for use with our Autodesk Maya Scientific Data Import Plugin
;
; Note: n-dimensional arrays of vectors are handled
; as n-dimensional arrays of compound datatype in HDF5
; which may be handled by using structures of n-dim arrays in IDL.
;
;Author: Stephen Vincena
;Original draft date: 01/20/2007  
;Adapted to the new specification of Feb 22, 2007
;
;
;
;GOTO,Skipread
;First, read in the 3dv data
inputfile=DIALOG_PICKFILE(PATH='./')
IF (inputfile EQ '') THEN GOTO, Cleanup
;spawn,'rm test.hdf5'
print,'enter time multiplication factor (decimation factor) or "1" if none'
read,dec_factor
dec_factor=float(dec_factor)

openr,in_lun,inputfile,/get_lun
header=fltarr(4)
readu,in_lun,header
print,inputfile,header
l1=long(header(0))
l2=long(header(1))
l3=long(header(2))
l4=long(header(3))
;l4=long(2) ; test using a small dataset
xyz=fltarr(3,l1,l2,l3)
vdata=fltarr(3,l1,l2,l3,l4)
dt=dec_factor*10e-9 ;10ns originally
tvals=findgen(l4)*dt
readu,in_lun,xyz

;;Skip into the action
;fdata = xyz
;for iskip=0,50 do begin
; readu,in_lun,fdata
;endfor


readu,in_lun,vdata
;vdata *= 1e4 ;Tesla to Gauss *we do this earlier *
free_lun,in_lun
sdata = sqrt(total(vdata^2,1))
Skipread:

HDF5_file = OBJ_NEW('HDF5_file')
filepath='./gridtest.hdf5'
HDF5_file->Create,filepath

;Create the 'Attributes' group
attributes_group=HDF5_file->Create_group('Attributes')
attributes_group->Write_attribute,'Version','2007-02-22'
attributes_group->Write_attribute,'Parameter count',long(0)

;Create the spatial grid dataset
xyz_source_struct = {xyz_struct,X:float(0),Y:float(0),Z:float(0)}
xyz_struct_array = replicate(xyz_source_struct,l1,l2,l3)
xyz_struct_array[*,*,*].X = reform(xyz[0,*,*,*])
xyz_struct_array[*,*,*].Y = reform(xyz[1,*,*,*])
xyz_struct_array[*,*,*].Z = reform(xyz[2,*,*,*])
HDF5_file->Write_dataset,'Spatial grid',xyz_struct_array;,CHUNK_DIMENSIONS=[l1,l2,l3];,GZIP=9


vector_group=HDF5_file->Create_group('B vectors')
vector_group->Write_attribute,'Name','Magnetic field vectors'
vector_group->Write_attribute,'Symbol','Bvec'
vector_group->Write_attribute,'Units','Gauss'
vector_group->Write_attribute,'Type','3-vector'
vector_group->Write_dataset,'Timesteps',tvals;,GZIP=9

vdata_source_struct = {vdata_struct,DATA_X:float(0),DATA_Y:float(0),DATA_Z:float(0)}
vdata_struct_array = replicate(vdata_source_struct,l1,l2,l3,l4)
vdata_struct_array[*,*,*,*].DATA_X = reform(vdata[0,*,*,*,*])
vdata_struct_array[*,*,*,*].DATA_Y = reform(vdata[1,*,*,*,*])
vdata_struct_array[*,*,*,*].DATA_Z = reform(vdata[2,*,*,*,*])
vector_group->Write_dataset,'Bvec',vdata_struct_array,CHUNK_DIMENSIONS=[l1,l2,l3,1];,GZIP=9

scalar_group=HDF5_file->Create_group('B amplitudes')
scalar_group->Write_attribute,'Name','Magnetic field amplitude'
scalar_group->Write_attribute,'Symbol','Bmag'
scalar_group->Write_attribute,'Units','Gauss'
scalar_group->Write_attribute,'Type','Scalar'
scalar_group->Write_dataset,'Timesteps',tvals;,GZIP=9
scalar_group->Write_dataset,'Bmag',sdata,CHUNK_DIMENSIONS=[l1,l2,l3,1];,GZIP=9



Cleanup:

OBJ_DESTROY,HDF5_file


end
