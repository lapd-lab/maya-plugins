/* ============================================================================
// forceField : secondaryData.cpp
//-
// ==========================================================================
// Copyright (C) 2012 Jim Bamber
//
// Cloned from C:\Program Files\Autodesk\Maya2011\devkit\plug-ins\lavaShader.h
//
// ==========================================================================
//+
//-
// ==========================================================================
// Copyright 1995,2006,2008 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk
// license agreement provided at the time of installation or download,
// or which otherwise accompanies this software in either electronic
// or hard copy form.
// ==========================================================================
//+
*/

#include "secondaryData.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

/*
// Local functions
float Noise(float, float, float);
void  Noise_init();
static float Omega(int i, int j, int k, float t[3]);
static float omega(float);
static double turbulence(double u,double v,double w,int octaves);

#define PI                  3.14159265358979323846

#ifdef FLOOR
#undef FLOOR
#endif
#define FLOOR(x)            ((int)floorf(x))
#define TABLELEN            512
#define TLD2                256    // TABLELEN 

// Local variables
static int                  Phi[TABLELEN];
static char                 fPhi[TABLELEN];
static float                G[TABLELEN][3];
*/


// Static data
MTypeId secondaryData::id( 0x00465 );

// Attributes

/*
MObject  secondaryData::aColorBase;
MObject  secondaryData::aColorFlame;
MObject  secondaryData::aDeform;
MObject  secondaryData::aWarp;
MObject  secondaryData::aSpeed;
MObject  secondaryData::aTurbulence;
MObject  secondaryData::aPower;
MObject  secondaryData::aFrame;
*/
MObject secondaryData::aDims;				
MObject secondaryData::aGridMin;			
MObject secondaryData::aGridMax;			
MObject secondaryData::aScalarDataGrid;	
MObject secondaryData::aDataMin;			
MObject secondaryData::aDataMax;	

MObject  secondaryData::aPointWorld;
MObject  secondaryData::aPlaceMat;

MObject  secondaryData::aScaledDatum;

//
// DESCRIPTION:
///////////////////////////////////////////////////////
void secondaryData::postConstructor()
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType secondaryData::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


//
// DESCRIPTION:
///////////////////////////////////////////////////////
secondaryData::secondaryData()
{
}

//
// DESCRIPTION:
///////////////////////////////////////////////////////
secondaryData::~secondaryData()
{
}

//
// DESCRIPTION:
///////////////////////////////////////////////////////
void * secondaryData::creator()
{
    return new secondaryData();
}

//
// DESCRIPTION:
///////////////////////////////////////////////////////
MStatus secondaryData::initialize()
{
    MFnMatrixAttribute mAttr;
    MFnNumericAttribute nAttr; 
	MFnTypedAttribute tAttr;
	MFnDoubleArrayData fnDoubleArrayData;	
	MStatus stat, stat2;

	// Input attributes

	/*
    aColorBase = nAttr.createColor("ColorBase", "cb");
    MAKE_INPUT(nAttr);

    aColorFlame = nAttr.createColor("ColorFlame", "cf");
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(1., 1., 1.) );

    aDeform = nAttr.create( "Deformation", "d", MFnNumericData::kLong);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(1) );
    diLOG_MSTATUS (nAttr.setMin(1) );
    diLOG_MSTATUS ( nAttr.setMax(10) );

    aWarp = nAttr.create( "Warp", "w", MFnNumericData::kFloat);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(0.1f) );
    diLOG_MSTATUS ( nAttr.setMin(0.0f) );
    diLOG_MSTATUS ( nAttr.setMax(10.0f) );

    aSpeed = nAttr.create( "Speed", "ws", MFnNumericData::kFloat);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(0.1f) );
    diLOG_MSTATUS (nAttr.setMin(0.0f) );
    diLOG_MSTATUS (nAttr.setMax(1.0f) );

    aTurbulence = nAttr.create( "Turbulence", "t", MFnNumericData::kLong);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(2) );
    diLOG_MSTATUS ( nAttr.setMin(1) );
    diLOG_MSTATUS ( nAttr.setMax(10) );

    aPower = nAttr.create( "Power", "pow", MFnNumericData::kFloat);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(1.0f) );
    diLOG_MSTATUS ( nAttr.setMin(0.0f) );
    diLOG_MSTATUS ( nAttr.setMax(100.0f) );

    aFrame = nAttr.create( "Frame", "f", MFnNumericData::kFloat);
    MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setDefault(1.0f) );
    diLOG_MSTATUS ( nAttr.setMin(0.0f) );
    diLOG_MSTATUS ( nAttr.setMax(1000.0f) );
	*/

	// grid dimensions
	aDims = nAttr.create( "gridDims", "gridDims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gridMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gridMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// scalar data grid
	aScalarDataGrid = tAttr.create( "scalarDataGrid", "scalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aScalarDataGrid) );

	// data min
	aDataMin = nAttr.create( "dataMin", "dataMin", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aDataMin) );

	// data max
	aDataMax = nAttr.create( "dataMax", "dataMax", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aDataMax) );

	// Implicit shading network attributes

    aPlaceMat = mAttr.create("placementMatrix", "pm", MFnMatrixAttribute::kFloat);
    MAKE_INPUT(mAttr);
    diLOG_MSTATUS ( addAttribute(aPlaceMat) );

    aPointWorld = nAttr.createPoint("pointWorld", "pw");
	MAKE_INPUT(nAttr);
    diLOG_MSTATUS ( nAttr.setHidden(true) );
    diLOG_MSTATUS ( addAttribute(aPointWorld) );


	// Create output attributes

    aScaledDatum = nAttr.create( "scaledDatum", "scaledDatum", MFnNumericData::kFloat);
	MAKE_OUTPUT(nAttr);
    diLOG_MSTATUS ( addAttribute(aScaledDatum) );

	/*
	// Add attributes to the node database.
	
    diLOG_MSTATUS ( addAttribute(aColorBase) );
    diLOG_MSTATUS ( addAttribute(aColorFlame) );
    diLOG_MSTATUS ( addAttribute(aDeform) );
    diLOG_MSTATUS ( addAttribute(aWarp) );
    diLOG_MSTATUS ( addAttribute(aSpeed) );
    diLOG_MSTATUS ( addAttribute(aTurbulence) );
    diLOG_MSTATUS ( addAttribute(aPower) );
    diLOG_MSTATUS ( addAttribute(aFrame) );
	*/

    // All inputs affect all outputs
	/*
    diLOG_MSTATUS ( attributeAffects (aColorBase, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aColorFlame, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aDeform, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aWarp, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aSpeed, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aTurbulence, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aPower, aOutAlpha) );
    diLOG_MSTATUS ( attributeAffects (aFrame, aOutAlpha) );
	*/
    diLOG_MSTATUS( attributeAffects(aDims, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aScalarDataGrid, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aDataMin, aScaledDatum) );
    diLOG_MSTATUS( attributeAffects(aDataMax, aScaledDatum) );
    diLOG_MSTATUS ( attributeAffects(aPointWorld, aScaledDatum) );
    diLOG_MSTATUS ( attributeAffects(aPlaceMat, aScaledDatum) );

    return MS::kSuccess;
}


///////////////////////////////////////////////////////
// DESCRIPTION:
// This function gets called by Maya to evaluate the texture.
//
///////////////////////////////////////////////////////

MStatus secondaryData::compute(const MPlug& plug, MDataBlock& block)
{
	MStatus stat;

    if ( (plug != aScaledDatum) && (plug.parent() != aScaledDatum) )
       return MS::kUnknownParameter;

    const float3& worldPos = block.inputValue(aPointWorld).asFloat3();
    const MFloatMatrix& placementMatrix( block.inputValue(aPlaceMat).asFloatMatrix() );
	/*
    const MFloatVector& cBase = block.inputValue(aColorBase).asFloatVector();
    const MFloatVector& cFlame =block.inputValue(aColorFlame).asFloatVector();

    const int   deform = block.inputValue( aDeform ).asLong();
    const float warp   = block.inputValue( aWarp ).asFloat();
    const float speed  = block.inputValue( aSpeed ).asFloat();
    const int   turbValue   = block.inputValue( aTurbulence ).asLong();
    const float power  = block.inputValue( aPower ).asFloat();
    const float frame  = block.inputValue( aFrame ).asFloat();
	*/

	MFloatPoint q(worldPos[0], worldPos[1], worldPos[2]);
    q *= placementMatrix;									// Convert into solid space
	MPoint renderLocation( q );

	/*
    float u, v, w;
    u = q.x; v = q.y; w = q.z;

    float dist = speed * frame;
    float au, av, aw;
    au = u + dist;
    av = v + dist;
    aw = w + dist;

	// Calculate 3 noise values
    float ascale = (float) turbulence( au, av, aw,(int)deform);
    float bscale = (float) turbulence( au,-av, aw,(int)deform);
    float cscale = (float) turbulence(-au, av,-aw,(int)deform);
    float dscale = warp;

	// Add this noise as a vector to the texture coordinates
	// (since we are only calculating one value, the
	// displacement will be alont the 1 1 1 vector ... this
	// displacement generates the "flicker" movement as the
	// value moves around the texture coordinate

    u += ascale * dscale;
    v += bscale * dscale;
    w += cscale * dscale;

	// Calculate a turbulence value for this point

    float scalar = (float) (turbulence(u,v,w,(int)turbValue) + 0.5);

	// convert scalar into a point on the color curve

    if (power != 1) scalar = powf (scalar, power);
	*/

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hScalarDataGrid( block.inputValue(aScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnDoubleArrayData fnScalarDataGrid( hScalarDataGrid.data() );
	MDoubleArray scalarDataGrid( fnScalarDataGrid.array() );

	MDataHandle hDataMin( block.inputValue(aDataMin, &stat) );   diLOG_MSTATUS( stat );
	double dataMin = hDataMin.asDouble();

	MDataHandle hDataMax( block.inputValue(aDataMax, &stat) );   diLOG_MSTATUS( stat );
	double dataMax = hDataMax.asDouble();

	double slope = 1.0;
	if ( dataMax != dataMin ) slope = 1.0 / (dataMax-dataMin);
	double halfWindow = 2.0;

	double scalarDatum = lapdCommon::interpolateScalar( renderLocation, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
	double scaledScalar = slope * (scalarDatum-dataMin);
	if ( scaledScalar < 0.0 ) scaledScalar=0.0;  if ( scaledScalar > 1.0 ) scaledScalar = 1.0;

	/*
    MDataHandle outHandle = block.outputValue( aOutColor );
    MFloatVector & outColor = outHandle.asFloatVector();
    outColor = ((cFlame-cBase)*scalar) + cBase;
    outHandle.setClean();
	*/

    MDataHandle outHandle( block.outputValue(aScaledDatum) );
    outHandle.asFloat() = (float)scaledScalar;
    outHandle.setClean();

    return MS::kSuccess;
}

