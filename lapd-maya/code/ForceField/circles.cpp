/* ============================================================================
// forceField : circles.cpp
//-
// ==========================================================================
// Copyright (C) 2007 Jim Bamber
//
// ==========================================================================
//+
*/
#include "circles.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshEdge.h>
#include <maya/MQuaternion.h>


// Static data
MTypeId circles::id( 0x00455 );
MPointArray circles::unitCircle;

// Attributes
MObject circles::aCurveCount;
MObject circles::aStartIndices;
MObject circles::aCVCounts;
MObject circles::aControlVertexList;
MObject circles::aTangentList;
MObject circles::aTubeRadius;
MObject circles::aStart;
MObject circles::aStep;
MObject circles::aCircleType;
MObject circles::aSegmentCount;
MObject circles::aDims;
MObject circles::aGridMin;
MObject circles::aGridMax;
MObject circles::aMultiplier;
MObject circles::aScalarDataGrid;

MObject circles::aSecondaryMesh;


/* ============================================================================
// createMesh()
*/
MStatus circles::createMesh(
	int curveCount,
	MIntArray startIndices,
	MIntArray CVCounts,
	MPointArray controlVertexList,
	MVectorArray tangentList,
	int circleType,
	int segmentCount,
	double tubeRadius,
	int start,
	int step,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MDoubleArray& scalarDataGrid,
	double multiplier,
	MObject& newOutputData )
{
	MStatus stat;

	MFnMesh fnMesh;
	MPointArray vertices;
	MIntArray polygonVertexCounts;
	MIntArray polygonVertexIndices;
	MVector translation;
	int index;
	MPoint controlVertex;
	MVector tangent;
	double halfWindow = 2.0;
	double scalarDatum;

	if ( tubeRadius < 1.0e-2 ) tubeRadius = 1.0e-2;

	if ( start < 0 ) start = 0;
	if ( step < 1 ) step = 1;

	// Loop through curves
	for ( int iCurve=0; iCurve<curveCount; iCurve++ )
	{
		int CVCount = CVCounts[ iCurve ];
		int startIndex = startIndices[ iCurve ];
		for ( int iCV = start; iCV < CVCount; iCV = iCV + step )
		{
			index = startIndex + iCV;
			controlVertex = controlVertexList[index];
			tangent = tangentList[index];

			scalarDatum = lapdCommon::interpolateScalar( controlVertex, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
			scalarDatum = multiplier * scalarDatum;

			if ( circleType == 0 )
				//addCircle( fnMesh, newOutputData, controlVertex, tangent, scalarDatum, tubeRadius, segmentCount );
				addCircle(
					controlVertex,
					tangent,
					scalarDatum,
					tubeRadius,
					segmentCount,
					vertices,
					polygonVertexCounts,
					polygonVertexIndices );
			//if ( circleType == 1 ) do something else...
		}
	}

	int vertexCount = vertices.length();
	int polygonCount = polygonVertexCounts.length();
	//sprintf( forceFieldCommon::logString, "vertexCount: %d  polygonCount: %d", vertexCount, polygonCount );
	//forceFieldCommon::logEntry( (char*) forceFieldCommon::logString );

	// Single call here to create the mesh
	fnMesh.create(
		vertexCount,
		polygonCount, 
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		newOutputData, 
		&stat );   diLOG_MSTATUS( stat );

	assignUVs( fnMesh, newOutputData, segmentCount );

	// Doesn't seem to do anything...
	//MItMeshEdge outEdgeIter( newOutputData, &stat );   diLOG_MSTATUS( stat );
	//outEdgeIter.reset();
	//while ( !(outEdgeIter.isDone()) )
	//{
	//	outEdgeIter.setSmoothing( true );
	//	outEdgeIter.next();
	//}

	return MStatus::kSuccess;
}


/* ============================================================================
// addCircle()
*/
MStatus circles::addCircle(
	//MFnMesh& fnMesh,
	//MObject& newOutputData,
	MPoint controlVertex,
	MVector tangent,
	double scalarDatum,
	double tubeRadius,
	int segmentCount,
	MPointArray& vertices,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices )
{
	MStatus stat;

	MVector zAxis( 0.0, 0.0, 1.0 );
	MQuaternion quaternion( zAxis, tangent );
	//MVector innerVector1, outerVector1, innerVector2, outerVector2;
	//MVector rInnerVector1, rOuterVector1, rInnerVector2, rOuterVector2;
	MVector vector;
	MVector rotatedVector;
	double length = tubeRadius + scalarDatum;
	//MPointArray vertexArray;
	MVector innerVector, outerVector;
	MVector rInnerVector, rOuterVector;

	// This way makes a circle around the outside
	/*
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		vector = circles::unitCircle[ iSegment ];
		vector = vector * length;
		rotatedVector = vector.rotateBy( quaternion );
		vertexArray.append( controlVertex + rotatedVector );
	}
	fnMesh.addPolygon( vertexArray, true, 1.0e-4, newOutputData, &stat );   diLOG_MSTATUS( stat );
	*/

	// This way builds the circle from a number of pie wedges
	/*
	vertexArray.append( controlVertex );
	vertexArray.append( controlVertex );
	vertexArray.append( controlVertex );   // to make sure there are 3 entries in the array
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		vector1 = circles::unitCircle[ iSegment ];
		if ( iSegment == segmentCount-1 ) vector2 = circles::unitCircle[ 0 ];
		else vector2 = circles::unitCircle[ iSegment + 1 ];

		vector1 = vector1 * length;
		vector2 = vector2 * length;

		rotatedVector1 = vector1.rotateBy( quaternion );
		rotatedVector2 = vector2.rotateBy( quaternion );

		vertexArray[ 1 ] = controlVertex + rotatedVector1;
		vertexArray[ 2 ] = controlVertex + rotatedVector2;
		fnMesh.addPolygon( vertexArray, true, 1.0e-4, newOutputData, &stat );   diLOG_MSTATUS( stat );
	}
	*/

	// This way builds the circle with a hole in the middle from a number of truncated pie wedges
	/*
	vertexArray.append( controlVertex );
	vertexArray.append( controlVertex );
	vertexArray.append( controlVertex );
	vertexArray.append( controlVertex );   // to make sure there are 4 entries in the array
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		innerVector1 = circles::unitCircle[ iSegment ];
		outerVector1 = innerVector1;
		if ( iSegment == segmentCount-1 ) innerVector2 = circles::unitCircle[ 0 ];
		else innerVector2 = circles::unitCircle[ iSegment + 1 ];
		outerVector2 = innerVector2;

		innerVector1 = innerVector1 * tubeRadius;
		outerVector1 = outerVector1 * length;
		innerVector2 = innerVector2 * tubeRadius;
		outerVector2 = outerVector2 * length;

		rInnerVector1 = innerVector1.rotateBy( quaternion );
		rOuterVector1 = outerVector1.rotateBy( quaternion );
		rInnerVector2 = innerVector2.rotateBy( quaternion );
		rOuterVector2 = outerVector2.rotateBy( quaternion );

		vertexArray[ 0 ] = controlVertex + rInnerVector1;
		vertexArray[ 1 ] = controlVertex + rOuterVector1;
		vertexArray[ 2 ] = controlVertex + rOuterVector2;
		vertexArray[ 3 ] = controlVertex + rInnerVector2;
		//fnMesh.addPolygon( vertexArray, true, 1.0e-4, newOutputData, &stat );   diLOG_MSTATUS( stat );
	}
	*/

	// This way also builds the circles (donuts, actually) out of truncated pie wedges, but without
	// using the addPolygon() method.
	//
	// First, calculate the vertices of the inner and outer circles
	unsigned int vertexCount = vertices.length();
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		innerVector = circles::unitCircle[ iSegment ];
		outerVector = innerVector;

		innerVector = innerVector * tubeRadius;
		outerVector = outerVector * length;

		rInnerVector = innerVector.rotateBy( quaternion );
		rOuterVector = outerVector.rotateBy( quaternion );

		vertices.append( controlVertex + rInnerVector );
		vertices.append( controlVertex + rOuterVector );
	}

	// Then, figure out the connectivity of all but the last pie wedges
	int vertexIndexBase;
	for ( int iSegment=0; iSegment < segmentCount-1; iSegment++ )
	{
		polygonVertexCounts.append(4);

		vertexIndexBase = vertexCount + iSegment*2;
		polygonVertexIndices.append( vertexIndexBase + 0 );
		polygonVertexIndices.append( vertexIndexBase + 1 );
		polygonVertexIndices.append( vertexIndexBase + 3 );
		polygonVertexIndices.append( vertexIndexBase + 2 );
	}

	// Finally, figure out the connectivity of the last pie wedge
	polygonVertexCounts.append(4);

	vertexIndexBase = vertexCount + (segmentCount-1)*2;
	polygonVertexIndices.append( vertexIndexBase + 0 );
	polygonVertexIndices.append( vertexIndexBase + 1 );
	polygonVertexIndices.append( vertexCount + 1 );
	polygonVertexIndices.append( vertexCount + 0 );

	return MStatus::kSuccess;
}


/* ============================================================================
// assignUVs()
*/
MStatus circles::assignUVs(
	MFnMesh& fnMesh,
	MObject& newOutputData,
	int segmentCount )
{
	MStatus stat;

	// U coordinate goes around the circle, equates to x coordinate of image
	MFloatArray uOffsets;
	uOffsets.append( 0.0 );
	uOffsets.append( 0.0 );
	uOffsets.append( (float)(1.0/segmentCount) );
	uOffsets.append( (float)(1.0/segmentCount) );

	// V coordinate goes from center ring to outer edge of the circle, equates to y coordinate of image
	MFloatArray vValues;
	vValues.append( 0.0 );
	vValues.append( 1.0 );
	vValues.append( 1.0 );
	vValues.append( 0.0 );

	int uvId=0;
	float uBase;
	float uValue, vValue;
	for (int iSegment=0; iSegment<segmentCount; iSegment++ )
	{
		uBase = ((float)iSegment) / ((float)segmentCount);
		for (int iVertex=0; iVertex<4; iVertex++)
		{
			uValue = uBase+uOffsets[iVertex];
			vValue = vValues[iVertex];
			diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue) );
			uvId++;
		}
	}

	int polygonCount = fnMesh.numPolygons( &stat );   diLOG_MSTATUS( stat );
	int circleCount = polygonCount / segmentCount;
	int iPolygon;
	for ( int iCircle=0; iCircle<circleCount; iCircle++ )
		for (int iSegment=0; iSegment<segmentCount; iSegment++ )
		{
			iPolygon = iCircle*segmentCount + iSegment;
			for (int iVertex=0; iVertex<4; iVertex++ )
			{
				uvId = iSegment*4 + iVertex;
				diLOG_MSTATUS( fnMesh.assignUV(iPolygon, iVertex, uvId) );
			}
		}

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the curves.
//
*/
MStatus circles::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> circles::compute()" );
	forceFieldCommon::logEntry( forceFieldCommon::eCircles );

	if ( (plug != aSecondaryMesh) )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from circles::compute(), Maya not asking for secondary mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hCurveCount( block.inputValue(aCurveCount, &stat) );   diLOG_MSTATUS( stat );
	int curveCount = hCurveCount.asInt();

	MDataHandle hStartIndices( block.inputValue(aStartIndices, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnStartIndices( hStartIndices.data() );
	MIntArray startIndices( fnStartIndices.array() );

	MDataHandle hCVCounts( block.inputValue(aCVCounts, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnCVCounts( hCVCounts.data() );
	MIntArray CVCounts( fnCVCounts.array() );

	MDataHandle hControlVertexList( block.inputValue(aControlVertexList, &stat) );   diLOG_MSTATUS( stat );
	MFnPointArrayData fnControlVertexList( hControlVertexList.data() );
	MPointArray controlVertexList( fnControlVertexList.array() );

	MDataHandle hTangentList( block.inputValue(aTangentList, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnTangentList( hTangentList.data() );
	MVectorArray tangentList( fnTangentList.array() );

	MDataHandle hTubeRadius( block.inputValue(aTubeRadius, &stat) );   diLOG_MSTATUS( stat );
	double tubeRadius = hTubeRadius.asDouble();

	MDataHandle hStart( block.inputValue(aStart, &stat) );   diLOG_MSTATUS( stat );
	int start = hStart.asInt();

	MDataHandle hStep( block.inputValue(aStep, &stat) );   diLOG_MSTATUS( stat );
	int step = hStep.asInt();

	MDataHandle hCircleType( block.inputValue(aCircleType, &stat) );   diLOG_MSTATUS( stat );
	int circleType = hCircleType.asInt();

	MDataHandle hSegmentCount( block.inputValue(aSegmentCount, &stat) );   diLOG_MSTATUS( stat );
	int segmentCount = hSegmentCount.asInt();

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hScalarDataGrid( block.inputValue(aScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnDoubleArrayData fnScalarDataGrid( hScalarDataGrid.data() );
	MDoubleArray scalarDataGrid( fnScalarDataGrid.array() );

	MDataHandle hMultiplier( block.inputValue(aMultiplier, &stat) );   diLOG_MSTATUS( stat );
	double multiplier = hMultiplier.asDouble();


	// Now do the output attributes.
	//
	MDataHandle hSecondaryMesh( block.outputValue(aSecondaryMesh, &stat) );   diLOG_MSTATUS( stat );


	// Create the data object which underlies the output mesh
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );

	// Unit circle
	unitCircle.clear();
	double angleFactor = (1.0/(double)segmentCount)*(2.0*3.1415927);
	double theta;
	double x, y, z = 0.0;
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		theta = (double)iSegment * angleFactor;
		x = cos( theta );
		y = sin( theta );
		unitCircle.append( x, y, z );
	}

	// Make the call to create the secondary mesh
	//forceFieldCommon::logEntry( (char*) "About to create the secondary mesh" );
	diLOG_MSTATUS(
		createMesh(
			curveCount,
			startIndices,
			CVCounts,
			controlVertexList,
			tangentList,
			circleType,
			segmentCount,
			tubeRadius,
			start,
			step,
			dims,
			gridMin,
			gridMax,
			scalarDataGrid,
			multiplier,
			newOutputData) );


	// Set the outputs as clean.
	hSecondaryMesh.set( newOutputData );
	block.setClean( plug );

	//forceFieldCommon::logEntry( (char*) "<-- circles::compute()" );
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information and the unit circle values
*/
MStatus circles::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> circles::initialize()" );


	// Create input attributes:
	//
	// curve count
	aCurveCount = nAttr.create( "curveCount", "curveCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveCount) );

	// start indices
	aStartIndices = tAttr.create( "startIndices", "startIndices", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aStartIndices) );

	// CV counts
	aCVCounts = tAttr.create( "CVCounts", "CVCounts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aCVCounts) );

	// control vertices
	aControlVertexList = tAttr.create( "controlVertexList", "controlVertexList", MFnData::kPointArray, fnPointArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aControlVertexList) );

	// tangents
	aTangentList = tAttr.create( "tangentList", "tangentList", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aTangentList) );

	// tube radius
	aTubeRadius = nAttr.create( "tubeRadius", "tubeRadius", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aTubeRadius) );

	// start
	aStart = nAttr.create( "start", "start", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	diLOG_MSTATUS( nAttr.setMin(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aStart) );

	// step
	aStep = nAttr.create( "step", "step", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aStep) );

	// mesh type
    aCircleType = enumAttr.create( "circleType", "circleType", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("2D full disks", 0) );
	//diLOG_MSTATUS( enumAttr.addField("something else...", 1) );
	MAKE_INPUT( enumAttr );
	diLOG_MSTATUS( addAttribute(aCircleType) );

	// segment count
	aSegmentCount = nAttr.create( "segmentCount", "segmentCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(18) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aSegmentCount) );

	// grid dimensions
	aDims = nAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gridMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gridMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// scalar data grid
	aScalarDataGrid = tAttr.create( "scalarDataGrid", "scalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aScalarDataGrid) );

	// multiplier
	aMultiplier = nAttr.create( "multiplier", "multiplier", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aMultiplier) );


	// Create output attributes
	//
	// secondary mesh
	aSecondaryMesh = tAttr.create( "secondaryMesh", "secondaryMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aSecondaryMesh) );


	// All inputs affect all outputs
	//
	// secondary mesh
    diLOG_MSTATUS( attributeAffects(aCurveCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStartIndices, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCVCounts, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aControlVertexList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTangentList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTubeRadius, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStart, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStep, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCircleType, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aSegmentCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aDims, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aScalarDataGrid, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aMultiplier, aSecondaryMesh) );

	forceFieldCommon::logEntry( (char*) "<-- circles::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
// circles()
*/
circles::circles()
{
}


/* ============================================================================
// postConstructor()
*/
void circles::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType circles::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * circles::creator()
{
    return new circles();
}


/* ============================================================================
// ~circles()
*/
circles::~circles()
{
}


