/* ==========================================================================
// Texture 3D : texture3Dcommon.h
//
// Copyright (C) 2011 Jim Bamber 
//
*/
#ifndef TEXTURE3DCOMMON_H
#define TEXTURE3DCOMMON_H

#include <maya/MVectorArray.h>
#include <maya/MPoint.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>

#include "../lapdCommon/defines.h"


class texture3Dcommon
{
    public:

	enum nodeNameEnum
	{
		eOther,
		eDefaultVectorShadingTable,
		eScalarDataInterpolation,
		eVectorFade,
		eVectorShadeLookup
	};

	static void logEntry(
		char *logString );

	static void logEntry(
		nodeNameEnum nodeName );

	static char logString[DI_LOG_STRING_LENGTH];

	private:

	static char logFilename[DI_LOG_FILENAME_LENGTH];
	static FILE* logFile;
    static int currentDay;
    static time_t currentRawTime;
	static int nodeCount;             // must match the number of entries in the nodeNameEnum
	static int callCounters[5];       // ""
	static char* nodeNameStrings[5];  // ""
	static int logTimestep;

};


#endif

