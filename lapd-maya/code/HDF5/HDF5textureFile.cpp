/* ==========================================================================
// HDF5 : HDF5textureFile.cpp
//
// Copyright (C) 2008 Jim Bamber 
// 
*/
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "HDF5common.h"
#include "errorHandling.h"
#include "HDF5textureFile.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MArrayDataBuilder.h>
#include <maya/MGlobal.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MFnStringArrayData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatVector.h>

#include <H5public.h>
#include <H5Apublic.h>
#include <H5Dpublic.h>
#include <H5Fpublic.h>
#include <H5Gpublic.h>
#include <H5Ppublic.h>
#include <H5Spublic.h>
#include <H5Tpublic.h>

MTypeId HDF5textureFile::id( 0x00342 );

MObject  HDF5textureFile::aFilename;        
MObject  HDF5textureFile::aTextureGroup;        
MObject  HDF5textureFile::aTextureGroups;
MObject  HDF5textureFile::aTextureTables;


/* ==========================================================================
// compute()
*/
MStatus HDF5textureFile::compute( const MPlug& plug, MDataBlock& data )
{
	//HDF5common::logEntry( (char*) "--> HDF5textureFile::compute()" );
	HDF5common::logEntry( HDF5common::eHDF5textureFile );
	MStatus stat;
 
	if ( ( plug == aTextureGroups ) || ( plug.parent() == aTextureGroups ) ||
		 ( plug == aTextureTables ) || ( plug.parent() == aTextureTables ) )
	{
		stat = computeKernel( plug, data );
		data.setClean( plug );
	} 
	else 
		stat  = MS::kUnknownParameter;

	//HDF5common::logEntry( (char*) "<-- HDF5textureFile::compute()" );
	return stat;
}


/* ==========================================================================
// computeKernel()
*/
MStatus HDF5textureFile::computeKernel( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;
 
	MDataHandle hTextureGroups( data.outputValue(aTextureGroups, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle hTextureTables( data.outputValue(aTextureTables, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle hFilename( data.inputValue(aFilename, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle hTextureGroup( data.inputValue(aTextureGroup, &stat) );  diLOG_MSTATUS( stat );

	//HDF5common::logEntry( (char*) "--> HDF5textureFile::computeKernel()" );


	// Open the file.
	//
	MString filename( hFilename.asString() );
	if ( filename == "" )
	{
		HDF5common::logEntry( (char*) "Abnormal exit from HDF5textureFile::computeKernel(), filename is blank" );
		return MS::kSuccess;
	}
	const char *filenameAsChar = filename.asChar();
	hid_t file_id = H5Fopen( filenameAsChar, H5F_ACC_RDONLY, H5P_DEFAULT );	  diLOG_HDF5STAT( file_id );
	if ( file_id < 0 )
	{
		sprintf_s( HDF5common::logString, DI_LOG_STRING_LENGTH, 
			"Abnormal exit from HDF5textureFile::computeKernel(), %s could not be opened", filenameAsChar );
		HDF5common::logEntry( HDF5common::logString );
		return MS::kFailure;
	}

	// Get the group selector.
	//
	MString textureGroup = hTextureGroup.asString();

	// Load up the output arrays
	//
	MFnStringArrayData fnTextureGroups( hTextureGroups.data() );
	MFnStringArrayData fnTextureTables( hTextureTables.data() );
	MStringArray textureGroups;
	MStringArray textureTables;
	hsize_t num_obj;
	char group_name[120];
	//char data_type[120];
    diLOG_HDF5STAT( H5Gget_num_objs(file_id, &num_obj) );
	for ( hsize_t idx=0; idx<num_obj; idx++ )
	{
		int objtype = H5Gget_objtype_by_idx( file_id, idx );  diLOG_HDF5STAT( objtype );
		if ( objtype == H5G_GROUP )
		{
			diLOG_HDF5STAT( (int)H5Gget_objname_by_idx(file_id, idx, group_name, 120) );
			hid_t group_id = H5Gopen2( file_id, group_name, H5P_DEFAULT );   diLOG_HDF5STAT( group_id );
			textureGroups.append( MString(group_name) );
			//hid_t attr_id = H5Aopen_name( group_id, "Type" );   diLOG_HDF5STAT( attr_id );
			//hid_t attr_type_id = H5Aget_type( attr_id );	 diLOG_HDF5STAT( attr_type_id );
			//diLOG_HDF5STAT( H5Aread(attr_id, attr_type_id, data_type) );
			//diLOG_HDF5STAT( H5Tclose(attr_type_id) );
			//diLOG_HDF5STAT( H5Aclose(attr_id) );
			if ( MString(group_name) == textureGroup )
			{
				hsize_t num_obj_in_group;
				char table_name[120];
				diLOG_HDF5STAT( H5Gget_num_objs(group_id, &num_obj_in_group) );
				for ( hsize_t idx=0; idx<num_obj_in_group; idx++ )
				{
					int objtype = H5Gget_objtype_by_idx( group_id, idx );  diLOG_HDF5STAT( objtype );
					if ( objtype == H5G_DATASET )
					{
						diLOG_HDF5STAT( (int)H5Gget_objname_by_idx(group_id, idx, table_name, 120) );
						hid_t dataset_id = H5Dopen2( group_id, table_name, H5P_DEFAULT );   diLOG_HDF5STAT( dataset_id );

						// Check that the data type is 3-vector.
						//
						hid_t data_type_id = H5Aopen_name( dataset_id, "Type" );   diLOG_HDF5STAT( data_type_id );
						hid_t data_type_type_id = H5Aget_type( data_type_id );	   diLOG_HDF5STAT( data_type_type_id );

						char data_type[120];
						diLOG_HDF5STAT( H5Aread(data_type_id, data_type_type_id, data_type) );
	
						if ( MString(data_type) == "3-vector" ) textureTables.append( MString(table_name) );

						diLOG_HDF5STAT( H5Tclose(data_type_type_id) );
						diLOG_HDF5STAT( H5Aclose(data_type_id) );
						diLOG_HDF5STAT( H5Dclose(dataset_id) );
					}
				}
			}
			diLOG_HDF5STAT( H5Gclose(group_id) );
		}
	}

	// Close the ids.
	//
	diLOG_HDF5STAT( H5Fclose(file_id) );

	// Set the outputs.
	//
	diLOG_MSTATUS( fnTextureGroups.set(textureGroups) );
	diLOG_MSTATUS( fnTextureTables.set(textureTables) );
	hTextureGroups.setClean();
	hTextureTables.setClean();

	//HDF5common::logEntry( (char*) "<-- HDF5textureFile::computeKernel()" );
	return MS::kSuccess;
}


/* ==========================================================================
// HDF5textureFile()
*/
HDF5textureFile::HDF5textureFile()
{
}


/* ==========================================================================
// postConstructor()
*/
void HDF5textureFile::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType HDF5textureFile::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// ~HDF5textureFile()
*/
HDF5textureFile::~HDF5textureFile()
{
}


/* ==========================================================================
// creator()
*/
void *HDF5textureFile::creator()
{
	return new HDF5textureFile();
}


/* ==========================================================================
// initialize()
*/
MStatus HDF5textureFile::initialize()
{
	MStatus				 stat, stat2;
	MFnTypedAttribute	 typedAttr;
	MFnNumericAttribute	 numericAttr;
	MFnCompoundAttribute compAttr;
	MFnStringData		 stringData;
	MFnStringArrayData	 stringArrayData;
	MFnVectorArrayData	 vectorArrayData;		

	HDF5common::logEntry( (char*) "--> HDF5textureFile::initialize()" );


    aFilename = typedAttr.create( "filename", "filename", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

    aTextureGroup = typedAttr.create( "textureGroup", "textureGroup", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

	aTextureGroups = typedAttr.create( "textureGroups", "textureGroups", MFnData::kStringArray, stringArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

	aTextureTables = typedAttr.create( "textureTables", "textureTables", MFnData::kStringArray, stringArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

	// Add the attributes to the node
	diLOG_MSTATUS( addAttribute(aFilename) );
	diLOG_MSTATUS( addAttribute(aTextureGroup) );
	diLOG_MSTATUS( addAttribute(aTextureGroups) );
	diLOG_MSTATUS( addAttribute(aTextureTables) );

	// Define the attribute dependencies
	diLOG_MSTATUS( attributeAffects(aFilename, aTextureGroups) );
	diLOG_MSTATUS( attributeAffects(aFilename, aTextureTables) );
	diLOG_MSTATUS( attributeAffects(aTextureGroup, aTextureGroups) );
	diLOG_MSTATUS( attributeAffects(aTextureGroup, aTextureTables) );

	HDF5common::logEntry( (char*) "<-- HDF5textureFile::initialize()" );
	return MS::kSuccess;
}
