/* ============================================================================
// Isosurface : isoSurface.h
//
// Copyright (C) 2008 Jim Bamber 
//
*/
#ifndef ISOSURFACE_H
#define ISOSURFACE_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class isoSurface : public MPxNode
{
    public:

    isoSurface();
	virtual void postConstructor();
    virtual ~isoSurface();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MStatus createMesh(
		double isoValue,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MDoubleArray& scalarDataGrid,
		MObject& newOutputData );

	static MStatus assignUVs(
		MFnMesh& fnMesh,
		float3& gridMin,
		float3& gridMax,
		MPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );

	static MStatus marchingCube(
		double isoValue,
		MIntArray& vertexLookup,
		MPointArray& vertices,
		MIntArray& vertexIndices,
		MVectorArray& normals,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices,
		int ix, int iy, int iz,
		int nx, int ny, int nz,
		float x, float y, float z,
		float dx, float dy, float dz,
		float3& gridMin, float3& gridMax,
		MDoubleArray& scalarDataGrid );

	// Input attributes
	static MObject aIsoValue;			// Isosurface contouring value (double).
										// Spatial grid is assumed to be uniform => is not used as an input.
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aScalarDataGrid;		// This supplies the scalars for the secondary mesh (scalar array).

	// Output attributes
	static MObject aOutputMesh;		// The newly created mesh.

	// Convenience arrays
	static int vertexOffsets[8][3];
	static int edgeConnections[12][2];
	static int edgeDirections[12][3];
	static bool edgeDuplicates[8][12];
	static int edgeOffsets[8][12][4];

	// Marching cubes tables
	static int cubeEdgeFlags[256];
	static int triangleConnectionTable[256][16];

};


#endif

