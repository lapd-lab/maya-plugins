/* ============================================================================
// forceField : linTransScalar.h
//
// Copyright (C) 2012 Jim Bamber 
//
*/
#ifndef LINTRANSSCALAR_H
#define LINTRANSSCALAR_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class linTransScalar : public MPxNode
{
    public:

    linTransScalar();
	virtual void postConstructor();
    virtual ~linTransScalar();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	// Input attributes
	static MObject aM;			// Slope (float).
	static MObject aB;			// X-intercept (float)

	static MObject aInput;		// Scalar input (float).

	// Output attributes
									// output = m * input + b
	static MObject aOutputScalar;	// Scalar output (float).
	static MObject aOutputRGB;		// RGB output (MFloatVector), outputRGB[0,1,2] = outputScalar.
};


#endif

