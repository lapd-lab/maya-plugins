/* ============================================================================
// bump : pluginMain.cpp
//
// Copyright (C) 2007 Jim Bamber 
// 
*/
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "vectorBump.h"
#include "scalarBump.h"
#include "meshReplicator.h"
#include "cutPlane.h"
#include "scalarBumpUV.h"

#include <maya/MFnPlugin.h>
#include <maya/MPxNode.h>
#include <maya/MGlobal.h>


/* ============================================================================
 * initializePlugin()
 */
MStatus initializePlugin( MObject obj )
{
    MFnPlugin plugin( obj, "Jim Bamber", "1.0", "Any");
	MGlobal::displayInfo( " " );
	MGlobal::displayInfo( "plugin: bump  " + lapdCommon::version );
	MGlobal::displayInfo( "==========================" );

	MGlobal::displayInfo( "plugin.registerNode(vectorBump)" );
	diLOG_MSTATUS ( plugin.registerNode( "vectorBump", vectorBump::id, 
                         &vectorBump::creator, &vectorBump::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(scalarBump)" );
	diLOG_MSTATUS ( plugin.registerNode( "scalarBump", scalarBump::id, 
                         &scalarBump::creator, &scalarBump::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(meshReplicator)" );
	diLOG_MSTATUS ( plugin.registerNode( "meshReplicator", meshReplicator::id, 
                         &meshReplicator::creator, &meshReplicator::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(cutPlane)" );
	diLOG_MSTATUS ( plugin.registerNode( "cutPlane", cutPlane::id, 
                         &cutPlane::creator, &cutPlane::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(scalarBumpUV)" );
	diLOG_MSTATUS ( plugin.registerNode( "scalarBumpUV", scalarBumpUV::id, 
                         &scalarBumpUV::creator, &scalarBumpUV::initialize ) );

	return MS::kSuccess;
}


/* ============================================================================
 * uninitializePlugin()
 */
MStatus uninitializePlugin( MObject obj )
{
    MFnPlugin plugin( obj );
	diLOG_MSTATUS ( plugin.deregisterNode( vectorBump::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( scalarBump::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( meshReplicator::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( cutPlane::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( scalarBumpUV::id ) );

    return MS::kSuccess;
}


