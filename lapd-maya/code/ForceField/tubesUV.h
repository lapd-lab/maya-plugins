/* ============================================================================
// forceField : tubesUV.h
//
// Copyright (C) 2012 Jim Bamber 
//
*/
#ifndef TUBESUV_H
#define TUBESUV_H

#include <maya/MPxSurfaceShape.h> 
//#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class tubesUV : public MPxSurfaceShape
{
    public:

    tubesUV();
	virtual void postConstructor();
    virtual ~tubesUV();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	static MPointArray unitCircle;

    static MStatus createMesh(
		MFnMesh& fnMesh,
		int fwdCurveCount,
		MIntArray fwdStartIndices,
		MIntArray fwdCVCounts,
		MPointArray fwdControlVertexList,
		MVectorArray fwdTangentList,
		int bakCurveCount,
		MIntArray bakStartIndices,
		MIntArray bakCVCounts,
		MPointArray bakControlVertexList,
		MVectorArray bakTangentList,
		int segmentCount,
		double tubeRadius,
		int UCycles,
		double spiralAngle,
		double fixedAngle,
		MObject& newOutputData );

	static MStatus addSection(
		int iCV,
		MFloatPoint controlVertex,
		MVector tangent,
		double tubeRadius,
		int segmentCount,
		int UCycles,
		double spiralAngleRadians,
		double fixedAngleRadians,
		MFloatPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );

	/*
	static MStatus addCube(
		MFloatPoint controlVertex,
		MFloatPointArray& cubeVertices,
		MIntArray& cubePolygonVertexCounts,
		MIntArray& cubePolygonVertexIndices );
		*/

	static MStatus assignUVs(
		MFnMesh& fnMesh,
		int UVMode,
		int fwdCurveCount,
		MIntArray fwdStartIndices,
		MIntArray fwdCVCounts,
		MPointArray fwdControlVertexList,
		int bakCurveCount,
		MIntArray bakStartIndices,
		MIntArray bakCVCounts,
		MPointArray bakControlVertexList,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MDoubleArray& scalarDataGrid,
		double dataMin,
		double dataMax,
		int segmentCount,
		int UCycles,
		int stepsPerVCycle );

	// Input attributes
	static MObject aUVMode;				// Determines what kind of UV's to assign, 0=="data-based" or 1=="geometry-based" (enum).

	static MObject aFwdCurveCount;		// Number of curves in the fwd input arrays (int).
	static MObject aFwdStartIndices;	// Used to index into the fwd control vertex and tangent arrays (int array).
	static MObject aFwdCVCounts;		// Number of CV's for each fwd curve (int array), for example:
										//   fwdIndex = fwdStartIndices[fwdCurveIndex] + fwdCVIndex, where fwdCVIndex = {0:fwdCVCounts[fwdCurveIndex]-1}
	static MObject aFwdControlVertexList; // Control vertices for fwd curves (point array).
	static MObject aFwdTangentList;		// Tangents for fwd curves (vector array).

	static MObject aBakCurveCount;		// Number of curves in the bak input arrays (int).
	static MObject aBakStartIndices;	// Used to index into the bak control vertex and tangent arrays (int array).
	static MObject aBakCVCounts;		// Number of CV's for each bak curve (int array), for example:
										//   bakIndex = bakStartIndices[bakCurveIndex] + bakCVIndex, where bakCVIndex = {0:bakCVCounts[bakCurveIndex]-1}
	static MObject aBakControlVertexList; // Control vertices for bak curves (point array).
	static MObject aBakTangentList;		// Tangents for bak curves (vector array).

	static MObject aTubeRadius;			// Radius of field line tubes in cm (double).
	static MObject aSegmentCount;		// Number of segments in the circle (int).
	static MObject aUCycles;			// How many U cycles, 0->1->0, going around the circumference (int, default: 1, range: 1-3)
	static MObject aSpiralAngle;		// Rotation of each segment about the field line with respect to the previous segment in degrees (double)
	static MObject aFixedAngle;			// Rotation of entire tube about the field line in degrees (double)
	static MObject aStepsPerVCycle;		// How many steps along the tube for V to cycle once, 0->1->0 (int, default: 20, min: 1)

										// Spatial grid is assumed to be uniform => is not needed as an input.
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aScalarDataGrid;		// This supplies the scalars for the UV values (double array).
	static MObject aDataMin;			// Minimum value of scalar data grid (double).
	static MObject aDataMax;			// Maximum value of scalar data grid (double).

	// Output attributes
	static MObject aSecondaryMesh;		// Output mesh data with both geometry-based and data-based UV's.
};


#endif

