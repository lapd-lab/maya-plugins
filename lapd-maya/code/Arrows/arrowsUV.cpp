/* ==========================================================================
// Arrows : arrowsUV.cpp
//-
// ==========================================================================
// Copyright (C) 2008 Jim Bamber
//
// ==========================================================================
//+
*/
#include "arrowsUV.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "arrowsCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MQuaternion.h>
#include <maya/MFnStringData.h>

// 2012-03-03 JB: commented this out as part of the Microsoft "safe" functions nonsense
//#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1  // causes functions like strcpy() to be replaced with more secure versions


// Static data
MTypeId arrowsUV::id( 0x00653 );
MPointArray arrowsUV::unitCircle;
int arrowsUV::uvIdCount( 256 );

// Attributes
MObject arrowsUV::aInputMesh;
MObject arrowsUV::aSpatialGrid;
MObject arrowsUV::aDims;
MObject arrowsUV::aGridMin;
MObject arrowsUV::aGridMax;
MObject arrowsUV::aVectorDataGrid;
MObject arrowsUV::aMultiplier;
MObject arrowsUV::aArrowProportions;
MObject arrowsUV::aBaseDiameter;	
MObject arrowsUV::aJointDiameter;	
MObject arrowsUV::aJointOffset;	
MObject arrowsUV::aHeadDiameter;	
MObject arrowsUV::aHeadLength;	
MObject arrowsUV::aSegmentCount;	
MObject arrowsUV::aLengthThreshold;	

MObject arrowsUV::aOutputMesh;
MObject arrowsUV::aDataMin;
MObject arrowsUV::aDataMax;


/* ==========================================================================
// calculateArrow()
*/
MStatus arrowsUV::calculateArrow(
	MPoint& point,
	MVector& dataVector,
	double multiplier,
	int arrowProportions,
	double baseDiameter,
	double jointDiameter,
	double jointOffset,
	double headDiameter,
	double headLength,
	int segmentCount,
	double lengthThreshold,
	MPointArray& vertices,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices )
{
	MStatus stat;

	MVector zAxis( 0.0, 0.0, 1.0 );
	MQuaternion quaternion( zAxis, dataVector );
	MVector vector;
	double scale, offset;
	unsigned int vertexCount = vertices.length();
	double arrowLength = multiplier * dataVector.length();


	// Calculations for arrow proportions
	//
	if ( arrowProportions == 0 )
	{
		// fully proportional scaling
		baseDiameter = baseDiameter * arrowLength;
		jointDiameter = jointDiameter * arrowLength;
		jointOffset = jointOffset * arrowLength;
		headDiameter = headDiameter * arrowLength;
		headLength = headLength * arrowLength;
	}

	if ( arrowProportions == 1 )
	{
		// length proportional scaling
		jointOffset = jointOffset * arrowLength;
		headLength = headLength * arrowLength;
	}


	// First, the vertices
	//
	// Circle at center of arrow base
	scale = 0.01;
	offset = 0.0;
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		vector = arrowsUV::unitCircle[ iSegment ];
		vector = vector * scale;							// scale it
		vector = vector + offset * zAxis;					// shift it
		vector = vector.rotateBy( quaternion );				// rotate it
		vertices.append( point + vector );
	}

	// Circle at outside of arrow base
	scale = 0.5 * baseDiameter;
	offset = 0.0;
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		vector = arrowsUV::unitCircle[ iSegment ];
		vector = vector * scale;							// scale it
		vector = vector + offset * zAxis;					// shift it
		vector = vector.rotateBy( quaternion );				// rotate it
		vertices.append( point + vector );
	}

	// Circle at joint
	scale = 0.5 * jointDiameter;
	offset = arrowLength - headLength + jointOffset;
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		vector = arrowsUV::unitCircle[ iSegment ];
		vector = vector * scale;							// scale it
		vector = vector + offset * zAxis;					// shift it
		vector = vector.rotateBy( quaternion );				// rotate it
		vertices.append( point + vector );
	}

	// Circle at start of arrow head
	scale = 0.5 * headDiameter;
	offset = arrowLength - headLength;
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		vector = arrowsUV::unitCircle[ iSegment ];
		vector = vector * scale;							// scale it
		vector = vector + offset * zAxis;					// shift it
		vector = vector.rotateBy( quaternion );				// rotate it
		vertices.append( point + vector );
	}

	// Circle at tip of arrow head
	scale = 0.01;
	offset = arrowLength;
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		vector = arrowsUV::unitCircle[ iSegment ];
		vector = vector * scale;							// scale it
		vector = vector + offset * zAxis;					// shift it
		vector = vector.rotateBy( quaternion );				// rotate it
		vertices.append( point + vector );
	}


	// Then the connectivity
	//
	int vertexIndexBase;
	for ( int i=0; i<4; i++ )
	{
		int iSectionOffset = i * segmentCount;
		for ( int iSegment=0; iSegment < segmentCount-1; iSegment++ )
		{
			vertexIndexBase = vertexCount + iSectionOffset + iSegment;
			polygonVertexCounts.append(4);
			polygonVertexIndices.append( vertexIndexBase                + 0 );
			polygonVertexIndices.append( vertexIndexBase + segmentCount + 0 );
			polygonVertexIndices.append( vertexIndexBase + segmentCount + 1 );
			polygonVertexIndices.append( vertexIndexBase                + 1 );
		}

		vertexIndexBase = vertexCount + iSectionOffset + segmentCount - 1;
		polygonVertexCounts.append(4);
		polygonVertexIndices.append( vertexIndexBase                + 0 );
		polygonVertexIndices.append( vertexIndexBase + segmentCount + 0 );
		polygonVertexIndices.append( vertexIndexBase + segmentCount + 1 - segmentCount );  // wraps around to beginning
		polygonVertexIndices.append( vertexIndexBase                + 1 - segmentCount );  // of the circle
	}

	return MStatus::kSuccess;
}


/* ==========================================================================
// setUVs()
*/
MStatus arrowsUV::setUVs(
	MFnMesh& fnMesh )
{
	MStatus stat;

	float uValue, vValue;
	int uvId = 0;
	for (int i=0; i<uvIdCount; i++ )
	{
		uValue = ((float)i) / ((float)(uvIdCount-1));
		vValue = uValue;
		diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue) );
		uvId++;
	}

	return MStatus::kSuccess;
}


/* ==========================================================================
// assignUVs()
*/
MStatus arrowsUV::assignUVs(
	int arrowCount,
	MDoubleArray vectorMagArray,
	double dataMin,
	double dataMax,
	int segmentCount,
	MFnMesh& fnMesh )
{
	MStatus stat;

	MIntArray uvCounts;
	MIntArray uvIds;

	for ( int iArrow=0; iArrow<arrowCount; iArrow++ )
	{
		double vectorMag = vectorMagArray[ iArrow ];

		// Calculate uvId
		float scaledVectorMag = (float) ( (vectorMag-dataMin)/(dataMax-dataMin) );
		int uvId = (int)(((float)(uvIdCount-1)) * scaledVectorMag);
		if ( uvId < 0 ) uvId = 0;
		if ( uvId > (uvIdCount-1) ) uvId = uvIdCount - 1;

		for ( int iPiece=0; iPiece<4; iPiece++ )
		{
			for (int iSegment=0; iSegment<segmentCount; iSegment++ )
			{
				uvCounts.append( 4 );
				for (int iVertex=0; iVertex<4; iVertex++ )
				{
					uvIds.append( uvId );
				}  // End looping through 4 vertices of this segment
			}  // End looping through all segments of this piece
		}  // End looping through all 4 pieces of this arrow
	}  // End looping through all arrows

	diLOG_MSTATUS( fnMesh.assignUVs(uvCounts, uvIds) );

	return MStatus::kSuccess;
}


/* ==========================================================================
// compute()
//
// This function gets called by Maya to calculate the arrows.
//
*/
MStatus arrowsUV::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	arrowsCommon::logEntry( arrowsCommon::eArrows );

	if ( (plug != aOutputMesh) )
	{
		arrowsCommon::logEntry( (char*) "Abnormal exit from arrowsUV::compute(), Maya not asking for output mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hInputMesh( block.inputValue(aInputMesh, &stat) );   diLOG_MSTATUS( stat );
	MObject inMesh( hInputMesh.asMeshTransformed() );
	MFnMesh fnInputMesh( inMesh );

	MDataHandle hSpatialGrid( block.inputValue(aSpatialGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnSpatialGrid( hSpatialGrid.data() );
	MVectorArray spatialGrid( fnSpatialGrid.array() );

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hVectorDataGrid( block.inputValue(aVectorDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnVectorDataGrid( hVectorDataGrid.data() );
	MVectorArray vectorDataGrid( fnVectorDataGrid.array() );

	MDataHandle hMultiplier( block.inputValue(aMultiplier, &stat) );   diLOG_MSTATUS( stat );
	double multiplier = hMultiplier.asDouble();

	MDataHandle hArrowProportions( block.inputValue(aArrowProportions, &stat) );   diLOG_MSTATUS( stat );
	int arrowProportions = hArrowProportions.asInt();

	MDataHandle hBaseDiameter( block.inputValue(aBaseDiameter, &stat) );   diLOG_MSTATUS( stat );
	double baseDiameter = hBaseDiameter.asDouble();

	MDataHandle hJointDiameter( block.inputValue(aJointDiameter, &stat) );   diLOG_MSTATUS( stat );
	double jointDiameter = hJointDiameter.asDouble();

	MDataHandle hJointOffset( block.inputValue(aJointOffset, &stat) );   diLOG_MSTATUS( stat );
	double jointOffset = hJointOffset.asDouble();

	MDataHandle hHeadDiameter( block.inputValue(aHeadDiameter, &stat) );   diLOG_MSTATUS( stat );
	double headDiameter = hHeadDiameter.asDouble();

	MDataHandle hHeadLength( block.inputValue(aHeadLength, &stat) );   diLOG_MSTATUS( stat );
	double headLength = hHeadLength.asDouble();

	MDataHandle hSegmentCount( block.inputValue(aSegmentCount, &stat) );   diLOG_MSTATUS( stat );
	int segmentCount = hSegmentCount.asInt();

	MDataHandle hLengthThreshold( block.inputValue(aLengthThreshold, &stat) );   diLOG_MSTATUS( stat );
	double lengthThreshold = hLengthThreshold.asDouble();


	// Now do the output attributes.
	//
	MDataHandle hOutputMesh( block.outputValue(aOutputMesh, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hDataMin( block.outputValue(aDataMin, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hDataMax( block.outputValue(aDataMax, &stat) );   diLOG_MSTATUS( stat );


	// Unit circle
	//
	unitCircle.clear();
	double angleFactor = (1.0/(double)segmentCount)*(2.0*3.1415927);
	double theta;
	double x, y, z = 0.0;
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		theta = (double)iSegment * angleFactor;
		x = cos( theta );
		y = sin( theta );
		unitCircle.append( x, y, z );
	}


	// Create the data object which underlies the output mesh
	MFnMesh fnMesh;
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );
	MPointArray vertices;
	MIntArray polygonVertexCounts;
	MIntArray polygonVertexIndices;


	// Find the data min and max
	//
	MVector dataVector( vectorDataGrid[0] );
	double vectorMag = dataVector.length();
	double dataMin = vectorMag;
	double dataMax = vectorMag;
	int nDataVectors = vectorDataGrid.length();
	for ( int i=1; i<nDataVectors; i++ )
	{
		dataVector = vectorDataGrid[i];
		vectorMag = dataVector.length();
		if ( vectorMag < dataMin ) dataMin = vectorMag;
		if ( vectorMag > dataMax ) dataMax = vectorMag;
	}


	// Loop through the vector data array
	//
	MPointArray meshPoints;
	diLOG_MSTATUS( fnInputMesh.getPoints(meshPoints, MSpace::kWorld) );
	int pointCount = meshPoints.length();
	double halfWindow = 2.0;

	//arrowsCommon::logEntry( (char*) "About to create arrows" );
	MPoint point;
	MDoubleArray vectorMagArray;
	int arrowCount = 0;
	for ( int iPoint=0; iPoint<pointCount; iPoint++ )
	{
		point = meshPoints[iPoint];
		dataVector = lapdCommon::interpolateVector( point, dims, gridMax, gridMin, vectorDataGrid, halfWindow );

		double arrowLength = dataVector.length() * multiplier;
		if ( arrowLength >= lengthThreshold )
		{
			calculateArrow(
				point,
				dataVector,
				multiplier,
				arrowProportions,
				baseDiameter,
				jointDiameter,
				jointOffset,
				headDiameter,
				headLength,
				segmentCount,
				lengthThreshold,
				vertices,
				polygonVertexCounts,
				polygonVertexIndices );

			arrowCount++;
			vectorMag = dataVector.length();
			vectorMagArray.append( vectorMag );
		}
	}

	int vertexCount = vertices.length();
	int polygonCount = polygonVertexCounts.length();
	//sprintf( arrowsCommon::logString, "vertexCount: %d  polygonCount: %d  arrowCount: %d",
	//	vertexCount, polygonCount, arrowCount );
	//arrowsCommon::logEntry( arrowsCommon::logString );

	// Single call here to create the mesh
	//arrowsCommon::logEntry( (char*) "Creating the arrows mesh" );
	fnMesh.create(
		vertexCount,
		polygonCount, 
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		newOutputData, 
		&stat );   diLOG_MSTATUS( stat );

	//arrowsCommon::logEntry( (char*) "Setting UVs on the arrows mesh" );
	setUVs( fnMesh );

	//arrowsCommon::logEntry( (char*) "Assigning UVs on the arrows mesh" );
	assignUVs( arrowCount, vectorMagArray, dataMin, dataMax, segmentCount, fnMesh );

	// Set the outputs as clean.
	hOutputMesh.set( newOutputData );
	hDataMin.set( (float)dataMin );
	hDataMax.set( (float)dataMax );
	block.setClean( plug );

	//arrowsCommon::logEntry( (char*) "<-- arrows::compute()" );

	return MStatus::kSuccess;
}


/* ==========================================================================
// initialize()
//
// initializes attribute information
*/
MStatus arrowsUV::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MFnStringData stringData;
	MStatus stat, stat2;

	arrowsCommon::logEntry( (char*) "--> arrowsUV::initialize()" );


	// Create input attributes:
	//
	// input mesh
	aInputMesh = tAttr.create( "inputMesh", "inputMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aInputMesh) );

	// spatial grid
    aSpatialGrid = tAttr.create( "spatialGrid", "grid", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aSpatialGrid) );

	// grid dimensions
	aDims = nAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// vector data grid
    aVectorDataGrid = tAttr.create( "vectorDataGrid", "vData", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aVectorDataGrid) );

	// multiplier
	aMultiplier = nAttr.create( "multiplier", "multiplier", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aMultiplier) );

	// arrow proportions
	aArrowProportions = enumAttr.create( "arrowProportions", "arrowProportions", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("fully proportional", 0) );
	diLOG_MSTATUS( enumAttr.addField("length proportional", 1) );
	diLOG_MSTATUS( enumAttr.addField("fixed (cm)", 2) );
	MAKE_INPUT( enumAttr );
    diLOG_MSTATUS( addAttribute(aArrowProportions) );

	// base diameter
	aBaseDiameter = nAttr.create( "baseDiameter", "baseDiameter", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.1) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aBaseDiameter) );

	// joint diameter
	aJointDiameter = nAttr.create( "jointDiameter", "jointDiameter", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.06) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aJointDiameter) );

	// joint offset
	aJointOffset = nAttr.create( "jointOffset", "jointOffset", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.08) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aJointOffset) );

	// head diameter
	aHeadDiameter = nAttr.create( "headDiameter", "headDiameter", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.2) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aHeadDiameter) );

	// head length
	aHeadLength = nAttr.create( "headLength", "headLength", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.3) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aHeadLength) );

	// segment count
	aSegmentCount = nAttr.create( "segmentCount", "segmentCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(16) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aSegmentCount) );

	// length threshold
	aLengthThreshold = nAttr.create( "lengthThreshold", "lengthThreshold", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setMin(0.0) );
	diLOG_MSTATUS( nAttr.setDefault(0.1) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aLengthThreshold) );


	// Create output attributes
	//
	// secondary mesh
	aOutputMesh = tAttr.create( "outputMesh", "outputMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aOutputMesh) );

	// data min
	aDataMin = nAttr.create( "dataMin", "dataMin", MFnNumericData::kFloat, 0, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aDataMin) );

	// data max
	aDataMax = nAttr.create( "dataMax", "dataMax", MFnNumericData::kFloat, 0, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aDataMax) );


	// All inputs affect all outputs, more or less
	//
	// output mesh
    diLOG_MSTATUS( attributeAffects(aInputMesh, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aSpatialGrid, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDims, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aVectorDataGrid, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aMultiplier, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aArrowProportions, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aBaseDiameter, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aJointDiameter, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aJointOffset, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aHeadDiameter, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aHeadLength, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aSegmentCount, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aLengthThreshold, aOutputMesh) );

	// data min
    diLOG_MSTATUS( attributeAffects(aSpatialGrid, aDataMin) );
    diLOG_MSTATUS( attributeAffects(aDims, aDataMin) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aDataMin) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aDataMin) );
    diLOG_MSTATUS( attributeAffects(aVectorDataGrid, aDataMin) );

	// data max
    diLOG_MSTATUS( attributeAffects(aSpatialGrid, aDataMax) );
    diLOG_MSTATUS( attributeAffects(aDims, aDataMax) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aDataMax) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aDataMax) );
    diLOG_MSTATUS( attributeAffects(aVectorDataGrid, aDataMax) );

	arrowsCommon::logEntry( (char*) "<-- arrowsUV::initialize()" );
    return MS::kSuccess;
}


/* ==========================================================================
// postConstructor()
*/
void arrowsUV::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType arrowsUV::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// arrowsUV()
*/
arrowsUV::arrowsUV()
{
}


/* ==========================================================================
// ~arrowsUV()
*/
arrowsUV::~arrowsUV()
{
}


/* ==========================================================================
// creator()
//
// creates an instance of the node
*/
void * arrowsUV::creator()
{
    return new arrowsUV();
}


