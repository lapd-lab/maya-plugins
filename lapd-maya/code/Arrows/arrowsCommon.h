/* ==========================================================================
// Arrows : arrowsCommon.h
//
// Copyright (C) 2011 Jim Bamber 
//
*/
#ifndef ARROWSCOMMON_H
#define ARROWSCOMMON_H

#include <maya/MVectorArray.h>
#include <maya/MPoint.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>

#include "../lapdCommon/defines.h"


class arrowsCommon
{
    public:

    enum nodeNameEnum
	{
		eOther,
		eArrows,
		eVectorShadeUVLookup
	};

	static void logEntry(
		char *logString );

	static void logEntry(
		nodeNameEnum nodeName );

	static char logString[DI_LOG_STRING_LENGTH];

	private:

	static char logFilename[DI_LOG_FILENAME_LENGTH];
	static FILE* logFile;
    static int currentDay;
    static time_t currentRawTime;
	static int nodeCount;             // must match the number of entries in the nodeNameEnum
	static int callCounters[3];       // ""
	static char* nodeNameStrings[3];  // ""
	static int logTimestep;

};


#endif

