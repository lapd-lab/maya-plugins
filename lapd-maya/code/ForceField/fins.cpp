/* ============================================================================
// forceField : fins.cpp
//-
// ==========================================================================
// Copyright (C) 2008 Jim Bamber
//
// ==========================================================================
//+
*/
#include "fins.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>
#include <time.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshEdge.h>
#include <maya/MQuaternion.h>


// Static data
MTypeId fins::id( 0x00457 );

// Attributes
MObject fins::aCurveCount;
MObject fins::aStartIndices;
MObject fins::aCVCounts;
MObject fins::aControlVertexList;
MObject fins::aTangentList;
MObject fins::aReverseTangents;
MObject fins::aTubeRadius;
MObject fins::aStartDegrees;
MObject fins::aFinCount;
MObject fins::aUVMapping;
MObject fins::aRepetitionWidth;
MObject fins::aDims;
MObject fins::aGridMin;
MObject fins::aGridMax;
MObject fins::aScalarDataGrid;
MObject fins::aMultiplier;

MObject fins::aSecondaryMesh;


/* ============================================================================
// createMesh()
*/
MStatus fins::createMesh(
	int curveCount,
	MIntArray startIndices,
	MIntArray CVCounts,
	MPointArray controlVertexList,
	MVectorArray tangentList,
	bool reverseTangents,
	double tubeRadius,
	double startDegrees,
	int finCount,
	short UVMapping,
	int repetitionWidth,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MDoubleArray& scalarDataGrid,
	double multiplier,
	MObject& newOutputData )
{
	MStatus stat;

	MFnMesh fnMesh;
	MPointArray vertices;
	MIntArray polygonVertexCounts;
	MIntArray polygonVertexIndices;
	int index;
	MPoint controlVertex;
	MVector tangent;
	double scalarDatum;
	MPointArray curveVertices;
	MVectorArray curveTangents;
	MDoubleArray scalarData;
	double halfWindow = 2.0;

	if ( finCount < 1 ) finCount = 1;

	// Loop through curves
	for ( int iCurve=0; iCurve<curveCount; iCurve++ )
	{
		//sprintf( forceFieldCommon::logString, "iCurve: %d", iCurve );
		//forceFieldCommon::logEntry( forceFieldCommon::logString );

		diLOG_MSTATUS( curveVertices.clear() );
		diLOG_MSTATUS( curveTangents.clear() );
		diLOG_MSTATUS( scalarData.clear() );
		int CVCount = CVCounts[ iCurve ];
		int startIndex = startIndices[ iCurve ];
		for ( int iCV = 0; iCV < CVCount; iCV++ )
		{
			index = startIndex + iCV;
			controlVertex = controlVertexList[index];
			tangent = tangentList[index];
			if ( reverseTangents ) tangent = -tangent;

			scalarDatum = lapdCommon::interpolateScalar( controlVertex, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
			scalarDatum = multiplier * scalarDatum;

			diLOG_MSTATUS( curveVertices.append(controlVertex) );
			diLOG_MSTATUS( curveTangents.append(tangent) );
			diLOG_MSTATUS( scalarData.append(scalarDatum) );
		}

		double finDegrees;
		double stepDegrees = 360.0 / finCount;
		for ( int iFin=0; iFin<finCount; iFin++ )
		{
			finDegrees = startDegrees + (double)iFin*stepDegrees;
			//addFin( fnMesh, newOutputData, curveVertices, curveTangents, scalarData, tubeRadius, finDegrees );
			addFin(
				curveVertices,
				curveTangents,
				scalarData,
				tubeRadius,
				finDegrees,
				vertices,
				polygonVertexCounts,
				polygonVertexIndices );
		}
	}

	int vertexCount = vertices.length();
	int polygonCount = polygonVertexCounts.length();
	//sprintf( forceFieldCommon::logString, "vertexCount: %d  polygonCount: %d", vertexCount, polygonCount );
	//forceFieldCommon::logEntry( forceFieldCommon::logString );

	// Single call here to create the mesh
	fnMesh.create(
		vertexCount,
		polygonCount, 
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		newOutputData, 
		&stat );   diLOG_MSTATUS( stat );

	assignUVs( UVMapping, repetitionWidth, CVCounts, finCount, fnMesh, newOutputData );

	return MStatus::kSuccess;
}


/* ============================================================================
// addFin()
*/
MStatus fins::addFin(
	//MFnMesh& fnMesh,
	//MObject& newOutputData,
	MPointArray curveVertices,
	MVectorArray curveTangents,
	MDoubleArray scalarData,
	double tubeRadius,
	double finDegrees,
	MPointArray& vertices,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices )
{
	MStatus stat;

	MVector zAxis( 0.0, 0.0, 1.0 );
	//MQuaternion tangent1Quaternion, tangent2Quaternion;
	MQuaternion tangentQuaternion;
	double finRadians = finDegrees * 3.1415927/180.0;
	MQuaternion finQuaternion( finRadians, zAxis );

	MVector innerFinVector, outerFinVector;
	MVector innerRotatedVector, outerRotatedVector;
	//MPointArray vertexArray;
	//MVector CV1, CV2, lineSegment;
	//double lineLength;
	//MVector tangent1, tangent2;
	//double offset1, offset2;
	//MVector offset1Vector, offset2Vector;
	MVector CV;
	MVector tangent;
	double innerOffset, outerOffset;
	MVector innerOffsetVector, outerOffsetVector;

	// This is the way to do it using addPolygon()
	/*
	int CVCount = curveVertices.length();
	for ( int iCV=0; iCV<CVCount-1; iCV++ )
	{
		tangent1 = curveTangents[iCV];
		tangent2 = curveTangents[iCV+1];
		tangent1Quaternion = MQuaternion( zAxis, tangent1 );
		tangent2Quaternion = MQuaternion( zAxis, tangent2 );

		CV1 = curveVertices[iCV];
		CV2 = curveVertices[iCV+1];
		lineSegment = CV2 - CV1;
		lineLength = lineSegment.length();

		offset1 = tubeRadius + scalarData[iCV];
		offset2 = tubeRadius + scalarData[iCV+1];

		offset1Vector = MVector( 0.0, offset1, 0.0 );
		offset2Vector = MVector( 0.0, offset2, 0.0 );

		vertexArray.clear();
		vertexArray.append( CV1 );
		vertexArray.append( CV2 );

		finVector = offset2Vector.rotateBy( finQuaternion );
		rotatedVector = finVector.rotateBy( tangent2Quaternion );
		vertexArray.append( CV2 + rotatedVector );

		finVector = offset1Vector.rotateBy( finQuaternion );
		rotatedVector = finVector.rotateBy( tangent1Quaternion );
		vertexArray.append( CV1 + rotatedVector );

		fnMesh.addPolygon( vertexArray, true, 1.0e-4, newOutputData, &stat );   diLOG_MSTATUS( stat );
	}
	*/


	// This is the way to do it "by hand"
	int CVCount = curveVertices.length();
	int vertexCount = vertices.length();
	for ( int iCV=0; iCV<CVCount; iCV++ )
	{
		tangent = curveTangents[iCV];
		tangentQuaternion = MQuaternion( zAxis, tangent );
		CV = curveVertices[iCV];

		innerOffset = tubeRadius;
		innerOffsetVector = MVector( 0.0, innerOffset, 0.0 );
		innerFinVector = innerOffsetVector.rotateBy( finQuaternion );
		innerRotatedVector = innerFinVector.rotateBy( tangentQuaternion );

		outerOffset = tubeRadius + scalarData[iCV];
		outerOffsetVector = MVector( 0.0, outerOffset, 0.0 );
		outerFinVector = outerOffsetVector.rotateBy( finQuaternion );
		outerRotatedVector = outerFinVector.rotateBy( tangentQuaternion );

		vertices.append( MPoint(CV + innerRotatedVector) );
		vertices.append( MPoint(CV + outerRotatedVector) );
		if ( iCV > 0 )
		{
			// add the connectivity information for the polygon
			polygonVertexCounts.append(4);
			polygonVertexIndices.append( vertexCount - 2 );
			polygonVertexIndices.append( vertexCount + 0 );
			polygonVertexIndices.append( vertexCount + 1 );
			polygonVertexIndices.append( vertexCount - 1 );
		}
		vertexCount = vertexCount + 2;
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// assignUVs()
*/
MStatus fins::assignUVs(
	short UVMapping,
	int repetitionWidth,
	MIntArray CVCounts,
	int finCount,
	MFnMesh& fnMesh,
	MObject& newOutputData )
{
	MStatus stat;
	if ( repetitionWidth < 1 ) repetitionWidth = 1;

	if ( UVMapping == 0 )
	{	// Cartesian UV mapping

		// U coordinate equates to x coordinate of image and goes along the tube
		MFloatArray uValues;
		float uStep = (float)( 1.0 / repetitionWidth );
		for ( int i=0; i<repetitionWidth; i++ )
		{
			//uValues.append( (float)(i+1)*uStep );
			//uValues.append( (float)i*uStep );
			//uValues.append( (float)i*uStep );
			//uValues.append( (float)(i+1)*uStep );
			uValues.append( (float)i*uStep );
			uValues.append( (float)(i+1)*uStep );
			uValues.append( (float)(i+1)*uStep );
			uValues.append( (float)i*uStep );
		}

		// V coordinate equates to y coordinate of image and is perpendicular to the tube
		MFloatArray vValues;
		for ( int i=0; i<repetitionWidth; i++ )
		{
			vValues.append( 0.0 );
			vValues.append( 0.0 );
			vValues.append( 1.0 );
			vValues.append( 1.0 );
		}

		// Create the standard UV values
		int uvId=0;
		float uValue, vValue;
		for (int i=0; i<(int)(uValues.length()); i++ )
		{
			uValue = uValues[i];
			vValue = vValues[i];
			diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue) );
			uvId++;
		}

		// Now assign these values
		//int polygonCount = fnMesh.numPolygons( &stat );   diLOG_MSTATUS( stat );
		int curveCount = CVCounts.length();
		int iPolygon = 0;
		int CVCount;
		int iRep;
		for ( int iCurve=0; iCurve<curveCount; iCurve++ )
		{
			for ( int iFin=0; iFin<finCount; iFin++ )
			{
				CVCount = CVCounts[iCurve];
				iRep = 0;
				for ( int iCV=0; iCV<CVCount-1; iCV++ )
				{
					for ( int iVertex=0; iVertex<4; iVertex++ )
					{
						uvId = iRep*4 + iVertex;
						diLOG_MSTATUS( fnMesh.assignUV(iPolygon, iVertex, uvId) );
					}   // end loop through all vertices of one polygon
					iPolygon++;
					iRep++;
					if ( iRep > repetitionWidth-1 ) iRep=0;
				}   // end loop through all polygons of one fin
			}   // end loop through all fins of one curve
		}   // end loop through all curves
	}   // end if Cartesian mapping

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the curves.
//
*/
MStatus fins::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> fins::compute()" );
	forceFieldCommon::logEntry( forceFieldCommon::eFins );

	if ( plug != aSecondaryMesh )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from fins::compute(), Maya not asking for secondary mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hCurveCount( block.inputValue(aCurveCount, &stat) );   diLOG_MSTATUS( stat );
	int curveCount = hCurveCount.asInt();

	MDataHandle hStartIndices( block.inputValue(aStartIndices, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnStartIndices( hStartIndices.data() );
	MIntArray startIndices( fnStartIndices.array() );

	MDataHandle hCVCounts( block.inputValue(aCVCounts, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnCVCounts( hCVCounts.data() );
	MIntArray CVCounts( fnCVCounts.array() );

	MDataHandle hControlVertexList( block.inputValue(aControlVertexList, &stat) );   diLOG_MSTATUS( stat );
	MFnPointArrayData fnControlVertexList( hControlVertexList.data() );
	MPointArray controlVertexList( fnControlVertexList.array() );

	MDataHandle hTangentList( block.inputValue(aTangentList, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnTangentList( hTangentList.data() );
	MVectorArray tangentList( fnTangentList.array() );

	MDataHandle hReverseTangents( block.inputValue(aReverseTangents, &stat) );   diLOG_MSTATUS( stat );
	bool reverseTangents = hReverseTangents.asBool();

	MDataHandle hTubeRadius( block.inputValue(aTubeRadius, &stat) );   diLOG_MSTATUS( stat );
	double tubeRadius = hTubeRadius.asDouble();

	MDataHandle hStartDegrees( block.inputValue(aStartDegrees, &stat) );   diLOG_MSTATUS( stat );
	double startDegrees = hStartDegrees.asDouble();

	MDataHandle hFinCount( block.inputValue(aFinCount, &stat) );   diLOG_MSTATUS( stat );
	int finCount = hFinCount.asInt();

	MDataHandle hUVMapping( block.inputValue(aUVMapping, &stat) );   diLOG_MSTATUS( stat );
	short UVMapping = hUVMapping.asShort();

	MDataHandle hRepetitionWidth( block.inputValue(aRepetitionWidth, &stat) );   diLOG_MSTATUS( stat );
	int repetitionWidth = hRepetitionWidth.asInt();

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hScalarDataGrid( block.inputValue(aScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnDoubleArrayData fnScalarDataGrid( hScalarDataGrid.data() );
	MDoubleArray scalarDataGrid( fnScalarDataGrid.array() );

	MDataHandle hMultiplier( block.inputValue(aMultiplier, &stat) );   diLOG_MSTATUS( stat );
	double multiplier = hMultiplier.asDouble();


	// Now do the output attributes.
	//
	MDataHandle hSecondaryMesh( block.outputValue(aSecondaryMesh, &stat) );   diLOG_MSTATUS( stat );


	// Create the data object which underlies the output mesh
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );

	// Make the call to create the secondary mesh
	//forceFieldCommon::logEntry( (char*) "About to create mesh" );
	diLOG_MSTATUS(
		createMesh(
			curveCount,
			startIndices,
			CVCounts,
			controlVertexList,
			tangentList,
			reverseTangents,
			tubeRadius,
			startDegrees,
			finCount,
			UVMapping,
			repetitionWidth,
			dims,
			gridMin,
			gridMax,
			scalarDataGrid,
			multiplier,
			newOutputData) );


	// Set the outputs as clean.
	hSecondaryMesh.set( newOutputData );
	block.setClean( plug );

	//forceFieldCommon::logEntry( (char*) "<-- fins::compute()" );
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information and the unit circle values
*/
MStatus fins::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> fins::initialize()" );


	// Create input attributes:
	//
	// curve count
	aCurveCount = nAttr.create( "curveCount", "curveCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveCount) );

	// start indices
	aStartIndices = tAttr.create( "startIndices", "startIndices", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aStartIndices) );

	// CV counts
	aCVCounts = tAttr.create( "CVCounts", "CVCounts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aCVCounts) );

	// control vertices
	aControlVertexList = tAttr.create( "controlVertexList", "controlVertexList", MFnData::kPointArray, fnPointArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aControlVertexList) );

	// tangents
	aTangentList = tAttr.create( "tangentList", "tangentList", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aTangentList) );

	// reverse tangents flag
	aReverseTangents = nAttr.create( "reverseTangents", "reverseTangents", MFnNumericData::kBoolean, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(false) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aReverseTangents) );

	// tube radius
	aTubeRadius = nAttr.create( "tubeRadius", "tubeRadius", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aTubeRadius) );

	// start degrees
	aStartDegrees = nAttr.create( "startDegrees", "startDegrees", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aStartDegrees) );

	// fin count
	aFinCount = nAttr.create( "finCount", "finCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aFinCount) );

	// UV mapping
    aUVMapping = enumAttr.create( "UVMapping", "UVMapping", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("Cartesian", 0) );
	diLOG_MSTATUS( enumAttr.addField("radial", 1) );
	MAKE_INPUT( enumAttr );
	diLOG_MSTATUS( addAttribute(aUVMapping) );

	// Repetition width
	aRepetitionWidth = nAttr.create( "repetitionWidth", "repetitionWidth", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(5) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aRepetitionWidth) );

	// grid dimensions
	aDims = nAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gridMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gridMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// scalar data grid
	aScalarDataGrid = tAttr.create( "scalarDataGrid", "scalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aScalarDataGrid) );

	// multiplier
	aMultiplier = nAttr.create( "multiplier", "multiplier", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aMultiplier) );


	// Create output attributes
	//
	// secondary mesh
	aSecondaryMesh = tAttr.create( "secondaryMesh", "secondaryMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aSecondaryMesh) );


	// All inputs affect all outputs
	//
	// secondary mesh
    diLOG_MSTATUS( attributeAffects(aCurveCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStartIndices, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCVCounts, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aControlVertexList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTangentList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aReverseTangents, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTubeRadius, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStartDegrees, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aFinCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aUVMapping, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aRepetitionWidth, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aDims, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aScalarDataGrid, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aMultiplier, aSecondaryMesh) );

	forceFieldCommon::logEntry( (char*) "<-- fins::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
// fins()
*/
fins::fins()
{
}


/* ============================================================================
// postConstructor()
*/
void fins::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType fins::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * fins::creator()
{
    return new fins();
}


/* ============================================================================
// ~fins()
*/
fins::~fins()
{
}


