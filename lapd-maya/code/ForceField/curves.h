/* ============================================================================
// forceField : curves.h
//
// Copyright (C) 2007 Jim Bamber 
//
*/
#ifndef CURVES_H
#define CURVES_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>


class curves : public MPxNode
{
    public:

    curves();
	virtual void postConstructor();
    virtual ~curves();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static int calculateCurve(
		MPoint &startPoint,
		float stepDistance,
		int maxSteps,
		bool reverseField,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MVectorArray& vectorDataGrid,
		MPointArray& cvs,
		MDoubleArray& knots,
		MVectorArray& tangents );

	// Input attributes
	static MObject aInputMesh;			// For the starting points of the field lines (mesh).
	static MObject aStepDistance;		// Control points of the NURBS curves will be separated by this distance (float).
	static MObject aMaxSteps;			// Normally, the field line tracing will stop when the line exits the data volume; this
										//   gives the calculation an ultimate limit (int).
	static MObject aReverseField;		// Gives the ability to trace the field lines in both directions (bool).
	static MObject aSpatialGrid;		// The spatial grid of the vector field below (vector array).  Note that this is not used currently.
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aVectorDataGrid;		// This supplies the vectors of the basic force field (vector array).

	// Output attributes
	static MObject aCurveCount;			// Number of curves in the output array.
    static MObject aOutputCurves;		// The calculated field line curves (array of NURBS curves).
	static MObject aStartIndices;		// Used to index into the control vertex and tangent arrays (int array).
	static MObject aCVCounts;			// Number of CV's for each curve (int array), for example:
										//   index = startIndices[curveNum] + cvIndex, where cvIndex = {0:CVCounts[curveNum]-1}
	static MObject aControlVertexList;	// Control vertices for curves (point array).
	static MObject aTangentList;		// Tangents for curves (vector array).
};


#endif

