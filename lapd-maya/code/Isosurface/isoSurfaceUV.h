/* ============================================================================
// Isosurface : isoSurfaceUV.h
//
// Copyright (C) 2013 Jim Bamber 
//
*/
#ifndef ISOSURFACEUV_H
#define ISOSURFACEUV_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class isoSurfaceUV : public MPxNode
{
    public:

    isoSurfaceUV();
	virtual void postConstructor();
    virtual ~isoSurfaceUV();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MStatus createMesh(
		int UVMode,
		double isoValue,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MDoubleArray& scalarDataGrid,
		double dataMin,
		double dataMax,
		MObject& newOutputData );

	static MStatus assignUVs(
		MFnMesh& fnMesh,
		int UVMode,
		double scaledIsoValue,
		float3& gridMin,
		float3& gridMax,
		MPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );

	static MStatus marchingCube(
		double isoValue,
		MIntArray& vertexLookup,
		MPointArray& vertices,
		MIntArray& vertexIndices,
		MVectorArray& normals,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices,
		int ix, int iy, int iz,
		int nx, int ny, int nz,
		float x, float y, float z,
		float dx, float dy, float dz,
		float3& gridMin, float3& gridMax,
		MDoubleArray& scalarDataGrid );

	// Input attributes
	static MObject aUVMode;				// Determines what kind of UV's to assign, 0=="data-based" or 1=="geometry-based" (enum).
	static MObject aIsoValue;			// Isosurface contouring value (double).
										// Spatial grid is assumed to be uniform => is not used as an input.
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aScalarDataGrid;		// This supplies the scalars for the secondary mesh (scalar array).
	static MObject aDataMin;			// Minimum value of scalar data grid (double).
	static MObject aDataMax;			// Maximum value of scalar data grid (double).

	// Output attributes
	static MObject aOutputMesh;		// The newly created mesh.

	// Convenience arrays
	static int vertexOffsets[8][3];
	static int edgeConnections[12][2];
	static int edgeDirections[12][3];
	static bool edgeDuplicates[8][12];
	static int edgeOffsets[8][12][4];

	// Marching cubes tables
	static int cubeEdgeFlags[256];
	static int triangleConnectionTable[256][16];

};


#endif

