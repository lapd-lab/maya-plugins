/* ============================================================================
 * lapdCommon : lapdCommon.cpp
 /-
 / ==========================================================================
 / Copyright (C) 2007 Jim Bamber
 /
 / ==========================================================================
 /+
 */
#include "lapdCommon.h"

#include <math.h>
#include <stdlib.h>
#include <time.h>

#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MDoubleArray.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>

// 2012-03-03 JB: commented this out as part of the Microsoft "safe" functions nonsense
//#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1  // causes functions like strcpy() to be replaced with more secure versions

char lapdCommon::logFilename[DI_LOG_FILENAME_LENGTH] = "";
FILE* lapdCommon::logFile = NULL;
char lapdCommon::logString[DI_LOG_STRING_LENGTH] = "";
int lapdCommon::day_of_month = 0;
MString lapdCommon::version = "version_2.6";


/* ============================================================================
 * interpolateScalar()
 */
double lapdCommon::interpolateScalar(
	MPoint& point,
	int3& dims,
	float3& gridMax,
	float3& gridMin,
	MDoubleArray& scalarDataGrid,
	double halfWindow )
{
	int nx = dims[0];
	int ny = dims[1];
	int nz = dims[2];
	if ( (nx<=1) || (ny<=1) || (nz<=1) ) return 0.0;

	// distances between grid points
	float xmin = gridMin[0];
	float ymin = gridMin[1];
	float zmin = gridMin[2];
	float dx = (gridMax[0]-xmin) / (nx-1);
	float dy = (gridMax[1]-ymin) / (ny-1);
	float dz = (gridMax[2]-zmin) / (nz-1);

	// central point
	float x0 = (float)point[0];
	float y0 = (float)point[1];
	float z0 = (float)point[2];

	// floating point indices of central point
	float fx0 = (x0-xmin) / dx;
	float fy0 = (y0-ymin) / dy;
	float fz0 = (z0-zmin) / dz;

	// number of steps to either side, based on smoothing radius
	int ixStart = (int)floor( fx0 - halfWindow );  if ( ixStart < 0  ) ixStart = 0;
	int iyStart = (int)floor( fy0 - halfWindow );  if ( iyStart < 0  ) iyStart = 0;
	int izStart = (int)floor( fz0 - halfWindow );  if ( izStart < 0  ) izStart = 0;
	int ixEnd   = (int)ceil( fx0 + halfWindow );   if ( ixEnd > nx-1 ) ixEnd   = nx-1;
	int iyEnd   = (int)ceil( fy0 + halfWindow );   if ( iyEnd > ny-1 ) iyEnd   = ny-1;
	int izEnd   = (int)ceil( fz0 + halfWindow );   if ( izEnd > nz-1 ) izEnd   = nz-1;

	// loop through the bounding "cube", total up values within the window
	int nplane = ny * nx;
	int iplane, irow;
	int iz, iy, ix;
	float rz, ry, rx;
	float rz2, ry2, rx2;
	float rz2_ry2;
	float r2;
	float limit = (float)(halfWindow * halfWindow);
	double weight;
	double weightedData = 0.0;
	double totalWeight = 0.0;
	double scalarDatum;
	unsigned int index;
	for ( iz=izStart; iz<=izEnd; iz++ )
	{
		rz = (float)iz - fz0;
		rz2 = rz * rz;
		iplane = iz * nplane;
		for ( iy=iyStart; iy<=iyEnd; iy++ )
		{
			ry = (float)iy - fy0;
			ry2 = ry * ry;
			rz2_ry2 = rz2 + ry2;
			irow = iplane + iy * nx;
			for ( ix=ixStart; ix<=ixEnd; ix++ )
			{
				rx = (float)ix - fx0;
				rx2 = rx * rx;
				r2 = rz2_ry2 + rx2;
				index = irow + ix;
				if ( r2 < limit )
				{
					weight = limit - r2;
					scalarDatum = scalarDataGrid[index];
					weightedData = weightedData + weight*scalarDatum;
					totalWeight = totalWeight + weight;
				} // end if within the smoothing radius
			} // end x loop
		} // end y loop
	} // end z loop

	if ( totalWeight > 0.0 )
		weightedData = weightedData/totalWeight;
	else
		weightedData = 0.0;

	return weightedData;
}


/* ============================================================================
 * interpolateVector()
 */
MVector lapdCommon::interpolateVector(
	MPoint& point,
	int3& dims,
	float3& gridMax,
	float3& gridMin,
	MVectorArray& vectorDataGrid,
	double halfWindow )
{
	MVector weightedVector( 0.0, 0.0, 0.0 );

	int nx = dims[0];
	int ny = dims[1];
	int nz = dims[2];
	if ( (nx<=1) || (ny<=1) || (nz<=1) ) return weightedVector;

	// distances between grid points
	float xmin = gridMin[0];
	float ymin = gridMin[1];
	float zmin = gridMin[2];
	float dx = (gridMax[0]-xmin) / (nx-1);
	float dy = (gridMax[1]-ymin) / (ny-1);
	float dz = (gridMax[2]-zmin) / (nz-1);

	// central point
	float x0 = (float)point.x;
	float y0 = (float)point.y;
	float z0 = (float)point.z;

	// floating point indices of central point
	float fx0 = (x0-xmin) / dx;
	float fy0 = (y0-ymin) / dy;
	float fz0 = (z0-zmin) / dz;

	// number of steps to either side, based on smoothing radius
	int ixStart = (int)floor( fx0 - halfWindow );  if ( ixStart < 0  ) ixStart = 0;
	int iyStart = (int)floor( fy0 - halfWindow );  if ( iyStart < 0  ) iyStart = 0;
	int izStart = (int)floor( fz0 - halfWindow );  if ( izStart < 0  ) izStart = 0;
	int ixEnd   = (int)ceil( fx0 + halfWindow );   if ( ixEnd > nx-1 ) ixEnd   = nx-1;
	int iyEnd   = (int)ceil( fy0 + halfWindow );   if ( iyEnd > ny-1 ) iyEnd   = ny-1;
	int izEnd   = (int)ceil( fz0 + halfWindow );   if ( izEnd > nz-1 ) izEnd   = nz-1;

	// loop through the bounding "cube", total up values within the window
	int nplane = ny * nx;
	int iplane, irow, index;
	int iz, iy, ix;
	float rz, ry, rx;
	float rz2, ry2, rx2;
	float rz2_ry2;
	float r2;
	float limit = (float)(halfWindow * halfWindow);
	float weight;
	float totalWeight = 0.0;

	for ( iz=izStart; iz<=izEnd; iz++ )
	{
		rz = (float)iz - fz0;
		rz2 = rz * rz;
		iplane = iz * nplane;
		for ( iy=iyStart; iy<=iyEnd; iy++ )
		{
			ry = (float)iy - fy0;
			ry2 = ry * ry;
			rz2_ry2 = rz2 + ry2;
			irow = iplane + iy * nx;
			for ( ix=ixStart; ix<=ixEnd; ix++ )
			{
				rx = (float)ix - fx0;
				rx2 = rx * rx;
				r2 = rz2_ry2 + rx2;
				index = irow + ix;
				if ( r2 < limit )
				{
					weight = limit - r2;
					weightedVector = weightedVector + weight*vectorDataGrid[index];
					totalWeight = totalWeight + weight;
				} // end if within the smoothing radius
			} // end x loop
		} // end y loop
	} // end z loop

	// Calculate force vector, use "magnitude" as a multiplicative factor
	if ( totalWeight > 0.0 )
		weightedVector = weightedVector/totalWeight;
	else
		weightedVector = MVector(0.0, 0.0, 0.0);

	return weightedVector;
}


/* ============================================================================
 * logEntry()
 */
void lapdCommon::logEntry(
	char* logString )
{
	/*
	// Check logging flag
	//  -if logging is not "On", return.
	//
	errno_t err;
	char* mayaLogging = NULL;
	err = _dupenv_s( &mayaLogging, NULL, "MAYA_LOGGING" );  if ( err ) return;  if ( mayaLogging == NULL ) return;
	if ( strcmp( mayaLogging, "On" ) != 0 ) { free( mayaLogging ); return; }
	else free( mayaLogging );


	// Get the current date and time
	//
	time_t rawtime;
	time ( &rawtime );
	struct tm timeinfo;
	err = localtime_s ( &timeinfo, &rawtime );  if ( err ) return;

	int year = timeinfo.tm_year+1900;
	int month = timeinfo.tm_mon+1;
	int day = timeinfo.tm_mday;
	int hour = timeinfo.tm_hour;
	int min = timeinfo.tm_min;
	int sec = timeinfo.tm_sec;


	// If logFilename is blank or day has changed or file handle is NULL, try to open new logfile
	//
	if ( (strcmp(logFilename, "") == 0) || (day_of_month != day) || (logFile == NULL) )
	{
		day_of_month = day;
		char* mayaLoggingHome = NULL;
		err = _dupenv_s( &mayaLoggingHome, NULL, "MAYA_LOGGING_HOME" );  if ( err ) return;  if ( mayaLoggingHome == NULL ) return;

		// Make a new folder for the day
		char folderName[1000];
		sprintf_s( folderName, 1000, "%s\\%4d-%02d-%02d", mayaLoggingHome, year, month, day );
		CreateDirectory(folderName, NULL);  // no error checking here, it is handled by checking logFile != NULL below

		// Now open the new logfile
		sprintf_s( logFilename, DI_LOG_FILENAME_LENGTH, "%s\\plug-in error log %4d-%02d-%02d.txt", folderName, year, month, day );
		if ( logFile != NULL ) fclose( logFile );
		err = fopen_s( &logFile, logFilename, "a" );  if ( err ) return;

		// Do not delete this cout as Windows may need the delay in order to get its act together
		cout << hour << ":" << min << ":" << sec << "  lapdCommon::logEntry() opening " << logFilename << endl;

		if ( logFile != NULL )
		{
			fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
			fprintf( logFile, "================================\n" );
			fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
			fprintf( logFile, "plug-in error log %4d-%02d-%02d opened\n", year, month, day );
			fflush( logFile );
		}
		else
		{
			cout << "Failed to open " << logFilename << endl;
		}

		free( mayaLoggingHome );
	}


	// Write out the log string
	//
	if ( logFile != NULL )
	{
		fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
		fprintf( logFile, "%s\n", logString );
		fflush( logFile );
	}
	*/
}


