/* ==========================================================================
// HDF5 : HDF5vectorShadingTable.h
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#ifndef HDF5VECTORSHADINGTABLE_H
#define HDF5VECTORSHADINGTABLE_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h> 

class HDF5vectorShadingTable : public MPxNode
{
public:
    MPxNode::SchedulingType schedulingType() const;

	virtual MStatus compute( const MPlug& plug, MDataBlock& data );

    HDF5vectorShadingTable();
	virtual void postConstructor();
    virtual ~HDF5vectorShadingTable();

	static  void *creator();
	static  MStatus initialize();

public:
	static MObject filename;
	static MObject groupName;
	static MObject tableName;
	static MObject vectorShadingTable;

	static	MTypeId	id;

private:
	MStatus computeKernel( const MPlug& plug, MDataBlock& data );
};

#endif
