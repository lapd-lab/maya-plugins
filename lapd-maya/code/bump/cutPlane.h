/* ============================================================================
// bump : cutPlane.h
//
// Copyright (C) 2008 Jim Bamber 
//
*/
#ifndef CUTPLANE_H
#define CUTPLANE_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MFnMesh.h>


class cutPlane : public MPxNode
{
    public:

    cutPlane();
	virtual void postConstructor();
    virtual ~cutPlane();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MStatus calculateCutPlane(
		double width,
		double height,
		double thickness,
		int widthSubdivisions,
		int heightSubdivisions,
		MPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );

	// Input attributes
	//static MObject aWidth;				// Width of the cut plane == x dimension (double).
	//static MObject aHeight;				// Height of the cut plane == y dimension (double).
	//static MObject aThickness;			// Thickness of the cut plane == z dimensions (double).
	static MObject aWidthSubdivisions;	// Number of width subdivisions (int).
	static MObject aHeightSubdivisions;	// Number of height subdivisions (int).

	// Output attributes
	static MObject aOutputMesh;			// The newly created mesh (mesh).
};


#endif

