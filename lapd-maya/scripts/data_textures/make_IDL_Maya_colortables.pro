; 3dv_to_mayahdf5
; As the name suggests, this program will translate
; a Vincena 3DV-format binary datafile into
; The Vincena-Bamber standard HDF5 format designed
; for use with our Autodesk Maya Scientific Data Import Plugin
;
; Note: n-dimensional arrays of vectors are handled
; as n-dimensional arrays of compound datatype in HDF5
; which may be handled by using structures of n-dim arrays in IDL.
;
;Author: Stephen Vincena
;Original draft date: 01/20/2007  
;Adapted to the new specification of Feb 22, 2007
;
;
;

filepath='./IDL_color_tables.hdf5'
spawn,'rm '+ filepath
HDF5_file = OBJ_NEW('HDF5_file')
HDF5_file->Create,filepath

;Create the 'Color tables' group
ct_group=HDF5_file->Create_group('Color tables')
; no attributes
;attributes_group->Write_attribute,'Version','2007-02-22'

;Create the spatial grid dataset
;ct_source_struct = {ct_struct,X:float(0),Y:float(0),Z:float(0)}

;ct_struct_array = replicate(ct_source_struct,l1,l2,l3)
;ct_struct_array[*,*,*].X = reform(ct[0,*,*,*])
;ct_struct_array[*,*,*].Y = reform(ct[1,*,*,*])
;ct_struct_array[*,*,*].Z = reform(ct[2,*,*,*])
;HDF5_file->Write_dataset,'Rainbow',ct_struct_array


;ct_source_struct = {ct_struct,RGB:fltarr(256,3)}
;ct_group->Write_dataset,'Rainbow',ct_source_struct

;ct_group->Write_attribute,'Type','3-vector'

ct_source_struct = {ct_struct,r:float(0),g:float(0),b:float(0)}
ct_struct_array = replicate(ct_source_struct,256)


rgb_table=bytarr(256,3)
colortable_names=strarr(41)
loadct,0,get_names=colortable_names
;Note: LOADCT doesn't seem to let you get both names and values at the same time!,/silent

;remove '/' characters from names since they are path separators in HDF
for i=0,n_elements(colortable_names)-1 do begin
 temp = byte(colortable_names[i])
 slashes = where(temp eq 47)
 if slashes[0] NE -1 then begin
  temp(slashes) = 45
  colortable_names[i] = string(temp) 
 endif
endfor

for i=0,n_elements(colortable_names)-1 do begin
;for i=34,34 do begin ;0,n_elements(colortable_names)-1 do begin
 loadct,i,rgb_table=rgb_table
 colortable_name=colortable_names[i]

 ct_struct_array[*].r = float(rgb_table[*,0])/255.
 ct_struct_array[*].g = float(rgb_table[*,1])/255.
 ct_struct_array[*].b = float(rgb_table[*,2])/255.
 ct_group->Write_Maya_color_table,colortable_name,ct_struct_array

endfor



Cleanup:

;OBJ_DESTROY,ct_group
;HDF5_file->Close
OBJ_DESTROY,HDF5_file


end
