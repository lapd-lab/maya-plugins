/* ==========================================================================
// Texture 3D : vectorShadeLookup.cpp
//-
// ==========================================================================
// Copyright (C) Jim Bamber
//
// ==========================================================================
//+
*/
#include "vectorShadeLookup.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "texture3Dcommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
//#include <maya/MFnPlugin.h> -- this turns the piece of code into the plug-in main
#include <maya/MFnVectorArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>


// Static data
MTypeId vectorShadeLookup::id( 0x00401 );

// Attributes
MObject vectorShadeLookup::scaledDatum;
MObject vectorShadeLookup::dataMinMapsTo;
MObject vectorShadeLookup::dataMaxMapsTo;
MObject vectorShadeLookup::vectorShadingTable;
MObject vectorShadeLookup::defaultVectorShade;
 
MObject vectorShadeLookup::vectorShade;


/* ==========================================================================
// postConstructor()
*/
void vectorShadeLookup::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType vectorShadeLookup::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// vectorShadeLookup()
*/
vectorShadeLookup::vectorShadeLookup()
{
}


/* ==========================================================================
// ~vectorShadeLookup()
*/
vectorShadeLookup::~vectorShadeLookup()
{
}


/* ==========================================================================
// creator()
//
// creates an instance of the node
*/
void * vectorShadeLookup::creator()
{
    return new vectorShadeLookup();
}


/* ==========================================================================
// initialize()
//
// initializes attribute information
*/
MStatus vectorShadeLookup::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnVectorArrayData vectorArrayData;		
	MStatus stat, stat2;

	texture3Dcommon::logEntry( (char*) "--> vectorShadeLookup::initialize()" );


	// Create input attributes:
	//
	// scaledDatum = -1.0   default is the flag value indicating that the point is outside the data volume
	scaledDatum = nAttr.create( "scaledDatum", "scaled", MFnNumericData::kFloat, 0.0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( nAttr.setDefault(-1.0) );

	// dataMinMapsTo = 0.0
	dataMinMapsTo = nAttr.create( "dataMinMapsTo", "minmap", MFnNumericData::kFloat, 0.0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( nAttr.setDefault(0.0) );

	// dataMaxMapsTo = 1.0
	dataMaxMapsTo = nAttr.create( "dataMaxMapsTo", "maxmap", MFnNumericData::kFloat, 0.0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( nAttr.setDefault(1.0) );

	// vectorShadingTable
	vectorShadingTable = tAttr.create(
	  "vectorShadingTable", "table", MFnData::kVectorArray, vectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );

	// defaultVectorShade = (0.0, 0.0, 0.0)
    defaultVectorShade = nAttr.createColor( "defaultVectorShade", "default", &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );


    // Create output attributes
	//
	// vectorShade
    vectorShade = nAttr.createColor( "vectorShade", "out", &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( nAttr );


    // Add the attributes
	//
    diLOG_MSTATUS( addAttribute(scaledDatum) );
    diLOG_MSTATUS( addAttribute(dataMinMapsTo) );
    diLOG_MSTATUS( addAttribute(dataMaxMapsTo) );
    diLOG_MSTATUS( addAttribute(vectorShadingTable) );
	diLOG_MSTATUS( addAttribute(defaultVectorShade) );

    diLOG_MSTATUS( addAttribute(vectorShade) );


    // All inputs affect the output shade
	//
    diLOG_MSTATUS( attributeAffects(scaledDatum, vectorShade) );
    diLOG_MSTATUS( attributeAffects(dataMinMapsTo, vectorShade) );
    diLOG_MSTATUS( attributeAffects(dataMaxMapsTo, vectorShade) );
    diLOG_MSTATUS( attributeAffects(vectorShadingTable, vectorShade) );
    diLOG_MSTATUS( attributeAffects(defaultVectorShade, vectorShade) );

	texture3Dcommon::logEntry( (char*) "<-- vectorShadeLookup::initialize()" );
    return MS::kSuccess;
}


/* ==========================================================================
// lookup()
*/
MFloatVector vectorShadeLookup::lookup(
	float mScaledDatum,
	float mDataMinMapsTo,
	float mDataMaxMapsTo,
	MFnVectorArrayData& mVectorShadingTable,
	float3& mDefaultVectorShade )
{
	MFloatVector resultVector( mDefaultVectorShade );

	if ( (mScaledDatum>=0.0) && (mScaledDatum<=1.0) )
	{
		// mScaledDatum is in range so proceed...
		int nVectors = mVectorShadingTable.length();
		if ( nVectors > 0)
		{
			// mVectorShadingTable is not zero length, so do the lookup
			float mMappedDatum = mDataMinMapsTo + mScaledDatum * ( mDataMaxMapsTo-mDataMinMapsTo );
			//if ( mMappedDatum < 0.0 ) mMappedDatum = 0.0;
			//if ( mMappedDatum > 1.0 ) mMappedDatum = 1.0;
			int index = (int)floor( mMappedDatum * (float)(nVectors-1) );
			if ( index < 0 ) index = 0;
			if ( index > (nVectors-1) ) index = nVectors - 1;
			MVector vectorElement( mVectorShadingTable[index] );
			resultVector = MFloatVector( vectorElement );
		}
	}

	return resultVector;
}


/* ==========================================================================
// compute()
//
// This function gets called by Maya to evaluate the texture.
//
*/
MStatus vectorShadeLookup::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	texture3Dcommon::logEntry( texture3Dcommon::eVectorShadeLookup );

	// looking for vectorShade
    if ( (plug != vectorShade) && (plug.parent() != vectorShade) )
	{
		texture3Dcommon::logEntry(
			(char*) "Abnormal exit from vectorShadeLookup::compute(), Maya not asking for vector shade" );
		return MS::kUnknownParameter;
	}

	MDataHandle scaledDatumHandle( block.inputValue(scaledDatum, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle dataMinMapsToHandle( block.inputValue(dataMinMapsTo, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle dataMaxMapsToHandle( block.inputValue(dataMaxMapsTo, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle vectorShadingTableHandle( block.inputValue(vectorShadingTable, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle defaultVectorShadeHandle( block.inputValue(defaultVectorShade, &stat) );  diLOG_MSTATUS( stat );

	float mScaledDatum = scaledDatumHandle.asFloat();
	float mDataMinMapsTo = dataMinMapsToHandle.asFloat();
	float mDataMaxMapsTo = dataMaxMapsToHandle.asFloat();
	MFnVectorArrayData mVectorShadingTable( vectorShadingTableHandle.data() );
	float3& mDefaultVectorShade = defaultVectorShadeHandle.asFloat3();

	//texture3Dcommon::logEntry( (char*) "About to do vector shade lookup" );
	MFloatVector resultVectorShade = lookup(
		mScaledDatum,
		mDataMinMapsTo,
		mDataMaxMapsTo,
		mVectorShadingTable,
		mDefaultVectorShade );

	// Set output color attribute
	MDataHandle vectorShadeHandle( block.outputValue(vectorShade) );
	MFloatVector& mVectorShade( vectorShadeHandle.asFloatVector() );
	mVectorShade = resultVectorShade;
	vectorShadeHandle.setClean();

	//texture3Dcommon::logEntry( (char*) "<-- vectorShadeLookup::compute()" );
    return MS::kSuccess;
}
