/* ==========================================================================
// HDF5 : HDF5scalarDataGrid.cpp
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "HDF5common.h"
#include "errorHandling.h"
#include "HDF5scalarDataGrid.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MArrayDataBuilder.h>
#include <maya/MGlobal.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MDoubleArray.h>
//#include <maya/MFloatVector.h>

#include <H5public.h>
#include <H5Apublic.h>
#include <H5Fpublic.h>
#include <H5Gpublic.h>
#include <H5Ppublic.h>
#include <H5Spublic.h>
#include <H5Tpublic.h>

MTypeId  HDF5scalarDataGrid::id( 0x00335 );


// HDF5scalarDataGrid has these attributes:
// Input
//   filename
//   group name
//   time index
//   parameter1-4 indices
//
//   multiplier
//   offset
//
MObject  HDF5scalarDataGrid::filename;        
MObject  HDF5scalarDataGrid::groupName;

MObject  HDF5scalarDataGrid::timeIndex;

MObject  HDF5scalarDataGrid::parameter1Index;        
MObject  HDF5scalarDataGrid::parameter2Index;        
MObject  HDF5scalarDataGrid::parameter3Index;        
MObject  HDF5scalarDataGrid::parameter4Index; 

MObject  HDF5scalarDataGrid::aMultiplier;        
MObject  HDF5scalarDataGrid::aOffset;        


// Output
//   3D volume of scalar data
//   max and min data values
//
MObject  HDF5scalarDataGrid::scalarDataGrid;
MObject  HDF5scalarDataGrid::max;
MObject  HDF5scalarDataGrid::min;


/* ==========================================================================
// compute()
*/
MStatus HDF5scalarDataGrid::compute( const MPlug& plug, MDataBlock& data )
{
	//HDF5common::logEntry( (char*) "--> HDF5scalarDataGrid::compute()" );
	HDF5common::logEntry( HDF5common::eHDF5scalarDataGrid );
	MStatus stat;
 
	if ( ( plug == scalarDataGrid ) || ( plug.parent() == scalarDataGrid ) ||
		 ( plug == max ) ||
		 ( plug == min ) )
	{
		stat = computeKernel( plug, data );
		data.setClean( plug );
	} 
	else 
		stat  = MS::kUnknownParameter;

	//HDF5common::logEntry( (char*) "<-- HDF5scalarDataGrid::compute()" );
	return stat;
}


/* ==========================================================================
// computeKernel()
*/
MStatus HDF5scalarDataGrid::computeKernel( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;
 
	MDataHandle scalarDataGridHandle( data.outputValue(scalarDataGrid, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle maxHandle( data.outputValue(max, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle minHandle( data.outputValue(min, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle filenameHandle( data.inputValue(filename, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle groupHandle( data.inputValue(groupName, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle timeIndexHandle( data.inputValue(timeIndex, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle parameter1IndexHandle( data.inputValue(parameter1Index, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle parameter2IndexHandle( data.inputValue(parameter2Index, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle parameter3IndexHandle( data.inputValue(parameter3Index, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle parameter4IndexHandle( data.inputValue(parameter4Index, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle multiplierHandle( data.inputValue(aMultiplier, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle offsetHandle( data.inputValue(aOffset, &stat) );  diLOG_MSTATUS( stat );

	//HDF5common::logEntry( (char*) "--> HDF5scalarDataGrid::computeKernel()" );


	// Open the file.
	//
	MString filenameStr( filenameHandle.asString() );
	if ( filenameStr == "" )
	{
		HDF5common::logEntry( (char*) "Abnormal exit from HDF5scalarDataGrid::computeKernel(), filename is blank" );
		return MStatus::kFailure;
	}
	const char *filenameAsChar = filenameStr.asChar();
	hid_t file_id = H5Fopen( filenameAsChar, H5F_ACC_RDONLY, H5P_DEFAULT );	diLOG_HDF5STAT( file_id );
	if ( file_id < 0 )
	{
		sprintf_s( HDF5common::logString, DI_LOG_STRING_LENGTH, "Could not open %s", filenameAsChar );
		HDF5common::logEntry( HDF5common::logString );
		return MStatus::kFailure;
	}

	// Read the parameter count attribute.
	//
	int parameter_count;
	hid_t attr_group_id = H5Gopen2( file_id, "/Attributes", H5P_DEFAULT );	diLOG_EXIT_HDF5STAT( attr_group_id );
	hid_t pcount_id = H5Aopen_name( attr_group_id, "Parameter count" );     diLOG_EXIT_HDF5STAT( pcount_id );
	hid_t pcount_type_id = H5Aget_type( pcount_id );					    diLOG_EXIT_HDF5STAT( pcount_type_id );

	diLOG_EXIT_HDF5STAT( H5Aread(pcount_id, pcount_type_id, &parameter_count) );
	//sprintf( HDF5common::logString, "Parameter count: %d", parameter_count );
	//HDF5common::logEntry( HDF5common::logString );

	diLOG_EXIT_HDF5STAT( H5Tclose(pcount_type_id) );
	diLOG_EXIT_HDF5STAT( H5Aclose(pcount_id) );
	diLOG_EXIT_HDF5STAT( H5Gclose(attr_group_id) );


	// Data vector should be array of floats of length nz*ny*nx with ix varying fastest (C-style)
	/*
	To read from a dataset:
	DONE -- Define memory dataspace of read. (Optional if dataspace is H5S_SELECT_ALL)
	DONE -- Open the dataset.
	DONE -- Get the dataset dataspace. (If using H5S_SELECT_ALL above)

	DONE -- Else define dataset dataspace of read. DONE -- Define the memory datatype. (Optional)
	DONE -- Define the memory buffer.
	ALREADY DONE -- Open the dataset.
	DONE -- Read data.

	DONE -- Close the datatype, dataspace, and property list. (As necessary)
	DONE -- Close the dataset.
	*/

	// Open the containing group.
	//
	MString groupStr( groupHandle.asString() );
	if ( groupStr == "" )
	{
		HDF5common::logEntry( (char*) "Abnormal exit from HDF5scalarDataGrid::computeKernel(), group name is blank" );
		diLOG_HDF5STAT( H5Fclose(file_id) );
		return MStatus::kFailure;
	}
	const char *groupAsChar = groupStr.asChar();
	hid_t group_id = H5Gopen2( file_id, groupAsChar, H5P_DEFAULT );   diLOG_EXIT_HDF5STAT( group_id );

	// Check that the data type is scalar.
	//
	hid_t data_type_id = H5Aopen_name( group_id, "Type" );   diLOG_EXIT_HDF5STAT( data_type_id );
	hid_t data_type_type_id = H5Aget_type( data_type_id );	 diLOG_EXIT_HDF5STAT( data_type_type_id );

	char data_type[120];
	diLOG_EXIT_HDF5STAT( H5Aread(data_type_id, data_type_type_id, data_type) );
	diLOG_EXIT_HDF5STAT( H5Tclose(data_type_type_id) );
	diLOG_EXIT_HDF5STAT( H5Aclose(data_type_id) );

	if ( MString(data_type) != "Scalar" ) 
	{
		HDF5common::logEntry( (char*) "Abnormal exit from HDF5scalarDataGrid::computeKernel(), data type is not scalar" );
		diLOG_HDF5STAT( H5Gclose(group_id) );
		diLOG_HDF5STAT( H5Fclose(file_id) );
		return MStatus::kFailure;
	}

	// Read the symbol attributes.
	//
	hid_t symbol_id = H5Aopen_name( group_id, "Symbol" );   diLOG_EXIT_HDF5STAT( symbol_id );
	hid_t symbol_type_id = H5Aget_type( symbol_id );		diLOG_EXIT_HDF5STAT( symbol_type_id );

	char data_symbol[120];
	diLOG_EXIT_HDF5STAT( H5Aread(symbol_id, symbol_type_id, data_symbol) );
	diLOG_EXIT_HDF5STAT( H5Tclose(symbol_type_id) );
	diLOG_EXIT_HDF5STAT( H5Aclose(symbol_id) );

	// Construct the dataset name.
	//
	MString datasetName(data_symbol);
	if ( parameter_count >= 1 )	datasetName = datasetName + "." + parameter1IndexHandle.asInt();
	if ( parameter_count >= 2 )	datasetName = datasetName + "." + parameter2IndexHandle.asInt();
	if ( parameter_count >= 3 )	datasetName = datasetName + "." + parameter3IndexHandle.asInt();
	if ( parameter_count == 4 )	datasetName = datasetName + "." + parameter4IndexHandle.asInt();
	const char *datasetAsChar = datasetName.asChar();
	//sprintf( HDF5common::logString, "Dataset name: %s", datasetAsChar );
	//HDF5common::logEntry( HDF5common::logString );

	// Open the dataset.
	//
	hid_t dataset_id = H5Dopen2( group_id, datasetAsChar, H5P_DEFAULT );	diLOG_EXIT_HDF5STAT( dataset_id );
	hid_t space_id = H5Dget_space( dataset_id );							diLOG_EXIT_HDF5STAT( space_id );
	hsize_t current_dims[4];
	hsize_t max_dims[4];
	int ndims = H5Sget_simple_extent_dims( space_id, current_dims, max_dims );   diLOG_EXIT_HDF5STAT( ndims );
	unsigned long nt = (unsigned long)current_dims[0];
	unsigned long nz = (unsigned long)current_dims[1];
	unsigned long ny = (unsigned long)current_dims[2];
	unsigned long nx = (unsigned long)current_dims[3];
	unsigned long npoints = nz * ny * nx;
	//sprintf( HDF5common::logString, "nt: %d  nz: %d  ny: %d  nx: %d", nt, nz, ny, nx );
	//HDF5common::logEntry( HDF5common::logString );

	// Prepare to read the dataset.  First allocate memory.
	//
	float *mem_buffer = (float *)malloc( npoints*sizeof(float) );
	//sprintf( HDF5common::logString, "npoints: %d", npoints );
	//HDF5common::logEntry( HDF5common::logString );

	// Define memory dataspace.
	//
	hsize_t mem_dims[4];
	mem_dims[0] = 1;
	mem_dims[1] = nz;
	mem_dims[2] = ny;
	mem_dims[3] = nx;
	hid_t memspace_id = H5Screate_simple( 4, mem_dims, mem_dims );   diLOG_EXIT_HDF5STAT( memspace_id );

	hsize_t start[4];
	hsize_t end[4];
	diLOG_EXIT_HDF5STAT( H5Sget_select_bounds(space_id, start, end) );
	//sprintf( HDF5common::logString, "start[0:3]: %d %d %d %d", start[0], start[1], start[2], start[3] );
	//HDF5common::logEntry( HDF5common::logString );
	//sprintf( HDF5common::logString, "end[0:3]: %d %d %d %d", end[0], end[1], end[2], end[3] );
	//HDF5common::logEntry( HDF5common::logString );

	// Define filespace hyperslab.
	//
	start[0] = timeIndexHandle.asInt();
	start[1] = 0;
	start[2] = 0;
	start[3] = 0;

	hsize_t stride[4];
	stride[0] = 1;
	stride[1] = 1;
	stride[2] = 1;
	stride[3] = 1;

	hsize_t count[4];
	count[0] = 1;
	count[1] = nz;
	count[2] = ny;
	count[3] = nx;

	hsize_t block[4];
	block[0] = 1;
	block[1] = 1;
	block[2] = 1;
	block[3] = 1;

	diLOG_EXIT_HDF5STAT( H5Sselect_hyperslab(space_id, H5S_SELECT_SET, start, stride, count, block) );
	htri_t valid_select = H5Sselect_valid( space_id );   diLOG_HDF5STAT( valid_select );
	diLOG_EXIT_BOOLSTAT( (valid_select > 0) );
	diLOG_EXIT_HDF5STAT( H5Sget_select_bounds(space_id, start, end) );
	//sprintf( HDF5common::logString, "start[0:3]: %d %d %d %d", start[0], start[1], start[2], start[3] );
	//HDF5common::logEntry( HDF5common::logString );
	//sprintf( HDF5common::logString, "end[0:3]: %d %d %d %d", end[0], end[1], end[2], end[3] );
	//HDF5common::logEntry( HDF5common::logString );

	// Read the data.
	//
	//HDF5common::logEntry( (char*) "About to call H5Dread()" );
	diLOG_EXIT_HDF5STAT( H5Dread(dataset_id, H5T_NATIVE_FLOAT, memspace_id, space_id, H5P_DEFAULT, mem_buffer) );
	float Data_item = (*mem_buffer);
	//sprintf( HDF5common::logString, "Data_item: %f", Data_item );
	//HDF5common::logEntry( HDF5common::logString );

	// Close the ids.
	//
	diLOG_EXIT_HDF5STAT( H5Sclose(space_id) );
	diLOG_EXIT_HDF5STAT( H5Sclose(memspace_id) );
	diLOG_EXIT_HDF5STAT( H5Dclose(dataset_id) );
	diLOG_EXIT_HDF5STAT( H5Gclose(group_id) );
	diLOG_EXIT_HDF5STAT( H5Fclose(file_id) );

	// Determine the case:
	// case 0 == no scaling, no offset
	// case 1 == no scaling, offset
	// case 2 == scaling, no offset
	// case 3 == scaling, offset
	//
	int caseNumber = 0;
	float multiplier = multiplierHandle.asFloat();
	float offset = offsetHandle.asFloat();
	float epsilon = (float)1.0e-10;
	if ( (offset < -epsilon) || (offset > epsilon) ) caseNumber = caseNumber + 1;
	if ( (multiplier < 1.0-epsilon) || (multiplier > 1.0+epsilon) ) caseNumber = caseNumber + 2;

	// Build the array attribute.
	//
	MFnDoubleArrayData mScalarDataGrid( scalarDataGridHandle.data() );
	MDoubleArray doubleArray;
	double element;
	float floatElement;
	float Data_max, Data_min;

	if ( caseNumber == 0 )
	{
		// case 0 == no scaling, no offset
        floatElement = (*mem_buffer);
		for (int i=0; i<(int)npoints; i++)
		{
			element = (double)floatElement;
			if ( i==0 )
			{
				Data_max = floatElement;
				Data_min = floatElement;
			}
			if ( floatElement > Data_max ) Data_max = floatElement;
			if ( floatElement < Data_min ) Data_min = floatElement;
			doubleArray.append( element );
			floatElement = (*mem_buffer++);
		}
	}

	if ( caseNumber == 1 )
	{
		// case 1 == no scaling, offset
        floatElement = (*mem_buffer);
		for (int i=0; i<(int)npoints; i++)
		{
			floatElement = floatElement + offset;
			element = (double)floatElement;
			if ( i==0 )
			{
				Data_max = floatElement;
				Data_min = floatElement;
			}
			if ( floatElement > Data_max ) Data_max = floatElement;
			if ( floatElement < Data_min ) Data_min = floatElement;
			doubleArray.append( element );
            floatElement = (*mem_buffer++);
		}
	}

	if ( caseNumber == 2 )
	{
		// case 2 == scaling, no offset
        floatElement = (*mem_buffer);
		for (int i=0; i<(int)npoints; i++)
		{
			floatElement = floatElement * multiplier;
			element = (double)floatElement;
			if ( i==0 )
			{
				Data_max = floatElement;
				Data_min = floatElement;
			}
			if ( floatElement > Data_max ) Data_max = floatElement;
			if ( floatElement < Data_min ) Data_min = floatElement;
			doubleArray.append( element );
            floatElement = (*mem_buffer++);
		}
	}

	if ( caseNumber == 3 )
	{
		// case 3 == scaling, offset
        floatElement = (*mem_buffer);
		for (int i=0; i<(int)npoints; i++)
		{
			floatElement = ( floatElement * multiplier ) + offset;
			element = (double)floatElement;
			if ( i==0 )
			{
				Data_max = floatElement;
				Data_min = floatElement;
			}
			if ( floatElement > Data_max ) Data_max = floatElement;
			if ( floatElement < Data_min ) Data_min = floatElement;
			doubleArray.append( element );
            floatElement = (*mem_buffer++);
		}
	}

	diLOG_EXIT_MSTATUS( mScalarDataGrid.set(doubleArray) );
	scalarDataGridHandle.setClean();

	float& mMax = maxHandle.asFloat();
	mMax = Data_max;
	maxHandle.setClean();

	float& mMin = minHandle.asFloat();
	mMin = Data_min;
	minHandle.setClean();

	//HDF5common::logEntry( (char*) "<-- HDF5scalarDataGrid::computeKernel()" );
	return MS::kSuccess;
}


/* ==========================================================================
// HDF5scalarDataGrid()
*/
HDF5scalarDataGrid::HDF5scalarDataGrid()
{
}


/* ==========================================================================
// postConstructor()
*/
void HDF5scalarDataGrid::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType HDF5scalarDataGrid::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// ~HDF5scalarDataGrid()
*/
HDF5scalarDataGrid::~HDF5scalarDataGrid()
{
}


/* ==========================================================================
// creator()
*/
void *HDF5scalarDataGrid::creator()
{
	return new HDF5scalarDataGrid();
}


/* ==========================================================================
// initialize()
*/
MStatus HDF5scalarDataGrid::initialize()
{
	MStatus				 stat, stat2;
	MFnTypedAttribute	 typedAttr;
	MFnNumericAttribute	 numericAttr;
	MFnCompoundAttribute compAttr;
	MFnStringData		 stringData;
	MFnDoubleArrayData	 doubleArrayData;		

	HDF5common::logEntry( (char*) "--> HDF5scalarDataGrid::initialize()" );


	// HDF5scalarDataGrid has these attributes:
	// Input
	//
	// filename
    filename = typedAttr.create( "filename", "file", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

	// group name
    groupName = typedAttr.create( "group", "group", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

	// time index
	timeIndex = numericAttr.create( "timeIndex", "tIndex", MFnNumericData::kInt, 0, &stat );
	diLOG_MSTATUS( stat );

	// parameter1-4 indices
	parameter1Index = numericAttr.create( "parameter1Index", "p1Index", MFnNumericData::kInt, 0, &stat );
	diLOG_MSTATUS( stat );
	parameter2Index = numericAttr.create( "parameter2Index", "p2Index", MFnNumericData::kInt, 0, &stat );
	diLOG_MSTATUS( stat );
	parameter3Index = numericAttr.create( "parameter3Index", "p3Index", MFnNumericData::kInt, 0, &stat );
	diLOG_MSTATUS( stat );
	parameter4Index = numericAttr.create( "parameter4Index", "p4Index", MFnNumericData::kInt, 0, &stat );
	diLOG_MSTATUS( stat );

	// multiplier
	aMultiplier = numericAttr.create( "multiplier", "multiplier", MFnNumericData::kFloat, 1.0, &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( numericAttr.setDefault(1.0) );

	// offset
	aOffset = numericAttr.create( "offset", "offset", MFnNumericData::kFloat, 0.0, &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( numericAttr.setDefault(0.0) );


	// Output
	//
	// 3D volume of scalar data
    scalarDataGrid = typedAttr.create( "scalarDataGrid", "sData", MFnData::kDoubleArray, doubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

	// max
	max = numericAttr.create( "maximum", "max", MFnNumericData::kFloat, 0.0, &stat );
	diLOG_MSTATUS( stat );

	// min
	min = numericAttr.create( "minimum", "min", MFnNumericData::kFloat, 0.0, &stat );
	diLOG_MSTATUS( stat );


	// Add the attributes to the node
	diLOG_MSTATUS( addAttribute(filename) );
	diLOG_MSTATUS( addAttribute(groupName) );
	diLOG_MSTATUS( addAttribute(timeIndex) );
	diLOG_MSTATUS( addAttribute(parameter1Index) );
	diLOG_MSTATUS( addAttribute(parameter2Index) );
	diLOG_MSTATUS( addAttribute(parameter3Index) );
	diLOG_MSTATUS( addAttribute(parameter4Index) );
	diLOG_MSTATUS( addAttribute(aMultiplier) );
	diLOG_MSTATUS( addAttribute(aOffset) );
	diLOG_MSTATUS( addAttribute(scalarDataGrid) );
	diLOG_MSTATUS( addAttribute(max) );
	diLOG_MSTATUS( addAttribute(min) );


	// Define the attribute dependencies
	diLOG_MSTATUS( attributeAffects(filename, scalarDataGrid) );
	diLOG_MSTATUS( attributeAffects(groupName, scalarDataGrid) );
	diLOG_MSTATUS( attributeAffects(timeIndex, scalarDataGrid) );
	diLOG_MSTATUS( attributeAffects(parameter1Index, scalarDataGrid) );
	diLOG_MSTATUS( attributeAffects(parameter2Index, scalarDataGrid) );
	diLOG_MSTATUS( attributeAffects(parameter3Index, scalarDataGrid) );
	diLOG_MSTATUS( attributeAffects(parameter4Index, scalarDataGrid) );
	diLOG_MSTATUS( attributeAffects(aMultiplier, scalarDataGrid) );
	diLOG_MSTATUS( attributeAffects(aOffset, scalarDataGrid) );

	diLOG_MSTATUS( attributeAffects(filename, max) );
	diLOG_MSTATUS( attributeAffects(groupName, max) );
	diLOG_MSTATUS( attributeAffects(timeIndex, max) );
	diLOG_MSTATUS( attributeAffects(parameter1Index, max) );
	diLOG_MSTATUS( attributeAffects(parameter2Index, max) );
	diLOG_MSTATUS( attributeAffects(parameter3Index, max) );
	diLOG_MSTATUS( attributeAffects(parameter4Index, max) );
	diLOG_MSTATUS( attributeAffects(aMultiplier, max) );
	diLOG_MSTATUS( attributeAffects(aOffset, max) );

	diLOG_MSTATUS( attributeAffects(filename, min) );
	diLOG_MSTATUS( attributeAffects(groupName, min) );
	diLOG_MSTATUS( attributeAffects(timeIndex, min) );
	diLOG_MSTATUS( attributeAffects(parameter1Index, min) );
	diLOG_MSTATUS( attributeAffects(parameter2Index, min) );
	diLOG_MSTATUS( attributeAffects(parameter3Index, min) );
	diLOG_MSTATUS( attributeAffects(parameter4Index, min) );
	diLOG_MSTATUS( attributeAffects(aMultiplier, min) );
	diLOG_MSTATUS( attributeAffects(aOffset, min) );

	HDF5common::logEntry( (char*) "<-- HDF5scalarDataGrid::initialize()" );
	return MS::kSuccess;
}
