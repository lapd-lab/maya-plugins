/* ============================================================================
// bump : meshReplicator.cpp
//-
// ==========================================================================
// Copyright (C) 2007 Jim Bamber
//
// ==========================================================================
//+
*/
#include "meshReplicator.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "bumpCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshEdge.h>


// Static data
MTypeId meshReplicator::id( 0x00502 );

// Attributes
MObject meshReplicator::aInputMesh;
MObject meshReplicator::aNx;
MObject meshReplicator::aNy;
MObject meshReplicator::aNz;
MObject meshReplicator::aDx;
MObject meshReplicator::aDy;
MObject meshReplicator::aDz;

MObject meshReplicator::aOutputMesh;


/* ============================================================================
 * replicateMesh()
 */
MStatus meshReplicator::replicateMesh(
	MPointArray& inVertices,
	MIntArray& inPolygonVertexCounts,
	MIntArray& inPolygonVertexIndices,
	int nx, int ny, int nz,
	double dx, double dy, double dz,
	MPointArray& outVertices,
	MIntArray& outPolygonVertexCounts,
	MIntArray& outPolygonVertexIndices )
{
	MStatus stat;

	unsigned int inVertexCount = inVertices.length();
	unsigned int inPolygonCount = inPolygonVertexCounts.length();
	unsigned int inPolygonVertexCount = inPolygonVertexIndices.length();
	//sprintf( bumpCommon::logString, "inVertexCount: %d  inPolygonCount: %d  inPolygonVertexCount: %d",
	//	inVertexCount, inPolygonCount, inPolygonVertexCount );
	//bumpCommon::logEntry( bumpCommon::logString );

	MVector translation;
	MPoint point;
	int vertexIndex, baseVertexIndex;
	if ( nx < 1 ) nx = 1;
	if ( ny < 1 ) ny = 1;
	if ( nz < 1 ) nz = 1;

	// Replication loops
	for ( int ix=0; ix<nx; ix++ )
		for ( int iy=0; iy<ny; iy++ )
			for ( int iz=0; iz<nz; iz++ )
			{
				// Replicate vertices (i.e. mesh points)
				translation = MVector( ix*dx, iy*dy, iz*dz );
				baseVertexIndex = outVertices.length();
				for ( int i=0; i<(int)inVertexCount; i++ )
				{
					point = inVertices[i] + translation;
					outVertices.append( point );
				}

				// Replicate polygon connection information
				for ( int i=0; i<(int)inPolygonCount; i++ ) outPolygonVertexCounts.append( inPolygonVertexCounts[i] );
				for ( int i=0; i<(int)inPolygonVertexCount; i++ )
				{
					vertexIndex = baseVertexIndex + inPolygonVertexIndices[i];
					outPolygonVertexIndices.append( vertexIndex );
				}
			}

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the curves.
//
*/
MStatus meshReplicator::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//bumpCommon::logEntry( "--> meshReplicator::compute()" );
	bumpCommon::logEntry( bumpCommon::eMeshReplicator );
	cout << "meshReplicator::compute()" << endl;

	if ( (plug != aOutputMesh) )
	{
		bumpCommon::logEntry( (char*) "Abnormal exit from meshReplicator::compute(), Maya not asking for output mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hInputMesh( block.inputValue(aInputMesh, &stat) );   diLOG_MSTATUS( stat );
	MObject inMesh( hInputMesh.asMeshTransformed() );
	MFnMesh fnInputMesh( inMesh );

	MDataHandle hNx( block.inputValue(aNx, &stat) );   diLOG_MSTATUS( stat );
	int nx = hNx.asInt();

	MDataHandle hNy( block.inputValue(aNy, &stat) );   diLOG_MSTATUS( stat );
	int ny = hNy.asInt();

	MDataHandle hNz( block.inputValue(aNz, &stat) );   diLOG_MSTATUS( stat );
	int nz = hNz.asInt();

	MDataHandle hDx( block.inputValue(aDx, &stat) );   diLOG_MSTATUS( stat );
	double dx = hDx.asDouble();

	MDataHandle hDy( block.inputValue(aDy, &stat) );   diLOG_MSTATUS( stat );
	double dy = hDy.asDouble();

	MDataHandle hDz( block.inputValue(aDz, &stat) );   diLOG_MSTATUS( stat );
	double dz = hDz.asDouble();


	// Now do the output attributes.
	//
	MDataHandle hOutputMesh( block.outputValue(aOutputMesh, &stat) );   diLOG_MSTATUS( stat );


	// Create the data object which underlies the output mesh
	//
	MFnMesh fnMesh;
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );
	MPointArray inVertices;
	MIntArray inPolygonVertexCounts;
	MIntArray inPolygonVertexIndices;
	MPointArray outVertices;
	MIntArray outPolygonVertexCounts;
	MIntArray outPolygonVertexIndices;

	diLOG_MSTATUS( fnInputMesh.getPoints(inVertices, MSpace::kWorld) );
	diLOG_MSTATUS( fnInputMesh.getVertices(inPolygonVertexCounts, inPolygonVertexIndices) );


	// Make the call to replicate the input mesh and create the output mesh
	//
	//bumpCommon::logEntry( "About to replicate mesh" );
	diLOG_MSTATUS(
		replicateMesh(inVertices, inPolygonVertexCounts, inPolygonVertexIndices,
			nx, ny, nz, dx, dy, dz,
			outVertices, outPolygonVertexCounts, outPolygonVertexIndices) );

	int outVertexCount = outVertices.length();
	int outPolygonCount = outPolygonVertexCounts.length();
	//sprintf( bumpCommon::logString, "outVertexCount: %d  outPolygonCount: %d", outVertexCount, outPolygonCount );
	//bumpCommon::logEntry( bumpCommon::logString );

	// Single call here to create the mesh
	fnMesh.create(
		outVertexCount,
		outPolygonCount, 
		outVertices,
		outPolygonVertexCounts,
		outPolygonVertexIndices,
		newOutputData, 
		&stat );   diLOG_MSTATUS( stat );


	// Set the outputs as clean.
	hOutputMesh.set( newOutputData );
	block.setClean( plug );

	//bumpCommon::logEntry( "<-- meshReplicator::compute()" );
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information
*/
MStatus meshReplicator::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	bumpCommon::logEntry( (char*) "--> meshReplicator::initialize()" );


	// Create input attributes:
	//
	// input mesh
	aInputMesh = tAttr.create( "inputMesh", "inMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aInputMesh) );

	// nx
	aNx = nAttr.create( "nx", "nx", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aNx) );

	// ny
	aNy = nAttr.create( "ny", "ny", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aNy) );

	// nz
	aNz = nAttr.create( "nz", "nz", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aNz) );

	// dx
	aDx = nAttr.create( "dx", "dx", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDx) );

	// dy
	aDy = nAttr.create( "dy", "dy", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDy) );

	// dz
	aDz = nAttr.create( "dz", "dz", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDz) );


	// Create output attributes
	//
	// output mesh
	aOutputMesh = tAttr.create( "outputMesh", "outMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aOutputMesh) );


	// All inputs affect all outputs
	//
	// output mesh
    diLOG_MSTATUS( attributeAffects(aInputMesh, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aNx, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aNy, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aNz, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDx, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDy, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDz, aOutputMesh) );

	bumpCommon::logEntry( (char*) "<-- meshReplicator::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
 * postConstructor()
 */
void meshReplicator::postConstructor( )
{
}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType meshReplicator::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
 * meshReplicator()
 */
meshReplicator::meshReplicator()
{
}


/* ============================================================================
 * ~meshReplicator()
 */
meshReplicator::~meshReplicator()
{
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * meshReplicator::creator()
{
    return new meshReplicator();
}


