/* ============================================================================
// bump : cutPlane.cpp
//-
// ==========================================================================
// Copyright (C) 2008 Jim Bamber
//
// ==========================================================================
//+
*/
#include "cutPlane.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "bumpCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MQuaternion.h>


// Static data
MTypeId cutPlane::id( 0x00503 );

// Attributes
//MObject cutPlane::aWidth;
//MObject cutPlane::aHeight;
//MObject cutPlane::aThickness;
MObject cutPlane::aWidthSubdivisions;
MObject cutPlane::aHeightSubdivisions;

MObject cutPlane::aOutputMesh;


/* ============================================================================
 * calculateCutPlane()
 */
MStatus cutPlane::calculateCutPlane(
		double width,
		double height,
		double thickness,
		int widthSubdivisions,
		int heightSubdivisions,
		MPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices )
{
	MStatus stat;
	double x, y, z;
	double xStart, yStart, zStart;
	double xStep, yStep, zStep;
	MPoint point;


	// First, the vertices
	//
	xStart = -0.5 * width;
	yStart = -0.5 * height;

	// Back face, then front face
	zStart = -0.5 * thickness;
	xStep  =  width / (double)widthSubdivisions;
	yStep  =  height / (double)heightSubdivisions;
	zStep  =  thickness;
	for ( int iz=0; iz < 2; iz++ )
	{
		z = zStart + (double)iz*zStep;
		for ( int iy=0; iy < heightSubdivisions+1; iy++ )  // Note that n subdivisions gives n+1 lines
		{
			y = yStart + (double)iy*yStep;
			for ( int ix=0; ix < widthSubdivisions+1; ix++ )
			{
				x = xStart + (double)ix*xStep;
				point = MPoint( x, y, z );
				vertices.append( point );
			}
		}
	}

	// Bottom edge, then top edge
	zStart = -0.5 * thickness + 0.01;
	xStep  =  width / (double)widthSubdivisions;
	yStep  =  height;
	zStep  =  thickness - 0.02;
	for ( int iy=0; iy < 2; iy++ )
	{
		y = yStart + (double)iy*yStep;
		for ( int ix=0; ix < widthSubdivisions+1; ix++ )
		{
			x = xStart + (double)ix*xStep;
			for ( int iz=0; iz < 2; iz++ )
			{
				z = zStart + (double)iz*zStep;
				point = MPoint( x, y, z );
				vertices.append( point );
			}
		}
	}

	// Left edge, then right edge
	zStart = -0.5 * thickness + 0.01;
	xStep  =  width;
	yStep  =  height / (double)heightSubdivisions;
	zStep  =  thickness - 0.02;
	for ( int ix=0; ix < 2; ix++ )
	{
		x = xStart + (double)ix*xStep;
		for ( int iy=0; iy < heightSubdivisions+1; iy++ )
		{
			y = yStart + (double)iy*yStep;
			for ( int iz=0; iz < 2; iz++ )
			{
				z = zStart + (double)iz*zStep;
				point = MPoint( x, y, z );
				vertices.append( point );
			}
		}
	}


	// Then the connectivity
	//
	int base0, base1, base2;
	int vertexIndexBase;
	int iRow = widthSubdivisions + 1;
	int iColumn = heightSubdivisions + 1;

	// Back face, then front face
	base0 = 0;
	for ( int iz=0; iz < 2; iz++ )
	{
		base1 = base0 + iz * iColumn * iRow;
		for ( int iy=0; iy < heightSubdivisions; iy++ )
		{
			base2 = base1 + iy * iRow;
			for ( int ix=0; ix < widthSubdivisions; ix++ )
			{
				vertexIndexBase = base2 + ix;
				polygonVertexCounts.append(4);
				polygonVertexIndices.append( vertexIndexBase        + 0 );
				polygonVertexIndices.append( vertexIndexBase        + 1 );
				polygonVertexIndices.append( vertexIndexBase + iRow + 1 );
				polygonVertexIndices.append( vertexIndexBase + iRow + 0 );
			}
		}
	}

	// Bottom edge, then top edge
	base0 = 2 * iColumn * iRow;
	for ( int iy=0; iy < 2; iy++ )
	{
		base1 = base0 + iy * 2 * iRow;
		for ( int ix=0; ix < widthSubdivisions; ix++ )
		{
			base2 = base1 + ix * 2;
			for ( int iz=0; iz < 1; iz++ )
			{
				vertexIndexBase = base2 + iz;
				polygonVertexCounts.append(4);
				polygonVertexIndices.append( vertexIndexBase     + 0 );
				polygonVertexIndices.append( vertexIndexBase     + 1 );
				polygonVertexIndices.append( vertexIndexBase + 2 + 1 );
				polygonVertexIndices.append( vertexIndexBase + 2 + 0 );
			}
		}
	}

	// Left edge, then right edge
	base0 = 2*iColumn*iRow + 2*2*iRow;
	for ( int ix=0; ix < 2; ix++ )
	{
		base1 = base0 + ix * 2 * iColumn;
		for ( int iy=0; iy < heightSubdivisions; iy++ )
		{
			base2 = base1 + iy * 2;
			for ( int iz=0; iz < 1; iz++ )
			{
				vertexIndexBase = base2 + iz;
				polygonVertexCounts.append(4);
				polygonVertexIndices.append( vertexIndexBase     + 0 );
				polygonVertexIndices.append( vertexIndexBase     + 1 );
				polygonVertexIndices.append( vertexIndexBase + 2 + 1 );
				polygonVertexIndices.append( vertexIndexBase + 2 + 0 );
			}
		}
	}


	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the cut plane.
//
*/
MStatus cutPlane::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//bumpCommon::logEntry( "--> cutPlane::compute()" );
	bumpCommon::logEntry( bumpCommon::eCutPlane );

	if ( (plug != aOutputMesh) )
	{
		bumpCommon::logEntry( (char*) "Abnormal exit from cutPlane::compute(), Maya not asking for output mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	//MDataHandle hWidth = block.inputValue( aWidth, &stat );   diLOG_MSTATUS( stat );
	//double width = hWidth.asDouble();
	double width = 1.0;

	//MDataHandle hHeight = block.inputValue( aHeight, &stat );   diLOG_MSTATUS( stat );
	//double height = hHeight.asDouble();
	double height = 1.0;

	//MDataHandle hThickness = block.inputValue( aThickness, &stat );   diLOG_MSTATUS( stat );
	//double thickness = hThickness.asDouble();
	double thickness = 1.0;

	MDataHandle hWidthSubdivisions( block.inputValue(aWidthSubdivisions, &stat) );   diLOG_MSTATUS( stat );
	int widthSubdivisions = hWidthSubdivisions.asInt();

	MDataHandle hHeightSubdivisions( block.inputValue(aHeightSubdivisions, &stat) );   diLOG_MSTATUS( stat );
	int heightSubdivisions = hHeightSubdivisions.asInt();


	// Now do the output attributes, starting with the easy one: curveCount.
	//
	MDataHandle hOutputMesh( block.outputValue(aOutputMesh, &stat) );   diLOG_MSTATUS( stat );


	// Create the data object which underlies the output mesh
	//
	MFnMesh fnMesh;
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );
	MPointArray vertices;
	MIntArray polygonVertexCounts;
	MIntArray polygonVertexIndices;


	// Now create the cut plane
	//
	//bumpCommon::logEntry( "About to create cut plane" );
	calculateCutPlane(
		width,
		height,
		thickness,
		widthSubdivisions,
		heightSubdivisions,
		vertices,
		polygonVertexCounts,
		polygonVertexIndices );

	int vertexCount = vertices.length();
	int polygonCount = polygonVertexCounts.length();
	//sprintf( bumpCommon::logString, "vertexCount: %d  polygonCount: %d", vertexCount, polygonCount );
	//bumpCommon::logEntry( bumpCommon::logString );

	// Single call here to create the mesh
	fnMesh.create(
		vertexCount,
		polygonCount, 
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		newOutputData, 
		&stat );   diLOG_MSTATUS( stat );

	// Set the outputs as clean.
	hOutputMesh.set( newOutputData );
	block.setClean( plug );

	//bumpCommon::logEntry( "<-- cutPlane::compute()" );
	return MStatus::kSuccess;
}


/* ============================================================================
// initialize()
//
// initializes attribute information
*/
MStatus cutPlane::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	bumpCommon::logEntry( (char*) "--> cutPlane::initialize()" );


	// Create input attributes:
	//
	//// width
	//aWidth = nAttr.create( "width", "width", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	//diLOG_MSTATUS( nAttr.setDefault(10.0) );
	//MAKE_INPUT( nAttr );
    //diLOG_MSTATUS( addAttribute(aWidth) );

	//// height
	//aHeight = nAttr.create( "height", "height", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	//diLOG_MSTATUS( nAttr.setDefault(10.0) );
	//MAKE_INPUT( nAttr );
    //diLOG_MSTATUS( addAttribute(aHeight) );

	//// thickness
	//aThickness = nAttr.create( "thickness", "thickness", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	//diLOG_MSTATUS( nAttr.setDefault(0.5) );
	//MAKE_INPUT( nAttr );
    //diLOG_MSTATUS( addAttribute(aThickness) );

	// width subdivisions
	aWidthSubdivisions = nAttr.create( "widthSubdivisions", "widthSubdivisions", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(10) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aWidthSubdivisions) );

	// height subdivisions
	aHeightSubdivisions = nAttr.create( "heightSubdivisions", "heightSubdivisions", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(10) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aHeightSubdivisions) );


	// Create output attributes
	//
	// secondary mesh
	aOutputMesh = tAttr.create( "outputMesh", "outputMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aOutputMesh) );


	// All inputs affect all outputs
	//
	// curve count
    //diLOG_MSTATUS( attributeAffects(aWidth, aOutputMesh) );
    //diLOG_MSTATUS( attributeAffects(aHeight, aOutputMesh) );
    //diLOG_MSTATUS( attributeAffects(aThickness, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aWidthSubdivisions, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aHeightSubdivisions, aOutputMesh) );

	bumpCommon::logEntry( (char*) "<-- cutPlane::initialize()" );
    return MS::kSuccess;
}


/* ============================================================================
 * postConstructor()
 */
void cutPlane::postConstructor( )
{
}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType cutPlane::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
 * cutPlane()
 */
cutPlane::cutPlane()
{
}


/* ============================================================================
 * ~cutPlane()
 */
cutPlane::~cutPlane()
{
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * cutPlane::creator()
{
    return new cutPlane();
}


