/* ============================================================================
// forceField : linTrans.h
//
// Copyright (C) 2012 Jim Bamber 
//
*/
#ifndef LINTRANS_H
#define LINTRANS_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class linTrans : public MPxNode
{
    public:

    linTrans();
	virtual void postConstructor();
    virtual ~linTrans();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	// Input attributes
	static MObject aM;				// Slope (float).
	static MObject aB;				// X-intercept (float)

	static MObject aInputMode;		// enum: 0 - scalar, 1 - RGB
	static MObject aInputScalar;	// scalar input (float).
	static MObject aInputRGB;		// RGB input (MFloatVector).
	static MObject aSecondaryData;	// scaled secondary data point (float)

	// Output attributes
									// input * secondaryData * m + b  ->  output
	static MObject aOutputScalar;	// Scalar output (float); (inputMode = RGB) -> (outputScalar = |outputRGB|).
	static MObject aOutputRGB;		// RGB output (MFloatVector); (inputMode = scalar) -> (outputRGB[0,1,2] = outputScalar).
};


#endif

