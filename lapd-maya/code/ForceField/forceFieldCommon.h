/* ============================================================================
// forceField : forceFieldCommon.h
//
// Copyright (C) 2011 Jim Bamber 
//
*/
#ifndef FORCEFIELDCOMMON_H
#define FORCEFIELDCOMMON_H

#include <iostream> // STV Dec 2015
#include <maya/MIOStream.h>
#include <maya/MVectorArray.h>
#include <maya/MPoint.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>

#include "../lapdCommon/defines.h"


class forceFieldCommon
{
    public:

	enum nodeNameEnum
	{
		eOther,
		eCircles,
		eCurveDissecter,
		eCurves,
		eEnvelopes,
		eFieldTracer,
		eFins,
		eLorentzForceField,
		eMarkers,
		eSquares,
		eTube,
		eTubeUV
	};

	static void logEntry(
		char *logString );

	static void logEntry(
		nodeNameEnum nodeName );

	static char logString[DI_LOG_STRING_LENGTH];

	private:

	static char logFilename[DI_LOG_FILENAME_LENGTH];
	static FILE* logFile;
    static int currentDay;
    static time_t currentRawTime;
	static int nodeCount;              // must match the number of entries in the nodeNameEnum
	static int callCounters[12];       // ""
	static char* nodeNameStrings[12];  // ""
	static int logTimestep;

};


#endif

