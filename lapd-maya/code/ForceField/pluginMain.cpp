/* ============================================================================
// forceField : pluginMain.cpp
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include <maya/MIOStream.h> // STV Dec 2015
#include "fieldTracer.h"
#include "lorentzForceField.h"
#include "curves.h"
#include "curveDissecter.h"
#include "circles.h"
#include "squares.h"
#include "fins.h"
#include "envelopes.h"
#include "tube.h"
#include "tubeUV.h"
#include "markers.h"
#include "linTrans.h"
#include "tubesUV.h"
#include "secondaryData.h"
#include "secondaryData2.h"
#include <maya/MFnPlugin.h>
#include <maya/MPxNode.h>
#include <maya/MGlobal.h>


/* ============================================================================
// initializePlugin()
*/
MStatus initializePlugin( MObject obj )
{
    MFnPlugin plugin( obj, "Jim Bamber", "1.0", "Any");
	MGlobal::displayInfo( " " );
	MGlobal::displayInfo( "plugin: forceField  " + lapdCommon::version );
	MGlobal::displayInfo( "================================" );

	MGlobal::displayInfo( "plugin.registerNode(fieldTracer)" );
	diLOG_MSTATUS ( plugin.registerNode( "fieldTracer", fieldTracer::id,
							&fieldTracer::creator, &fieldTracer::initialize,
							MPxNode::kFieldNode ) );

	MGlobal::displayInfo( "plugin.registerNode(lorentzForceField)" );
	diLOG_MSTATUS ( plugin.registerNode( "lorentzForceField", lorentzForceField::id,
							&lorentzForceField::creator, &lorentzForceField::initialize,
							MPxNode::kFieldNode ) );

	MGlobal::displayInfo( "plugin.registerNode(curves)" );
	diLOG_MSTATUS ( plugin.registerNode( "curves", curves::id, 
                         &curves::creator, &curves::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(curveDissecter)" );
	diLOG_MSTATUS ( plugin.registerNode( "curveDissecter", curveDissecter::id, 
                         &curveDissecter::creator, &curveDissecter::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(circles)" );
	diLOG_MSTATUS ( plugin.registerNode( "circles", circles::id, 
                         &circles::creator, &circles::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(squares)" );
	diLOG_MSTATUS ( plugin.registerNode( "squares", squares::id, 
                         &squares::creator, &squares::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(fins)" );
	diLOG_MSTATUS ( plugin.registerNode( "fins", fins::id, 
                         &fins::creator, &fins::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(envelopes)" );
	diLOG_MSTATUS ( plugin.registerNode( "envelopes", envelopes::id, 
                         &envelopes::creator, &envelopes::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(tube)" );
	diLOG_MSTATUS ( plugin.registerNode( "tube", tube::id, 
                         &tube::creator, &tube::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(tubeUV)" );
	diLOG_MSTATUS ( plugin.registerNode( "tubeUV", tubeUV::id, 
                         &tubeUV::creator, &tubeUV::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(markers)" );
	diLOG_MSTATUS ( plugin.registerNode( "markers", markers::id, 
                         &markers::creator, &markers::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(linTrans)" );
	diLOG_MSTATUS ( plugin.registerNode( "linTrans", linTrans::id, 
                         &linTrans::creator, &linTrans::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(tubesUV)" );
	diLOG_MSTATUS ( plugin.registerNode( "tubesUV", tubesUV::id, 
                         &tubesUV::creator, &tubesUV::initialize ) );

	MGlobal::displayInfo( "plugin.registerNode(secondaryData)" );
    const MString Texture3D( "texture/3d" );
	diLOG_MSTATUS ( plugin.registerNode( "secondaryData", secondaryData::id, 
                         &secondaryData::creator, &secondaryData::initialize, MPxNode::kDependNode, &Texture3D ) );

	MGlobal::displayInfo( "plugin.registerNode(secondaryData2)" );
	diLOG_MSTATUS ( plugin.registerNode( "secondaryData2", secondaryData2::id, 
                         &secondaryData2::creator, &secondaryData2::initialize, MPxNode::kDependNode, &Texture3D ) );

    return MS::kSuccess;
}


/* ============================================================================
// uninitializePlugin()
*/
MStatus uninitializePlugin( MObject obj )
{
    MFnPlugin plugin( obj );
	diLOG_MSTATUS ( plugin.deregisterNode( fieldTracer::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( lorentzForceField::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( curves::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( curveDissecter::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( circles::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( squares::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( fins::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( envelopes::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( tube::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( tubeUV::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( markers::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( linTrans::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( tubesUV::id ) );
	diLOG_MSTATUS ( plugin.deregisterNode( secondaryData::id ) );

    return MS::kSuccess;
}


