/* ============================================================================
// forceField : tube.h
//
// Copyright (C) 2008 Jim Bamber 
//
*/
#ifndef TUBE_H
#define TUBE_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class tube : public MPxNode
{
    public:

    tube();
	virtual void postConstructor();
    virtual ~tube();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	static MPointArray unitCircle;

    static MStatus createMesh(
		int curveCount,
		MIntArray startIndices,
		MIntArray CVCounts,
		MPointArray controlVertexList,
		MVectorArray tangentList,
		int curveIndex,
		int segmentCount,
		double tubeRadius,
		MObject& newOutputData );

	static MStatus addSection(
		MFnMesh& fnMesh,
		MObject& newOutputData,
		MPoint controlVertex1,
		MPoint controlVertex2,
		MVector tangent1,
		MVector tangent2,
		double tubeRadius,
		int segmentCount );

	// Input attributes
	static MObject aCurveCount;			// Number of curves in the input array.
	static MObject aStartIndices;		// Used to index into the control vertex and tangent arrays (int array).
	static MObject aCVCounts;			// Number of CV's for each curve (int array), for example:
										//   index = startIndices[curveIndex] + cvIndex, where cvIndex = {0:CVCounts[curveIndex]-1}
	static MObject aControlVertexList;	// Control vertices for curves (point array).
	static MObject aTangentList;		// Tangents for curves (vector array).
	static MObject aCurveIndex;			// Index of curve to use as basis for output tube (int, 0-based).
	static MObject aTubeRadius;			// Radius of field line tube in cm (double).
	static MObject aSegmentCount;		// Number of segments in the circle.

	// Output attributes
	static MObject aSecondaryMesh;		// The newly created mesh.
};


#endif

