// DISTRIBUTE MAGNETS (PROC)

global proc distributeMagnets(
  float $z_start,
  float $z_end,
  float $theta_start,
  float $theta_end)
{

  // Purple magnets

  if (($z_end > 304.0) && ($z_start < 1360.0))
  {
    purpleMagnet("purpleMagnet", $theta_start, $theta_end);

    // Get the first purple pair into position
    select -r purpleMagnet;
    sets -add lapdSet;
    int $skipped = 0;
    $z_offset_init = 320.0 - 16.0 + 4.2;
    while ($z_offset_init < $z_start)
    {
      $z_offset_init = $z_offset_init + 32.0;
      $skipped = $skipped + 1;
    }
    move -r 0.0 0.0 $z_offset_init;

    instance;
    sets -add lapdSet;
    $x_offset = -0.3;
    $y_offset =  0.2;
    float $z_offset = -8.4 - 0.1;
    move -r $x_offset $y_offset $z_offset;

    // (33-$skipped) more purple pairs, going toward the cathode
    int $i;
    for ($i=0; $i<(33-$skipped); $i=$i+1)
    {
      $z_offset = ($i+1)*32.0;
      if (($z_offset_init + $z_offset) < $z_end)
      {
        instanceMagnetPair("purpleMagnet", $z_offset);
      }
    }
  } // end if purple magnets are in range


  // Yellow magnets (anode end)

  int $yellowMagnetExists = 0;
  int $skipped = 0;
  if (($z_end > -16.0) && ($z_start < 272.0))
  {
    yellowMagnet("yellowMagnetAnode", $theta_start, $theta_end);
    $yellowMagnetExists = 1;

    // Get the first yellow magnet into position
    select -r yellowMagnetAnode;
    sets -add lapdSet;
    $z_offset_init = 9*32.0 - 16.0;
    $z_offset = $z_offset_init;
    while ($z_offset > $z_end)
    {
      $z_offset = $z_offset - 32.0;
      $skipped = $skipped + 1;
    }
    move -r 0.0 0.0 $z_offset;

    // (9-$skipped) more yellow magnets, going away from the cathode
    for ($i=0; $i<(9-$skipped); $i=$i+1)
    {
      $z_offset = -($i+1)*32.0;
      if (($z_offset_init + $z_offset) > $z_start)
      {
        instanceMagnet("yellowMagnetAnode", $z_offset);
      }
    }
  }

  // double up last magnet
  if (($z_end > -30.9) && ($z_start < -30.9))
  {
    if ($yellowMagnetExists)
    {
      $z_offset = -(9-$skipped)*32.0 - 14.9;
      instanceMagnet("yellowMagnetAnode", $z_offset);
    } // end if yellow magnet already exists
    else
    {
      yellowMagnet("yellowMagnetAnode", $theta_start, $theta_end);
      select -r yellowMagnetAnode;
      sets -add lapdSet;
      $z_offset = -30.9;
      move -r 0.0 0.0 $z_offset;
    } // end else yellow magnet must be created
  } // end if last yellow magnet (anode end) is in range



  //Yellow magnets (cathode end)
  $yellowMagnetExists = 0;
  if (($z_end > 1392.0) && ($z_start < 1680.0))
  {
    yellowMagnet("yellowMagnetCathode", $theta_start, $theta_end);
    $yellowMagnetExists = 1;
    select -r yellowMagnetCathode;
    sets -add lapdSet;
    $z_offset_init = 1392.0;

    // Get the first yellow magnet into position
    $z_offset = $z_offset_init;
    $skipped=0;
    while ($z_offset < $z_start)
    {
      $z_offset = $z_offset + 32.0;
      $skipped = $skipped + 1;
    }
    move -r 0.0 0.0 $z_offset;

    // now (9-$skipped) more yellow magnets on the cathode side of the purple magnet pairs
    for ($i=0; $i<(9-$skipped); $i=$i+1)
    {
      $z_offset = ($i+1)*32.0;
      if (($z_offset_init + $z_offset) < $z_end)
      {
        instanceMagnet("yellowMagnetCathode", $z_offset);
      }
    }
  }

  // double up last magnet
  if (($z_end > 1694.9) && ($z_start < 1694.9))
  {
    if ($yellowMagnetExists)
    {
      $z_offset = (9-$skipped)*32.0 + 14.9;
      instanceMagnet("yellowMagnetCathode", $z_offset);
    } // end if yellow magnet already exists
    else
    {
      yellowMagnet("yellowMagnetCathode", $theta_start, $theta_end);
      select -r yellowMagnetCathode;
      sets -add lapdSet;
      $z_offset = 1694.9;
      move -r 0.0 0.0 $z_offset;
    } // end else yellow magnet must be created
  } // end if last yellow magnet (cathode end) is in range
}
