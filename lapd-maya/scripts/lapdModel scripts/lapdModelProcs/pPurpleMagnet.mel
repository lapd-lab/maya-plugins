// PURPLE MAGNET (PROC)
// *** call any time

global proc purpleMagnet(
  string $baseName,
  float $theta_start,
  float $theta_end)
{
  global string $swatchHome;

  string $surfaceNode  = $baseName;
  string $surfaceNodeA = $baseName + "A";
  string $surfaceNodeB = $baseName + "B";
  string $shapeNode  = $baseName +  "Shape";
  string $shapeNodeA = $baseName + "AShape";
  string $shapeNodeB = $baseName + "BShape";

  curve -d 1
  -p 55.0 0.0  4.2
  -p 75.0 0.0  4.2
  -p 75.0 0.0 -4.2
  -p 55.0 0.0 -4.2
  -p 55.0 0.0  4.2
  -name magnet_curve_1;

  revolve  -ch 1 -po 0 -rn 0 -ssw (360-$theta_end) -esw (360-$theta_start) -degree 3 -s 36 -ulp 1 -ax 0 0 1 "magnet_curve_1";
  rename revolvedSurface1 $surfaceNode;
  select -r $surfaceNode ;
  disconnectAttr ($shapeNode + ".instObjGroups[0]") initialShadingGroup.dagSetMembers[0];

  if (($theta_start!=0.0) || ($theta_end!=360.0))
  {
    polyCube -name $surfaceNodeA -height 20.0 -width 8.4 -depth 0.1;
    float $angle_rad = $theta_start * 3.1415927/180.0;
    float $x = 65.0 * cos($angle_rad);
    float $y = 65.0 * sin($angle_rad);
    rotate -r -os 0.0 90.0 90.0 ;
    rotate -r -ws 0.0 0.0 $theta_start ;
    move -a $x $y 0.0;
    disconnectAttr ($shapeNodeA + ".instObjGroups[0]") initialShadingGroup.dagSetMembers[0];

    polyCube -name $surfaceNodeB -height 20.0 -width 8.4 -depth 0.1;
    $angle_rad = $theta_end * 3.1415927/180.0;
    $x = 65.0 * cos($angle_rad);
    $y = 65.0 * sin($angle_rad);
    rotate -r -os 0.0 90.0 90.0 ;
    rotate -r -ws 0.0 0.0 $theta_end ;
    move -a $x $y 0.0;
    disconnectAttr ($shapeNodeB + ".instObjGroups[0]") initialShadingGroup.dagSetMembers[0];

    select -r $surfaceNodeA $surfaceNodeB;
    select -tgl $surfaceNode;
    parent;
  }

  delete magnet_curve_1;

  string $shaderNode = $baseName + "Phong1";
  shadingNode -asShader phongE -name $shaderNode;
  sets -renderable true -noSurfaceShader true -empty -name ($shaderNode + "SG");
  connectAttr -f ($shaderNode + ".outColor") ($shaderNode + "SG.surfaceShader");
  connectAttr -f ($shapeNode +  ".instObjGroups[0]") ($shaderNode + "SG.dagSetMembers[0]");
  if (($theta_start!=0.0) || ($theta_end!=360.0))
  {
    connectAttr -f ($shapeNodeA + ".instObjGroups[0]") ($shaderNode + "SG.dagSetMembers[1]");
    connectAttr -f ($shapeNodeB + ".instObjGroups[0]") ($shaderNode + "SG.dagSetMembers[2]");
  }
  setAttr ($shaderNode + ".specularColor") -type double3 0.815806 0.926 0.880857 ;
  setAttr ($shaderNode + ".reflectivity") 0.76858;
  setAttr ($shaderNode + ".diffuse") 0.90908;
  //setAttr ($shaderNode + ".eccentricity") 0.29752;
  //setAttr ($shaderNode + ".specularRollOff") 0.70248;
  setAttr ($shaderNode + ".reflectivity") 0.2;
  setAttr ($shaderNode + ".roughness") 0.6;

  string $fileNode = $baseName + "File1";
  string $textureNode = $baseName + "Texture1";
  shadingNode -asTexture file -name $fileNode;
  shadingNode -asUtility place2dTexture -name $textureNode;
  lapdConnectTexture($textureNode, $fileNode);

  connectAttr -force ($fileNode +".outColor") ($shaderNode + ".color");
  setAttr ($fileNode + ".fileTextureName") -type "string" ($swatchHome + "/purple swatch 3.jpg");
  setAttr ($fileNode + ".colorGain") -type double3 0.69 0.69 0.69 ;

  defaultNavigation -defaultTraversal -destination ($shaderNode + ".normalCamera");

  string $brownianNode = $baseName + "Brownian1";
  string $texture3dNode = $baseName + "Texture3d1";
  string $bump3dNode = $baseName + "Bump3d1";
  shadingNode -asTexture brownian -name $brownianNode;
  shadingNode -asUtility place3dTexture -name $texture3dNode;
  connectAttr ($texture3dNode + ".wim[0]") ($brownianNode + ".pm");
  defaultNavigation -connectToExisting -destination ($shaderNode + ".normalCamera") -source $brownianNode;
  setAttr ($brownianNode + ".alphaIsLuminance") true;
  setAttr ($brownianNode + ".lacunarity") 6;
  rename bump3d1 $bump3dNode;
  setAttr ($bump3dNode + ".bumpDepth") 1.0;

  sets -add lapdSet $shaderNode ($shaderNode + "SG") $fileNode $textureNode $brownianNode $texture3dNode $bump3dNode;
}


