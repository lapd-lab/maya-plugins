/* ============================================================================
// forceField : secondaryData.h
//
// Copyright (C) 2012 Jim Bamber
//
// Cloned from C:\Program Files\Autodesk\Maya2011\devkit\plug-ins\lavaShader.cpp
//
//-
// ==========================================================================
// Copyright 1995,2006,2008 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk
// license agreement provided at the time of installation or download,
// or which otherwise accompanies this software in either electronic
// or hard copy form.
// ==========================================================================
//+
*/

#ifndef SECONDARYDATA_H
#define SECONDARYDATA_H

#include <math.h>
#include <stdlib.h>
#include <iostream> //STV Dec 2015
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatPoint.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MDoubleArray.h>
//#include <maya/MFnPlugin.h>


class secondaryData : public MPxNode
{
	public:
                    secondaryData();
    virtual         ~secondaryData();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );
	virtual void    postConstructor();

    static  void *  creator();
    static  MStatus initialize();

	//  Id tag for use with binary file format
    static  MTypeId id;

	private:

	// Input attributes
										// Spatial grid is assumed to be uniform => is not needed as an input.
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aScalarDataGrid;		// This supplies the scalars for the UV values (double array).
	static MObject aDataMin;			// Minimum value of scalar data grid (double).
	static MObject aDataMax;			// Maximum value of scalar data grid (double).

	/*
	static MObject aColorBase;
    static MObject aColorFlame;
    static MObject aDeform;
    static MObject aWarp;
    static MObject aSpeed;
    static MObject aTurbulence;
    static MObject aPower;
    static MObject aFrame;
	*/
    static MObject aPointWorld;
    static MObject aPlaceMat;

	// Output attributes
    static MObject aScaledDatum;
};

#endif
