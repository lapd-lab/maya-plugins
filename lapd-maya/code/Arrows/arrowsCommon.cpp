/* ==========================================================================
// Arrows : arrowsCommon.cpp
//-
// ==========================================================================
// Copyright (C) 2011 Jim Bamber
//
// ==========================================================================
//+
*/
#include "arrowsCommon.h"

#include <math.h>
#include <stdlib.h>
#include <time.h>
//#include <windows.h>

#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MDoubleArray.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>

// 2012-03-03 JB: commented this out as part of the Microsoft "safe" functions nonsense
//#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1  // causes functions like strcpy() to be replaced with more secure versions

char arrowsCommon::logFilename[DI_LOG_FILENAME_LENGTH] = "";
FILE* arrowsCommon::logFile = NULL;
char arrowsCommon::logString[DI_LOG_STRING_LENGTH] = "";
int arrowsCommon::nodeCount = 3;
int arrowsCommon::callCounters[3] = { 0, 0, 0 };
char* arrowsCommon::nodeNameStrings[3] = 
{
	(char*) "Other",
    (char*) "Arrows",
    (char*) "VectorShadeUVLookup"
};
int arrowsCommon::currentDay = 0;
time_t arrowsCommon::currentRawTime = 0;
int arrowsCommon::logTimestep = 1;  // seconds


/* ==========================================================================
// logEntry()
*/
void arrowsCommon::logEntry(
	char* logString )
{
	/*
	// Check logging flag
	//  -if logging is not "On", return.
	//
	errno_t err;
	char* mayaLogging = NULL;
	err = _dupenv_s( &mayaLogging, NULL, "MAYA_LOGGING" );  if ( err ) return;  if ( mayaLogging == NULL ) return;
	if ( strcmp(mayaLogging, "On") != 0 )  { free( mayaLogging ); return; }
	else free( mayaLogging );


	// Get the current date and time
	//
	time_t rawtime;
	time ( &rawtime );
	struct tm timeinfo;
	err = localtime_s ( &timeinfo, &rawtime );  if ( err ) return;

	int year = (timeinfo).tm_year+1900;
	int month = (timeinfo).tm_mon+1;
	int day = (timeinfo).tm_mday;
	int hour = (timeinfo).tm_hour;
	int min = (timeinfo).tm_min;
	int sec = (timeinfo).tm_sec;


	// If logFilename is blank or day has changed or file handle is NULL, try to open new logfile
	//
	if ( (strcmp(logFilename, "") == 0) || (currentDay != day) || (logFile == NULL) )
	{
		currentDay = day;
		char* mayaLoggingHome = NULL;
		err = _dupenv_s( &mayaLoggingHome, NULL, "MAYA_LOGGING_HOME" );  if ( err ) return;  if ( mayaLoggingHome == NULL ) return;

		// Make a new folder for the day
		char folderName[1000];
		sprintf_s( folderName, 1000, "%s\\%4d-%02d-%02d", mayaLoggingHome, year, month, day );
		CreateDirectory(folderName, NULL);  // no error checking here, it is handled by checking logFile != NULL below

		// Now open the new logfile
		sprintf_s( logFilename, DI_LOG_FILENAME_LENGTH, "%s\\Arrows log %4d-%02d-%02d.txt", folderName, year, month, day );
		if ( logFile != NULL ) fclose( logFile );
		err = fopen_s( &logFile, logFilename, "a" );  if ( err ) return;

		// Do not delete this cout as Windows may need the delay in order to get its act together
		cout << hour << ":" << min << ":" << sec << "  arrowsCommon::logEntry() opening " << logFilename << endl;

		if ( logFile != NULL )
		{
			fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
			fprintf( logFile, "================================\n" );
			fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
			fprintf( logFile, "Arrows log %4d-%02d-%02d opened\n", year, month, day );
			fflush( logFile );
		}
		else
		{
			cout << "Failed to open " << logFilename << endl;
		}

		free( mayaLoggingHome );
	}


	// Write out the log string
	//
	//cout << logString << endl;
	if ( logFile != NULL )
	{
		fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
		fprintf( logFile, "%s\n", logString );
		fflush( logFile );
		currentRawTime = rawtime;
	}
	*/
}


/* ==========================================================================
// logEntry()
*/
void arrowsCommon::logEntry(
	nodeNameEnum nodeName )
{
	/*
	// Check node name.
	//
	if ( nodeName == eOther )
	{
		// -do nothing, the nodeName should always refer to an actual node
	}
	else
	{
		// -write out non-zero call counts for all nodes once per logTimestep
		// -first increment the appropriate call counter
        callCounters[nodeName]++;

		time_t rawTime;
		time ( &rawTime );
		if ( (rawTime-currentRawTime) >= logTimestep )
		{
			// -logTimestep has passed, form the string, write it out, then reset counters
			sprintf_s( logString, DI_LOG_STRING_LENGTH, "" );
			for ( int i=1; i<nodeCount; i++ )
				if ( callCounters[i] > 0 )
				{
					sprintf_s( logString, DI_LOG_STRING_LENGTH, "%s%s: %d  ", logString, nodeNameStrings[i], callCounters[i] );
					callCounters[i] = 0;
				}

			logEntry( logString );
		}
	}
	*/
}

