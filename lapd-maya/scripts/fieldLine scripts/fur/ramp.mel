// Script to add "fur" to a field lines scene.
// -------------------------------------------
//
// This uses the circles node and ramps for color and transparency.
//


// Parameter section.  Adjust these as necessary.
// Note that the circles node does not have the "randomize" attribute nor the "start" and "step" attributes yet.
//
float $tubeRadius = 0.4;    // The tube radius is set in the field lines UI window.  Units are cm.
int $segmentCount = 16;     // Number of straight line segments making up each circle.
float $multiplier = 500.0;  // If your input data is not in cm, use this multiplier to correct.
int $furFrameOffset = 0;    // Used to animate the timestep.  It must match the one in the field lines UI window.
float $glow = 0.2;          // Glow intensity of the "fur" in the rendered image.  Ranges from 0.0 to 1.0.                      
string $furFilePath =
  "C:/UCLA development/maya/Sandbox/HDF5 sandbox/test_2007-23-02.hdf5";  // HDF file path for secondary data (scalar).

string $furDataGroup = "B amplitudes";  // Data group within the HDF secondary data file.


// Create the transforms and the meshes (these are receptacles to hold the generated meshes).
//
createNode transform -n furCirclesFwdMesh1;
createNode mesh -n furCirclesFwdShape1 -p furCirclesFwdMesh1;
createNode transform -n furCirclesBakMesh1;
createNode mesh -n furCirclesBakShape1 -p furCirclesBakMesh1;


// Create the shading nodes (these are used to apply the ramp shading to the circles).
//
shadingNode -asShader blinn -name furBlinn;
setAttr "furBlinn.reflectivity" 0;
setAttr "furBlinn.specularColor" -type double3 0 0 0 ;
setAttr "furBlinn.glowIntensity" $glow;
sets -renderable true -noSurfaceShader true -empty -name furBlinnSG;
connectAttr -f furBlinn.outColor furBlinnSG.surfaceShader;
connectAttr -f furCirclesFwdShape1.instObjGroups[0] furBlinnSG.dagSetMembers[0];
connectAttr -f furCirclesBakShape1.instObjGroups[0] furBlinnSG.dagSetMembers[1];


// Set up the forward and backward "squares" nodes.
//
// Forward
createNode circles -name furCirclesFwd1;
connectAttr flCurves.curveCount furCirclesFwd1.curveCount;
connectAttr flCurves.startIndices furCirclesFwd1.startIndices;
connectAttr flCurves.CVCounts furCirclesFwd1.CVCounts;
connectAttr flCurves.controlVertexList furCirclesFwd1.controlVertexList;
connectAttr flCurves.tangentList furCirclesFwd1.tangentList;
setAttr furCirclesFwd1.tubeRadius $tubeRadius;
setAttr furCirclesFwd1.circleType 0;   // Regular circles, no other options yet.
setAttr furCirclesFwd1.segmentCount $segmentCount;
connectAttr flSpatialGrid.dims furCirclesFwd1.dims;
connectAttr flSpatialGrid.min furCirclesFwd1.gridMin;
connectAttr flSpatialGrid.max furCirclesFwd1.gridMax;

createNode HDF5scalarDataGrid -name furDataGrid;
setAttr furDataGrid.filename -type "string" $furFilePath;
setAttr furDataGrid.group -type "string" $furDataGroup;
string $furExpression = "furDataGrid.timeIndex = frame + " + $furFrameOffset + ";";
expression -s $furExpression -o furDataGrid -ae 1 -uc all -name furFrameExpression;
connectAttr furDataGrid.scalarDataGrid furCirclesFwd1.scalarDataGrid;
setAttr furCirclesFwd1.multiplier $multiplier;

connectAttr furCirclesFwd1.secondaryMesh furCirclesFwdShape1.inMesh;

// Backward
createNode circles -name furCirclesBak1;
connectAttr flBackCurves.curveCount furCirclesBak1.curveCount;
connectAttr flBackCurves.startIndices furCirclesBak1.startIndices;
connectAttr flBackCurves.CVCounts furCirclesBak1.CVCounts;
connectAttr flBackCurves.controlVertexList furCirclesBak1.controlVertexList;
connectAttr flBackCurves.tangentList furCirclesBak1.tangentList;
setAttr furCirclesBak1.tubeRadius $tubeRadius;
setAttr furCirclesBak1.circleType 0;   // Regular circles, no other options yet.
setAttr furCirclesBak1.segmentCount $segmentCount;
connectAttr flSpatialGrid.dims furCirclesBak1.dims;
connectAttr flSpatialGrid.min furCirclesBak1.gridMin;
connectAttr flSpatialGrid.max furCirclesBak1.gridMax;

connectAttr furDataGrid.scalarDataGrid furCirclesBak1.scalarDataGrid;
setAttr furCirclesBak1.multiplier $multiplier;

connectAttr furCirclesBak1.secondaryMesh furCirclesBakShape1.inMesh;


// Connect up the ramps.
//
// Color
shadingNode -asTexture ramp -name furColorRamp;
shadingNode -asUtility place2dTexture -name place2dFurColor;
connectAttr place2dFurColor.outUV furColorRamp.uv;
connectAttr place2dFurColor.outUvFilterSize furColorRamp.uvFilterSize;
setAttr "furColorRamp.colorEntryList[0].position" 0.0;
setAttr "furColorRamp.colorEntryList[1].position" 0.5;
setAttr "furColorRamp.colorEntryList[2].position" 1.0;
setAttr "furColorRamp.colorEntryList[0].color" -type double3 0.977131 0.879 1 ;
setAttr "furColorRamp.colorEntryList[1].color" -type double3 0.993259 0.251 1 ;
setAttr "furColorRamp.colorEntryList[2].color" -type double3 0.342714 0.069 1 ;
connectAttr -force furColorRamp.outColor furBlinn.color;

// Transparency
shadingNode -asTexture ramp -name furTransparencyRamp;
shadingNode -asUtility place2dTexture -name place2dFurTransparency;
connectAttr place2dFurTransparency.outUV furTransparencyRamp.uv;
connectAttr place2dFurTransparency.outUvFilterSize furTransparencyRamp.uvFilterSize;
setAttr "furTransparencyRamp.colorEntryList[0].position" 0.0;
setAttr "furTransparencyRamp.colorEntryList[1].position" 0.5;
setAttr "furTransparencyRamp.colorEntryList[2].position" 1.0;
setAttr "furTransparencyRamp.colorEntryList[0].color" -type double3 0 0 0 ;
setAttr "furTransparencyRamp.colorEntryList[1].color" -type double3 0.6 0.6 0.6 ;
setAttr "furTransparencyRamp.colorEntryList[2].color" -type double3 1 1 1 ;
connectAttr -force furTransparencyRamp.outColor furBlinn.transparency;


