/* ============================================================================
 * lapdCommon : defines.h
 *
 * Copyright (C) 2006 Jim Bamber
 *
 */
#ifndef DEFINES_H
#define DEFINES_H
#define sprintf_s snprintf  // uncomment this for compiling on the Mac

#include <maya/MStatus.h>


#define DI_LOG_FILENAME_LENGTH 1000
#define DI_LOG_STRING_LENGTH 5000

typedef int errno_t;  // this is a fix for the Mac which also works on Windows

//#define STATCHECK( stat, string ) \
//	if ( !stat ) \
//	{ \
//		MGlobal::displayError( string ); \
//		return MS::kFailure; \
//	}

#define BOOLCHECK( x )								\
{													\
	bool _bool_stat = ( x );						\
	if ( !_bool_stat )								\
	{												\
		cerr << "\nError detected in " << __FILE__	\
			 <<	" at line "	<< __LINE__ << endl;	\
	}												\
}

#define diLOG_BOOLSTAT( x )								\
{														\
	bool _bool_stat = ( x );							\
	if ( !_bool_stat )									\
	{													\
		sprintf_s( lapdCommon::logString,				\
			DI_LOG_STRING_LENGTH,						\
			"Error detected in %s at line %d",			\
			__FILE__, __LINE__ );						\
		lapdCommon::logEntry( lapdCommon::logString );	\
		cerr << "\nError detected in " << __FILE__		\
			 <<	" at line "	<< __LINE__ << endl;		\
	}													\
}

#define diLOG_EXIT_BOOLSTAT( x )						\
{														\
	bool _bool_stat = ( x );							\
	if ( !_bool_stat )									\
	{													\
		sprintf_s( lapdCommon::logString,				\
			DI_LOG_STRING_LENGTH,						\
			"Error detected in %s at line %d",			\
			__FILE__, __LINE__ );						\
		lapdCommon::logEntry( lapdCommon::logString );	\
		cerr << "\nError detected in " << __FILE__		\
			 <<	" at line "	<< __LINE__ << endl;		\
		return MStatus::kFailure;						\
	}													\
}

#define HDF5CHECK( x )									\
{														\
	int _hdf5_stat = ( x );								\
	if ( _hdf5_stat < 0 )								\
	{													\
		cerr << "\nHDF5 error detected in " << __FILE__	\
			 <<	" at line "	<< __LINE__ << endl;		\
	}													\
}

#define diLOG_HDF5STAT( x )								\
{														\
	int _hdf5_stat = ( x );								\
	if ( _hdf5_stat < 0 )								\
	{													\
		sprintf_s( lapdCommon::logString,				\
			DI_LOG_STRING_LENGTH,						\
			"HDF5 error detected in %s at line %d",		\
			__FILE__, __LINE__ );						\
		lapdCommon::logEntry( lapdCommon::logString );	\
		cerr << "\nHDF5 error detected in " << __FILE__	\
			 <<	" at line "	<< __LINE__ << endl;		\
	}													\
}

#define diLOG_EXIT_HDF5STAT( x )						\
{														\
	int _hdf5_stat = ( x );								\
	if ( _hdf5_stat < 0 )								\
	{													\
		sprintf_s( lapdCommon::logString,				\
			DI_LOG_STRING_LENGTH,						\
			"HDF5 error detected in %s at line %d",		\
			__FILE__, __LINE__ );						\
		lapdCommon::logEntry( lapdCommon::logString );	\
		cerr << "\nHDF5 error detected in " << __FILE__	\
			 <<	" at line "	<< __LINE__ << endl;		\
		return MStatus::kFailure;						\
	}													\
}

#define diLOG_MSTATUS( x )										\
{																\
	MStatus _maya_status = (x);									\
	if ( MStatus::kSuccess != _maya_status ) 					\
	{															\
		sprintf_s( lapdCommon::logString,						\
			DI_LOG_STRING_LENGTH,								\
			"API error detected in %s at line %d:\n                     %s",\
			__FILE__, __LINE__,									\
			_maya_status.errorString().asChar() );				\
		lapdCommon::logEntry( lapdCommon::logString );			\
		_maya_status.perror ( "" ); 							\
	}															\
}

#define diLOG_EXIT_MSTATUS( x )									\
{																\
	MStatus _maya_status = (x);									\
	if ( MStatus::kSuccess != _maya_status ) 					\
	{															\
		sprintf_s( lapdCommon::logString,						\
			DI_LOG_STRING_LENGTH,								\
			"API error detected in %s at line %d:\n                     %s",\
			__FILE__, __LINE__,									\
			_maya_status.errorString().asChar() );				\
		lapdCommon::logEntry( lapdCommon::logString );			\
		_maya_status.perror ( "" ); 							\
		return MStatus::kFailure;								\
	}															\
}

#define MAKE_INPUT(attr)						\
    diLOG_MSTATUS ( attr.setKeyable(true) );	\
	diLOG_MSTATUS ( attr.setStorable(true) );	\
    diLOG_MSTATUS ( attr.setReadable(true) );	\
	diLOG_MSTATUS ( attr.setWritable(true) );

#define MAKE_OUTPUT(attr)						\
    diLOG_MSTATUS ( attr.setKeyable(false) );	\
	diLOG_MSTATUS ( attr.setStorable(false) );	\
    diLOG_MSTATUS ( attr.setReadable(true) );	\
	diLOG_MSTATUS ( attr.setWritable(false) );


#endif
