/* ============================================================================
// Isosurface : isoSurfaceUV.cpp
//-
// ==========================================================================
// 2013 Jim Bamber
//
// Cloned from isoSurface.cpp.  Add an option for data-based UV mapping.  It's
// a matter of making the V value a constant, corresponding to the isoValue.
//
// 2008 Jim Bamber
//
// Note that this code is based on the "Marching cubes" example program by
// Cory Bloyd.  The comments from the top of his program are:
//
//
// *** Marching Cubes Example Program 
// *** by Cory Bloyd (corysama@yahoo.com)
// *** 
// *** A simple, portable and complete implementation of the Marching Cubes
// *** and Marching Tetrahedrons algorithms in a single source file.
// *** There are many ways that this code could be made faster, but the 
// *** intent is for the code to be easy to understand.
// *** 
// *** For a description of the algorithm go to
// *** http://astronomy.swin.edu.au/pbourke/modelling/polygonise/
// *** 
// *** This code is public domain.
//
// 
// My work was only to adapt the code to the Maya framework.
//
// ==========================================================================
//+
*/
#include "isoSurfaceUV.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "isoSurfaceCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MFloatArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshEdge.h>
#include <maya/MQuaternion.h>


// Static data
MTypeId isoSurfaceUV::id( 0x00602 );

// Attributes
MObject isoSurfaceUV::aUVMode;
MObject isoSurfaceUV::aIsoValue;
MObject isoSurfaceUV::aDims;
MObject isoSurfaceUV::aGridMin;
MObject isoSurfaceUV::aGridMax;
MObject isoSurfaceUV::aScalarDataGrid;
MObject isoSurfaceUV::aDataMin;
MObject isoSurfaceUV::aDataMax;

MObject isoSurfaceUV::aOutputMesh;


/* ============================================================================
 * createMesh()
 */
MStatus isoSurfaceUV::createMesh(
	int UVMode,
	double isoValue,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MDoubleArray& scalarDataGrid,
	double dataMin,
	double dataMax,
	MObject& newOutputData )
{
	MStatus stat;
	MFnMesh fnMesh;

	// Mesh arrays
	//
	// vertices -- point (vertex) array. This should include all the vertices in the mesh, and no extras. For example,
	// a cube could have the vertices: { (-1,-1,-1), (1,-1,-1), (1,-1,1), (-1,-1,1), (-1,1,-1), (-1,1,1), (1,1,1), (1,1,-1) }
	//
	// polygonVertexCounts -- array of vertex counts for each polygon. For example the cube would have 6 faces, each of which
	// had 4 verts, so the polygonVertexCounts would be {4,4,4,4,4,4}.
	//
	// polygonVertexIndices -- array of vertices for each polygon. For example, in the cube, we have 4 vertices for
	// every face, so we list the vertices for face0, face1, etc consecutively in the array. These are specified by indexes
	// in the vertexArray: e.g for the cube: { 0, 1, 2, 3, 4, 5, 6, 7, 3, 2, 6, 5, 0, 3, 5, 4, 0, 4, 7, 1, 1, 7, 6, 2 }.  The
	// connections between the vertices are implied by the order in the list.
	//
	MPointArray vertices;
	MIntArray vertexIndices;
	MVectorArray normals;
	MIntArray polygonVertexCounts;
	MIntArray polygonVertexIndices;

	// This array has an entry for each edge of each cube: -1 if there was no intersection with the surface, the index into
	// "vertices" array if there was.
	MIntArray vertexLookup;

	// dimensions
	int nx = dims[0];
	int ny = dims[1];
	int nz = dims[2];

	// distances between grid points
	float xmin = gridMin[0];
	float ymin = gridMin[1];
	float zmin = gridMin[2];
	float dx = (gridMax[0]-xmin) / (nx-1);
	float dy = (gridMax[1]-ymin) / (ny-1);
	float dz = (gridMax[2]-zmin) / (nz-1);

	// Load up the arrays of vertices and polygon connections
	for ( int ix = 0; ix < nx-1; ix = ix + 1 )
	{
		//sprintf( isoSurfaceCommon::logString, "ix: %d", ix );
		//isoSurfaceCommon::logEntry( isoSurfaceCommon::logString );

		float x = xmin + (float)ix*dx;
		for ( int iy = 0; iy < ny-1; iy = iy + 1 )
		{
			float y = ymin + (float)iy*dy;
			for ( int iz = 0; iz < nz-1; iz = iz + 1 )
			{
				float z = zmin + (float)iz*dz;
				marchingCube(
					isoValue, vertexLookup, vertices, vertexIndices, normals,
					polygonVertexCounts, polygonVertexIndices,
					ix, iy, iz, nx, ny, nz, x, y, z, dx, dy, dz, gridMin, gridMax, scalarDataGrid );
			}
		}
	}

	int vertexCount = vertices.length();
	int polygonCount = polygonVertexCounts.length();

	// Single call here to create the mesh
	fnMesh.create(
		vertexCount,
		polygonCount, 
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		newOutputData, 
		&stat );   diLOG_MSTATUS( stat );

	double scaledIsoValue = (isoValue-dataMin) / (dataMax-dataMin);

	diLOG_MSTATUS( fnMesh.cleanupEdgeSmoothing() );
	diLOG_MSTATUS( assignUVs(fnMesh, UVMode, scaledIsoValue, gridMin, gridMax, vertices, polygonVertexCounts, polygonVertexIndices) );

	//diLOG_MSTATUS( fnMesh.setVertexNormals(normals, vertexIndices) );

	return MStatus::kSuccess;
}


/* ============================================================================
 * assignUVs()
 *
 * Assign a UV set to the given MFnMesh which refers to a newly created isosurface.  The isosurface is
 * assumed to be topologically a cylinder.  U varies around the circumference, i.e. is linearly mapped to
 * the polar angle, and V varies along z.
 *
 */
MStatus isoSurfaceUV::assignUVs(
	MFnMesh& fnMesh,
	int UVMode,
	double scaledIsoValue,
	float3& gridMin,
	float3& gridMax,
	MPointArray& vertices,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices )
{
	MStatus stat;
	cout << "Entered isoSurfaceUV::assignUVs()" << endl;


	// There are two UV modes: 0 == "data-based", 1 == "geom-based"
	//
	// UVMode = 0  "data-based":
	// U coordinate linearly mapped to polar angle, equates to x coordinate of texture swatch.
	// V coordinate set to scaled isoValue, equates to y coordinate of texture swatch.
	//
	// UVMode = 1  "geom-based":
	// U coordinate linearly mapped to polar angle, equates to x coordinate of texture swatch.
	// V coordinate linearly mapped to z, equates to y coordinate of texture swatch.
	//
	// Set up all possible UV pairs.  Make 121 U values, ranging from 0 to 1 and back down to 0.
	// Make 1001 V values from 0 to 1. 
	//
	int segmentCount = 120;
	int UCycles = 1;
	int cycleSegmentCount = segmentCount / UCycles;
	int halfCycleSegmentCount = cycleSegmentCount / 2;
	MFloatArray uValues;
	for ( int iUCycle=0; iUCycle<UCycles; iUCycle++ )
	{
		for ( int iU=0; iU<cycleSegmentCount; iU++ )
		{
			if ( iU < halfCycleSegmentCount ) uValues.append( (float)iU/(float)halfCycleSegmentCount );
			else uValues.append( (float)(cycleSegmentCount-iU)/(float)halfCycleSegmentCount );
		}
	}
	uValues.append( 0.0 );

	int uvId=0;
	float uValue, vValue;
	MFloatArray uArray;
	MFloatArray vArray;


	// data-based: V set to scaled isoValue
	//
	if ( UVMode == 0 )
	{
		vValue = (float)scaledIsoValue;
		for ( int iU=0; iU<(segmentCount+1); iU++ )
		{
			uValue = uValues[iU];
			uArray.append( uValue );
			vArray.append( vValue );
			//diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue, dataBasedUVSetNamePtr) );
			//uvId++;
		}
	}  // End if UVMode = 0  "data-based"

	// geom-based: V mapped to z in 1001 steps
	//
	if ( UVMode == 1 )
	{
		for ( int iV=0; iV<1001; iV++ )
		{
			vValue = (float)( iV / 1000.0 );
			for ( int iU=0; iU<(segmentCount+1); iU++ )
			{
				uValue = uValues[iU];
				uArray.append( uValue );
				vArray.append( vValue );
				//diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue, dataBasedUVSetNamePtr) );
				//uvId++;
			}
		}
	}  // End if UVMode = 1  "geom-based"

	cout << "uArray.length(): " << uArray.length() << endl;
	cout << "vArray.length(): " << vArray.length() << endl;

	diLOG_MSTATUS( fnMesh.setUVs(uArray, vArray) );


	// Prepare to assign the UV's
	//
	MPoint vertex;
	int uvIdBase;

	MIntArray uvCounts;
	MIntArray uvIds;

	double z_min = gridMin[2];
	double z_max = gridMax[2];
	double z_length = z_max - z_min;
	double z_scale = 1.0;
	if ( z_length != 0.0 ) z_scale = 1.0 / z_length;

	double x_center = (gridMin[0] + gridMax[0]) / 2.0;
	double y_center = (gridMin[1] + gridMax[1]) / 2.0;

	double pi = 3.1415927;


	// Assign UV's for all polygons
	//
	int indexBase = 0;
	int polygonCount = polygonVertexCounts.length();
	cout << "polygonCount: " << polygonCount << endl;

	for ( int iPolygon = 0; iPolygon < polygonCount; iPolygon++ )
	{
		int polygonVertexCount = polygonVertexCounts[iPolygon];
		uvCounts.append( polygonVertexCount );
		for ( int iVertex = 0; iVertex < polygonVertexCount; iVertex++ )
		{
			int vertexIndex = polygonVertexIndices[indexBase + iVertex];
			vertex = vertices[vertexIndex];
			int iV;

			// UVMode = 0  data-based
			//
			if ( UVMode == 0 )
			{
				iV = 0;
			}

			// UVMode = 1  geom-based
			//
			if ( UVMode == 1 )
			{
				double z = vertex.z;
				double scaled_z = (z - z_min) * z_scale;
				if ( scaled_z < 0.0 ) scaled_z = 0.0;
				if ( scaled_z > 1.0 ) scaled_z = 1.0;
				iV = (int)(scaled_z * 1000.0);
			}

			uvIdBase = iV * (segmentCount + 1);

			double x = vertex.x;
			double y = vertex.y;
			double phi = atan2( y, x );
			phi = phi + pi;
			double phi_degrees = phi * 180.0 / pi;
			if ( phi_degrees <   0.0 ) phi_degrees =   0.0;
			if ( phi_degrees > 360.0 ) phi_degrees = 360.0;
			int iU = (int)(phi_degrees / 3.0 );  // changes range to 0 -> 120

			uvId = uvIdBase + iU;      uvIds.append( uvId );
			//if ( (uvId<0) || (uvId>121120) ) cout << "uvId out of range: " << uvId << endl;
		}  // End looping through all vertices for this polygon
		indexBase = indexBase + polygonVertexCount;
	}  // End looping through polygons


	// UVMode: 0=="data-based" or 1=="geometry-based" 
	//
	diLOG_MSTATUS( fnMesh.assignUVs(uvCounts, uvIds) );
	cout << "uvCounts.length(): " << uvCounts.length() << endl;

	cout << "Finished isosurface::assignUVs()" << endl;

	return MStatus::kSuccess;
}


/* ============================================================================
 * marchingCube()
 */
MStatus isoSurfaceUV::marchingCube(
	double isoValue,
	MIntArray& vertexLookup,
	MPointArray& vertices,
	MIntArray& vertexIndices,
	MVectorArray& normals,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices,
	int ix, int iy, int iz,
	int nx, int ny, int nz,
	float x, float y, float z,
	float dx, float dy, float dz,
	float3& gridMin, float3& gridMax,
	MDoubleArray& scalarDataGrid )
{
	MStatus stat;
	double halfWindow = 2.0;

    int iCorner, iVertex, iVertexTest, iEdge, iTriangle, iFlagIndex, iEdgeFlags;
	int iCubeFlag;
	int vertexIndex;
    float offset;
    double cubeValues[8];
    MPoint edgeVertex;
	MPointArray vertexArray;

    //Make a local copy of the values at the cube's corners
	int nplane = ny * nx;
    for ( iVertex = 0; iVertex < 8; iVertex++ )
    {
		int iplane = (iz + vertexOffsets[iVertex][2]) * nplane;
		int irow = iplane + (iy + vertexOffsets[iVertex][1]) * nx;
		int index = irow + (ix + vertexOffsets[iVertex][0]);
        cubeValues[iVertex] = scalarDataGrid[index];
    }

    //Find which vertices are inside of the surface and which are outside
    iFlagIndex = 0;
    for ( iVertexTest = 0; iVertexTest < 8; iVertexTest++ )
    {
	    if ( cubeValues[iVertexTest] <= isoValue ) 
            iFlagIndex |= 1<<iVertexTest;
    }

    //Find which edges are intersected by the surface
    iEdgeFlags = cubeEdgeFlags[iFlagIndex];

    //If the cube is entirely inside or outside of the surface, then there will be no intersections
    if ( iEdgeFlags == 0 ) 
    {
		for ( iEdge=0; iEdge<12; iEdge++) vertexLookup.append( -1 );
        return MStatus::kSuccess;
    }

	//Calculate the cube position flag
	iCubeFlag = 0;
	if ( ix > 0 ) iCubeFlag |= 4;
	if ( iy > 0 ) iCubeFlag |= 2;
	if ( iz > 0 ) iCubeFlag |= 1;

    //Find the point of intersection of the surface with each edge
    //Then find the normal to the surface at those points
    for ( iEdge = 0; iEdge < 12; iEdge++ )
    {
		vertexIndex = -1;

		// Check if this edge is a duplicate
		bool isDuplicateEdge = edgeDuplicates[iCubeFlag][iEdge];
		if ( isDuplicateEdge )
		{
			// edge is a duplicate, just look up the index
			int ixLookup = ix + edgeOffsets[iCubeFlag][iEdge][0];
			int iyLookup = iy + edgeOffsets[iCubeFlag][iEdge][1];
			int izLookup = iz + edgeOffsets[iCubeFlag][iEdge][2];
			int iEdgeLookup = edgeOffsets[iCubeFlag][iEdge][3];
			int iLookup = (ixLookup*(nz-1)*(ny-1) + iyLookup*(nz-1) + izLookup) * 12 + iEdgeLookup;
			vertexIndex = vertexLookup[ iLookup ];
		}
		else
		{
			// edge is not a duplicate, see if there is an intersection
			if ( iEdgeFlags & (1<<iEdge) )
			{
				// there is an intersection on this edge, calculate where it is (scaled from 0.0 to 1.0)
				double value1 = cubeValues[edgeConnections[iEdge][0]];
				double value2 = cubeValues[edgeConnections[iEdge][1]];
				double delta = value2 - value1;

				if ( delta == 0.0 ) offset = 0.5;
				else offset = (float) ((isoValue - value1) / delta);

				int iEdgeStart = edgeConnections[iEdge][0];

				// now calculate the actual point location
				edgeVertex.x = x + ( (float)vertexOffsets[iEdgeStart][0] + offset*(float)edgeDirections[iEdge][0] ) * dx;
				edgeVertex.y = y + ( (float)vertexOffsets[iEdgeStart][1] + offset*(float)edgeDirections[iEdge][1] ) * dy;
				edgeVertex.z = z + ( (float)vertexOffsets[iEdgeStart][2] + offset*(float)edgeDirections[iEdge][2] ) * dz;

				// append to vertex arrays
				vertexIndex = vertices.length();
				vertices.append( edgeVertex );
				//vertexIndices.append( vertexIndex );

				//// now the normal for this vertex
				//int3 dims;
				//dims[0] = nx;  dims[1] = ny;  dims[2] = nz;
				//double evX = edgeVertex.x;  double evY = edgeVertex.y;  double evZ = edgeVertex.z;
				//double sampleX1 =
				//	lapdCommon::interpolateScalar( MPoint(evX-0.01, evY, evZ), dims, gridMax, gridMin, scalarDataGrid, halfWindow );
				//double sampleX2 =
				//	lapdCommon::interpolateScalar( MPoint(evX+0.01, evY, evZ), dims, gridMax, gridMin, scalarDataGrid, halfWindow );
				//double sampleY1 =
				//	lapdCommon::interpolateScalar( MPoint(evX, evY-0.01, evZ), dims, gridMax, gridMin, scalarDataGrid, halfWindow );
				//double sampleY2 =
				//	lapdCommon::interpolateScalar( MPoint(evX, evY+0.01, evZ), dims, gridMax, gridMin, scalarDataGrid, halfWindow );
				//double sampleZ1 =
				//	lapdCommon::interpolateScalar( MPoint(evX, evY, evZ-0.01), dims, gridMax, gridMin, scalarDataGrid, halfWindow );
				//double sampleZ2 =
				//	lapdCommon::interpolateScalar( MPoint(evX, evY, evZ+0.01), dims, gridMax, gridMin, scalarDataGrid, halfWindow );
				
				//double sampleX = sampleX1 - sampleX2;
				//double sampleY = sampleY1 - sampleY2;
				//double sampleZ = sampleZ1 - sampleZ2;

				//double length = sqrt( (sampleX * sampleX) +	(sampleY * sampleY) + (sampleZ * sampleZ) );

				//MVector normal;
				//if ( length == 0.0 ) normal = MVector( sampleX, sampleY, sampleZ );
				//else
				//{
				//	double scale = 1.0/length;
				//	normal = MVector( sampleX*scale, sampleY*scale, sampleZ*scale );
				//}

				//normals.append( normal );
			} // end if there was an intersection
		} // end if this is a duplicate edge

		vertexLookup.append( vertexIndex );
    }

    //Store the triangles that were found.  There can be up to five per cube
	int iCubeOffset = (ix*(nz-1)*(ny-1) + iy*(nz-1) + iz) * 12;

    for(iTriangle = 0; iTriangle < 5; iTriangle++)
    {
        if ( triangleConnectionTable[iFlagIndex][3*iTriangle] < 0 ) break;

		polygonVertexCounts.append(3);
        for ( iCorner = 0; iCorner < 3; iCorner++ )
        {
            iEdge = triangleConnectionTable[iFlagIndex][3*iTriangle+iCorner];
			vertexIndex = vertexLookup[iCubeOffset+iEdge];
			polygonVertexIndices.append( vertexIndex );
        }
    }

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the curves.
//
*/
MStatus isoSurfaceUV::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//isoSurfaceCommon::logEntry( "--> isoSurfaceUV::compute()" );
	//isoSurfaceCommon::logEntry( isoSurfaceCommon::eIsoSurfaceUV );

	if ( plug != aOutputMesh )
	{
		isoSurfaceCommon::logEntry( (char*) "Abnormal exit from isoSurfaceUV::compute(), Maya not asking for output mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hUVMode( block.inputValue(aUVMode, &stat) );   diLOG_MSTATUS( stat );
	int UVMode = hUVMode.asInt();

	MDataHandle hIsoValue( block.inputValue(aIsoValue, &stat) );   diLOG_MSTATUS( stat );
	double isoValue = hIsoValue.asDouble();

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hScalarDataGrid( block.inputValue(aScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnDoubleArrayData fnScalarDataGrid( hScalarDataGrid.data() );
	MDoubleArray scalarDataGrid( fnScalarDataGrid.array() );

	MDataHandle hDataMin( block.inputValue(aDataMin, &stat) );   diLOG_MSTATUS( stat );
	double dataMin = hDataMin.asDouble();

	MDataHandle hDataMax( block.inputValue(aDataMax, &stat) );   diLOG_MSTATUS( stat );
	double dataMax = hDataMax.asDouble();


	// Now do the output attributes.
	//
	MDataHandle hOutputMesh( block.outputValue(aOutputMesh, &stat) );   diLOG_MSTATUS( stat );


	// Create the data object which underlies the output mesh
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );

	// Make the call to create the secondary mesh
	//isoSurfaceCommon::logEntry( "About to create mesh" );
	diLOG_MSTATUS(
		createMesh(
		    UVMode,
			isoValue,
			dims,
			gridMin,
			gridMax,
			scalarDataGrid,
			dataMin,
			dataMax,
			newOutputData) );


	// Set the outputs as clean.
	hOutputMesh.set( newOutputData );
	block.setClean( plug );

	//isoSurfaceCommon::logEntry( "<-- isoSurfaceUV::compute()" );
	return stat;
}


/* ============================================================================
 * initialize()
 *
 * initializes attribute information and the unit circle values
 */
MStatus isoSurfaceUV::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	isoSurfaceCommon::logEntry( (char*) "--> isoSurfaceUV::initialize()" );


	// Create output attributes
	//
	// secondary mesh
	aOutputMesh = tAttr.create( "outputMesh", "outputMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aOutputMesh) );


	// Create input attributes:
	//
	// mode
    aUVMode = enumAttr.create( "UVMode", "UVMode", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("data-based", 0) );
	diLOG_MSTATUS( enumAttr.addField("geometry-based", 1) );
	MAKE_INPUT( enumAttr );
	diLOG_MSTATUS( addAttribute(aUVMode) );

	// isosurface contouring value
	aIsoValue = nAttr.create( "isoValue", "isoValue", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aIsoValue) );

	// grid dimensions
	aDims = nAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gridMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gridMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// scalar data grid
	aScalarDataGrid = tAttr.create( "scalarDataGrid", "scalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aScalarDataGrid) );

	// data min
	aDataMin = nAttr.create( "dataMin", "dataMin", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aDataMin) );

	// data max
	aDataMax = nAttr.create( "dataMax", "dataMax", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aDataMax) );


	// All inputs affect all outputs
	//
    diLOG_MSTATUS( attributeAffects(aUVMode, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aIsoValue, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDims, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aScalarDataGrid, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMin, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMax, aOutputMesh) );

	isoSurfaceCommon::logEntry( (char*) "<-- isoSurfaceUV::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
 * isoSurfaceUV()
 */
isoSurfaceUV::isoSurfaceUV()
{
}


/* ============================================================================
 * postConstructor()
 */
void isoSurfaceUV::postConstructor( )
{
}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType isoSurfaceUV::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
 * creator()
 *
 * creates an instance of the node
 */
void * isoSurfaceUV::creator()
{
    return new isoSurfaceUV();
}


/* ============================================================================
 * ~isoSurfaceUV()
 */
isoSurfaceUV::~isoSurfaceUV()
{
}


/* ============================================================================
 * Convenience arrays
 */

//vertexOffsets lists the positions, relative to vertex0, of each of the 8 vertices of a cube
int isoSurfaceUV::vertexOffsets[8][3] =
{
        {0, 0, 0},{1, 0, 0},{1, 1, 0},{0, 1, 0},
        {0, 0, 1},{1, 0, 1},{1, 1, 1},{0, 1, 1}
};

//edgeConnections lists the index of the endpoint vertices for each of the 12 edges of the cube
int isoSurfaceUV::edgeConnections[12][2] = 
{
        {0,1}, {1,2}, {2,3}, {3,0},
        {4,5}, {5,6}, {6,7}, {7,4},
        {0,4}, {1,5}, {2,6}, {3,7}
};

//edgeDirections lists the direction vector (vertex1-vertex0) for each edge in the cube
int isoSurfaceUV::edgeDirections[12][3] =
{
        {1, 0, 0},{0, 1, 0},{-1, 0, 0},{0, -1, 0},
        {1, 0, 0},{0, 1, 0},{-1, 0, 0},{0, -1, 0},
        {0, 0, 1},{0, 0, 1},{ 0, 0, 1},{0,  0, 1}
};

// edgeDuplicates lists which cube edges will have already been tested, which depends on whether the 
// cube is at the edge of the measurement volume: first index bit 0 = iz>0, bit 1 = iy>0, bit 2 = ix>0
bool isoSurfaceUV::edgeDuplicates[8][12] =
{
		// 00000000
		{false, false, false, false,
         false, false, false, false,
		 false, false, false, false},

		// 00000001
		{ true,  true,  true,  true,
         false, false, false, false,
		 false, false, false, false},

		// 00000010
		{ true, false, false, false,
          true, false, false, false,
		  true,  true, false, false},

		// 00000011
		{ true,  true,  true,  true,
          true, false, false, false,
		  true,  true, false, false},

		// 00000100
		{false, false, false,  true,
         false, false, false,  true,
		  true, false, false,  true},

		// 00000101
		{ true,  true,  true,  true,
         false, false, false,  true,
		  true, false, false,  true},

		// 00000110
		{ true, false, false,  true,
          true, false, false,  true,
		  true,  true, false,  true},

		// 00000111
		{ true,  true,  true,  true,
          true, false, false,  true,
		  true,  true, false,  true}
};

// edgeOffsets lists the indexing offsets {ix, iy, iz, iedge}, which depend on whether the 
// cube is at the edge of the measurement volume: first index bit 0 = iz>0, bit 1 = iy>0, bit 2 = ix>0
int isoSurfaceUV::edgeOffsets[8][12][4] =
{
		// 00000000
		{ { 0, 0, 0, 0}, { 0, 0, 0, 1}, { 0, 0, 0, 2}, { 0, 0, 0, 3},
          { 0, 0, 0, 4}, { 0, 0, 0, 5}, { 0, 0, 0, 6}, { 0, 0, 0, 7},
		  { 0, 0, 0, 8}, { 0, 0, 0, 9}, { 0, 0, 0,10}, { 0, 0, 0,11} },

		// 00000001
		{ { 0, 0,-1, 4}, { 0, 0,-1, 5}, { 0, 0,-1, 6}, { 0, 0,-1, 7},
          { 0, 0, 0, 4}, { 0, 0, 0, 5}, { 0, 0, 0, 6}, { 0, 0, 0, 7},
		  { 0, 0, 0, 8}, { 0, 0, 0, 9}, { 0, 0, 0,10}, { 0, 0, 0,11} },

		// 00000010
		{ { 0,-1, 0, 2}, { 0, 0, 0, 1}, { 0, 0, 0, 2}, { 0, 0, 0, 3},
          { 0,-1, 0, 6}, { 0, 0, 0, 5}, { 0, 0, 0, 6}, { 0, 0, 0, 7},
		  { 0,-1, 0,11}, { 0,-1, 0,10}, { 0, 0, 0,10}, { 0, 0, 0,11} },

		// 00000011
		{ { 0,-1, 0, 2}, { 0, 0,-1, 5}, { 0, 0,-1, 6}, { 0, 0,-1, 7},
          { 0,-1, 0, 6}, { 0, 0, 0, 5}, { 0, 0, 0, 6}, { 0, 0, 0, 7},
		  { 0,-1, 0,11}, { 0,-1, 0,10}, { 0, 0, 0,10}, { 0, 0, 0,11} },

		// 00000100
		{ { 0, 0, 0, 0}, { 0, 0, 0, 1}, { 0, 0, 0, 2}, {-1, 0, 0, 1},
          { 0, 0, 0, 4}, { 0, 0, 0, 5}, { 0, 0, 0, 6}, {-1, 0, 0, 5},
		  {-1, 0, 0, 9}, { 0, 0, 0, 9}, { 0, 0, 0,10}, {-1, 0, 0,10} },

		// 00000101
		{ { 0, 0,-1, 4}, { 0, 0,-1, 5}, { 0, 0,-1, 6}, {-1, 0, 0, 1},
          { 0, 0, 0, 4}, { 0, 0, 0, 5}, { 0, 0, 0, 6}, {-1, 0, 0, 5},
		  {-1, 0, 0, 9}, { 0, 0, 0, 9}, { 0, 0, 0,10}, {-1, 0, 0,10} },

		// 00000110
		{ { 0,-1, 0, 2}, { 0, 0, 0, 1}, { 0, 0, 0, 2}, {-1, 0, 0, 1},
          { 0,-1, 0, 6}, { 0, 0, 0, 5}, { 0, 0, 0, 6}, {-1, 0, 0, 5},
		  {-1, 0, 0, 9}, { 0,-1, 0,10}, { 0, 0, 0,10}, {-1, 0, 0,10} },

		// 00000111
		{ { 0,-1, 0, 2}, { 0, 0,-1, 5}, { 0, 0,-1, 6}, {-1, 0, 0, 1},
          { 0,-1, 0, 6}, { 0, 0, 0, 5}, { 0, 0, 0, 6}, {-1, 0, 0, 5},
		  {-1, 0, 0, 9}, { 0,-1, 0,10}, { 0, 0, 0,10}, {-1, 0, 0,10} }
};


/* ============================================================================
 * Marching cubes arrays
 */

// *** comment by Cory Bloyd
// For any edge, if one vertex is inside of the surface and the other is outside of the surface
//  then the edge intersects the surface
// For each of the 8 vertices of the cube can be two possible states : either inside or outside of the surface
// For any cube the are 2^8=256 possible sets of vertex states
// This table lists the edges intersected by the surface for all 256 possible vertex states
// There are 12 edges.  For each entry in the table, if edge #n is intersected, then bit #n is set to 1
int isoSurfaceUV::cubeEdgeFlags[256]=
{
        0x000, 0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c, 0x80c, 0x905, 0xa0f, 0xb06, 0xc0a, 0xd03, 0xe09, 0xf00, 
        0x190, 0x099, 0x393, 0x29a, 0x596, 0x49f, 0x795, 0x69c, 0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90, 
        0x230, 0x339, 0x033, 0x13a, 0x636, 0x73f, 0x435, 0x53c, 0xa3c, 0xb35, 0x83f, 0x936, 0xe3a, 0xf33, 0xc39, 0xd30, 
        0x3a0, 0x2a9, 0x1a3, 0x0aa, 0x7a6, 0x6af, 0x5a5, 0x4ac, 0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0, 
        0x460, 0x569, 0x663, 0x76a, 0x066, 0x16f, 0x265, 0x36c, 0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a, 0x963, 0xa69, 0xb60, 
        0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0x0ff, 0x3f5, 0x2fc, 0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0, 
        0x650, 0x759, 0x453, 0x55a, 0x256, 0x35f, 0x055, 0x15c, 0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53, 0x859, 0x950, 
        0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0x0cc, 0xfcc, 0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0, 
        0x8c0, 0x9c9, 0xac3, 0xbca, 0xcc6, 0xdcf, 0xec5, 0xfcc, 0x0cc, 0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9, 0x7c0, 
        0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c, 0x15c, 0x055, 0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650, 
        0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6, 0xfff, 0xcf5, 0xdfc, 0x2fc, 0x3f5, 0x0ff, 0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0, 
        0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c, 0x36c, 0x265, 0x16f, 0x066, 0x76a, 0x663, 0x569, 0x460, 
        0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af, 0xaa5, 0xbac, 0x4ac, 0x5a5, 0x6af, 0x7a6, 0x0aa, 0x1a3, 0x2a9, 0x3a0, 
        0xd30, 0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c, 0x53c, 0x435, 0x73f, 0x636, 0x13a, 0x033, 0x339, 0x230, 
        0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895, 0x99c, 0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x099, 0x190, 
        0xf00, 0xe09, 0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c, 0x70c, 0x605, 0x50f, 0x406, 0x30a, 0x203, 0x109, 0x000
};


// *** comment by Cory Bloyd
//  For each of the possible vertex states listed in aiCubeEdgeFlags there is a specific triangulation
//  of the edge intersection points.  The triangleConnectionTable lists all of them in the form of
//  0-5 edge triples with the list terminated by the invalid value -1.
//  For example: triangleConnectionTable[3] list the 2 triangles formed when corner[0] 
//  and corner[1] are inside of the surface, but the rest of the cube is not.
//
//  I found this table in an example program someone wrote long ago.  It was probably generated by hand
int isoSurfaceUV::triangleConnectionTable[256][16] = 
{
        {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1},
        {3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1},
        {3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1},
        {3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1},
        {9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1},
        {1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1},
        {9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
        {2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1},
        {8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1},
        {9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
        {4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1},
        {3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1},
        {1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1},
        {4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1},
        {4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1},
        {9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1},
        {1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
        {5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1},
        {2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1},
        {9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
        {0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
        {2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1},
        {10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1},
        {4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1},
        {5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1},
        {5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1},
        {9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1},
        {0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1},
        {1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1},
        {10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1},
        {8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1},
        {2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1},
        {7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1},
        {9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1},
        {2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1},
        {11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1},
        {9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1},
        {5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1},
        {11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1},
        {11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
        {1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1},
        {9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1},
        {5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1},
        {2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
        {0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
        {5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1},
        {6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1},
        {0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1},
        {3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1},
        {6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1},
        {5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1},
        {1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
        {10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1},
        {6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1},
        {1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1},
        {8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1},
        {7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1},
        {3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
        {5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1},
        {0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1},
        {9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1},
        {8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1},
        {5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1},
        {0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1},
        {6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1},
        {10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1},
        {10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1},
        {8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1},
        {1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1},
        {3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1},
        {0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1},
        {10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1},
        {0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1},
        {3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1},
        {6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1},
        {9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1},
        {8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1},
        {3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1},
        {6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1},
        {0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1},
        {10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1},
        {10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1},
        {1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1},
        {2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1},
        {7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1},
        {7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1},
        {2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1},
        {1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1},
        {11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1},
        {8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1},
        {0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1},
        {7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
        {10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
        {2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
        {6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1},
        {7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1},
        {2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1},
        {1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1},
        {10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1},
        {10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1},
        {0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1},
        {7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1},
        {6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1},
        {8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1},
        {9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1},
        {6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1},
        {1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1},
        {4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1},
        {10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1},
        {8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1},
        {0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1},
        {1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1},
        {8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1},
        {10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1},
        {4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1},
        {10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
        {5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
        {11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1},
        {9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
        {6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1},
        {7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1},
        {3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1},
        {7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1},
        {9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1},
        {3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1},
        {6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1},
        {9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1},
        {1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1},
        {4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1},
        {7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1},
        {6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1},
        {3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1},
        {0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1},
        {6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1},
        {1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1},
        {0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1},
        {11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1},
        {6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1},
        {5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1},
        {9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1},
        {1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1},
        {1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1},
        {10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1},
        {0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1},
        {5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1},
        {10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1},
        {11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1},
        {0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1},
        {9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1},
        {7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1},
        {2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1},
        {8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1},
        {9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1},
        {9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1},
        {1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1},
        {9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1},
        {9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1},
        {5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1},
        {0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1},
        {10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1},
        {2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1},
        {0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1},
        {0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1},
        {9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1},
        {5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1},
        {3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1},
        {5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1},
        {8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1},
        {0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1},
        {9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1},
        {0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1},
        {1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1},
        {3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1},
        {4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1},
        {9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1},
        {11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1},
        {11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1},
        {2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1},
        {9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1},
        {3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1},
        {1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1},
        {4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1},
        {4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1},
        {0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1},
        {3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1},
        {3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1},
        {0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1},
        {9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1},
        {1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
        {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}
};
