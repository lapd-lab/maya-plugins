/* ============================================================================
// forceField : tubeUV.cpp
//-
// ==========================================================================
// Copyright (C) 2011 Jim Bamber
//
// ==========================================================================
//+
*/
#include "tubeUV.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnStringData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MItDag.h>
#include <maya/MDagPath.h>
#include <maya/MPointArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshEdge.h>
#include <maya/MQuaternion.h>


// Static data
MTypeId tubeUV::id( 0x00461 );
MPointArray tubeUV::unitCircle;

// Attributes
//MObject tubeUV::aMeshName;
//MObject tubeUV::aMode;
MObject tubeUV::aTransform;
MObject tubeUV::aCurveCount;
MObject tubeUV::aStartIndices;
MObject tubeUV::aCVCounts;
MObject tubeUV::aControlVertexList;
MObject tubeUV::aTangentList;
MObject tubeUV::aCurveIndex;
MObject tubeUV::aTubeRadius;
MObject tubeUV::aSegmentCount;
MObject tubeUV::aUCycles;
MObject tubeUV::aSpiralAngle;
MObject tubeUV::aFixedAngle;
MObject tubeUV::aStepsPerVCycle;
MObject tubeUV::aDims;				
MObject tubeUV::aGridMin;			
MObject tubeUV::aGridMax;			
MObject tubeUV::aScalarDataGrid;	
MObject tubeUV::aDataMin;			
MObject tubeUV::aDataMax;			

//MObject tubeUV::aGeometryBasedMesh;
//MObject tubeUV::aDataBasedMesh;
MObject tubeUV::aSecondaryMesh;


/* ============================================================================
// createMesh()
*/
MStatus tubeUV::createMesh(
	//int mode,
	//MFnMesh& fnGeometryBasedMesh,
	//MFnMesh& fnDataBasedMesh,
	MObject transform,
	MFnMesh& fnMesh,
	int curveCount,
	MIntArray startIndices,
	MIntArray CVCounts,
	MPointArray controlVertexList,
	MVectorArray tangentList,
	int curveIndex,
	int segmentCount,
	double tubeRadius,
	int UCycles,
	double spiralAngle,
	double fixedAngle,
	//MObject& geometryBasedOutputData,
	//MObject& dataBasedOutputData )
	MObject& newOutputData )
{
	MStatus stat;

	MFloatPointArray vertices;
	MIntArray polygonVertexCounts;
	MIntArray polygonVertexIndices;
	int index;
	MFloatPoint controlVertex;
	MVector tangent;

	double pi = 3.1415927;
	double spiralAngleRadians = spiralAngle * pi / 180.0;
	double fixedAngleRadians = fixedAngle * pi / 180.0;

	int CVCount = 0;
	int startIndex = 0;
	if ( (curveIndex >= 0) && (curveIndex < curveCount) )
	{
		CVCount = CVCounts[ curveIndex ];
		startIndex = startIndices[ curveIndex ];

		for ( int iCV = 0; iCV < CVCount; iCV++ )
		{
			index = startIndex + iCV;
			controlVertex = MFloatPoint( controlVertexList[index] );
			tangent = tangentList[index];

			addSection(
				iCV,
				controlVertex,
				tangent,
				tubeRadius,
				segmentCount,
				UCycles,
				spiralAngleRadians,
				fixedAngleRadians,
				vertices,
				polygonVertexCounts,
				polygonVertexIndices );
		}
	}

	int vertexCount=vertices.length();
	int polygonCount=polygonVertexCounts.length();

	/*
	// Single call here to create the mesh
	if ( mode == 0 )
	{
		// 0 == create
		fnMesh.create(
			vertexCount,
			polygonCount,
			vertices,
			polygonVertexCounts,
			polygonVertexIndices,
			transform,
			&stat );  diLOG_MSTATUS( stat );
	}
	else if ( mode == 1 )
	{
		stat = fnMesh.createInPlace(
			vertexCount,
			polygonCount, 
			vertices,
			polygonVertexCounts,
			polygonVertexIndices );  diLOG_MSTATUS( stat );
	}
	else
	{
		// mode neither "create" nor "update"; this should never happen
	}
	*/

	fnMesh.create(
		vertexCount,
		polygonCount,
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		transform,
		&stat );  diLOG_MSTATUS( stat );

	// Do this to load up newOutputData with the mesh data
	MFnMesh fnTempMesh;
	fnTempMesh.create(
		vertexCount,
		polygonCount,
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		newOutputData,
		&stat );  diLOG_MSTATUS( stat );


	/*
	fnGeometryBasedMesh.create(
		vertexCount,
		polygonCount,
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		geometryBasedOutputData,
		&stat );  diLOG_MSTATUS( stat );

	fnDataBasedMesh.create(
		vertexCount,
		polygonCount,
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		dataBasedOutputData,
		&stat );  diLOG_MSTATUS( stat );
		*/

	return MStatus::kSuccess;
}


/* ============================================================================
// addSection()
*/
MStatus tubeUV::addSection(
	int iCV,
	MFloatPoint controlVertex,
	MVector tangent,
	double tubeRadius,
	int segmentCount,
	int UCycles,
	double spiralAngleRadians,
	double fixedAngleRadians,
	MFloatPointArray& vertices,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices )
{
	MStatus stat;

	// This is the way to build the mesh by hand
	//
	MVector zAxis( 0.0, 0.0, 1.0 );
	MQuaternion quaternion( zAxis, tangent );
	MVector vector;
	MVector alignedVector;
	double length = tubeRadius;

	double totalAngle = fixedAngleRadians + spiralAngleRadians * (double)iCV;

	// First, the vertices
	unsigned int vertexCount = vertices.length();
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		vector = tubeUV::unitCircle[ iSegment ];
		vector = vector * length;
		vector = vector.rotateBy( MVector::kZaxis, totalAngle );
		alignedVector = vector.rotateBy( quaternion );
		vertices.append( controlVertex + alignedVector );
	}

	// Then the connectivity, as long as it's not the first CV
	if ( iCV > 0 )
	{
		int vertexIndexBase;

		// Connectivity for all but the last segment of the circle
		for ( int iSegment=0; iSegment < segmentCount-1; iSegment++ )
		{
			vertexIndexBase = vertexCount + iSegment;  // vertexCount == 0-based index of first vertex in new circle
			polygonVertexCounts.append(4);
			polygonVertexIndices.append( vertexIndexBase - segmentCount + 0 );
			polygonVertexIndices.append( vertexIndexBase                + 0 );
			polygonVertexIndices.append( vertexIndexBase                + 1 );
			polygonVertexIndices.append( vertexIndexBase - segmentCount + 1 );
		}

		// Connectivity for the last segment of the circle
		vertexIndexBase = vertexIndexBase + 1;
		polygonVertexCounts.append(4);
		polygonVertexIndices.append( vertexIndexBase - segmentCount + 0 );
		polygonVertexIndices.append( vertexIndexBase                + 0 );
		polygonVertexIndices.append( vertexIndexBase                + 1 - segmentCount );  // wraps around to beginning
		polygonVertexIndices.append( vertexIndexBase - segmentCount + 1 - segmentCount );  // of the circle
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// assignUVs()
//
// You can't assign multiple UV sets at this point.  It only works when the MFnMesh refers to a shape;
// that is, when the mesh was created by a command like "createNode mesh -name $shape -p $transform;".
// So, this method has to move into its own specialized node.
//
*/
MStatus tubeUV::assignUVs(
	//MFnMesh& fnGeometryBasedMesh,
	//MFnMesh& fnDataBasedMesh,
	MFnMesh& fnMesh,
	int curveCount,
	MIntArray startIndices,
	MIntArray CVCounts,
	MPointArray controlVertexList,
	int curveIndex,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MDoubleArray& scalarDataGrid,
	double dataMin,
	double dataMax,
	int segmentCount,
	int UCycles,
	int stepsPerVCycle)
{
	MStatus stat;
	double slope = 1.0;
	if ( dataMax != dataMin ) slope = 1.0 / (dataMax-dataMin);

	// U coordinate goes around the circle, equates to x coordinate of texture swatch
	// V coordinate goes along the tube, its value is:
	//   (a) scaled to the scalarDatum at the control vertex, equates to y coordinate of texture swatch, if the UVMode is "data-based"
	//   (b) cycles up and down, 0->1->0 as determined by stepsPerVCycle, if the UVMode is "geometry-based"
	//
	// Set up all possible UV pairs.  Make segmentCount+1 U values, for example:
	//   0, 0.25, 0.5, 0.75, 1, 0.75, 0.5, 0.25, 0 (9 U values for 8 segments).
	//
	// For data-based UVMode, make 1024 V values from 0 to 1. 
	// For geometry-based UVMode, make stepsPerVCycle+1 V values, as in the U case
	//
	int cycleSegmentCount = segmentCount / UCycles;
	int halfCycleSegmentCount = cycleSegmentCount / 2;
	MFloatArray uValues;
	for ( int iUCycle=0; iUCycle<UCycles; iUCycle++ )
	{
		for ( int iU=0; iU<cycleSegmentCount; iU++ )
		{
			if ( iU < halfCycleSegmentCount ) uValues.append( (float)iU/(float)halfCycleSegmentCount );
			else uValues.append( (float)(cycleSegmentCount-iU)/(float)halfCycleSegmentCount );
		}
	}
	uValues.append( 0.0 );

	int uvId=0;
	float uValue, vValue;
	int CVCount = 2;
	MString dataBasedUVSetName;
	MFloatArray uArray;
	MFloatArray vArray;


	// Geometry-based UV set
	//
	// V mapped to CV index
	if ( (curveIndex >= 0) && (curveIndex < curveCount) )
		if ( CVCounts[curveIndex] > 1 ) CVCount = CVCounts[curveIndex];

	int halfCycleCVCount = stepsPerVCycle / 2;
	MFloatArray vValues;
	for ( int iV=0; iV<stepsPerVCycle; iV++ )
	{
		if ( iV < halfCycleCVCount ) vValues.append( (float)iV/(float)halfCycleCVCount );
		else vValues.append( (float)(stepsPerVCycle-iV)/(float)halfCycleCVCount );
	}
	vValues.append( 0.0 );

	for ( int iCV=0; iCV<CVCount; iCV++ )
	{
		div_t divResult;
		divResult = div( iCV, stepsPerVCycle );
		int iV = divResult.rem; 
		vValue = vValues[iV];
		for ( int iU=0; iU<(segmentCount+1); iU++ )
		{
			uValue = uValues[iU];
			uArray.append( uValue );
			vArray.append( vValue );
			//diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue) );
			//uvId++;
		}
	}
	diLOG_MSTATUS( fnMesh.setUVs(uArray, vArray) );


	// Data-based UV set
	//
	dataBasedUVSetName = fnMesh.createUVSetWithName( MString("dataBased") );
	uArray.clear();
	vArray.clear();
	
	// V mapped to secondary data in 1024 steps
	for ( int iScalarDatum=0; iScalarDatum<1024; iScalarDatum++ )
	{
		vValue = (float)( iScalarDatum / 1023.0 );
		for ( int iU=0; iU<(segmentCount+1); iU++ )
		{
			uValue = uValues[iU];
			uArray.append( uValue );
			vArray.append( vValue );
			//diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue, dataBasedUVSetNamePtr) );
			//uvId++;
		}
	}
	diLOG_MSTATUS( fnMesh.setUVs(uArray, vArray, &dataBasedUVSetName) );


	// Step through control vertex array
	//
	int index;
	int iPolygonBase, iPolygon;
	MPoint controlVertex1, controlVertex2;
	double scalarDatum1, scalarDatum2;
	double vValue1, vValue2;
	int uvIdBase1, uvIdBase2;
	double halfWindow = 2.0;

	MIntArray geomBasedUVCounts;
	MIntArray geomBasedUVIds;
	MIntArray dataBasedUVCounts;
	MIntArray dataBasedUVIds;

	CVCount = 0;
	int startIndex = 0;
	if ( (curveIndex >= 0) && (curveIndex < curveCount) )
	{
		CVCount = CVCounts[ curveIndex ];
		startIndex = startIndices[ curveIndex ];

		for ( int iCV = 0; iCV < CVCount-1; iCV++ )
		{
			iPolygonBase = iCV*segmentCount;
			index = startIndex + iCV;
			
			// Geometry-based UV set (default set)
			//
			// V mapped to CV index
			//vValue1 = (double)iCV / (double)(CVCount - 1);
			//vValue2 = (double)(iCV + 1) / (double)(CVCount - 1);
			//if ( vValue1 < 0.0 ) vValue1=0.0;  if ( vValue1 > 1.0 ) vValue1 = 1.0;
			//if ( vValue2 < 0.0 ) vValue2=0.0;  if ( vValue2 > 1.0 ) vValue2 = 1.0;
			//uvIdBase1 = (int)(vValue1*(CVCount - 1)) * (segmentCount+1);
			//uvIdBase2 = (int)(vValue2*(CVCount - 1)) * (segmentCount+1);
			uvIdBase1 = iCV * (segmentCount+1);
			uvIdBase2 = (iCV+1) * (segmentCount+1);

			for (int iSegment=0; iSegment<segmentCount; iSegment++ )
			{
				geomBasedUVCounts.append( 4 );
				iPolygon = iPolygonBase + iSegment;

				uvId = uvIdBase1 + iSegment;
				geomBasedUVIds.append( uvId );
				//diLOG_MSTATUS( fnMesh.assignUV(iPolygon, 0, uvId) );

				uvId = uvIdBase2 + iSegment;
				geomBasedUVIds.append( uvId );
				//diLOG_MSTATUS( fnMesh.assignUV(iPolygon, 1, uvId) );

				uvId = uvIdBase2 + iSegment + 1;
				geomBasedUVIds.append( uvId );
				//diLOG_MSTATUS( fnMesh.assignUV(iPolygon, 2, uvId) );

				uvId = uvIdBase1 + iSegment + 1;
				geomBasedUVIds.append( uvId );
				//diLOG_MSTATUS( fnMesh.assignUV(iPolygon, 3, uvId) );
			}
			

			// Data-based UV set
			//
			// V mapped to secondary data in 1024 steps
			controlVertex1 = controlVertexList[index];
			controlVertex2 = controlVertexList[index+1];
			scalarDatum1 = lapdCommon::interpolateScalar( controlVertex1, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
			scalarDatum2 = lapdCommon::interpolateScalar( controlVertex2, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
			vValue1 = slope * (scalarDatum1-dataMin);
			vValue2 = slope * (scalarDatum2-dataMin);
			if ( vValue1 < 0.0 ) vValue1=0.0;  if ( vValue1 > 1.0 ) vValue1 = 1.0;
			if ( vValue2 < 0.0 ) vValue2=0.0;  if ( vValue2 > 1.0 ) vValue2 = 1.0;
			uvIdBase1 = (int)(vValue1*1023.0) * (segmentCount+1);
			uvIdBase2 = (int)(vValue2*1023.0) * (segmentCount+1);

			for (int iSegment=0; iSegment<segmentCount; iSegment++ )
			{
				dataBasedUVCounts.append( 4 );
				iPolygon = iPolygonBase + iSegment;

				uvId = uvIdBase1 + iSegment;
				dataBasedUVIds.append( uvId );
				//diLOG_MSTATUS( fnMesh.assignUV(iPolygon, 0, uvId, &dataBasedUVSetName) );

				uvId = uvIdBase2 + iSegment;
				dataBasedUVIds.append( uvId );
				//diLOG_MSTATUS( fnMesh.assignUV(iPolygon, 1, uvId, &dataBasedUVSetName) );

				uvId = uvIdBase2 + iSegment + 1;
				dataBasedUVIds.append( uvId );
				//diLOG_MSTATUS( fnMesh.assignUV(iPolygon, 2, uvId, &dataBasedUVSetName) );

				uvId = uvIdBase1 + iSegment + 1;
				dataBasedUVIds.append( uvId );
				//diLOG_MSTATUS( fnMesh.assignUV(iPolygon, 3, uvId, &dataBasedUVSetName) );
			}
		}
	//diLOG_MSTATUS( fnGeometryBasedMesh.assignUVs(geomBasedUVCounts, geomBasedUVIds) );
	//diLOG_MSTATUS( fnDataBasedMesh.assignUVs(dataBasedUVCounts, dataBasedUVIds) );
	diLOG_MSTATUS( fnMesh.assignUVs(geomBasedUVCounts, geomBasedUVIds) );
	diLOG_MSTATUS( fnMesh.assignUVs(dataBasedUVCounts, dataBasedUVIds, &dataBasedUVSetName) );
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the UV-mapped tubes.
//
*/
MStatus tubeUV::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> tubeUV::compute()" );
	forceFieldCommon::logEntry( forceFieldCommon::eTubeUV );
	cout << "--> tubeUV::compute()" << endl;

	if ( plug != aSecondaryMesh )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from tubeUV::compute(), Maya not asking for secondary mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	/*
	MDataHandle hMeshName = block.inputValue( aMeshName, &stat );   diLOG_MSTATUS( stat );
	MString meshName = hMeshName.asString();

	MDataHandle hMode = block.inputValue( aMode, &stat );   diLOG_MSTATUS( stat );
	int mode = hMode.asInt();
	cout << "mode: " << mode << endl;
	*/

	MDataHandle hTransform( block.inputValue(aTransform, &stat) );   diLOG_MSTATUS( stat );
	MString transformName = hTransform.asString();

	MDataHandle hCurveCount( block.inputValue(aCurveCount, &stat) );   diLOG_MSTATUS( stat );
	int curveCount = hCurveCount.asInt();

	MDataHandle hStartIndices( block.inputValue(aStartIndices, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnStartIndices( hStartIndices.data() );
	MIntArray startIndices( fnStartIndices.array() );

	MDataHandle hCVCounts( block.inputValue(aCVCounts, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnCVCounts( hCVCounts.data() );
	MIntArray CVCounts( fnCVCounts.array() );

	MDataHandle hControlVertexList( block.inputValue(aControlVertexList, &stat) );   diLOG_MSTATUS( stat );
	MFnPointArrayData fnControlVertexList( hControlVertexList.data() );
	MPointArray controlVertexList( fnControlVertexList.array() );

	MDataHandle hTangentList( block.inputValue(aTangentList, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnTangentList( hTangentList.data() );
	MVectorArray tangentList( fnTangentList.array() );

	MDataHandle hCurveIndex( block.inputValue(aCurveIndex, &stat) );   diLOG_MSTATUS( stat );
	int curveIndex = hCurveIndex.asInt();

	MDataHandle hTubeRadius( block.inputValue(aTubeRadius, &stat) );   diLOG_MSTATUS( stat );
	double tubeRadius = hTubeRadius.asDouble();

	MDataHandle hSegmentCount( block.inputValue(aSegmentCount, &stat) );   diLOG_MSTATUS( stat );
	int segmentCount = hSegmentCount.asInt();

	MDataHandle hUCycles( block.inputValue(aUCycles, &stat) );   diLOG_MSTATUS( stat );
	int UCycles = hUCycles.asInt();

	MDataHandle hSpiralAngle( block.inputValue(aSpiralAngle, &stat) );   diLOG_MSTATUS( stat );
	double spiralAngle = hSpiralAngle.asDouble();

	MDataHandle hFixedAngle( block.inputValue(aFixedAngle, &stat) );   diLOG_MSTATUS( stat );
	double fixedAngle = hFixedAngle.asDouble();

	MDataHandle hStepsPerVCycle( block.inputValue(aStepsPerVCycle, &stat) );   diLOG_MSTATUS( stat );
	int stepsPerVCycle = hStepsPerVCycle.asInt();

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hScalarDataGrid( block.inputValue(aScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnDoubleArrayData fnScalarDataGrid( hScalarDataGrid.data() );
	MDoubleArray scalarDataGrid( fnScalarDataGrid.array() );

	MDataHandle hDataMin( block.inputValue(aDataMin, &stat) );   diLOG_MSTATUS( stat );
	double dataMin = hDataMin.asDouble();

	MDataHandle hDataMax( block.inputValue(aDataMax, &stat) );   diLOG_MSTATUS( stat );
	double dataMax = hDataMax.asDouble();


	// Now do the output attributes.
	//
	//MDataHandle hGeometryBasedMesh =  block.outputValue( aGeometryBasedMesh, &stat );   diLOG_MSTATUS( stat );
	//MDataHandle hDataBasedMesh =  block.outputValue( aDataBasedMesh, &stat );   diLOG_MSTATUS( stat );
	MDataHandle hSecondaryMesh( block.outputValue(aSecondaryMesh, &stat) );   diLOG_MSTATUS( stat );


	// Create the data objects which will be passed out as aGeometryBasedMesh, aDataBasedMesh
	//MFnMeshData fnGeometryBasedMeshData;
	//MFnMeshData fnDataBasedMeshData;
	//MObject geometryBasedOutputData = fnGeometryBasedMeshData.create( &stat );   diLOG_MSTATUS( stat );
	//MObject dataBasedOutputData = fnDataBasedMeshData.create( &stat );   diLOG_MSTATUS( stat );
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create( &stat ) );   diLOG_MSTATUS( stat );

	// Force segmentCount to be multiple of 2*UCycles
	div_t divResult;
	divResult = div( segmentCount, 2*UCycles );
	segmentCount = segmentCount + divResult.rem; 

	// Force stepsPerVCycle to be multiple of 2
	divResult = div( stepsPerVCycle, 2 );
	stepsPerVCycle = stepsPerVCycle + divResult.rem; 

	// Unit circle
	unitCircle.clear();
	double angleFactor = (1.0/(double)segmentCount)*(2.0*3.1415927);
	double theta;
	double x, y, z = 0.0;
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		theta = (double)iSegment * angleFactor;
		x = cos( theta );
		y = sin( theta );
		unitCircle.append( x, y, z );
	}


	// Search through the Directed Acyclic Graph (DAG) for the given transform name
	//
	MItDag itDag( MItDag::kDepthFirst, MFn::kTransform, &stat );  diLOG_MSTATUS( stat );
	MObject transform;
	bool transformFound = false;
    for ( ; !itDag.isDone(); itDag.next() )
	{
        //get the current DAG path
        //
        MDagPath currentDagPath;
        diLOG_MSTATUS( itDag.getPath(currentDagPath) );
        MFnDagNode fnCurrentDagNode( currentDagPath, &stat );  diLOG_MSTATUS( stat );
		MString partialPathName( fnCurrentDagNode.partialPathName(&stat) );  diLOG_MSTATUS( stat );
		cout << "partialPathName: " << partialPathName.asChar() << endl;
		unsigned int parentCount = fnCurrentDagNode.parentCount();
		if ( transformName == partialPathName )
		{
			transformFound = true;
			transform = currentDagPath.node( &stat );  diLOG_MSTATUS( stat );
		}
	}

	if ( !transformFound )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from tubeUV::compute(), transform not found" );
		return MS::kFailure;
	}

	/*
	// If the mode is "create", then start with a blank fnMesh and do a create inside createMesh(), with the transform as the parent.
	// If the mode is "update", then attach fnMesh to the actual mesh, then do the work to create the vertices and UV's.
	//
	// Note that in both cases the vertex information ends up in "newOutputData", so that it can be passed explicitly in the dependency graph.
	// This is a kludge.  Maybe there is some way around it but for now that's how I do it.
	//
	if ( mode == 0 )
	{
		// 0 == create
		MFnMesh fnMesh;

		cout << "About to call createMesh()" << endl;
		diLOG_MSTATUS(
			createMesh(
				mode,
				fnMesh,
				transform,
				curveCount,
				startIndices,
				CVCounts,
				controlVertexList,
				tangentList,
				curveIndex,
				segmentCount,
				tubeRadius,
				UCycles,
				spiralAngle,
				fixedAngle,
				newOutputData) );

		// Make the call to assign the UV's
		diLOG_MSTATUS(
			assignUVs(
				fnMesh,
				curveCount,
				startIndices,
				CVCounts,
				controlVertexList,
				curveIndex,
				dims,
				gridMin,
				gridMax,
				scalarDataGrid,
				dataMin,
				dataMax,
				segmentCount,
				UCycles,
				stepsPerVCycle) );
	}
	else if ( mode == 1 )
	{
		// 1 == update
		cout << "About to call fnMesh( mesh, &stat );" << endl;
		MFnMesh fnMesh( mesh, &stat );  diLOG_MSTATUS( stat );

		cout << "About to call createMesh()" << endl;
		diLOG_MSTATUS(
			createMesh(
				mode,
				fnMesh,
				transform,
				curveCount,
				startIndices,
				CVCounts,
				controlVertexList,
				tangentList,
				curveIndex,
				segmentCount,
				tubeRadius,
				UCycles,
				spiralAngle,
				fixedAngle,
				newOutputData) );

		// Make the call to assign the UV's
		diLOG_MSTATUS(
			assignUVs(
				fnMesh,
				curveCount,
				startIndices,
				CVCounts,
				controlVertexList,
				curveIndex,
				dims,
				gridMin,
				gridMax,
				scalarDataGrid,
				dataMin,
				dataMax,
				segmentCount,
				UCycles,
				stepsPerVCycle) );
	}
	else
	{
		// mode neither "create" nor "update"; this should not happen
	}
	*/

	//MFnMesh fnGeometryBasedMesh;
	//MFnMesh fnDataBasedMesh;
	MFnMesh fnMesh;
	cout << "About to call createMesh()" << endl;
	diLOG_MSTATUS(
		createMesh(
			//mode,
			//fnGeometryBasedMesh,
			//fnDataBasedMesh,
			transform,
			fnMesh,
			curveCount,
			startIndices,
			CVCounts,
			controlVertexList,
			tangentList,
			curveIndex,
			segmentCount,
			tubeRadius,
			UCycles,
			spiralAngle,
			fixedAngle,
			//geometryBasedOutputData,
			//dataBasedOutputData) );
			newOutputData) );

	// Make the call to assign the UV's
	diLOG_MSTATUS(
		assignUVs(
			//fnGeometryBasedMesh,
			//fnDataBasedMesh,
			fnMesh,
			curveCount,
			startIndices,
			CVCounts,
			controlVertexList,
			curveIndex,
			dims,
			gridMin,
			gridMax,
			scalarDataGrid,
			dataMin,
			dataMax,
			segmentCount,
			UCycles,
			stepsPerVCycle) );


	// Set the outputs as clean.
	//hGeometryBasedMesh.set( geometryBasedOutputData );
	//hDataBasedMesh.set( dataBasedOutputData );
	hSecondaryMesh.set( newOutputData );
	block.setClean( plug );

	//forceFieldCommon::logEntry( (char*) "<-- tubeUV::compute()" );
	cout << "<-- tubeUV::compute()" << endl;
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information and the unit circle values
*/
MStatus tubeUV::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MFnStringData stringData;
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> tubeUV::initialize()" );


	// Create input attributes:
	//
	/*
	// mesh name
	aMeshName = tAttr.create( "meshName", "meshName", MFnData::kString, stringData.create(&stat2), &stat );  diLOG_MSTATUS( stat );  diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aMeshName) );

	// mode
    aMode = enumAttr.create( "mode", "mode", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("create", 0) );
	diLOG_MSTATUS( enumAttr.addField("update", 1) );
	MAKE_INPUT( enumAttr );
	diLOG_MSTATUS( addAttribute(aMode) );
	*/

	// mesh name
	aTransform = tAttr.create( "transform", "transform", MFnData::kString, stringData.create(&stat2), &stat );  diLOG_MSTATUS( stat );  diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aTransform) );

	// curve count
	aCurveCount = nAttr.create( "curveCount", "curveCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveCount) );

	// start indices
	aStartIndices = tAttr.create( "startIndices", "startIndices", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aStartIndices) );

	// CV counts
	aCVCounts = tAttr.create( "CVCounts", "CVCounts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aCVCounts) );

	// control vertices
	aControlVertexList = tAttr.create( "controlVertexList", "controlVertexList", MFnData::kPointArray, fnPointArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aControlVertexList) );

	// tangents
	aTangentList = tAttr.create( "tangentList", "tangentList", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aTangentList) );

	// curve index
	aCurveIndex = nAttr.create( "curveIndex", "curveIndex", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveIndex) );

	// tube radius
	aTubeRadius = nAttr.create( "tubeRadius", "tubeRadius", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aTubeRadius) );

	// segment count
	aSegmentCount = nAttr.create( "segmentCount", "segmentCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(24) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aSegmentCount) );

	// U cycles
	aUCycles = nAttr.create( "UCycles", "UCycles", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	diLOG_MSTATUS( nAttr.setMax(3) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aUCycles) );

	// spiral angle
	aSpiralAngle = nAttr.create( "spiralAngle", "spiralAngle", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aSpiralAngle) );

	// fixed angle
	aFixedAngle = nAttr.create( "fixedAngle", "fixedAngle", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aFixedAngle) );

	// steps per V cycle
	aStepsPerVCycle = nAttr.create( "stepsPerVCycle", "stepsPerVCycle", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(20) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aStepsPerVCycle) );

	// grid dimensions
	aDims = nAttr.create( "gridDims", "gridDims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gridMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gridMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// scalar data grid
	aScalarDataGrid = tAttr.create( "scalarDataGrid", "scalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aScalarDataGrid) );

	// data min
	aDataMin = nAttr.create( "dataMin", "dataMin", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aDataMin) );

	// data max
	aDataMax = nAttr.create( "dataMax", "dataMax", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aDataMax) );


	// Create output attributes
	//
	// secondary mesh
	aSecondaryMesh = tAttr.create( "secondaryMesh", "secondaryMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aSecondaryMesh) );


	// All inputs affect all outputs
	//
	// secondary mesh
    //diLOG_MSTATUS( attributeAffects(aMeshName, aSecondaryMesh) );
    //diLOG_MSTATUS( attributeAffects(aMode, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTransform, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCurveCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStartIndices, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCVCounts, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aControlVertexList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTangentList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCurveIndex, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTubeRadius, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aSegmentCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aUCycles, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aSpiralAngle, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aFixedAngle, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStepsPerVCycle, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aDims, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aScalarDataGrid, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMin, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMax, aSecondaryMesh) );

	forceFieldCommon::logEntry( (char*) "<-- tubeUV::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
// tubeUV()
*/
tubeUV::tubeUV()
{
}


/* ============================================================================
// postConstructor()
*/
void tubeUV::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType tubeUV::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * tubeUV::creator()
{
    return new tubeUV();
}


/* ============================================================================
// ~tubeUV()
*/
tubeUV::~tubeUV()
{
}


