/* ==========================================================================
// HDF5 : HDF5file.h
//
// Copyright (C) 2008 Jim Bamber 
// 
*/
#ifndef HDF5FILE_H
#define HDF5FILE_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h> 

class HDF5file : public MPxNode
{
public:
    MPxNode::SchedulingType schedulingType() const;

	virtual MStatus compute( const MPlug& plug, MDataBlock& data );

    HDF5file();
	virtual void postConstructor();
    virtual ~HDF5file();

	static  void *creator();
	static  MStatus initialize();

public:
	static MObject aFilename;

	static MObject aScalarDataGroups;		// Array of scalar dataset group names (string array)
	static MObject aVectorDataGroups;		// Array of scalar dataset group names (string array)

	static	MTypeId id;

private:
	MStatus computeKernel( const MPlug& plug, MDataBlock& data );

};

#endif
