/* ============================================================================
// forceField : markers.cpp
//-
// ==========================================================================
// Copyright (C) 2008 Jim Bamber
//
// ==========================================================================
//+
*/
#include "markers.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>
#include <time.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshEdge.h>
#include <maya/MQuaternion.h>


// Static data
MTypeId markers::id( 0x00460 );

// Attributes
MObject markers::aMarkerMesh;
MObject markers::aCurveCount;
MObject markers::aStartIndices;
MObject markers::aCVCounts;
MObject markers::aControlVertexList;
MObject markers::aTangentList;
MObject markers::aCurveIndex;
MObject markers::aStart;
MObject markers::aStep;
MObject markers::aReverse;

MObject markers::aSecondaryMesh;


/* ============================================================================
// createMesh()
*/
MStatus markers::createMesh(
	MObject& markerMesh,
	int curveCount,
	MIntArray startIndices,
	MIntArray CVCounts,
	MPointArray controlVertexList,
	MVectorArray tangentList,
	int curveIndex,
	int start,
	int step,
	bool reverse,
	MObject& newOutputData )
{
	MStatus stat;

	MFnMesh fnMesh;
	int index;
	MPoint controlVertex;
	MVector tangent;

	if ( start < 0 ) start = 0;
	if ( step < 1 ) step = 1;

	// Quaternion used for reversal
	MVector yAxis( 0.0, 1.0, 0.0 );
	double reverseRadians;
	if ( reverse ) reverseRadians = 3.1415927;
	else reverseRadians = 0.0;
	MQuaternion reverseQuaternion( reverseRadians, yAxis );

	// Create the working mesh object, needed to handle the "reverse" option
	MFnMesh fnWorkMarkerMesh;
	MFnMeshData fnWorkMarkerMeshData;
	MObject workMarkerMeshData( fnWorkMarkerMeshData.create(&stat) );   diLOG_MSTATUS( stat );

	// Iterate through input marker mesh polygons and reverse vertices, if necessary
	MItMeshPolygon markerPolygonIter( markerMesh, &stat );   diLOG_MSTATUS( stat );
	unsigned int markerPolygonCount = markerPolygonIter.count( &stat );   diLOG_MSTATUS( stat );
	MPointArray polygonVertices;
	MVector vector, reversedVector;
	for ( int i=0; i<(int)markerPolygonCount; i++ )
	{
		markerPolygonIter.getPoints( polygonVertices, MSpace::kWorld, &stat );   diLOG_MSTATUS( stat );
		for ( int j=0; j<(int)(polygonVertices.length()); j++ )
		{
			vector = MVector( polygonVertices[j] );
			reversedVector = vector.rotateBy( reverseQuaternion );
			polygonVertices[j] = MPoint( reversedVector );
		}
		fnWorkMarkerMesh.addPolygon( polygonVertices, true, 1.0e-4, workMarkerMeshData, &stat );   diLOG_MSTATUS( stat );
		markerPolygonIter.next();
	}

	int CVCount = 0;
	int startIndex = 0;
	if ( curveIndex < curveCount )
	{
		CVCount = CVCounts[ curveIndex ];
		startIndex = startIndices[ curveIndex ];
	}

	for ( int iCV = start; iCV < CVCount; iCV = iCV + step )
	{
		index = startIndex + iCV;
		controlVertex = controlVertexList[index];
		tangent = tangentList[index];

		addMarker( workMarkerMeshData, fnMesh, newOutputData, controlVertex, tangent );
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// addMarker()
*/
MStatus markers::addMarker(
	MObject& workMarkerMeshData,
	MFnMesh& fnMesh,
	MObject& newOutputData,
	MPoint controlVertex,
	MVector tangent )
{
	MStatus stat;

	MVector zAxis( 0.0, 0.0, 1.0 );
	MQuaternion tangentQuaternion( zAxis, tangent );
	MVector vector, rotatedVector;
	MVectorArray vectors;
	MPointArray vertexArray;

	// Iterate through work marker mesh polygons and rotate and translate them
	MItMeshPolygon markerPolygonIter( workMarkerMeshData, &stat );   diLOG_MSTATUS( stat );
	unsigned int markerPolygonCount = markerPolygonIter.count( &stat );   diLOG_MSTATUS( stat );
	MPointArray polygonVertices;
	for ( int i=0; i<(int)markerPolygonCount; i++ )
	{
		markerPolygonIter.getPoints( polygonVertices, MSpace::kWorld, &stat );   diLOG_MSTATUS( stat );
		for ( int j=0; j<(int)(polygonVertices.length()); j++ )
		{
			vector = MVector( polygonVertices[j] );
			rotatedVector = vector.rotateBy( tangentQuaternion );
			polygonVertices[j] = controlVertex + MPoint( rotatedVector );
		}
		fnMesh.addPolygon( polygonVertices, true, 1.0e-4, newOutputData, &stat );   diLOG_MSTATUS( stat );
		markerPolygonIter.next();
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the curves.
//
*/
MStatus markers::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> markers::compute()" );
	forceFieldCommon::logEntry( forceFieldCommon::eMarkers );

	if ( plug != aSecondaryMesh )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from markers::compute(), Maya not asking for secondary mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hMarkerMesh( block.inputValue(aMarkerMesh, &stat) );   diLOG_MSTATUS( stat );
	MObject markerMesh( hMarkerMesh.asMeshTransformed() );

	MDataHandle hCurveCount( block.inputValue(aCurveCount, &stat) );   diLOG_MSTATUS( stat );
	int curveCount = hCurveCount.asInt();

	MDataHandle hStartIndices( block.inputValue(aStartIndices, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnStartIndices( hStartIndices.data() );
	MIntArray startIndices( fnStartIndices.array() );

	MDataHandle hCVCounts( block.inputValue(aCVCounts, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnCVCounts( hCVCounts.data() );
	MIntArray CVCounts( fnCVCounts.array() );

	MDataHandle hControlVertexList( block.inputValue(aControlVertexList, &stat) );   diLOG_MSTATUS( stat );
	MFnPointArrayData fnControlVertexList( hControlVertexList.data() );
	MPointArray controlVertexList( fnControlVertexList.array() );

	MDataHandle hTangentList( block.inputValue(aTangentList, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnTangentList( hTangentList.data() );
	MVectorArray tangentList( fnTangentList.array() );

	MDataHandle hCurveIndex( block.inputValue(aCurveIndex, &stat) );   diLOG_MSTATUS( stat );
	int curveIndex = hCurveIndex.asInt();

	MDataHandle hStart( block.inputValue(aStart, &stat) );   diLOG_MSTATUS( stat );
	int start = hStart.asInt();

	MDataHandle hStep( block.inputValue(aStep, &stat) );   diLOG_MSTATUS( stat );
	int step = hStep.asInt();

	MDataHandle hReverse( block.inputValue(aReverse, &stat) );   diLOG_MSTATUS( stat );
	bool reverse = hReverse.asBool();


	// Now do the output attributes.
	//
	MDataHandle hSecondaryMesh( block.outputValue(aSecondaryMesh, &stat) );   diLOG_MSTATUS( stat );


	// Create the data object which underlies the output mesh
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );

	// Make the call to create the secondary mesh
	//forceFieldCommon::logEntry( (char*) "About to create mesh" );
	diLOG_MSTATUS(
		createMesh(
			markerMesh,
			curveCount,
			startIndices,
			CVCounts,
			controlVertexList,
			tangentList,
			curveIndex,
			start,
			step,
			reverse,
			newOutputData) );


	// Set the outputs as clean.
	hSecondaryMesh.set( newOutputData );
	block.setClean( plug );

	//forceFieldCommon::logEntry( (char*) "<-- markers::compute()" );
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information and the unit circle values
*/
MStatus markers::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> markers::initialize()" );


	// Create input attributes:
	//
	// marker mesh
	aMarkerMesh = tAttr.create( "markerMesh", "markerMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aMarkerMesh) );

	// curve count
	aCurveCount = nAttr.create( "curveCount", "curveCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveCount) );

	// start indices
	aStartIndices = tAttr.create( "startIndices", "startIndices", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aStartIndices) );

	// CV counts
	aCVCounts = tAttr.create( "CVCounts", "CVCounts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aCVCounts) );

	// control vertices
	aControlVertexList = tAttr.create( "controlVertexList", "controlVertexList", MFnData::kPointArray, fnPointArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aControlVertexList) );

	// tangents
	aTangentList = tAttr.create( "tangentList", "tangentList", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aTangentList) );

	// curve index
	aCurveIndex = nAttr.create( "curveIndex", "curveIndex", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveIndex) );

	// start
	aStart = nAttr.create( "start", "start", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	diLOG_MSTATUS( nAttr.setMin(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aStart) );

	// step
	aStep = nAttr.create( "step", "step", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aStep) );

	// reverse
	aReverse = nAttr.create( "reverse", "reverse", MFnNumericData::kBoolean, false, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aReverse) );


	// Create output attributes
	//
	// secondary mesh
	aSecondaryMesh = tAttr.create( "secondaryMesh", "secondaryMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aSecondaryMesh) );


	// All inputs affect all outputs
	//
	// secondary mesh
    diLOG_MSTATUS( attributeAffects(aMarkerMesh, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCurveCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStartIndices, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCVCounts, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aControlVertexList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTangentList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCurveIndex, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStart, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStep, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aReverse, aSecondaryMesh) );

	forceFieldCommon::logEntry( (char*) "<-- markers::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
// markers()
*/
markers::markers()
{
}


/* ============================================================================
// postConstructor()
*/
void markers::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType markers::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * markers::creator()
{
    return new markers();
}


/* ============================================================================
// ~markers()
*/
markers::~markers()
{
}


