/* ==========================================================================
// HDF5 : HDF5scalarDataGrid.h
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#ifndef HDF5SCALARDATAGRID_H
#define HDF5SCALARDATAGRID_H

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h> 

class HDF5scalarDataGrid : public MPxNode
{
public:
    MPxNode::SchedulingType schedulingType() const;

	virtual MStatus compute( const MPlug& plug, MDataBlock& data );

    HDF5scalarDataGrid();
	virtual void postConstructor();
    virtual ~HDF5scalarDataGrid();

	static  void *creator();
	static  MStatus initialize();

public:

	// HDF5scalarDataNode has these attributes:
	// Input
	//   filename
	//   group name
	//   time index
	//   parameter1-4 indices
	//
	//   multiplier -- applied first
	//   offset     -- applied second
	//
	static MObject filename;
	static MObject groupName;

	static MObject timeIndex;

	static MObject parameter1Index;
	static MObject parameter2Index;
	static MObject parameter3Index;
	static MObject parameter4Index;

	static MObject aMultiplier;
	static MObject aOffset;


	// Output
	//   3D volume of scalar data
	//   max and min data values
	//
	static MObject scalarDataGrid;
	static MObject max;
	static MObject min;

	static	MTypeId id;

private:
	MStatus computeKernel( const MPlug& plug, MDataBlock& data );

};

#endif
