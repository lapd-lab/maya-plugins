/* ============================================================================
// bump : scalarBump.cpp
//-
// ==========================================================================
// Copyright (C) 2007 Jim Bamber
//
// ==========================================================================
//+
*/
#include "scalarBump.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "bumpCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MFnMeshData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>


// Static data
MTypeId scalarBump::id( 0x00501 );
int scalarBump::uvIdCount( 256 );

// Attributes
MObject scalarBump::aInputMesh;
MObject scalarBump::aInputMeshNormal;
MObject scalarBump::aDims;
MObject scalarBump::aGridMin;
MObject scalarBump::aGridMax;
MObject scalarBump::aDataMin;
MObject scalarBump::aDataMax;
MObject scalarBump::aScalarDataGrid;
MObject scalarBump::aBumpRotate;
MObject scalarBump::aBumpRotateOrder;
MObject scalarBump::aBumpScale;
MObject scalarBump::aCentering;
MObject scalarBump::aCenterValue;
MObject scalarBump::aDataMinMapsTo;
MObject scalarBump::aDataMaxMapsTo;

MObject scalarBump::aOutputMesh;


/* ============================================================================
 * calculateBump()
 */
MStatus scalarBump::calculateBump(
	MPointArray& inVertices,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	double dataMin,
	double dataMax,
	MDoubleArray& scalarDataGrid,   /* argument group 1: used to interpolate and scale the scalar datum */
	/*.................*/	
	MVector& bumpNormal,
	double bumpScale,
	short centering,
	double centerValue,
	MPointArray& outVertices,   /* argument group 2: used to calculate the bump offset */
	/*.................*/	
	MIntArray& uvIdArray )   /* argument group 3: used to calculate the color value */
{
	MPoint point, newPoint;
	double scalar;
	int uvId;
	double scaledDatum;
	MDoubleArray scalars;
	MColor color;
	MVector vector;
	double halfWindow = 2.0;
	double sum = 0.0;

	int pointCount = inVertices.length();
	for ( int i=0; i<pointCount; i++ )
	{
		// first do all the interpolation
		point = inVertices[i];
		scalar = lapdCommon::interpolateScalar( point, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
		scalars.append( scalar );
		sum = sum + scalar;

		scaledDatum = (scalar-dataMin) / (dataMax-dataMin);
		if ( scaledDatum < 0.0 ) scaledDatum = 0.0;
		if ( scaledDatum > 1.0 ) scaledDatum = 1.0;

		// map scaledDatum to uvId
		uvId = (int)(((float)(uvIdCount-1)) * scaledDatum);
		if ( uvId < 0 ) uvId = 0;
		if ( uvId > (uvIdCount-1) ) uvId = uvIdCount - 1;
		uvIdArray.append( uvId );
	}

	if ( centering == 0 ) centerValue = 0.0;
	if ( centering == 1 ) centerValue = sum / pointCount;

	//sprintf( bumpCommon::logString, "bumpScale: %d", bumpScale );
	//bumpCommon::logEntry( bumpCommon::logString );
	for ( int i=0; i<pointCount; i++ )
	{
		vector = bumpScale * (scalars[i]-centerValue) * bumpNormal;
		point = inVertices[i];
		newPoint = point + MPoint( vector );
		outVertices.append( newPoint );
	}

	return MStatus::kSuccess;
}


/* ============================================================================
 * setUVs()
 */
MStatus scalarBump::setUVs(
	float dataMinMapsTo,
	float dataMaxMapsTo,
	MFnMesh& fnMesh )
{
	MStatus stat;

	float uValue, vValue;
	int uvId = 0;
	for (int i=0; i<uvIdCount; i++ )
	{
		vValue = ((float)i) / ((float)(uvIdCount-1));
		uValue = dataMinMapsTo + vValue * (dataMaxMapsTo-dataMinMapsTo);
		if ( uValue < 0.0 ) uValue = 0.0;
		if ( uValue > 1.0 ) uValue = 1.0;
		diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue) );
		uvId++;
	}

	return MStatus::kSuccess;
}


/* ============================================================================
 * assignUVs()
 */
MStatus scalarBump::assignUVs(
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices,
	MIntArray& uvIdArray,
	MFnMesh& fnMesh )
{
	MStatus stat;
	int arrayIndex = 0;
	int polygonCount = polygonVertexCounts.length();

	for ( int iPolygon=0; iPolygon<polygonCount; iPolygon++ )
	{
		int vertexCount = polygonVertexCounts[iPolygon];
		for ( int iVertex=0; iVertex<vertexCount; iVertex++ )
		{
			// Calculate uvId
			int iVertexIndex = polygonVertexIndices[arrayIndex];
			int uvId = uvIdArray[iVertexIndex];

			diLOG_MSTATUS( fnMesh.assignUV(iPolygon, iVertex, uvId) );
			arrayIndex++;
		}
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the bump.
//
*/
MStatus scalarBump::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//bumpCommon::logEntry( "--> scalarBump::compute()" );
	bumpCommon::logEntry( bumpCommon::eScalarBump );

	if ( (plug != aOutputMesh) )
	{
		bumpCommon::logEntry( (char*) "Abnormal exit from scalarBump::compute(), Maya not asking for output mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes related to the scalar data grid.
	//
	MDataHandle hInputMesh( block.inputValue(aInputMesh, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hInputMeshNormal( block.inputValue(aInputMeshNormal, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hDataMin( block.inputValue(aDataMin, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hDataMax( block.inputValue(aDataMax, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hScalarDataGrid( block.inputValue(aScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );

	MObject inMesh( hInputMesh.asMeshTransformed() );
	MFnMesh fnInputMesh( inMesh );
	float3& inputMeshNormal = hInputMeshNormal.asFloat3();
	int3& dims = hDims.asInt3();
	float3& gridMin = hGridMin.asFloat3();
	float3& gridMax = hGridMax.asFloat3();
	double& dataMin = hDataMin.asDouble();
	double& dataMax = hDataMax.asDouble();
	MFnDoubleArrayData fnScalarDataGrid( hScalarDataGrid.data() );
	MDoubleArray scalarDataGrid( fnScalarDataGrid.array() );


	// Extract the input attributes related to the bump.
	//
	MDataHandle hBumpRotate( block.inputValue(aBumpRotate, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hBumpRotateOrder( block.inputValue(aBumpRotateOrder, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hBumpScale( block.inputValue(aBumpScale, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hCentering( block.inputValue(aCentering, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hCenterValue( block.inputValue(aCenterValue, &stat) );   diLOG_MSTATUS( stat );

	float3& bumpRotate = hBumpRotate.asFloat3();
	double rotXYZ[3];
	float pi = 3.1415927;
	for ( int i=0; i<3; i++ ) rotXYZ[i] = bumpRotate[i] * pi/180.0;
	int bumpRotateOrder = hBumpRotateOrder.asInt();
	MTransformationMatrix::RotationOrder order;
	if ( bumpRotateOrder == 0 ) order = MTransformationMatrix::kXYZ;
	if ( bumpRotateOrder == 1 ) order = MTransformationMatrix::kYZX;
	if ( bumpRotateOrder == 2 ) order = MTransformationMatrix::kZXY;
	if ( bumpRotateOrder == 3 ) order = MTransformationMatrix::kXZY;
	if ( bumpRotateOrder == 4 ) order = MTransformationMatrix::kYXZ;
	if ( bumpRotateOrder == 5 ) order = MTransformationMatrix::kZYX;
	MVector inputMeshNormalVector( MVector( inputMeshNormal[0], inputMeshNormal[1], inputMeshNormal[2]) );
	inputMeshNormalVector.normalize();
    MVector bumpNormal( inputMeshNormalVector.rotateBy(rotXYZ, order) );
	double bumpScale = hBumpScale.asDouble();
	short centering = hCentering.asShort();
	double centerValue = hCenterValue.asDouble();


	// Extract the input attributes related to the coloring.
	//
	MDataHandle hDataMinMapsTo( block.inputValue(aDataMinMapsTo, &stat) );   diLOG_MSTATUS( stat );
	MDataHandle hDataMaxMapsTo( block.inputValue(aDataMaxMapsTo, &stat) );	 diLOG_MSTATUS( stat );

	float dataMinMapsTo = hDataMinMapsTo.asFloat();
	float dataMaxMapsTo = hDataMaxMapsTo.asFloat();


	// Now do the output attributes.
	//
	MDataHandle hOutputMesh( block.outputValue(aOutputMesh, &stat) );   diLOG_MSTATUS( stat );


	// Get the input mesh points and calculate output mesh points.
	int numMeshPoints = fnInputMesh.numVertices( &stat );   diLOG_MSTATUS( stat );
	MPointArray inVertices;
	MIntArray polygonVertexCounts;
	MIntArray polygonVertexIndices;
	diLOG_MSTATUS( fnInputMesh.getPoints(inVertices, MSpace::kWorld) );
	diLOG_MSTATUS( fnInputMesh.getVertices(polygonVertexCounts, polygonVertexIndices) );

	MPointArray outVertices;
	MIntArray uvIdArray;
	//bumpCommon::logEntry( "About to calculate scalar bump" );
	diLOG_MSTATUS( 
		calculateBump(
			inVertices,
			dims,
			gridMin,
			gridMax,
			dataMin,
			dataMax,
			scalarDataGrid,   /* argument group 1: used to interpolate and scale the scalar datum */
			/*...........*/
			bumpNormal,
			bumpScale,
			centering,
			centerValue,
			outVertices,   /* argument group 2: used to calculate the bump offset */
			/*...........*/
			uvIdArray) );   /* argument group 3: used to calculate the color value */


	// Create the output mesh.
	MFnMesh fnMesh;
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );

	int vertexCount = outVertices.length();
	int polygonCount = polygonVertexCounts.length();
	//sprintf( bumpCommon::logString, "vertexCount: %d  polygonCount: %d", vertexCount, polygonCount );
	//bumpCommon::logEntry( bumpCommon::logString );


	// Single call here to create the mesh
	fnMesh.create(
		vertexCount,
		polygonCount, 
		outVertices,
		polygonVertexCounts,
		polygonVertexIndices,
		newOutputData, 
		&stat );   diLOG_MSTATUS( stat );

	setUVs( dataMinMapsTo, dataMaxMapsTo, fnMesh );
	assignUVs( polygonVertexCounts, polygonVertexIndices, uvIdArray, fnMesh );


	// Set the outputs as clean.
	hOutputMesh.set( newOutputData );
	block.setClean( plug );

	//bumpCommon::logEntry( "<-- scalarBump::compute()" );
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information
*/
MStatus scalarBump::initialize()
{
    MFnNumericAttribute nAttr;
	MFnEnumAttribute enumAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnVectorArrayData fnVectorArrayData;		
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	bumpCommon::logEntry( (char*) "--> scalarBump::initialize()" );


	// Create input attributes:
	//
	// input mesh
	aInputMesh = tAttr.create( "inputMesh", "inputMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aInputMesh) );

	// mesh normal
	aInputMeshNormal = nAttr.create( "inputMeshNormal", "inputMeshNormal", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aInputMeshNormal) );

	// grid dimensions
	aDims = nAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gridMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gridMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// data min
	aDataMin = nAttr.create( "dataMin", "dataMin", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDataMin) );

	// data max
	aDataMax = nAttr.create( "dataMax", "dataMax", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDataMax) );

	// scalar data grid
	aScalarDataGrid = tAttr.create( "scalarDataGrid", "scalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aScalarDataGrid) );

	// bump rotate
	aBumpRotate = nAttr.create( "bumpRotate", "bumpRotate", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aBumpRotate) );

	// bump rotate order
    aBumpRotateOrder = enumAttr.create( "bumpRotateOrder", "bumpRotateOrder", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("xyz", 0) );
	diLOG_MSTATUS( enumAttr.addField("yzx", 1) );
	diLOG_MSTATUS( enumAttr.addField("zxy", 2) );
	diLOG_MSTATUS( enumAttr.addField("xzy", 3) );
	diLOG_MSTATUS( enumAttr.addField("yxz", 4) );
	diLOG_MSTATUS( enumAttr.addField("zyx", 5) );
	MAKE_INPUT( enumAttr );
	diLOG_MSTATUS( addAttribute(aBumpRotateOrder) );

	// bump scale
	aBumpScale = nAttr.create( "bumpScale", "bumpScale", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aBumpScale) );

	// centering
    aCentering = enumAttr.create( "centering", "centering", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("none", 0) );
	diLOG_MSTATUS( enumAttr.addField("on average", 1) );
	diLOG_MSTATUS( enumAttr.addField("manual", 2) );
	MAKE_INPUT( enumAttr );
	diLOG_MSTATUS( addAttribute(aCentering) );

	// center value
	aCenterValue = nAttr.create( "centerValue", "centerValue", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aCenterValue) );

	// dataMinMapsTo = 0.0
	aDataMinMapsTo = nAttr.create( "dataMinMapsTo", "dataMinMapsTo", MFnNumericData::kFloat, 0.0, &stat );   diLOG_MSTATUS( stat );
    diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDataMinMapsTo) );

	// dataMaxMapsTo = 1.0
	aDataMaxMapsTo = nAttr.create( "dataMaxMapsTo", "dataMaxMapsTo", MFnNumericData::kFloat, 0.0, &stat );   diLOG_MSTATUS( stat );
    diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDataMaxMapsTo) );


	// Create output attributes
	//
	// output mesh
	aOutputMesh = tAttr.create( "outputMesh", "outputMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aOutputMesh) );


	// All inputs affect all outputs
	//
    diLOG_MSTATUS( attributeAffects(aInputMesh, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aInputMeshNormal, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDims, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMin, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMax, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aScalarDataGrid, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aBumpRotate, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aBumpRotateOrder, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aBumpScale, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aCentering, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aCenterValue, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMinMapsTo, aOutputMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMaxMapsTo, aOutputMesh) );

	bumpCommon::logEntry( (char*) "<-- scalarBump::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
 * postConstructor()
 */
void scalarBump::postConstructor( )
{
}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType scalarBump::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
 * scalarBump()
 */
scalarBump::scalarBump()
{
}


/* ============================================================================
 * ~scalarBump()
 */
scalarBump::~scalarBump()
{
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * scalarBump::creator()
{
    return new scalarBump();
}


