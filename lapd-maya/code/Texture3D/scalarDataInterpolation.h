/* ==========================================================================
// Texture 3D : scalarDataInterpolation.h
//
// Copyright (C) 2006 Jim Bamber 
//
*/
#ifndef SCALARDATAINTERPOLATION_H
#define SCALARDATAINTERPOLATION_H

#include <maya/MPxNode.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MTypeId.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>


class scalarDataInterpolation : public MPxNode
{
    public:

    scalarDataInterpolation();
    virtual ~scalarDataInterpolation();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );
	virtual void    postConstructor();

    static  void *  creator();
    static  MStatus initialize();

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	static float calculateScaledDatum(
		MFloatPoint& solidPos,
		int3& mDims,
		float3& mGridMax,
		float3& mGridMin,
		float mDataMax,
		float mDataMin,
		MFnDoubleArrayData& mScalarDataGrid,
		double mHalfWindow );

	// Input attributes
	static MObject halfWindow;

	static MObject aPlaceMat;
	static MObject aPointWorld;

	//static MObject grid;
	static MObject scalarDataGrid;
	static MObject dims;
	static MObject gridMax;
	static MObject gridMin;
	static MObject dataMax;
	static MObject dataMin;

	// Output attributes
    static MObject scaledDatum;
};


#endif

