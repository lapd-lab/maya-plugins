#-
# ==========================================================================
# Copyright (c) 2018 Autodesk, Inc.
# All rights reserved.
# 
# These coded instructions, statements, and computer programs contain
# unpublished proprietary information written by Autodesk, Inc., and are
# protected by Federal copyright law. They may not be disclosed to third
# parties or copied or duplicated in any form, in whole or in part, without
# the prior written consent of Autodesk, Inc.
# ==========================================================================
#+


cmake_minimum_required(VERSION 2.8)

# include the project setting file
include($ENV{DEVKIT_LOCATION}/cmake/pluginEntry.cmake)

# specify project name
set(PROJECT_NAME Arrows)


# HDF5
set(HDF5_INCLUDE_DIR $ENV{HDF5_INCLUDE_DIR})
include_directories(${HDF5_INCLUDE_DIR})

# set SOURCE_FILES
set(SOURCE_FILES
    arrows.cpp arrowsCommon.cpp arrowsUV.cpp pluginMain.cpp vectorShadeUVLookup.cpp vectorShadeUVLookup2.cpp
    arrows.h arrowsCommon.h arrowsUV.h vectorShadeUVLookup.h vectorShadeUVLookup2.h
    ../HDF5/HDF5file.cpp ../HDF5/HDF5common.cpp
    ../HDF5/HDF5file.h ../HDF5/HDF5file.h ../HDF5/errorHandling.h
    ${HDF5_INCLUDE_DIR}/H5public.h
    ../lapdCommon/lapdCommon.cpp
    ../lapdCommon/lapdCommon.h ../lapdCommon/defines.h
)

# set linking libraries
set(LIBRARIES
     OpenMaya Foundation OpenMayaAnim OpenMayaFX OpenMayaRender OpenMayaUI
)

# find z
find_zlib()

# find Alembic
find_alembic()

# Build plugin
build_plugin()

