# LAPD Maya plug-ins for Maya 2020 (and moving forward)

*November 5, 2020*
- It was necessary to refactor the build for the LAPD Maya plug-ins with the 2020 release of Maya.  It seems that XCode is no longer supported and a number of other incompatibilities had crept in over time.

*December 28, 2020*
- At this point, the plug-in functionality has been restored and all the code, MEL scripts and other supporting material is being migrated into a GitLab project.  This README file describes:
    - how to check out the project from GitLab
    - how to run the code in Maya
    - (optionally) how to develop the code or add to the project
 
 *February 21, 2020*
 - The project root is being changed to a standard location.  For the Mac this is:
 ```
/Users/Shared/maya-local/Logisymetrix/maya-plugins
```
- This will allow for reliable sharing of scenes, either as quick-starts in the project or custom scenes developed by users.

## How to check out the project from GitLab:
This section describes how to do a checkout from GitLab, either of the master branch if no changes to the project will be made, or to create a new branch to make changes to the project with version control.

### SSH key pair
- If you have no SSH key pair on your machine, you will need to follow the instructions in this section.
- To see if you have an SSH key pair, do the following in a command shell:
```
ls -la ~/.ssh
```
- If you do, you will typically see something like:
```
-rw-------   1 jamesbamber  staff  1831 28 Dec 12:22 id_rsa
-rw-r--r--   1 jamesbamber  staff   405 28 Dec 12:22 id_rsa.pub
```
- If not, please follow the instructions here: https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair
- Then add the SSH key to your GitLab account: https://docs.gitlab.com/ee/ssh/README.html#adding-an-ssh-key-to-your-gitlab-account

### Clone the repository
- This step is necessary only for the first time that you check out the code.
- Go to the Maya-plugins "Project overview" page: https://gitlab.com/lapd-lab/maya-plugins
- Click the Clone button and copy the link for "Clone with SSH"
- At this point, you have to make sure you have git installed on your computer.  Follow the instructions here: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- In the command shell, go to the directory that will contain the Maya-plugins project
- For the Mac, type:
```
cd /Users/Shared
mkdir maya-local
cd maya-local
mkdir data
mkdir saved-scenes
mkdir Logisymetrix
cd Logisymetrix
git clone <SSH clone link copied above>
```
- A directory called maya-plugins will be created.  This is the $BASE directory in the previous version of this README.

### Checkout the master branch
- If you just cloned the repository, you can skip this step.
- In a command shell, go to the Maya-plugins git project created when you cloned the repository.
- Type:
```
git fetch
git checkout master
```
- To verify that the checkout worked, in the $BASE directory:
```
ls -la lapd-maya

# something like...
total 16
drwxr-xr-x@  8 jamesbamber  staff   256 28 Dec 13:13 .
drwxr-xr-x  10 jamesbamber  staff   320  3 Jan 16:50 ..
-rw-r--r--@  1 jamesbamber  staff  6148 28 Dec 13:13 .DS_Store
drwxr-xr-x@ 14 jamesbamber  staff   448  8 Dec 18:24 code
drwxr-xr-x@  2 jamesbamber  staff    64 24 Oct 21:18 icons
drwxr-xr-x@ 10 jamesbamber  staff   320  9 Dec 21:03 plug-ins
drwxr-xr-x@ 17 jamesbamber  staff   544  5 Dec 21:31 scripts
```
- Note that $BASE/lapd-maya/plug-ins already contains the compiled plug-ins.

## How to run the code in Maya
- If this is the first checkout, setup Maya.env:
```
cd /Users/<username>/Library/Preferences/Autodesk/maya
mv 2020 "2020 (original)"
cp -r "$BASE/resources/Maya 2020" 2020

# Maya.env does not need to be edited
```
- Start Maya:
    - either start it from Launchpad, or...
    - for better logging, start from the shell: /Applications/Autodesk/maya2020/Maya.app/Contents/MacOS/Maya
- If this is the first checkout, verify that the plug-ins are loaded:
    - Windows > Settings/Preferences > Plug-in Manager
    - the plug-ins should be listed
    - click on “Loaded” and “Auto load” for “Apply To All:”
    - it is possible that you will get blocked because your Mac thinks this might be malware
        - in this case, click the ? in the window and follow the instructions on how to make an exception for each bundle
        - this might be handled awkwardly by MacOS, in which case try to load each bundle individually
- As well, if this is the first checkout, change the default project directory:
    - Windows > Settings/Preferences > Preferences
        - Settings > Files/Projects
        - Default projects directory: "/Users/Shared/maya-local/saved-scenes"
        - Always start in this project: "/Users/Shared/maya-local/saved-scenes"
- To begin:
    - load a scene:
        - File > Open Scene...
        - navigate to either the default projects directory (above) or "Users/Shared/maya-local/Logisymetrix/maya-plugins/resources/Quick starts"
        - double-click the desired scene
    - launch the Data Illuminator:
        - Custom shelf > Data Illuminator UI button
    - detailed instructions on how to work from the "Quick starts" are provided in the various documents in $BASE/resources/Quick starts

## How to modify the project on GitLab
Whether you want to upload a single file or do development work on the code, this is the process to follow.  This could include adding more sample data or saved scenes into the GitLab project to be shared.  Note that data files should not exceed 10 MB (although there seems to be a small allowance for files slightly larger than this).

### Create a new branch in GitLab
Any modification to the project should be made on a separate "branch".  This is the standard way of working with a code repository which ensures that you can always recover from a modification that goes wrong for some reason.
- Go to https://gitlab.com/lapd-lab/maya-plugins
- Repository > Branches > New branch
- Branch name: your-new-branch-name
- Create from: master
- Create branch

### Checkout this branch on your local machine
- Open a bash window:
```
cd $BASE
git fetch
git checkout your-new-branch-name
```

### Make changes and rebuild Maya plug-ins, if necessary
If you are simply adding a file or making modifications to MEL scripts, for example, no rebuild is necessary and you can skip to the Update GitLab step.  However, if you make changes to the C++ plug-in code, you will have to do a rebuild.  At the time of writing, no GitLab CI/CD process has been made so the build process is still manual.  The LAPD Maya modules are built with CMake.  Here are the instructions to do this:
- Check if you have the Maya DevKit installed:
  - check if there is a devkitBase directory under $BASE
- Install the DevKit if necessary:
  - roughly follow the directions at https://help.autodesk.com/view/MAYAUL/2017/ENU/?guid=__files_Setting_up_your_build_environment_Mac_OS_X_environment_htm:
    - what they refer to as $HOME on this page, we refer to here as $BASE
    - note that to download the devkit, go here: https://www.autodesk.com/developer-network/platform-technologies/maya
    - put the *.dmg in $BASE and double-click it
    - it makes it into a drive, so you’ll have to create a devkitBase folder under $BASE and copy the contents there
    - copy $BASE/devkitBase/devkit to /Applications/Autodesk/maya2020
- In $BASE/lapd-maya/code, make a “build” directory.
- Set environment variables, in ~/.bash_profile, for example, add:
```
export MAYA_LOCATION=/Applications/Autodesk/maya2020/Maya.app/Contents
export BASE=/Users/Shared/maya-local/Logisymetrix/maya-plugins
export DEVKIT_LOCATION=$BASE/devkitBase
export HDF5_DIR=$BASE/hdf5_installation
export HDF5_INCLUDE_DIR=$BASE/hdf5_installation/HDF_Group/HDF5/1.8.20/include
```
- Export the environment variables, then ensure they are there:
```
source ~/.bash_profile
env
```
- In the “build” directory, run these commands:
```
rm -rf *
cmake ..
cmake --build .
```
- For each plug-in, there will be a directory under “build” that contains the bundle, i.e. the plug-in:
  - for example: $BASE/lapd-maya/code/build/Arrows/Arrows.bundle
  - copy all these bundles to $BASE/lapd-maya/plug-ins

### Update project in GitLab
Before updating the project in GitLab, you need to ensure that your changes are good.  In order to do this, I strongly recommend using a tool like Sourcetree.
- If this is the first time using Sourcetree, open the maya-plugins project:
    - File > Open... > browse to $BASE
- In the main window there will be the branch history, click on "Uncommitted changes" on top
- There should be a number of files listed under "Unstaged files":
  - when you click on a file, you will see the changes that you have made
  - verify that the changes are correct and then click the checkbox for that file to stage it
- When all files are staged, click on the Commit button in the upper right
  - write a one-line commit message which will appear in the branch history
- Go back to the bash window and type:
```
git status
```
- You should see a response like:
```
On branch your-new-branch-name
Your branch is ahead of 'origin/your-new-branch-name' by 1 commit.
  (use "git push" to publish your local commits)

nothing to commit, working tree clean
```
- If you are sure that you want to check your branch in to GitLab, type:
```
git push
```
- Now you can create a Merge Request to merge your branch onto master:
  - in GitLab: Merge Requests > New Merge Request
    - Source branch: your-new-branch-name
    - Target branch: master
    - Compare branches and continue
    - on the next page, add a Description if desired and Submit merge request
- If you want someone to look it over, signal them and they can look at the changes you have made
- Once you are sure that you want to merge onto master, you can:
  - go to the Merge Request and click on Overview
  - typically check Delete source branch (unless there is more work to do on it)
  - Click Merge
