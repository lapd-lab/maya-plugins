//=================================================================================================
// Master UI params 2011-03-05
//
// Contains window and field sizes and any other parameters that can be used to make the interface
// look right.
//
// Also contains mapping connection information.
//

// MasterUI window
// ===============
//
global int $muiWidth = 565;
//global int $muiHeight = 560;
global int $muiHeight = 606;
global int $muiTop = 250;
global int $muiLeft = 730;

global int $muiShadingNetworksListWidth = 350;
global int $muiShadingNetworksListHeight = 75;

global int $muiDatasetsFrameHeight = 130;
global int $muiDatasetsListWidth = 350;
global int $muiDatasetsListHeight = 60;

global int $muiVizObjectsListWidth = 210;
global int $muiVizObjectsListHeight = 75;

global int $muiTabLayoutHeight = 175;

global string $muiPreviousDataset;    // used to save the name of the previous dataset and the previous viz object, reset to 
global string $muiPreviousVizObject;  // these when adding a secondary dataset (the old "resetSelection" idea)


// Callbacks
// =========
//
global string $dsDatasetWindowExitCommand;
global string $snRenderingWindowExitCommand;
global string $snSelectDataSourceWindowExitCommand;
global string $voNewShadingNetworkWindowExitCommand;


// Dataset window
// ==============
//
global int $dsWidth = 510;
global int $dsHeight = 210;
global int $dsTop = 300;
global int $dsLeft = 850;


// New shading network window (vizObject procs)
// ============================================
//
global int $vosnWidth = 505;
global int $vosnHeight = 150;
global int $vosnTop = 350;
global int $vosnLeft = 820;
global int $vosnShadingNetworkListWidth = 340;
global int $vosnShadingNetworkListHeight = 100;


// Cut plane window
// ================
//
global int $cpWidth = 530;
global int $cpHeight = 240;
global int $cpTop = 200;
global int $cpLeft = 800;

global int $cpPositionWidth = 510;
global int $cpPositionHeight = 115;
global int $cpShaderWidth = 530;
global int $cpShaderHeight = 305;
global int $cpShaderTop = 400;
global int $cpShaderLeft = 310;


// Isosurface window
// =================
//
global int $isWidth = 530;
global int $isHeight = 170;
global int $isTop = 200;
global int $isLeft = 800;

global int $isShaderWidth = 530;
global int $isShaderHeight = 350;
global int $isShaderTop = 400;
global int $isShaderLeft = 310;


// Arrows window
// =============
//
global int $arWidth = 565;
global int $arHeight = 370;
global int $arTop = 300;
global int $arLeft = 800;

global int $arEditWidth = 560;
global int $arEditHeight = 145;

global int $arShaderWidth = 530;
global int $arShaderHeight = 75;
global int $arShaderTop = 300;
global int $arShaderLeft = 850;

global int $arEditCustomLeft = 300;


// Field lines window
// ==================
//
global int $flWidth = 560;
global int $flHeight = 240;
global int $flTop = 300;
global int $flLeft = 800;

global int $flEditHeight = 150;

global int $flEditCustomLeft = 350;

global int $flDataSourcesListWidth = 350;
global int $flDataSourcesListHeight = 100;
global int $flShaderWidth = 510;
global int $flShaderHeight = 180;
global int $flShaderTop = 300;
global int $flShaderLeft = 850;


// Field lines rendering window (now Field lines UV mapping window)
// ============================
//
global int $flrWidth = 600;
global int $flrHeight = 150;
global int $flrTop = 300;
global int $flrLeft = 650;


// Field lines data sources window
// ===============================
//
global int $fldsWidth = 620;
global int $fldsHeight = 150;
global int $fldsTop = 350;
global int $fldsLeft = 820;

global int $fldsDataSourcesListWidth = 340;
global int $fldsDataSourcesListHeight = 100;


// Field lines edit mapping window (now a sub-section of the rendering window)
// ===============================
//
global int $flemWidth = 530;
global int $flemHeight = 730;
global int $flemTop = 300;
global int $flemLeft = 800;


// Shading network rendering window
// ================================
//
global int $snrWidth = 600;
global int $snrHeightLarge = 725;
global int $snrHeightSmall = 285;
global int $snrTop = 290;
global int $snrLeft = 600;

global int $snrTexturesListWidth = 330;
global int $snrTexturesListHeight = 60;


// Shading network create overlay window
// =====================================
//
global int $snoWidth = 600;
global int $snoHeight = 310;
global int $snoTop = 350;
global int $snoLeft = 600;

global int $snoTexturesListWidth = 330;
global int $snoTexturesListHeight = 60;


// Shading network add mapping window
// ==================================
//
global int $snamWidth = 300;
global int $snamHeight = 250;
global int $snamTop = 350;
global int $snamLeft = 820;


// Shading network edit mapping connections window
// ===============================================
//
global int $snecWidth = 720;
global int $snecHeight = 620;
global int $snecTop = 400;
global int $snecLeft = 405;

global int $snecConnectionsListWidth = 540;
global int $snecConnectionsListHeight = 100;

global int $snecElementWidth;
global int $snecElementHeight;
$snecElementWidth = ($snecWidth/3) - 8;
$snecElementHeight = $snecHeight - 225;

global int $snecRadioCollectionWidth;
global int $snecRadioCollectionHeight;
$snecRadioCollectionWidth = $snecElementWidth - 15;
$snecRadioCollectionHeight = $snecElementHeight - 260;

global int $snecIconWidth = 100;
global int $snecIconHeight = 100;

global int $snecDropdownHeight = 24;


// Shading network select shading network window
// =============================================
//
global int $snsnWidth = 465;
global int $snsnHeight = 150;
global int $snsnTop = 350;
global int $snsnLeft = 820;
global int $snsnShadingNetworkListWidth = 340;
global int $snsnShadingNetworkListHeight = 100;

global string $snImportedShadingNetwork;  // used to save which shading network was selected in import window


// Shading network select secondary dataset window
// ===============================================
//
global int $sndsWidth = 620;
global int $sndsHeight = 150;
global int $sndsTop = 350;
global int $sndsLeft = 820;
global int $sndsDataSourcesListWidth = 350;
global int $sndsDataSourcesListHeight = 100;
global int $snShaderWidth = 510;
global int $snShaderHeight = 180;
global int $snShaderTop = 300;
global int $snShaderLeft = 850;


// Colors
// ======
//
//global float $snR = 0.95;
//global float $snG = 0.5;
//global float $snB = 0.2;
global float $snR = 0.9;
global float $snG = 0.7;
global float $snB = 0.6;  // shading network color
global float $ecR = 0.7;
global float $ecG = 0.7;
global float $ecB = 0.85;  // edit connections color


// Recipe related
// ==============
//
global string $rRecipeVersion = "version_2.6";
global string $originalNames[];
global string $importedNames[];
global int $recursionDepth = 0;  // for calls to rReadRecipeKernel, to know when to clear the two arrays declared above


// Recipe auto-save mode window
// ============================
//
global int $rasWidth = 620;
global int $rasHeight = 180;
global int $rasTop = 350;
global int $rasLeft = 820;


// Shader tweaks
// =============
//
// These values are not strings.  They're part of a kludge to deal with arrays of float3's.
//
global string $stTransparency[];
global string $stAmbientColor[];
global string $stReflectivity[];
global string $stDiffuse[];
global string $stReflectedColor[];
global string $stTranslucence[];
global string $stTranslucenceDepth[];
global string $stTranslucenceFocus[];
global string $stSpecularColor[];
global string $stEccentricity[];
global string $stSpecularRollOff[];
global string $stMatteOpacity[];
global string $stGlowIntensity[];

// flat                                       // metal                                      // glass
$stTransparency[0]      = "0.00 0.00 0.00";   $stTransparency[1]      = "0.00 0.00 0.00";   $stTransparency[2]      = "0.85 0.85 0.85";      
$stAmbientColor[0]      = "0.00 0.00 0.00";   $stAmbientColor[1]      = "0.20 0.20 0.20";   $stAmbientColor[2]      = "0.20 0.20 0.20";      
$stReflectivity[0]      = "0.50";             $stReflectivity[1]      = "0.00";             $stReflectivity[2]      = "0.00";      
$stDiffuse[0]           = "0.80";             $stDiffuse[1]           = "0.50";             $stDiffuse[2]           = "0.50";      
$stReflectedColor[0]    = "0.00 0.00 0.00";   $stReflectedColor[1]    = "0.72 0.88 0.85";   $stReflectedColor[2]    = "0.72 0.88 0.85";      
$stTranslucence[0]      = "0.00";             $stTranslucence[1]      = "1.50";             $stTranslucence[2]      = "1.50";      
$stTranslucenceDepth[0] = "0.50";             $stTranslucenceDepth[1] = "5.00";             $stTranslucenceDepth[2] = "5.00";      
$stTranslucenceFocus[0] = "0.50";             $stTranslucenceFocus[1] = "0.69";             $stTranslucenceFocus[2] = "0.69";      
$stSpecularColor[0]     = "0.50 0.50 0.50";   $stSpecularColor[1]     = "0.77 0.90 0.93";   $stSpecularColor[2]     = "0.77 0.90 0.93";      
$stEccentricity[0]      = "0.30";             $stEccentricity[1]      = "0.29";             $stEccentricity[2]      = "0.29";      
$stSpecularRollOff[0]   = "0.70";             $stSpecularRollOff[1]   = "0.46";             $stSpecularRollOff[2]   = "0.46";      
$stMatteOpacity[0]      = "1.00";             $stMatteOpacity[1]      = "10.0";             $stMatteOpacity[2]      = "10.0";      
$stGlowIntensity[0]     = "0.00";             $stGlowIntensity[1]     = "0.00";             $stGlowIntensity[2]     = "0.00";      


// Blend mode info
// ===============
//
global string $snecBlendModes[];
global string $snecBlendModeAnnotations[];
global string $snecBlendModeColorEqns[];
global string $snecBlendModeAlphaEqns[];

$snecBlendModes[ 0] = "None";      
$snecBlendModes[ 1] = "Over";      
$snecBlendModes[ 2] = "In";        
$snecBlendModes[ 3] = "Out";       
$snecBlendModes[ 4] = "Add";       
$snecBlendModes[ 5] = "Subtract";  
$snecBlendModes[ 6] = "Multiply";  
$snecBlendModes[ 7] = "Difference";
$snecBlendModes[ 8] = "Lighten";   
$snecBlendModes[ 9] = "Darken";    
$snecBlendModes[10] = "Saturate";  
$snecBlendModes[11] = "Desaturate";
$snecBlendModes[12] = "Illuminate";

$snecBlendModeAnnotations[ 0] = "None: The foreground texture covers up the background texture entirely.";
$snecBlendModeAnnotations[ 1] = "Over: The foreground texture is applied like a decal to the background. The shape of the decal is determined by the foreground alpha.";
$snecBlendModeAnnotations[ 2] = "In: The result is the background texture cut in the shape of the foreground alpha.";
$snecBlendModeAnnotations[ 3] = "Out: The result is the opposite of In. It is as if the shape of the foreground alpha has been cut out of the background.";
$snecBlendModeAnnotations[ 4] = "Add: The result color is the foreground color added to the background color as if being projected on the background through a slide projector. The result color is then applied over the background color using the foreground alpha to define the opacity of the result.";
$snecBlendModeAnnotations[ 5] = "Subtract: The result color is the foreground color subtracted from the background color. The result color is then applied over the background color using the foreground alpha to define the opacity of the result.";
$snecBlendModeAnnotations[ 6] = "Multiply: The result color is the foreground color multiplied by the background color. The result color is then applied over the background color using the foreground alpha to define the opacity of the result.";
$snecBlendModeAnnotations[ 7] = "Difference: The result color is the difference between the foreground color and the background color. The result color is then applied over the background color using the foreground alpha to define the opacity of the result. Difference is not supported by hardware texturing.";
$snecBlendModeAnnotations[ 8] = "Lighten: The result color of each pixel is the background color or foreground color, whichever is lighter. The result color is then applied over the background color using the foreground alpha to define the opacity of the result. Lighten is not supported by hardware texturing.";
$snecBlendModeAnnotations[ 9] = "Darken: The result color of each pixel is the background color or foreground color, whichever is darker. The result color is then applied over the background color using the foreground alpha to define the opacity of the result. Darken is not supported by hardware texturing.";
$snecBlendModeAnnotations[10] = "Saturate: The result color is the background color with saturation increased in proportion to the foreground color scaled by foreground alpha. If the foreground color is red, for example, the result color will be the background color with more saturated reds.";
$snecBlendModeAnnotations[11] = "Desaturate: The result color is the background color with saturation decreased in proportion to the foreground color scaled by foreground alpha. If the foreground color is red, for example, the result color will be the background color with desaturated reds.";
$snecBlendModeAnnotations[12] = "Illuminate: The result color is the background color mixed with the foreground color, brighter where the foreground is bright and darker where the foreground is dark. It is as if the foreground texture represents the light falling on the background. The result color is then applied over the background color using the foreground alpha to define the opacity of the result.";

$snecBlendModeColorEqns[ 0] = "color = fc";
$snecBlendModeColorEqns[ 1] = "color = fc * fa + (bc * (1 - fa))";
$snecBlendModeColorEqns[ 2] = "color = bc * fa";
$snecBlendModeColorEqns[ 3] = "color = bc * (1 - fa)";
$snecBlendModeColorEqns[ 4] = "color = bc + (fc * fa)";
$snecBlendModeColorEqns[ 5] = "color = bc - (fc * fa)";
$snecBlendModeColorEqns[ 6] = "color = bc * (fc * fa + 1 - fa)";
$snecBlendModeColorEqns[ 7] = "color = (abs((fc * fa) - bc)) * fa + bc * (1 - fa)";
$snecBlendModeColorEqns[ 8] = "color = (max((fc * fa), bc)) * fa + bc * (1 - fa)";
$snecBlendModeColorEqns[ 9] = "color = (min((fc * fa), bc)) * fa + bc * (1 - fa)";
$snecBlendModeColorEqns[10] = "color = bc * (1 + (fc * fa))";
$snecBlendModeColorEqns[11] = "color = bc * (1 - (fc * fa))";
$snecBlendModeColorEqns[12] = "color = bc * (2 * fc * fa + 1 - fa)";

$snecBlendModeAlphaEqns[ 0] = "alpha = fa";
$snecBlendModeAlphaEqns[ 1] = "alpha = 1 - ((1 - ba) * (1 - fa))";
$snecBlendModeAlphaEqns[ 2] = "alpha = ba * fa";
$snecBlendModeAlphaEqns[ 3] = "alpha = ba * (1 - fa)";
$snecBlendModeAlphaEqns[ 4] = "alpha = ba";
$snecBlendModeAlphaEqns[ 5] = "alpha = ba";
$snecBlendModeAlphaEqns[ 6] = "alpha = ba";
$snecBlendModeAlphaEqns[ 7] = "alpha = ba";
$snecBlendModeAlphaEqns[ 8] = "alpha = ba";
$snecBlendModeAlphaEqns[ 9] = "alpha = ba";
$snecBlendModeAlphaEqns[10] = "alpha = ba";
$snecBlendModeAlphaEqns[11] = "alpha = ba";
$snecBlendModeAlphaEqns[12] = "alpha = ba";


// Markers window
// ==============
//
global int $moMarkersWidth = 410;
global int $moMarkersHeight = 150;
global int $moMarkersTop = 300;
global int $moMarkersLeft = 850;

global int $moEditMarkersTop = 300;
global int $moEditMarkersLeft = 300;


// Fur window
// ==========
//
global int $foFurObjectsListWidth = 350;
global int $foFurObjectsListHeight = 100;
global int $foMasterWidth = 475;
global int $foMasterHeight = 330;
global int $foMasterTop = 310;
global int $foMasterLeft = 810;

//global int $foEditFurTop = 300;
//global int $foEditFurLeft = 300;


// Fur data sources window
// =======================
//
global int $fodsWidth = 280;
global int $fodsHeight = 260;
global int $fodsTop = 350;
global int $fodsLeft = 880;


// **********
// Data model
// **********
//
// Data model: dataset
// ===================
//
global string $dmDatasetAttrs[];
global string $dmDatasetAttrTypes[];

int $i = 0;
$dmDatasetAttrs[$i] = "datasetType";        $dmDatasetAttrTypes[$i] = "string";	 $i++;  
$dmDatasetAttrs[$i] = "objectNumber";       $dmDatasetAttrTypes[$i] = "long";	 $i++;  
$dmDatasetAttrs[$i] = "prefix";             $dmDatasetAttrTypes[$i] = "string";	 $i++;  
$dmDatasetAttrs[$i] = "shortFilename";      $dmDatasetAttrTypes[$i] = "string";	 $i++;  
$dmDatasetAttrs[$i] = "displayName";        $dmDatasetAttrTypes[$i] = "string";	 $i++;  
$dmDatasetAttrs[$i] = "spatialGridDims";    $dmDatasetAttrTypes[$i] = "long3";   $i++;  // to hold the spatial grid node in memory
$dmDatasetAttrs[$i] = "fieldLinesCounter";  $dmDatasetAttrTypes[$i] = "long";	 $i++;  
$dmDatasetAttrs[$i] = "arrowsCounter";      $dmDatasetAttrTypes[$i] = "long";	 $i++;  
$dmDatasetAttrs[$i] = "cutPlanesCounter";   $dmDatasetAttrTypes[$i] = "long";	 $i++;  
$dmDatasetAttrs[$i] = "isosurfacesCounter"; $dmDatasetAttrTypes[$i] = "long";	 $i++;  
$dmDatasetAttrs[$i] = "dataDirectory";      $dmDatasetAttrTypes[$i] = "string";	 $i++;  
$dmDatasetAttrs[$i] = "dataGroup";          $dmDatasetAttrTypes[$i] = "string";	 $i++;  
$dmDatasetAttrs[$i] = "showBoundingBox";    $dmDatasetAttrTypes[$i] = "long";	 $i++;  
$dmDatasetAttrs[$i] = "vectorMultiplier";   $dmDatasetAttrTypes[$i] = "float3";	 $i++;  
$dmDatasetAttrs[$i] = "vectorOffset";       $dmDatasetAttrTypes[$i] = "float3";	 $i++;  
$dmDatasetAttrs[$i] = "scalarMultiplier";   $dmDatasetAttrTypes[$i] = "float";	 $i++;  
$dmDatasetAttrs[$i] = "scalarOffset";       $dmDatasetAttrTypes[$i] = "float";	 $i++;  


// Data model: fieldLines
// ======================
//
global string $dmFieldLineAttrs[];
global string $dmFieldLineAttrTypes[];

int $i = 0;
$dmFieldLineAttrs[$i] = "objectType";           $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "objectNumber";         $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "parent";               $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "visible";              $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "emitter";              $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "emitterType";          $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "emitterScript";        $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "hasMarkers";           $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "shaderInfo";           $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "shadingNetwork";       $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "tubeRadius";           $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "selectedFurObject";    $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "furObjectCounter";     $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "secondaryData";        $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "secondaryDataDisplay"; $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "tubeVMapType";         $dmFieldLineAttrTypes[$i] = "long";    $i++;  // 0=="data-based", 1=="geometry-based"
$dmFieldLineAttrs[$i] = "autoRender";           $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "fwdCurveCount";        $dmFieldLineAttrTypes[$i] = "long";    $i++;  // these curve counts are just used to hold the curves nodes in memory
$dmFieldLineAttrs[$i] = "bakCurveCount";        $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "polyPlane";            $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "polyCone";             $dmFieldLineAttrTypes[$i] = "string";  $i++;
$dmFieldLineAttrs[$i] = "emitterCenterX";       $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "emitterCenterY";       $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "emitterCenterZ";       $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "emitterHeight";        $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "emitterWidth";         $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "emitterHeightSubdiv";  $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "emitterWidthSubdiv";   $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "emitterRotationX";     $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "emitterRotationY";     $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "emitterRotationZ";     $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "emitterRadius";        $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "emitterRadiusSubdiv";  $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "emitterAngleSubdiv";   $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "stepSize";             $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "maxForwardSteps";      $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "maxBackwardSteps";     $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "UCycles";              $dmFieldLineAttrTypes[$i] = "long";	   $i++;
$dmFieldLineAttrs[$i] = "fixedAngle";           $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "spiralAngle";          $dmFieldLineAttrTypes[$i] = "float";   $i++;
$dmFieldLineAttrs[$i] = "stepsPerVCycle";       $dmFieldLineAttrTypes[$i] = "long";	   $i++;


// Data model: texture
// ===================
//
global string $dmTextureAttrs[];
global string $dmTextureAttrTypes[];

int $i = 0;
$dmTextureAttrs[$i] = "objectNumber";     $dmTextureAttrTypes[$i] = "long";    $i++;
$dmTextureAttrs[$i] = "parent";           $dmTextureAttrTypes[$i] = "string";  $i++;
$dmTextureAttrs[$i] = "nodeType";         $dmTextureAttrTypes[$i] = "string";  $i++;
$dmTextureAttrs[$i] = "mayaTextureType";  $dmTextureAttrTypes[$i] = "string";  $i++;
$dmTextureAttrs[$i] = "displayName";      $dmTextureAttrTypes[$i] = "string";  $i++;
$dmTextureAttrs[$i] = "actualNode";       $dmTextureAttrTypes[$i] = "string";  $i++;
$dmTextureAttrs[$i] = "layerCount";       $dmTextureAttrTypes[$i] = "long";	   $i++;
$dmTextureAttrs[$i] = "inverseScaling";   $dmTextureAttrTypes[$i] = "float";   $i++;
$dmTextureAttrs[$i] = "translateFrameV";  $dmTextureAttrTypes[$i] = "float";   $i++;
$dmTextureAttrs[$i] = "dataRangeMax";     $dmTextureAttrTypes[$i] = "float";   $i++;
$dmTextureAttrs[$i] = "dataRangeMin";     $dmTextureAttrTypes[$i] = "float";   $i++;
$dmTextureAttrs[$i] = "shortFilename";    $dmTextureAttrTypes[$i] = "string";  $i++;
$dmTextureAttrs[$i] = "fileDirectory";    $dmTextureAttrTypes[$i] = "string";  $i++;
$dmTextureAttrs[$i] = "repeatU";          $dmTextureAttrTypes[$i] = "float";   $i++;
$dmTextureAttrs[$i] = "repeatV";          $dmTextureAttrTypes[$i] = "float";   $i++;
$dmTextureAttrs[$i] = "offsetU";          $dmTextureAttrTypes[$i] = "float";   $i++;
$dmTextureAttrs[$i] = "offsetV";          $dmTextureAttrTypes[$i] = "float";   $i++;
$dmTextureAttrs[$i] = "rotateUV";         $dmTextureAttrTypes[$i] = "float";   $i++;
$dmTextureAttrs[$i] = "wrapU";            $dmTextureAttrTypes[$i] = "float";   $i++;
$dmTextureAttrs[$i] = "wrapV";            $dmTextureAttrTypes[$i] = "float";   $i++;


// Data model: connection
// ======================
//
global string $dmConnectionAttrs[];
global string $dmConnectionAttrTypes[];

int $i = 0;
$dmConnectionAttrs[$i] = "objectNumber";                  $dmConnectionAttrTypes[$i] = "long";    $i++;
$dmConnectionAttrs[$i] = "parent";                        $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "connected";                     $dmConnectionAttrTypes[$i] = "long";    $i++;
$dmConnectionAttrs[$i] = "sourceConvenienceNode";         $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "sourceNode";                    $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "sourceNodeType";                $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "sourceDisplay";                 $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "sourceAttr";                    $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "sourceAttrType";                $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "destinationConvenienceNode";    $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "destinationNode";               $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "destinationNodeType";           $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "destinationDisplay";            $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "destinationAttr";               $dmConnectionAttrTypes[$i] = "string";  $i++;
$dmConnectionAttrs[$i] = "destinationAttrType";           $dmConnectionAttrTypes[$i] = "string";  $i++;
//$dmConnectionAttrs[$i] = "destinationAttrBlendMode";      $dmConnectionAttrTypes[$i] = "long";    $i++;
//$dmConnectionAttrs[$i] = "destinationAttrIsVisible";      $dmConnectionAttrTypes[$i] = "long";    $i++;
$dmConnectionAttrs[$i] = "linTransEnabled";               $dmConnectionAttrTypes[$i] = "long";    $i++;
$dmConnectionAttrs[$i] = "secondaryDataAttached";         $dmConnectionAttrTypes[$i] = "long";    $i++;
$dmConnectionAttrs[$i] = "linTransM";                     $dmConnectionAttrTypes[$i] = "float";   $i++;
$dmConnectionAttrs[$i] = "linTransB";                     $dmConnectionAttrTypes[$i] = "float";   $i++;


// Data model: arrows
// ==================
//
global string $dmArrowsAttrs[];
global string $dmArrowsAttrTypes[];

int $i = 0;
$dmArrowsAttrs[$i] = "objectType";           $dmArrowsAttrTypes[$i] = "string";  $i++;
$dmArrowsAttrs[$i] = "objectNumber";         $dmArrowsAttrTypes[$i] = "long";    $i++;
$dmArrowsAttrs[$i] = "parent";               $dmArrowsAttrTypes[$i] = "string";  $i++;
$dmArrowsAttrs[$i] = "visible";              $dmArrowsAttrTypes[$i] = "long";    $i++;
$dmArrowsAttrs[$i] = "emitter";              $dmArrowsAttrTypes[$i] = "string";  $i++;
$dmArrowsAttrs[$i] = "emitterType";          $dmArrowsAttrTypes[$i] = "string";  $i++;
$dmArrowsAttrs[$i] = "emitterScript";        $dmArrowsAttrTypes[$i] = "string";  $i++;
$dmArrowsAttrs[$i] = "shaderInfo";           $dmArrowsAttrTypes[$i] = "string";  $i++;
$dmArrowsAttrs[$i] = "shadingNetwork";       $dmArrowsAttrTypes[$i] = "string";  $i++;
$dmArrowsAttrs[$i] = "VMapType";             $dmArrowsAttrTypes[$i] = "long";    $i++;
$dmArrowsAttrs[$i] = "replicatorNx";         $dmArrowsAttrTypes[$i] = "long";    $i++;
$dmArrowsAttrs[$i] = "replicatorNy";         $dmArrowsAttrTypes[$i] = "long";    $i++;
$dmArrowsAttrs[$i] = "replicatorNz";         $dmArrowsAttrTypes[$i] = "long";    $i++;
$dmArrowsAttrs[$i] = "replicatorDx";         $dmArrowsAttrTypes[$i] = "long";    $i++;
$dmArrowsAttrs[$i] = "replicatorDy";         $dmArrowsAttrTypes[$i] = "long";    $i++;
$dmArrowsAttrs[$i] = "replicatorDz";         $dmArrowsAttrTypes[$i] = "long";    $i++;
$dmArrowsAttrs[$i] = "polyPlane";            $dmArrowsAttrTypes[$i] = "string";  $i++;
$dmArrowsAttrs[$i] = "polyCone";             $dmArrowsAttrTypes[$i] = "string";  $i++;
$dmArrowsAttrs[$i] = "emitterCenterX";       $dmArrowsAttrTypes[$i] = "float";   $i++;
$dmArrowsAttrs[$i] = "emitterCenterY";       $dmArrowsAttrTypes[$i] = "float";   $i++;
$dmArrowsAttrs[$i] = "emitterCenterZ";       $dmArrowsAttrTypes[$i] = "float";   $i++;
$dmArrowsAttrs[$i] = "emitterHeight";        $dmArrowsAttrTypes[$i] = "float";   $i++;
$dmArrowsAttrs[$i] = "emitterWidth";         $dmArrowsAttrTypes[$i] = "float";   $i++;
$dmArrowsAttrs[$i] = "emitterHeightSubdiv";  $dmArrowsAttrTypes[$i] = "long";	 $i++;
$dmArrowsAttrs[$i] = "emitterWidthSubdiv";   $dmArrowsAttrTypes[$i] = "long";	 $i++;
$dmArrowsAttrs[$i] = "emitterRotationX";     $dmArrowsAttrTypes[$i] = "float";   $i++;
$dmArrowsAttrs[$i] = "emitterRotationY";     $dmArrowsAttrTypes[$i] = "float";   $i++;
$dmArrowsAttrs[$i] = "emitterRotationZ";     $dmArrowsAttrTypes[$i] = "float";   $i++;
$dmArrowsAttrs[$i] = "emitterRadius";        $dmArrowsAttrTypes[$i] = "float";   $i++;
$dmArrowsAttrs[$i] = "emitterRadiusSubdiv";  $dmArrowsAttrTypes[$i] = "long";	 $i++;
$dmArrowsAttrs[$i] = "emitterAngleSubdiv";   $dmArrowsAttrTypes[$i] = "long";	 $i++;
$dmArrowsAttrs[$i] = "multiplier";           $dmArrowsAttrTypes[$i] = "float";	 $i++;
$dmArrowsAttrs[$i] = "proportionType";       $dmArrowsAttrTypes[$i] = "long";	 $i++;
$dmArrowsAttrs[$i] = "baseDiameter";         $dmArrowsAttrTypes[$i] = "float";	 $i++;
$dmArrowsAttrs[$i] = "jointDiameter";        $dmArrowsAttrTypes[$i] = "float";	 $i++;
$dmArrowsAttrs[$i] = "headDiameter";         $dmArrowsAttrTypes[$i] = "float";	 $i++;
$dmArrowsAttrs[$i] = "jointOffset";          $dmArrowsAttrTypes[$i] = "float";	 $i++;
$dmArrowsAttrs[$i] = "headLength";           $dmArrowsAttrTypes[$i] = "float";	 $i++;
$dmArrowsAttrs[$i] = "lengthThreshold";      $dmArrowsAttrTypes[$i] = "float";	 $i++;


// Data model: cut planes
// ======================
//
global string $dmCutPlanesAttrs[];
global string $dmCutPlanesAttrTypes[];

int $i = 0;
$dmCutPlanesAttrs[$i] = "objectType";             $dmCutPlanesAttrTypes[$i] = "string";  $i++;
$dmCutPlanesAttrs[$i] = "objectNumber";           $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "parent";                 $dmCutPlanesAttrTypes[$i] = "string";  $i++;
$dmCutPlanesAttrs[$i] = "visible";                $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "shaderInfo";             $dmCutPlanesAttrTypes[$i] = "string";  $i++;
$dmCutPlanesAttrs[$i] = "shadingNetwork";         $dmCutPlanesAttrTypes[$i] = "string";  $i++;
$dmCutPlanesAttrs[$i] = "VMapType";               $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "replicatorNx";           $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "replicatorNy";           $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "replicatorNz";           $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "replicatorDx";           $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "replicatorDy";           $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "replicatorDz";           $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "planeCenterX";           $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "planeCenterY";           $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "planeCenterZ";           $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "planeHeight";            $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "planeWidth";             $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "planeThickness";         $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "planeRotationX";         $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "planeRotationY";         $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "planeRotationZ";         $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "bumpScale";              $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "bumpWidthSubdivisions";  $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "bumpHeightSubdivisions"; $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "bumpCentering";          $dmCutPlanesAttrTypes[$i] = "long";    $i++;
$dmCutPlanesAttrs[$i] = "bumpCenterValue";        $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "contoursMin";            $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "contoursMax";            $dmCutPlanesAttrTypes[$i] = "float";   $i++;
$dmCutPlanesAttrs[$i] = "contoursIntervalCount";  $dmCutPlanesAttrTypes[$i] = "long";    $i++;


// Data model: isosurfaces
// =======================
//
global string $dmIsosurfacesAttrs[];
global string $dmIsosurfacesAttrTypes[];

int $i = 0;
$dmIsosurfacesAttrs[$i] = "objectType";      $dmIsosurfacesAttrTypes[$i] = "string";  $i++;
$dmIsosurfacesAttrs[$i] = "objectNumber";    $dmIsosurfacesAttrTypes[$i] = "long";    $i++;
$dmIsosurfacesAttrs[$i] = "parent";          $dmIsosurfacesAttrTypes[$i] = "string";  $i++;
$dmIsosurfacesAttrs[$i] = "visible";         $dmIsosurfacesAttrTypes[$i] = "long";    $i++;
$dmIsosurfacesAttrs[$i] = "shaderInfo";      $dmIsosurfacesAttrTypes[$i] = "string";  $i++;
$dmIsosurfacesAttrs[$i] = "shadingNetwork";  $dmIsosurfacesAttrTypes[$i] = "string";  $i++;
$dmIsosurfacesAttrs[$i] = "secondaryData";	 $dmIsosurfacesAttrTypes[$i] = "string";  $i++;
$dmIsosurfacesAttrs[$i] = "VMapType";		 $dmIsosurfacesAttrTypes[$i] = "long";    $i++;
$dmIsosurfacesAttrs[$i] = "isosurfaceValue"; $dmIsosurfacesAttrTypes[$i] = "float";   $i++;
$dmIsosurfacesAttrs[$i] = "resampling";		 $dmIsosurfacesAttrTypes[$i] = "float3";  $i++;
$dmIsosurfacesAttrs[$i] = "halfWidth";		 $dmIsosurfacesAttrTypes[$i] = "float";   $i++;


// Data model: shading network
// ===========================
//
global string $dmShadingNetworkAttrs[];
global string $dmShadingNetworkAttrTypes[];

int $i = 0;
$dmShadingNetworkAttrs[$i] = "objectType";                    $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "objectNumber";                  $dmShadingNetworkAttrTypes[$i] = "long";    $i++;
$dmShadingNetworkAttrs[$i] = "prefix";                        $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "briefAnnotation";               $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "creatorVizObject";              $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "shader";                        $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "shaderType";                    $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "shaderTweak";                   $dmShadingNetworkAttrTypes[$i] = "long";    $i++;
$dmShadingNetworkAttrs[$i] = "shadingGroup";                  $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "displayName";                   $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "secondaryData";                 $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "secondaryDataDisplay";          $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "selectedTexture";               $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "textureCounter";                $dmShadingNetworkAttrTypes[$i] = "long";    $i++;
$dmShadingNetworkAttrs[$i] = "selectedConnection";            $dmShadingNetworkAttrTypes[$i] = "string";  $i++;
$dmShadingNetworkAttrs[$i] = "connectionCounter";             $dmShadingNetworkAttrTypes[$i] = "long";    $i++;


//// Color bar mapping
//// =================
//global string $mccbSources[];
//$mccbSources[0] = "ColorBarFile.outColor";
//$mccbSources[1] = "ColorBarFile.outTransparency";
//$mccbSources[2] = "ColorBarFile.outAlpha";
//$mccbSources[3] = "SecondaryData.scaledDatum";
//
//global string $mccbSourceTypes[];
//$mccbSourceTypes[0] = "RGB";
//$mccbSourceTypes[1] = "RGB";
//$mccbSourceTypes[2] = "scalar";
//$mccbSourceTypes[3] = "scalar";
//
//global string $mccbDestinations[];
//$mccbDestinations[ 0] = "Blinn.color";
//$mccbDestinations[ 1] = "Blinn.glowIntensity";
//$mccbDestinations[ 2] = "Blinn.diffuse";
//$mccbDestinations[ 3] = "Blinn.ambientColor";
//$mccbDestinations[ 4] = "Blinn.incandescence";
//$mccbDestinations[ 5] = "Blinn.translucence";
//$mccbDestinations[ 6] = "Blinn.transparency";
//$mccbDestinations[ 7] = "Blinn.materialAlphaGain";
//$mccbDestinations[ 8] = "Blinn.specularColor";
//$mccbDestinations[ 9] = "Blinn.reflectivity";
//$mccbDestinations[10] = "Blinn.reflectedColor";
//$mccbDestinations[11] = "Blinn.eccentricity";
//
//global string $mccbDestinationTypes[];
//$mccbDestinationTypes[ 0] = "RGB";
//$mccbDestinationTypes[ 1] = "scalar";
//$mccbDestinationTypes[ 2] = "scalar";
//$mccbDestinationTypes[ 3] = "RGB";
//$mccbDestinationTypes[ 4] = "RGB";
//$mccbDestinationTypes[ 5] = "scalar";
//$mccbDestinationTypes[ 6] = "RGB";
//$mccbDestinationTypes[ 7] = "scalar";
//$mccbDestinationTypes[ 8] = "RGB";
//$mccbDestinationTypes[ 9] = "scalar";
//$mccbDestinationTypes[10] = "RGB";
//$mccbDestinationTypes[11] = "scalar";
//
//
//// Image file mapping
//// =================
//global string $mcifSources[];
//$mcifSources[0] = "ImageFile.outColor";
//$mcifSources[1] = "ImageFile.outColorR";
//$mcifSources[2] = "ImageFile.outColorG";
//$mcifSources[3] = "ImageFile.outColorB";
//$mcifSources[4] = "ImageFile.outTransparency";
//$mcifSources[5] = "ImageFile.outAlpha";
//$mcifSources[6] = "SecondaryData.scaledDatum";
//
//global string $mcifSourceTypes[];
//$mcifSourceTypes[0] = "RGB";
//$mcifSourceTypes[1] = "scalar";
//$mcifSourceTypes[2] = "scalar";
//$mcifSourceTypes[3] = "scalar";
//$mcifSourceTypes[4] = "RGB";
//$mcifSourceTypes[5] = "scalar";
//$mcifSourceTypes[6] = "scalar";
//
//global string $mcifDestinations[];
//$mcifDestinations[ 0] = "Blinn.color";
//$mcifDestinations[ 1] = "Blinn.colorR";
//$mcifDestinations[ 2] = "Blinn.colorG";
//$mcifDestinations[ 3] = "Blinn.colorB";
//$mcifDestinations[ 4] = "Blinn.glowIntensity";
//$mcifDestinations[ 5] = "Blinn.diffuse";
//$mcifDestinations[ 6] = "Blinn.ambientColor";
//$mcifDestinations[ 7] = "Blinn.incandescence";
//$mcifDestinations[ 8] = "Blinn.translucence";
//$mcifDestinations[ 9] = "Blinn.transparency";
//$mcifDestinations[10] = "Blinn.materialAlphaGain";
//$mcifDestinations[11] = "Blinn.specularColor";
//$mcifDestinations[12] = "Blinn.reflectivity";
//$mcifDestinations[13] = "Blinn.reflectedColor";
//$mcifDestinations[14] = "Blinn.eccentricity";
//
//global string $mcifDestinationTypes[];
//$mcifDestinationTypes[ 0] = "RGB";
//$mcifDestinationTypes[ 1] = "scalar";
//$mcifDestinationTypes[ 2] = "scalar";
//$mcifDestinationTypes[ 3] = "scalar";
//$mcifDestinationTypes[ 4] = "scalar";
//$mcifDestinationTypes[ 5] = "scalar";
//$mcifDestinationTypes[ 6] = "RGB";
//$mcifDestinationTypes[ 7] = "RGB";
//$mcifDestinationTypes[ 8] = "scalar";
//$mcifDestinationTypes[ 9] = "RGB";
//$mcifDestinationTypes[10] = "scalar";
//$mcifDestinationTypes[11] = "RGB";
//$mcifDestinationTypes[12] = "scalar";
//$mcifDestinationTypes[13] = "RGB";
//$mcifDestinationTypes[14] = "scalar";
