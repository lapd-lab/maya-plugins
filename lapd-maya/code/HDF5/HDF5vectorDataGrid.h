/* ==========================================================================
// HDF5 : HDF5vectorDataGrid.h
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#ifndef HDF5VECTORDATAGRID_H
#define HDF5VECTORDATAGRID_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h> 

class HDF5vectorDataGrid : public MPxNode
{
public:
    MPxNode::SchedulingType schedulingType() const;

	virtual MStatus compute( const MPlug& plug, MDataBlock& data );

    HDF5vectorDataGrid();
	virtual void postConstructor();
    virtual ~HDF5vectorDataGrid();

	static  void *creator();
	static  MStatus initialize();

public:


	// HDF5vectorDataNode has these attributes:
	// Input
	//   filename
	//   group name
	//   time index
	//   parameter1-4 indices
	//   multiplier -- applied first (this is a float3, i.e. each component is scaled separately)
	//   offset     -- applied second (also a float3)
	//
	static MObject filename;
	static MObject groupName;

	static MObject timeIndex;

	static MObject parameter1Index;
	static MObject parameter2Index;
	static MObject parameter3Index;
	static MObject parameter4Index;

	static MObject aMultiplier;
	static MObject aOffset;


	// Output
	//	3D volume of 3-vector data
    //
	static MObject vectorDataGrid;
	static MObject max;
	static MObject min;

	static	MTypeId id;

private:
	MStatus computeKernel( const MPlug& plug, MDataBlock& data );

};

#endif
