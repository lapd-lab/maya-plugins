/* ==========================================================================
// Arrows : arrows.h
//
// Copyright (C) 2008 Jim Bamber 
//
*/
#ifndef ARROWS_H
#define ARROWS_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MFnMesh.h>


class arrows : public MPxNode
{
    public:

    arrows();
	virtual void postConstructor();
    virtual ~arrows();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	static MPointArray unitCircle;
	static int uvIdCount;

    static MStatus calculateArrow(
		MPoint& point,
		MVector& dataVector,
		double multiplier,
		int arrowProportions,
		double baseDiameter,
		double jointDiameter,
		double jointOffset,
		double headDiameter,
		double headLength,
		int segmentCount,
		double lengthThreshold,
		MPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );

	static MStatus setUVs(
		MFnMesh& fnMesh );

	static MStatus assignUVs(
		int iArrow,
		double vectorMag,
		double dataMin,
		double dataMax,
		float dataMinMapsTo,
		float dataMaxMapsTo,
		int segmentCount,
		MFnMesh& fnMesh );

	// Input attributes
	static MObject aInputMesh;			// For the locations of the arrows (mesh).
	static MObject aSpatialGrid;		// The spatial grid of the vector field below (vector array).  Note that this is not used currently.
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aVectorDataGrid;		// This supplies the vectors of the basic force field (vector array).
	static MObject aDataMinMapsTo;		// Minimum vector magnitude maps to this value, default 0.0 (float).
	static MObject aDataMaxMapsTo;		// Maximum vector magnitude maps to this value, default 1.0 (float).
	static MObject aMultiplier;			// Used to scale up or down the size of the arrows (double).
	static MObject aArrowProportions;	// Flag to indicate fixed, fully proportional or length proportional arrows (int).
	static MObject aBaseDiameter;		// Diameter of the circle at the base of the arrow shaft (double).
	static MObject aJointDiameter;		// Diameter of the circle where the arrow shaft joins the arrow head (double).
	static MObject aJointOffset;		// The joint is moved this distance forward of the start of the arrow head (double).
	static MObject aHeadDiameter;		// Diameter of the base of the arrow head (double).
	static MObject aHeadLength;			// Length of the arrow head (double).
	static MObject aSegmentCount;		// Number of line segments in the circles forming the arrow (int).
	static MObject aLengthThreshold;	// Arrows shorter than this will not be displayed (double).

	// Output attributes
	static MObject aOutputMesh;			// The newly created mesh.
	static MObject aDataMin;			// Minimum vector magnitude (float).
	static MObject aDataMax;			// Maximum vector magnitude (float).
};


#endif

