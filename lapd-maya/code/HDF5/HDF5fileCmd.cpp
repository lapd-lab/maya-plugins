/* ==========================================================================
// HDF5 : HDF5fileCmd.cpp
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#include "errorHandling.h"
#include "HDF5fileCmd.h"
#include <maya/MGlobal.h>
#include <maya/MString.h>
#include <maya/MSyntax.h>
#include <maya/MArgDatabase.h>

#include <H5Apublic.h>
#include <H5Fpublic.h>
#include <H5FDstdio.h>
#include <H5Gpublic.h>
#include <H5Ppublic.h>
#include <H5Tpublic.h>

const char *fileFlag = "-f", *fileLongFlag = "-file";
int HDF5fileCmd::spatialGridNodeCount = 0;
int HDF5fileCmd::vectorDataNodeCount = 0;
int HDF5fileCmd::scalarDataNodeCount = 0;


/* ==========================================================================
// newSyntax()
*/
MSyntax HDF5fileCmd::newSyntax()
{
    MSyntax syntax;

	syntax.addFlag( fileFlag, fileLongFlag, MSyntax::kString );
	syntax.enableQuery(true);
	syntax.enableEdit(true);

	return syntax;
}


/* ==========================================================================
// doIt()
*/
MStatus HDF5fileCmd::doIt ( const MArgList &args )
{ 
	// Get the filename argument.
	//
	MStatus stat;
	MString filename;
	MArgDatabase argData( syntax(), args, &stat );   CHECK_MSTATUS( stat );
	bool flagIsSet = argData.isFlagSet( fileFlag, &stat );
	CHECK_MSTATUS( stat );
	if ( !flagIsSet )
	{
		MGlobal::displayError( "Usage: HDF5file -f <filename>" );
		return MStatus::kFailure;
	}

	CHECK_MSTATUS( argData.getFlagArgument(fileFlag, 0, filename) );


	// Open the file.
	//
	const char *filenameAsChar = filename.asChar();
	hid_t file_id = H5Fopen( filenameAsChar, H5F_ACC_RDONLY, H5P_DEFAULT );   HDF5CHECK( file_id );


	// Read the version attribute.  This is a quick check on the validity of the file.
	//
	char version[120];
	hid_t attr_group_id = H5Gopen( file_id, "/Attributes", H5P_DEFAULT );	HDF5CHECK( attr_group_id );
	hid_t attr_id = H5Aopen_name( attr_group_id, "Version" );   HDF5CHECK( attr_id );
	hid_t attr_type_id = H5Aget_type( attr_id );				HDF5CHECK( attr_type_id );

	HDF5CHECK( H5Aread(attr_id, attr_type_id, version) );
	MGlobal::displayInfo( (MString)"File version: " + version );


	// Create the spatial grid node.
	//
	MObject HDF5spatialGridNodeObj = dgMod.createNode( "HDF5spatialGrid", &stat );   CHECK_MSTATUS( stat );
	spatialGridNodeCount++;
	MString nodeName = MString("HDF5spatialGrid") + spatialGridNodeCount;
	CHECK_MSTATUS( dgMod.renameNode(HDF5spatialGridNodeObj, nodeName) );
	MGlobal::displayInfo( "Created node: " + nodeName );

	MString cmd = MString( "setAttr " ) + nodeName + ".filename -type \"string\" \"" + filename + "\";";
	CHECK_MSTATUS( dgMod.commandToExecute(cmd) ); 
	MGlobal::displayInfo( "Executed command: " + cmd );


	// Create the data nodes.
	// To do this, loop through the group objects, sorting by the Type attribute.  Type will be either: "Scalar" or "3-vector".
	//
	hsize_t num_objs;
	HDF5CHECK( H5Gget_num_objs(file_id, &num_objs) );

	// Loop through all objects in HDF5 file...
	for ( hsize_t i=0; i<num_objs; i++ ) 
	{
	  int objtype = H5Gget_objtype_by_idx( file_id, i );   HDF5CHECK( objtype );

	  // If the object is a group, continue further...
	  if ( objtype == H5G_GROUP )
	  {
	    // Get the group name
	    char group_name[120];
	    size_t tot_chars = H5Gget_objname_by_idx( file_id, i, group_name, 120 );   HDF5CHECK( tot_chars );

		// If the group name is not "Attributes", continue on...
		if ( MString(group_name) != "Attributes" )
		{
	      // Read the type attribute
		  hid_t group_id = H5Gopen( file_id, group_name, H5P_DEFAULT );   HDF5CHECK( group_id );
		  attr_id = H5Aopen_name( group_id, "Type" );		 HDF5CHECK( attr_id );
		  attr_type_id = H5Aget_type( attr_id );			 HDF5CHECK( attr_type_id );

		  char data_type[120];
		  HDF5CHECK( H5Aread(attr_id, attr_type_id, data_type) );


		  // If the Type is "Scalar"...
		  if ( MString(data_type) == "Scalar" )
		  {
		    // Create a scalar data node.
		    MObject HDF5scalarDataNodeObj = dgMod.createNode( "HDF5scalarDataGrid", &stat );   CHECK_MSTATUS( stat );
	        scalarDataNodeCount++;
	        MString nodeName = MString( "HDF5scalarDataGrid" ) + scalarDataNodeCount;
	        CHECK_MSTATUS( dgMod.renameNode(HDF5scalarDataNodeObj, nodeName) );
	        MGlobal::displayInfo( "Created node: " + nodeName );

	        MString cmd = MString( "setAttr " ) + nodeName + ".filename -type \"string\" \"" + filename + "\";";
	        CHECK_MSTATUS( dgMod.commandToExecute(cmd) ); 
	        MGlobal::displayInfo( "Executed command: " + cmd );

	        cmd = MString( "setAttr " ) + nodeName + ".group -type \"string\" \"" + group_name + "\";";
	        CHECK_MSTATUS( dgMod.commandToExecute(cmd) ); 
	        MGlobal::displayInfo( "Executed command: " + cmd );
		  }


		  // If the Type is "3-vector"...
		  if ( MString(data_type) == "3-vector" )
		  {
		    // Create a vector data node.
	        MObject HDF5vectorDataNodeObj = dgMod.createNode( "HDF5vectorDataGrid", &stat );   CHECK_MSTATUS( stat );
	        vectorDataNodeCount++;
	        MString nodeName = MString( "HDF5vectorDataGrid" ) + vectorDataNodeCount;
	        CHECK_MSTATUS( dgMod.renameNode(HDF5vectorDataNodeObj, nodeName) );
	        MGlobal::displayInfo( "Created node: " + nodeName );

	        MString cmd = MString( "setAttr " ) + nodeName + ".filename -type \"string\" \"" + filename + "\";";
	        CHECK_MSTATUS( dgMod.commandToExecute(cmd) ); 
	        MGlobal::displayInfo( "Executed command: " + cmd );

	        cmd = MString( "setAttr " ) + nodeName + ".group -type \"string\" \"" + group_name + "\";";
	        CHECK_MSTATUS( dgMod.commandToExecute(cmd) ); 
	        MGlobal::displayInfo( "Executed command: " + cmd );
		  }
		} // End if not the "Attributes" group
	  } // End if the object is a group.
	} // End looping through all objects in the HDF5 file.


	// Close all opened HDF5 objects.
	HDF5CHECK( H5Tclose(attr_type_id) );
	HDF5CHECK( H5Aclose(attr_id) );
	HDF5CHECK( H5Gclose(attr_group_id) );
	HDF5CHECK( H5Fclose(file_id) );

	return redoIt();
}


/* ==========================================================================
// undoIt()
*/
MStatus HDF5fileCmd::undoIt()
{
	return dgMod.undoIt();
}


/* ==========================================================================
// redoIt()
*/
MStatus HDF5fileCmd::redoIt()
{
	return dgMod.doIt();
}

