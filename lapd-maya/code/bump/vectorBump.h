/* ============================================================================
// bump : vectorBump.h
//
// Copyright (C) 2007 Jim Bamber 
//
*/
#ifndef VECTORBUMP_H
#define VECTORBUMP_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>


class vectorBump : public MPxNode
{
    public:

    vectorBump();
	virtual void postConstructor();
    virtual ~vectorBump();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MPointArray calculateBump(
		MPointArray& inMeshPoints,
		MMatrix& worldMatrix,
		double bumpScale,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MVectorArray& vectorDataGrid );

	// Input attributes
	static MObject aInputMesh;			// The base object to deform.
	static MObject aWorldMatrix;		// Transformation matrix applied to base object.
	static MObject aBumpScale;			// A multiplying factor to control bump height.
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aVectorDataGrid;		// This supplies the vectors of the basic force field (vector array).

	// Output attributes
	static MObject aOutputMesh;			// The deformed object.
};


#endif

