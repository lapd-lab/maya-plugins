/* ============================================================================
// Isosurface : resample.cpp
//
// Copyright (C) 2008 Jim Bamber 
//
*/
#include "resample.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "isoSurfaceCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatArray.h>


// Static data
MTypeId resample::id( 0x00601 );

// Attributes
MObject resample::aResamplingFactors;
MObject resample::aHalfWindow;
MObject resample::aInDims;
MObject resample::aGridMin;
MObject resample::aGridMax;
MObject resample::aInScalarDataGrid;

MObject resample::aOutDims;
MObject resample::aOutScalarDataGrid;


/* ============================================================================
 * resample()
 */
MStatus resample::doResampling(
	float3& resamplingFactors,
	double halfWindow,
	int3& inDims,
	float3& gridMin,
	float3& gridMax,
	MDoubleArray& inScalarDataGrid,
	int3& outDims,
	MDoubleArray& outScalarDataGrid )
{
	MStatus stat;
	//double halfWindow = 2.0;

	for ( int i=0; i<3; i++ )
		outDims[i] = (int)( (float)inDims[i] * resamplingFactors[i] );

	// dimensions of output grid
	int nx = outDims[0];
	int ny = outDims[1];
	int nz = outDims[2];

	// distances between output grid points
	float xmin = gridMin[0];
	float ymin = gridMin[1];
	float zmin = gridMin[2];
	float dx = (gridMax[0]-xmin) / (nx-1);
	float dy = (gridMax[1]-ymin) / (ny-1);
	float dz = (gridMax[2]-zmin) / (nz-1);

	outScalarDataGrid.clear();
	for ( int iz=0; iz<nz; iz++ )
	{
		double z = zmin + (double)iz*dz;
		for ( int iy=0; iy<ny; iy++ )
		{
			double y = ymin + (double)iy*dy;
			for ( int ix=0; ix<nx; ix++ )
			{
				double x = xmin + (double)ix*dx;
				MPoint point( x, y, z );
				double resampledScalar =
					lapdCommon::interpolateScalar( point, inDims, gridMax, gridMin, inScalarDataGrid, halfWindow );
				outScalarDataGrid.append( resampledScalar );
			}
		}
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
*/
MStatus resample::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//isoSurfaceCommon::logEntry( "--> resample::compute()" );
	isoSurfaceCommon::logEntry( isoSurfaceCommon::eResample );

	if ( (plug != aOutScalarDataGrid) && (plug != aOutDims) )
	{
		isoSurfaceCommon::logEntry((char*) "Abnormal exit from resample::compute(), Maya not asking for scalar data grid or dims" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hResamplingFactors( block.inputValue(aResamplingFactors, &stat) );   diLOG_MSTATUS( stat );
	float3& resamplingFactors = hResamplingFactors.asFloat3();

	MDataHandle hHalfWindow( block.inputValue(aHalfWindow, &stat) );   diLOG_MSTATUS( stat );
	double halfWindow = hHalfWindow.asDouble();

	MDataHandle hInDims( block.inputValue(aInDims, &stat) );   diLOG_MSTATUS( stat );
	int3& inDims = hInDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hInScalarDataGrid( block.inputValue(aInScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnDoubleArrayData fnInScalarDataGrid( hInScalarDataGrid.data() );
	MDoubleArray inScalarDataGrid( fnInScalarDataGrid.array() );


	// Now do the output attributes.
	//
	MDataHandle hOutDims( block.outputValue(aOutDims, &stat) );   diLOG_MSTATUS( stat );
	int3& outDims = hOutDims.asInt3();

	MDataHandle hOutScalarDataGrid( block.outputValue(aOutScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnDoubleArrayData fnOutScalarDataGrid( hOutScalarDataGrid.data() );
	MDoubleArray outScalarDataGrid( fnOutScalarDataGrid.array() );


	// Make the call to resample the data
	//isoSurfaceCommon::logEntry( "About to do the resampling" );
	diLOG_MSTATUS(
		doResampling(
			resamplingFactors,
			halfWindow,
			inDims,
			gridMin,
			gridMax,
			inScalarDataGrid,
			outDims,
			outScalarDataGrid) );


	// Set the outputs as clean.
	hOutDims.setClean();
	diLOG_MSTATUS( fnOutScalarDataGrid.set(outScalarDataGrid) );
	hOutScalarDataGrid.setClean();
	block.setClean( plug );

	//isoSurfaceCommon::logEntry( "<-- resample::compute()" );
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information and the unit circle values
*/
MStatus resample::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	isoSurfaceCommon::logEntry( (char*) "--> resample::initialize()" );


	// Create output attributes
	//
	// resampled grid dimensions
	aOutDims = nAttr.create( "outDims", "outDims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_OUTPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aOutDims) );

	// resampled scalar data grid
	aOutScalarDataGrid = tAttr.create( "outScalarDataGrid", "outScalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_OUTPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aOutScalarDataGrid) );


	// Create input attributes:
	//
	// resampling factors
	aResamplingFactors = nAttr.create( "resamplingFactors", "resamplingFactors", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0, 1.0, 1.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aResamplingFactors) );
    diLOG_MSTATUS( attributeAffects(aResamplingFactors, aOutDims) );
    diLOG_MSTATUS( attributeAffects(aResamplingFactors, aOutScalarDataGrid) );

	// half window
	aHalfWindow = nAttr.create( "halfWindow", "halfWindow", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(2.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aHalfWindow) );
    diLOG_MSTATUS( attributeAffects(aHalfWindow, aOutDims) );
    diLOG_MSTATUS( attributeAffects(aHalfWindow, aOutScalarDataGrid) );

	// input grid dimensions
	aInDims = nAttr.create( "inDims", "inDims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aInDims) );
    diLOG_MSTATUS( attributeAffects(aInDims, aOutDims) );
    diLOG_MSTATUS( attributeAffects(aInDims, aOutScalarDataGrid) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gridMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aOutDims) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aOutScalarDataGrid) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gridMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aOutDims) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aOutScalarDataGrid) );

	// input scalar data grid
	aInScalarDataGrid = tAttr.create( "inScalarDataGrid", "inScalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aInScalarDataGrid) );
    diLOG_MSTATUS( attributeAffects(aInScalarDataGrid, aOutDims) );
    diLOG_MSTATUS( attributeAffects(aInScalarDataGrid, aOutScalarDataGrid) );


	isoSurfaceCommon::logEntry( (char*) "<-- resample::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
 * resample()
 */
resample::resample()
{
}


/* ============================================================================
 * postConstructor()
 */
void resample::postConstructor( )
{
}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType resample::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * resample::creator()
{
    return new resample();
}


/* ============================================================================
 * ~resample()
 */
resample::~resample()
{
}


