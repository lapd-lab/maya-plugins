/* ============================================================================
// forceField : markers.h
//
// Copyright (C) 2008 Jim Bamber 
//
*/
#ifndef MARKERS_H
#define MARKERS_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class markers : public MPxNode
{
    public:

    markers();
	virtual void postConstructor();
    virtual ~markers();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MStatus createMesh(
		MObject& markerMesh,
		int curveCount,
		MIntArray startIndices,
		MIntArray CVCounts,
		MPointArray controlVertexList,
		MVectorArray tangentList,
		int curveIndex,
		int start,
		int step,
		bool reverse,
		MObject& newOutputData );

	static MStatus addMarker(
		MObject& workMarkerMeshData,
		MFnMesh& fnMesh,
		MObject& newOutputData,
		MPoint controlVertex,
		MVector tangent );

	// Input attributes
	static MObject aMarkerMesh;			// The mesh of the base marker object to replicate.
	static MObject aCurveCount;			// Number of curves in the output array.
	static MObject aStartIndices;		// Used to index into the control vertex and tangent arrays (int array).
	static MObject aCVCounts;			// Number of CV's for each curve (int array), for example:
										//   index = startIndices[curveNum] + cvIndex, where cvIndex = {0:CVCounts[curveNum]-1}
	static MObject aControlVertexList;	// Control vertices for curves (point array).
	static MObject aTangentList;		// Tangents for curves (vector array).
	static MObject aCurveIndex;			// Index of curve to use as basis for output mesh (int, 0-based).
	static MObject aStart;				// Start CV for placement of markers (int).
	static MObject aStep;				// Size of step through CV's for placement of markers (int).
	static MObject aReverse;			// Reverse the direction of the markers (bool)

	// Output attributes
	static MObject aSecondaryMesh;		// The newly created mesh.
};


#endif

