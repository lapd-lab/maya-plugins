/* ============================================================================
// forceField : squares.cpp
//-
// ==========================================================================
// Copyright (C) 2008 Jim Bamber
//
// ==========================================================================
//+
*/
#include "squares.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>
#include <time.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshEdge.h>
#include <maya/MQuaternion.h>


// Static data
MTypeId squares::id( 0x00456 );

// Attributes
MObject squares::aCurveCount;
MObject squares::aStartIndices;
MObject squares::aCVCounts;
MObject squares::aControlVertexList;
MObject squares::aTangentList;
MObject squares::aTubeRadius;
MObject squares::aRandomize;
//MObject squares::aCurveIndex;
MObject squares::aStart;
MObject squares::aStep;
MObject squares::aUVScale;
MObject squares::aXYSquares;
MObject squares::aXZSquares;
MObject squares::aYZSquares;
MObject squares::aDims;
MObject squares::aGridMin;
MObject squares::aGridMax;
MObject squares::aMultiplier;
MObject squares::aScalarDataGrid;

MObject squares::aSecondaryMesh;


/* ============================================================================
// createMesh()
*/
MStatus squares::createMesh(
	int curveCount,
	MIntArray startIndices,
	MIntArray CVCounts,
	MPointArray controlVertexList,
	MVectorArray tangentList,
	double tubeRadius,
	bool randomize,
	//int curveIndex,
	int start,
	int step,
	double UVScale,
	bool xySquares,
	bool xzSquares,
	bool yzSquares,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MDoubleArray& scalarDataGrid,
	double multiplier,
	MObject& newOutputData )
{
	MStatus stat;

	MFnMesh fnMesh;
	MPointArray vertices;
	MIntArray polygonVertexCounts;
	MIntArray polygonVertexIndices;
	MVector translation;
	int index;
	MPoint controlVertex;
	MVector tangent;
	double halfWindow = 2.0;
	double scalarDatum;
	srand( (unsigned)time( NULL ) );

	if ( start < 0 ) start = 0;
	if ( step < 1 ) step = 1;

	int CVCount = 0;
	int startIndex = 0;
	//if ( curveIndex < curveCount )
	//{
	//	CVCount = CVCounts[ curveIndex ];
	//	startIndex = startIndices[ curveIndex ];
	//}

	for ( int iCurve = 0; iCurve < curveCount; iCurve++ )
	{
		CVCount = CVCounts[ iCurve ];
		startIndex = startIndices[ iCurve ];
		for ( int iCV = start; iCV < CVCount; iCV = iCV + step )
		{
			index = startIndex + iCV;
			controlVertex = controlVertexList[index];
			tangent = tangentList[index];

			scalarDatum = lapdCommon::interpolateScalar( controlVertex, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
			scalarDatum = multiplier * scalarDatum;

			//addSquare( fnMesh, newOutputData, controlVertex, tangent, scalarDatum, tubeRadius, randomize );
			addSquare(
				controlVertex,
				tangent,
				scalarDatum,
				tubeRadius,
				randomize,
				xySquares,
				xzSquares,
				yzSquares,
				vertices,
				polygonVertexCounts,
				polygonVertexIndices );
		}
	}

	int vertexCount = vertices.length();
	int polygonCount = polygonVertexCounts.length();
	//sprintf( forceFieldCommon::logString, "vertexCount: %d  polygonCount: %d", vertexCount, polygonCount );
	//forceFieldCommon::logEntry( forceFieldCommon::logString );

	// Single call here to create the mesh
	fnMesh.create(
		vertexCount,
		polygonCount, 
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		newOutputData, 
		&stat );   diLOG_MSTATUS( stat );

	//diLOG_MSTATUS( fnMesh.cleanupEdgeSmoothing() );

	assignUVs( UVScale, fnMesh, newOutputData );

	return MStatus::kSuccess;
}


/* ============================================================================
// addSquare()
*/
MStatus squares::addSquare(
	//MFnMesh& fnMesh,
	//MObject& newOutputData,
	MPoint controlVertex,
	MVector tangent,
	double scalarDatum,
	double tubeRadius,
	bool randomize,
	bool xySquares,
	bool xzSquares,
	bool yzSquares,
	MPointArray& vertices,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices )
{
	MStatus stat;

	MVector xAxis( 1.0, 0.0, 0.0 );
	MVector yAxis( 0.0, 1.0, 0.0 );
	MVector zAxis( 0.0, 0.0, 1.0 );
	double randomTheta;
	double cosRandomTheta = 1.0;
	double sinRandomTheta = 0.0;
	if ( randomize )
	{
		int randomNumber = rand();
		randomTheta = ((double)randomNumber/(double)RAND_MAX) * 2.0 * 3.1415927;
		cosRandomTheta = cos( randomTheta );
		sinRandomTheta = sin( randomTheta );
	}
	MVector randomAxis( cosRandomTheta, sinRandomTheta, 0.0 );
	MQuaternion randomQuaternion( xAxis, randomAxis );

	MQuaternion tangentQuaternion( zAxis, tangent );
	MVector vector1, vector2, vector3;
	MVector randomizedVector1, randomizedVector2, randomizedVector3;
	MVector rotatedVector1, rotatedVector2, rotatedVector3;
	MVectorArray vectors1, vectors2, vectors3;
	double length = tubeRadius + scalarDatum;
	length = length * 1.41421356;   // diagonal from center to corner of square
	MPointArray vertexArray1, vertexArray2, vertexArray3;

	// Make squares centered on the field line
	vectors1.append( MVector(-length,  length, 0.0) );
	vectors1.append( MVector( length,  length, 0.0) );
	vectors1.append( MVector( length, -length, 0.0) );
	vectors1.append( MVector(-length, -length, 0.0) );

	vectors2.append( MVector(-length, 0.0,  length) );
	vectors2.append( MVector( length, 0.0,  length) );
	vectors2.append( MVector( length, 0.0, -length) );
	vectors2.append( MVector(-length, 0.0, -length) );

	vectors3.append( MVector(0.0, -length,  length) );
	vectors3.append( MVector(0.0,  length,  length) );
	vectors3.append( MVector(0.0,  length, -length) );
	vectors3.append( MVector(0.0, -length, -length) );

	if ( randomize )
		for ( int i=0; i<4; i++ )
		{
			vector1 = vectors1[i];
			vector2 = vectors2[i];
			vector3 = vectors3[i];
			randomizedVector1 = vector1.rotateBy( randomQuaternion );
			randomizedVector2 = vector2.rotateBy( randomQuaternion );
			randomizedVector3 = vector3.rotateBy( randomQuaternion );
			rotatedVector1 = randomizedVector1.rotateBy( tangentQuaternion );
			rotatedVector2 = randomizedVector2.rotateBy( tangentQuaternion );
			rotatedVector3 = randomizedVector3.rotateBy( tangentQuaternion );
			vertexArray1.append( controlVertex + rotatedVector1 );
			vertexArray2.append( controlVertex + rotatedVector2 );
			vertexArray3.append( controlVertex + rotatedVector3 );
		}
	else
		for ( int i=0; i<4; i++ )
		{
			vector1 = vectors1[i];
			vector2 = vectors2[i];
			vector3 = vectors3[i];
			rotatedVector1 = vector1.rotateBy( tangentQuaternion );
			rotatedVector2 = vector2.rotateBy( tangentQuaternion );
			rotatedVector3 = vector3.rotateBy( tangentQuaternion );
			vertexArray1.append( controlVertex + rotatedVector1 );
			vertexArray2.append( controlVertex + rotatedVector2 );
			vertexArray3.append( controlVertex + rotatedVector3 );
		}

	//fnMesh.addPolygon( vertexArray1, true, 1.0e-4, newOutputData, &stat );   diLOG_MSTATUS( stat );
	//fnMesh.addPolygon( vertexArray2, true, 1.0e-4, newOutputData, &stat );   diLOG_MSTATUS( stat );
	//fnMesh.addPolygon( vertexArray3, true, 1.0e-4, newOutputData, &stat );   diLOG_MSTATUS( stat );

	int vertexCount = vertices.length();

	if ( xySquares )
	{
		polygonVertexCounts.append(4);
		for ( int i=0; i<4; i++ )
		{
			vertices.append( vertexArray1[i] );
			polygonVertexIndices.append( vertexCount );
			vertexCount++;
		}
	}

	if ( xzSquares )
	{
		polygonVertexCounts.append(4);
		for ( int i=0; i<4; i++ )
		{
			vertices.append( vertexArray2[i] );
			polygonVertexIndices.append( vertexCount );
			vertexCount++;
		}
	}

	if ( yzSquares )
	{
		polygonVertexCounts.append(4);
		for ( int i=0; i<4; i++ )
		{
			vertices.append( vertexArray3[i] );
			polygonVertexIndices.append( vertexCount );
			vertexCount++;
		}
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// assignUVs()
*/
MStatus squares::assignUVs(
	double UVScale,
	MFnMesh& fnMesh,
	MObject& newOutputData )
{
	MStatus stat;
	int UVStepCount = 100;
	double stepFactor = 1.0 / (2.0*(double)UVStepCount);

	// U coordinate equates to x coordinate of image
	MFloatArray uValues;
	if ( UVScale < 0.0 )
	{	// Scale to the size of the square
		uValues.append( 0.0 );
		uValues.append( 1.0 );
		uValues.append( 1.0 );
		uValues.append( 0.0 );
	}
	else
	{	// Scale to UVScale, this requires a lot of UV value steps
		for ( int i=1; i<=UVStepCount; i++ )
		{
			uValues.append( (float)( 0.5 - i*stepFactor ) );
			uValues.append( (float)( 0.5 + i*stepFactor ) );
			uValues.append( (float)( 0.5 + i*stepFactor ) );
			uValues.append( (float)( 0.5 - i*stepFactor ) );
		}
	}

	// V coordinate equates to y coordinate of image
	MFloatArray vValues;
	if ( UVScale < 0.0 )
	{	// Scale to the size of the square
		vValues.append( 1.0 );
		vValues.append( 1.0 );
		vValues.append( 0.0 );
		vValues.append( 0.0 );
	}
	else
	{	// Scale to UVScale, this requires a lot of UV value steps
		for ( int i=1; i<=UVStepCount; i++ )
		{
			vValues.append( (float)( 0.5 + i*stepFactor ) );
			vValues.append( (float)( 0.5 + i*stepFactor ) );
			vValues.append( (float)( 0.5 - i*stepFactor ) );
			vValues.append( (float)( 0.5 - i*stepFactor ) );
		}
	}

	// Create the standard UV values
	int uvId=0;
	float uValue, vValue;
	for (int i=0; i<(int)(uValues.length()); i++ )
	{
		uValue = uValues[i];
		vValue = vValues[i];
		diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue) );
		uvId++;
	}

	// Now assign these values
	if ( UVScale < 0.0 )
	{ // Scale to the size of the square
		int polygonCount = fnMesh.numPolygons( &stat );   diLOG_MSTATUS( stat );
		for ( int iPolygon=0; iPolygon<polygonCount; iPolygon++ )
			for ( int iVertex=0; iVertex<4; iVertex++ )
			{
				uvId = iVertex;
				diLOG_MSTATUS( fnMesh.assignUV(iPolygon, iVertex, uvId) );
			}
	}
	else
	{ // Scale to UVScale
		MPointArray pointArray;
		MVector edgeVector;
		double edgeLength;
		int iPolygon;
		int UVStep, uvIdBase;
		MItMeshPolygon polygonIter( newOutputData, &stat );   diLOG_MSTATUS( stat );
		polygonIter.reset();
		while ( !(polygonIter.isDone()) )
		{
			polygonIter.getPoints( pointArray, MSpace::kObject, &stat );   diLOG_MSTATUS( stat );
			edgeVector = pointArray[0] - pointArray[1];
			edgeLength = edgeVector.length();
			UVStep = (int)( edgeLength*UVStepCount / (2.0*UVScale) );
			if ( UVStep < 0 ) UVStep = 0;
			if ( UVStep > UVStepCount-1 ) UVStep = UVStepCount - 1;
			uvIdBase = UVStep * 4;
			iPolygon = polygonIter.index( &stat );   diLOG_MSTATUS( stat );
			for ( int iVertex=0; iVertex<4; iVertex++ )
			{
				uvId = uvIdBase + iVertex;
				diLOG_MSTATUS( fnMesh.assignUV(iPolygon, iVertex, uvId) );
			}
			polygonIter.next();
		}
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the curves.
//
*/
MStatus squares::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> squares::compute()" );
	forceFieldCommon::logEntry( forceFieldCommon::eSquares );

	if ( plug != aSecondaryMesh )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from squares::compute(), Maya not asking for secondary mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hCurveCount( block.inputValue(aCurveCount, &stat) );   diLOG_MSTATUS( stat );
	int curveCount = hCurveCount.asInt();

	MDataHandle hStartIndices( block.inputValue(aStartIndices, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnStartIndices( hStartIndices.data() );
	MIntArray startIndices( fnStartIndices.array() );

	MDataHandle hCVCounts( block.inputValue(aCVCounts, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnCVCounts( hCVCounts.data() );
	MIntArray CVCounts( fnCVCounts.array() );

	MDataHandle hControlVertexList( block.inputValue(aControlVertexList, &stat) );   diLOG_MSTATUS( stat );
	MFnPointArrayData fnControlVertexList( hControlVertexList.data() );
	MPointArray controlVertexList( fnControlVertexList.array() );

	MDataHandle hTangentList( block.inputValue(aTangentList, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnTangentList( hTangentList.data() );
	MVectorArray tangentList( fnTangentList.array() );

	MDataHandle hTubeRadius( block.inputValue(aTubeRadius, &stat) );   diLOG_MSTATUS( stat );
	double tubeRadius = hTubeRadius.asDouble();

	MDataHandle hRandomize( block.inputValue(aRandomize, &stat) );   diLOG_MSTATUS( stat );
	bool randomize = hRandomize.asBool();

	//MDataHandle hCurveIndex = block.inputValue( aCurveIndex, &stat );   diLOG_MSTATUS( stat );
	//int curveIndex = hCurveIndex.asInt();

	MDataHandle hStart( block.inputValue(aStart, &stat) );   diLOG_MSTATUS( stat );
	int start = hStart.asInt();

	MDataHandle hStep( block.inputValue(aStep, &stat) );   diLOG_MSTATUS( stat );
	int step = hStep.asInt();

	MDataHandle hUVScale( block.inputValue(aUVScale, &stat) );   diLOG_MSTATUS( stat );
	double UVScale = hUVScale.asDouble();

	MDataHandle hXYSquares( block.inputValue(aXYSquares, &stat) );   diLOG_MSTATUS( stat );
	bool xySquares = hXYSquares.asBool();

	MDataHandle hXZSquares( block.inputValue(aXZSquares, &stat) );   diLOG_MSTATUS( stat );
	bool xzSquares = hXZSquares.asBool();

	MDataHandle hYZSquares( block.inputValue(aYZSquares, &stat) );   diLOG_MSTATUS( stat );
	bool yzSquares = hYZSquares.asBool();

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hScalarDataGrid( block.inputValue(aScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnDoubleArrayData fnScalarDataGrid( hScalarDataGrid.data() );
	MDoubleArray scalarDataGrid( fnScalarDataGrid.array() );

	MDataHandle hMultiplier( block.inputValue(aMultiplier, &stat) );   diLOG_MSTATUS( stat );
	double multiplier = hMultiplier.asDouble();


	// Now do the output attributes.
	//
	MDataHandle hSecondaryMesh( block.outputValue(aSecondaryMesh, &stat) );   diLOG_MSTATUS( stat );


	// Create the data object which underlies the output mesh
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );

	// Make the call to create the secondary mesh
	//forceFieldCommon::logEntry( (char*) "About to create mesh" );
	diLOG_MSTATUS(
		createMesh(
			curveCount,
			startIndices,
			CVCounts,
			controlVertexList,
			tangentList,
			tubeRadius,
			randomize,
			//curveIndex,
			start,
			step,
			UVScale,
			xySquares,
			xzSquares,
			yzSquares,
			dims,
			gridMin,
			gridMax,
			scalarDataGrid,
			multiplier,
			newOutputData) );


	// Set the outputs as clean.
	hSecondaryMesh.set( newOutputData );
	block.setClean( plug );

	//forceFieldCommon::logEntry( (char*) "<-- squares::compute()" );
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information and the unit circle values
*/
MStatus squares::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> squares::initialize()" );


	// Create input attributes:
	//
	// curve count
	aCurveCount = nAttr.create( "curveCount", "curveCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveCount) );

	// start indices
	aStartIndices = tAttr.create( "startIndices", "startIndices", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aStartIndices) );

	// CV counts
	aCVCounts = tAttr.create( "CVCounts", "CVCounts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aCVCounts) );

	// control vertices
	aControlVertexList = tAttr.create( "controlVertexList", "controlVertexList", MFnData::kPointArray, fnPointArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aControlVertexList) );

	// tangents
	aTangentList = tAttr.create( "tangentList", "tangentList", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aTangentList) );

	// tube radius
	aTubeRadius = nAttr.create( "tubeRadius", "tubeRadius", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aTubeRadius) );

	// randomize
	aRandomize = nAttr.create( "randomize", "randomize", MFnNumericData::kBoolean, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(true) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aRandomize) );

	// curve index
	//aCurveIndex = nAttr.create( "curveIndex", "curveIndex", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	//MAKE_INPUT( nAttr );
    //diLOG_MSTATUS( addAttribute(aCurveIndex) );

	// start
	aStart = nAttr.create( "start", "start", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	diLOG_MSTATUS( nAttr.setMin(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aStart) );

	// step
	aStep = nAttr.create( "step", "step", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aStep) );

	// UV Scale
	aUVScale = nAttr.create( "UVScale", "UVScale", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(10.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aUVScale) );

	// xy squares
	aXYSquares = nAttr.create( "XYSquares", "XYSquares", MFnNumericData::kBoolean, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(true) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aXYSquares) );

	// xz squares
	aXZSquares = nAttr.create( "XZSquares", "XZSquares", MFnNumericData::kBoolean, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(true) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aXZSquares) );

	// yz squares
	aYZSquares = nAttr.create( "YZSquares", "YZSquares", MFnNumericData::kBoolean, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(true) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aYZSquares) );

	// grid dimensions
	aDims = nAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gridMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gridMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// scalar data grid
	aScalarDataGrid = tAttr.create( "scalarDataGrid", "scalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aScalarDataGrid) );

	// multiplier
	aMultiplier = nAttr.create( "multiplier", "multiplier", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aMultiplier) );


	// Create output attributes
	//
	// secondary mesh
	aSecondaryMesh = tAttr.create( "secondaryMesh", "secondaryMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aSecondaryMesh) );


	// All inputs affect all outputs
	//
	// secondary mesh
    diLOG_MSTATUS( attributeAffects(aCurveCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStartIndices, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCVCounts, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aControlVertexList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTangentList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTubeRadius, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aRandomize, aSecondaryMesh) );
    //diLOG_MSTATUS( attributeAffects(aCurveIndex, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStart, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStep, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aUVScale, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aXYSquares, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aXZSquares, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aYZSquares, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aDims, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aScalarDataGrid, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aMultiplier, aSecondaryMesh) );

	forceFieldCommon::logEntry( (char*) "<-- squares::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
// squares()
*/
squares::squares()
{
}


/* ============================================================================
// postConstructor()
*/
void squares::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType squares::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * squares::creator()
{
    return new squares();
}


/* ============================================================================
// ~squares()
*/
squares::~squares()
{
}


