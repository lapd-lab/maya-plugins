/* ============================================================================
// forceField : envelopes.cpp
//-
// ==========================================================================
// Copyright (C) 2007 Jim Bamber
//
// ==========================================================================
//+
*/
#include "envelopes.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshEdge.h>
#include <maya/MQuaternion.h>


// Static data
MTypeId envelopes::id( 0x00458 );
MPointArray envelopes::unitCircle;

// Attributes
MObject envelopes::aCurveCount;
MObject envelopes::aStartIndices;
MObject envelopes::aCVCounts;
MObject envelopes::aControlVertexList;
MObject envelopes::aTangentList;
MObject envelopes::aTubeRadius;
MObject envelopes::aSegmentCount;
MObject envelopes::aDims;
MObject envelopes::aGridMin;
MObject envelopes::aGridMax;
MObject envelopes::aMultiplier;
MObject envelopes::aScalarDataGrid;

MObject envelopes::aSecondaryMesh;


/* ============================================================================
// createMesh()
*/
MStatus envelopes::createMesh(
	int curveCount,
	MIntArray startIndices,
	MIntArray CVCounts,
	MPointArray controlVertexList,
	MVectorArray tangentList,
	int segmentCount,
	double tubeRadius,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MDoubleArray& scalarDataGrid,
	double multiplier,
	MObject& newOutputData )
{
	MStatus stat;

	MFnMesh fnMesh;
	MPointArray vertices;
	MIntArray polygonVertexCounts;
	MIntArray polygonVertexIndices;
	MVector translation;
	int index;
	MPoint controlVertex;
	MVector tangent;
	double halfWindow = 2.0;
	double scalarDatum;

	// Loop through curves
	for ( int iCurve=0; iCurve<curveCount; iCurve++ )
	{
		int CVCount = CVCounts[ iCurve ];
		int startIndex = startIndices[ iCurve ];
		for ( int iCV = 0; iCV < CVCount; iCV++ )
		{
			index = startIndex + iCV;
			controlVertex = controlVertexList[index];
			tangent = tangentList[index];

			scalarDatum = lapdCommon::interpolateScalar( controlVertex, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
			scalarDatum = multiplier * scalarDatum;

			addSection(
				iCV,
				controlVertex,
				tangent,
				scalarDatum,
				tubeRadius,
				segmentCount,
				vertices,
				polygonVertexCounts,
				polygonVertexIndices );
		}
	}

	int vertexCount = vertices.length();
	int polygonCount = polygonVertexCounts.length();
	//sprintf( forceFieldCommon::logString, "vertexCount: %d  polygonCount: %d", vertexCount, polygonCount );
	//forceFieldCommon::logEntry( forceFieldCommon::logString );

	// Single call here to create the mesh
	fnMesh.create(
		vertexCount,
		polygonCount, 
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		newOutputData, 
		&stat );   diLOG_MSTATUS( stat );

	assignUVs( fnMesh, segmentCount );

	return MStatus::kSuccess;
}


/* ============================================================================
// addSection()
*/
MStatus envelopes::addSection(
	int iCV,
	MPoint controlVertex,
	MVector tangent,
	double scalarDatum,
	double tubeRadius,
	int segmentCount,
	MPointArray& vertices,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices )
{
	MStatus stat;

	// This is the way to build the mesh by hand
	//
	MVector zAxis( 0.0, 0.0, 1.0 );
	MQuaternion quaternion( zAxis, tangent );
	MVector vector;
	MVector rotatedVector;
	double length = tubeRadius + scalarDatum;

	// First, the vertices
	unsigned int vertexCount = vertices.length();
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		vector = envelopes::unitCircle[ iSegment ];
		vector = vector * length;
		rotatedVector = vector.rotateBy( quaternion );
		vertices.append( controlVertex + rotatedVector );
	}

	// Then the connectivity, as long as it's not the first CV
	if ( iCV > 0 )
	{
		int vertexIndexBase;

		// Connectivity for all but the last segment of the circle
		for ( int iSegment=0; iSegment < segmentCount-1; iSegment++ )
		{
			vertexIndexBase = vertexCount + iSegment;  // vertexCount == 0-based index of first vertex in new circle
			polygonVertexCounts.append(4);
			polygonVertexIndices.append( vertexIndexBase - segmentCount + 0 );
			polygonVertexIndices.append( vertexIndexBase                + 0 );
			polygonVertexIndices.append( vertexIndexBase                + 1 );
			polygonVertexIndices.append( vertexIndexBase - segmentCount + 1 );
		}

		// Connectivity for the last segment of the circle
		vertexIndexBase = vertexIndexBase + 1;
		polygonVertexCounts.append(4);
		polygonVertexIndices.append( vertexIndexBase - segmentCount + 0 );
		polygonVertexIndices.append( vertexIndexBase                + 0 );
		polygonVertexIndices.append( vertexIndexBase                + 1 - segmentCount );  // wraps around to beginning
		polygonVertexIndices.append( vertexIndexBase - segmentCount + 1 - segmentCount );  // of the circle
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// assignUVs()
*/
MStatus envelopes::assignUVs(
	MFnMesh& fnMesh,
	int segmentCount )
{
	MStatus stat;

	// U coordinate goes around the circle, equates to x coordinate of image
	MFloatArray uOffsets;
	uOffsets.append( 0.0 );
	uOffsets.append( 0.0 );
	uOffsets.append( (float)(1.0/segmentCount) );
	uOffsets.append( (float)(1.0/segmentCount) );

	// V coordinate goes from one circle to the next, equates to y coordinate of image
	MFloatArray vValues;
	vValues.append( 0.0 );
	vValues.append( 1.0 );
	vValues.append( 1.0 );
	vValues.append( 0.0 );

	int uvId=0;
	float uBase;
	float uValue, vValue;
	for (int iSegment=0; iSegment<segmentCount; iSegment++ )
	{
		uBase = ((float)iSegment) / ((float)segmentCount);
		for (int iVertex=0; iVertex<4; iVertex++)
		{
			uValue = uBase+uOffsets[iVertex];
			vValue = vValues[iVertex];
			diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue) );
			uvId++;
		}
	}

	int polygonCount = fnMesh.numPolygons( &stat );   diLOG_MSTATUS( stat );
	int circleCount = polygonCount / segmentCount;
	int iPolygon;
	for ( int iCircle=0; iCircle<circleCount; iCircle++ )
		for (int iSegment=0; iSegment<segmentCount; iSegment++ )
		{
			iPolygon = iCircle*segmentCount + iSegment;
			for (int iVertex=0; iVertex<4; iVertex++ )
			{
				uvId = iSegment*4 + iVertex;
				diLOG_MSTATUS( fnMesh.assignUV(iPolygon, iVertex, uvId) );
			}
		}

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the curves.
//
*/
MStatus envelopes::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> envelopes::compute()" );
	forceFieldCommon::logEntry( forceFieldCommon::eEnvelopes );

	if ( plug != aSecondaryMesh )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from envelopes::compute(), Maya not asking for secondary mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hCurveCount( block.inputValue(aCurveCount, &stat) );   diLOG_MSTATUS( stat );
	int curveCount = hCurveCount.asInt();

	MDataHandle hStartIndices( block.inputValue(aStartIndices, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnStartIndices( hStartIndices.data() );
	MIntArray startIndices( fnStartIndices.array() );

	MDataHandle hCVCounts( block.inputValue(aCVCounts, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnCVCounts( hCVCounts.data() );
	MIntArray CVCounts( fnCVCounts.array() );

	MDataHandle hControlVertexList( block.inputValue(aControlVertexList, &stat) );   diLOG_MSTATUS( stat );
	MFnPointArrayData fnControlVertexList( hControlVertexList.data() );
	MPointArray controlVertexList( fnControlVertexList.array() );

	MDataHandle hTangentList( block.inputValue(aTangentList, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnTangentList( hTangentList.data() );
	MVectorArray tangentList( fnTangentList.array() );

	MDataHandle hTubeRadius( block.inputValue(aTubeRadius, &stat) );   diLOG_MSTATUS( stat );
	double tubeRadius = hTubeRadius.asDouble();

	MDataHandle hSegmentCount( block.inputValue(aSegmentCount, &stat) );   diLOG_MSTATUS( stat );
	int segmentCount = hSegmentCount.asInt();

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hScalarDataGrid( block.inputValue(aScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnDoubleArrayData fnScalarDataGrid( hScalarDataGrid.data() );
	MDoubleArray scalarDataGrid( fnScalarDataGrid.array() );

	MDataHandle hMultiplier( block.inputValue(aMultiplier, &stat) );   diLOG_MSTATUS( stat );
	double multiplier = hMultiplier.asDouble();


	// Now do the output attributes.
	//
	MDataHandle hSecondaryMesh( block.outputValue(aSecondaryMesh, &stat) );   diLOG_MSTATUS( stat );


	// Create the data object which underlies the output mesh
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );

	// Unit circle
	unitCircle.clear();
	double angleFactor = (1.0/(double)segmentCount)*(2.0*3.1415927);
	double theta;
	double x, y, z = 0.0;
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		theta = (double)iSegment * angleFactor;
		x = cos( theta );
		y = sin( theta );
		unitCircle.append( x, y, z );
	}

	// Make the call to create the secondary mesh
	//forceFieldCommon::logEntry( (char*) "About to create mesh" );
	diLOG_MSTATUS(
		createMesh(
			curveCount,
			startIndices,
			CVCounts,
			controlVertexList,
			tangentList,
			segmentCount,
			tubeRadius,
			dims,
			gridMin,
			gridMax,
			scalarDataGrid,
			multiplier,
			newOutputData) );


	// Set the outputs as clean.
	hSecondaryMesh.set( newOutputData );
	block.setClean( plug );

	//forceFieldCommon::logEntry( (char*) "<-- envelopes::compute()" );
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information and the unit circle values
*/
MStatus envelopes::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> envelopes::initialize()" );


	// Create input attributes:
	//
	// curve count
	aCurveCount = nAttr.create( "curveCount", "curveCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveCount) );

	// start indices
	aStartIndices = tAttr.create( "startIndices", "startIndices", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aStartIndices) );

	// CV counts
	aCVCounts = tAttr.create( "CVCounts", "CVCounts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aCVCounts) );

	// control vertices
	aControlVertexList = tAttr.create( "controlVertexList", "controlVertexList", MFnData::kPointArray, fnPointArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aControlVertexList) );

	// tangents
	aTangentList = tAttr.create( "tangentList", "tangentList", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aTangentList) );

	// tube radius
	aTubeRadius = nAttr.create( "tubeRadius", "tubeRadius", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aTubeRadius) );

	// segment count
	aSegmentCount = nAttr.create( "segmentCount", "segmentCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(18) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aSegmentCount) );

	// grid dimensions
	aDims = nAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gridMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gridMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// scalar data grid
	aScalarDataGrid = tAttr.create( "scalarDataGrid", "scalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aScalarDataGrid) );

	// multiplier
	aMultiplier = nAttr.create( "multiplier", "multiplier", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aMultiplier) );


	// Create output attributes
	//
	// secondary mesh
	aSecondaryMesh = tAttr.create( "secondaryMesh", "secondaryMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aSecondaryMesh) );


	// All inputs affect all outputs
	//
	// secondary mesh
    diLOG_MSTATUS( attributeAffects(aCurveCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStartIndices, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCVCounts, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aControlVertexList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTangentList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTubeRadius, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aSegmentCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aDims, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aScalarDataGrid, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aMultiplier, aSecondaryMesh) );

	forceFieldCommon::logEntry( (char*) "<-- envelopes::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
// envelopes()
*/
envelopes::envelopes()
{
}


/* ============================================================================
// postConstructor()
*/
void envelopes::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType envelopes::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * envelopes::creator()
{
    return new envelopes();
}


/* ============================================================================
// ~envelopes()
*/
envelopes::~envelopes()
{
}


