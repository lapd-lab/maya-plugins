/* ============================================================================
// forceField : linTrans.cpp
//-
// ==========================================================================
// Copyright (C) 2012 Jim Bamber
//
// ==========================================================================
//+
*/
#include "linTrans.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MVector.h>


// Static data
MTypeId linTrans::id( 0x00462 );

// Attributes
MObject linTrans::aM;
MObject linTrans::aB;
MObject linTrans::aInputMode;
MObject linTrans::aInputScalar;
MObject linTrans::aInputRGB;
MObject linTrans::aSecondaryData;

MObject linTrans::aOutputScalar;
MObject linTrans::aOutputRGB;


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the UV-mapped tubes.
//
*/
MStatus linTrans::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> linTrans::compute()" );

	if ( (plug != aOutputScalar) && (plug != aOutputRGB) )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from linTrans::compute(), Maya not asking for outputScalar or outputRGB" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hM( block.inputValue(aM, &stat) );   diLOG_MSTATUS( stat );
	float m = hM.asFloat();

	MDataHandle hB( block.inputValue(aB, &stat) );   diLOG_MSTATUS( stat );
	float b = hB.asFloat();

	MDataHandle hInputMode( block.inputValue(aInputMode, &stat) );   diLOG_MSTATUS( stat );
	short inputMode = hInputMode.asShort();

	MDataHandle hInputScalar( block.inputValue(aInputScalar, &stat) );   diLOG_MSTATUS( stat );
	float inputScalar = hInputScalar.asFloat();

	MDataHandle hInputRGB( block.inputValue(aInputRGB, &stat) );   diLOG_MSTATUS( stat );
	MFloatVector inputRGB = hInputRGB.asFloatVector();

	MDataHandle hScaledDatum( block.inputValue(aSecondaryData, &stat) );   diLOG_MSTATUS( stat );
	float scaledDatum = hScaledDatum.asFloat();


	// Now do the output attributes.
	//
	MDataHandle hOutputScalar( block.outputValue(aOutputScalar, &stat) );   diLOG_MSTATUS( stat );
	float& outputScalar = hOutputScalar.asFloat();

	MDataHandle hOutputRGB( block.outputValue(aOutputRGB, &stat) );   diLOG_MSTATUS( stat );
	MFloatVector& outputRGB = hOutputRGB.asFloatVector();


	// Do the linear transformation
	//
	float output_scalar;
	MFloatVector output_RGB;

	if ( inputMode == 0 )
	{
		// scalar input mode
		output_scalar = m * scaledDatum * inputScalar + b;
		outputScalar = output_scalar;  // Not totally sure if this is necessary

		output_RGB.x = output_scalar;
		output_RGB.y = output_scalar;
		output_RGB.z = output_scalar;
		outputRGB = output_RGB;  // Not totally sure if this is necessary
	}

	if ( inputMode == 1 )
	{
		// RGB input mode
		float rescaledDatum = m * scaledDatum;
		output_RGB.x = rescaledDatum * inputRGB.x + b;
		output_RGB.y = rescaledDatum * inputRGB.y + b;
		output_RGB.z = rescaledDatum * inputRGB.z + b;
		outputRGB = output_RGB;  // Not totally sure if this is necessary

		float output_scalar = output_RGB.length();
		outputScalar = output_scalar;  // Not totally sure if this is necessary
	}


	// Set the outputs as clean.
	//block.setClean( plug );
	hOutputScalar.setClean();
	hOutputRGB.setClean();

	//forceFieldCommon::logEntry( (char*) "<-- linTrans::compute()" );
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information and the unit circle values
*/
MStatus linTrans::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> linTrans::initialize()" );


	// Create input attributes:
	//
	// m
	aM = nAttr.create( "m", "m", MFnNumericData::kFloat, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aM) );

	// b
	aB = nAttr.create( "b", "b", MFnNumericData::kFloat, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aB) );

	// inputMode
    aInputMode = enumAttr.create( "inputMode", "inputMode", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("scalar", 0) );
	diLOG_MSTATUS( enumAttr.addField("RGB", 1) );
	MAKE_INPUT( enumAttr );
	diLOG_MSTATUS( addAttribute(aInputMode) );

	// inputScalar
	aInputScalar = nAttr.create( "inputScalar", "inputScalar", MFnNumericData::kFloat, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aInputScalar) );

	// inputRGB
	aInputRGB = nAttr.createColor( "inputRGB", "inputRGB", &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0, 1.0, 1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aInputRGB) );

	// scaledDatum
	aSecondaryData = nAttr.create( "secondaryData", "secondaryData", MFnNumericData::kFloat, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aSecondaryData) );


	// Create output attributes
	//
	// outputScalar
	aOutputScalar = nAttr.create( "outputScalar", "outputScalar", MFnNumericData::kFloat, 0, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aOutputScalar) );

	// outputRGB
	aOutputRGB = nAttr.createColor( "outputRGB", "outputRGB", &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aOutputRGB) );


	// All inputs affect all outputs
	//
	// outputScalar
    diLOG_MSTATUS( attributeAffects(aM, aOutputScalar) );
    diLOG_MSTATUS( attributeAffects(aB, aOutputScalar) );
    diLOG_MSTATUS( attributeAffects(aInputMode, aOutputScalar) );
    diLOG_MSTATUS( attributeAffects(aInputScalar, aOutputScalar) );
    diLOG_MSTATUS( attributeAffects(aInputRGB, aOutputScalar) );
    diLOG_MSTATUS( attributeAffects(aSecondaryData, aOutputScalar) );

	// outputRGB
    diLOG_MSTATUS( attributeAffects(aM, aOutputRGB) );
    diLOG_MSTATUS( attributeAffects(aB, aOutputRGB) );
    diLOG_MSTATUS( attributeAffects(aInputMode, aOutputRGB) );
    diLOG_MSTATUS( attributeAffects(aInputScalar, aOutputRGB) );
    diLOG_MSTATUS( attributeAffects(aInputRGB, aOutputRGB) );
    diLOG_MSTATUS( attributeAffects(aSecondaryData, aOutputRGB) );

	forceFieldCommon::logEntry( (char*) "<-- linTrans::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
// linTrans()
*/
linTrans::linTrans()
{
}


/* ============================================================================
// postConstructor()
*/
void linTrans::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType linTrans::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * linTrans::creator()
{
    return new linTrans();
}


/* ============================================================================
// ~linTrans()
*/
linTrans::~linTrans()
{
}


