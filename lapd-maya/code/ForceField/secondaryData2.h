/* ============================================================================
// forceField : secondaryData2.h
//
// Copyright (C) 2012 Jim Bamber
//
// Cloned from C:\Program Files\Autodesk\Maya2011\devkit\plug-ins\lavaShader.cpp
//
//-
// ==========================================================================
// Copyright 1995,2006,2008 Autodesk, Inc. All rights reserved.
//
// Use of this software is subject to the terms of the Autodesk
// license agreement provided at the time of installation or download,
// or which otherwise accompanies this software in either electronic
// or hard copy form.
// ==========================================================================
//+
*/

#ifndef SECONDARYDATA2_H
#define SECONDARYDATA2_H

#include <math.h>
#include <stdlib.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatPoint.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MDoubleArray.h>
//#include <maya/MFnPlugin.h>


class secondaryData2 : public MPxNode
{
	public:
                    secondaryData2();
    virtual         ~secondaryData2();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );
	virtual void    postConstructor();

    static  void *  creator();
    static  MStatus initialize();

	//  Id tag for use with binary file format
    static  MTypeId id;

	private:

	void readHDF5VizFile();

	// Input attributes

	/*
	static MObject aColorBase;
    static MObject aColorFlame;
    static MObject aDeform;
    static MObject aWarp;
    static MObject aSpeed;
    static MObject aTurbulence;
    static MObject aPower;
    static MObject aFrame;
	*/
    static MObject aPointWorld;				// Hidden Maya input attribute: world position of point being currently rendered
    static MObject aPlaceMat;				// Hidden Maya input attribute: UV placement matrix, used to determine (u,v) from position

	static MObject aFilename;               // HDF5 viz file name
	static MObject aGroupName;              // Group name which contains scalar data array(s)

	static MObject aTimeIndex;				// Time index into scalar data array

	static MObject aParameter1Index;		// There could be multiple scalar data arrays, organized by parameters
	static MObject aParameter2Index;		//
	static MObject aParameter3Index;		//
	static MObject aParameter4Index;		//

											// No multiplier or offset because data gets scaled into 0 -> 1 range.
											//

	// Output attributes
    static MObject aScaledDatum;

	// Internal variables
										// Spatial grid is assumed to be uniform => is not needed as an input.
	int3 mGridDims;						// Dimensions of the spatial grid.
	float3 mGridMin;					// Minimum point of spatial grid.
	float3 mGridMax;					// Maximum point of spatial grid.
	MDoubleArray mScalarDataGrid;		// This is the original scalar data grid.
	MDoubleArray mScaledScalarDataGrid;	// This supplies the scalars for the UV values.
	float mDataMin;						// Minimum value of original scalar data grid.
	float mDataMax;						// Maximum value of original scalar data grid.

	MString mFilename; 
	MString mGroupName;

	int mTimeIndex;

	int mParameter1Index;
	int mParameter2Index;
	int mParameter3Index;
	int mParameter4Index;
};

#endif
