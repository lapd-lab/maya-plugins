/* ==========================================================================
// HDF5 : HDF5file.cpp
//
// Copyright (C) 2008 Jim Bamber 
// 
*/
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "HDF5common.h"
#include "errorHandling.h"
#include "HDF5file.h"

#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MArrayDataBuilder.h>
#include <maya/MGlobal.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnStringData.h>
#include <maya/MFnStringArrayData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatVector.h>

#include <H5public.h>
#include <H5Apublic.h>
#include <H5Fpublic.h>
#include <H5Gpublic.h>
#include <H5Ppublic.h>
#include <H5Spublic.h>
#include <H5Tpublic.h>

MTypeId HDF5file::id( 0x00341 );

MObject  HDF5file::aFilename;        
MObject  HDF5file::aScalarDataGroups;
MObject  HDF5file::aVectorDataGroups;


/* ==========================================================================
// compute()
*/
MStatus HDF5file::compute( const MPlug& plug, MDataBlock& data )
{
	//HDF5common::logEntry( (char*) "--> HDF5file::compute()" );
	HDF5common::logEntry( (char*) HDF5common::eHDF5file );
	MStatus stat;
 
	if ( ( plug == aScalarDataGroups ) || ( plug.parent() == aScalarDataGroups ) ||
		 ( plug == aVectorDataGroups ) || ( plug.parent() == aVectorDataGroups ) )
	{
		stat = computeKernel( plug, data );
		data.setClean( plug );
	} 
	else 
		stat  = MS::kUnknownParameter;

	//HDF5common::logEntry( (char*) "<-- HDF5file::compute()" );
	return stat;
}


/* ==========================================================================
// computeKernel()
*/
MStatus HDF5file::computeKernel( const MPlug& plug, MDataBlock& data )
{
	//HDF5common::logEntry( (char*) "--> HDF5file::computeKernel()" );
	MStatus stat;
 
	MDataHandle hScalarDataGroups( data.outputValue(aScalarDataGroups, &stat) );	  diLOG_MSTATUS( stat );
	MDataHandle hVectorDataGroups( data.outputValue(aVectorDataGroups, &stat) );	  diLOG_MSTATUS( stat );
	MDataHandle hFilename( data.inputValue(aFilename, &stat) );		  diLOG_MSTATUS( stat );

	// Open the file.
	//
	MString filename( hFilename.asString() );
	if ( filename == "" )
	{
		HDF5common::logEntry( (char*) "Abnormal exit from HDF5file::computeKernel(), filename is blank" );
		return MS::kSuccess;
	}
	const char *filenameAsChar = filename.asChar();
	cout << filenameAsChar << endl;
	hid_t file_id = H5Fopen( filenameAsChar, H5F_ACC_RDONLY, H5P_DEFAULT );	  diLOG_HDF5STAT( file_id );
	cout << "Just called H5FOpen" << endl;
	if ( file_id < 0 )
	{
		sprintf_s( HDF5common::logString, DI_LOG_STRING_LENGTH, "Could not open %s", filenameAsChar );
		HDF5common::logEntry( HDF5common::logString );
		return MS::kFailure;
	}

	MFnStringArrayData fnScalarDataGroups( hScalarDataGroups.data() );
	MFnStringArrayData fnVectorDataGroups( hVectorDataGroups.data() );
	MStringArray scalarDataGroups;
	MStringArray vectorDataGroups;
	hsize_t num_obj;
	char group_name[120];
	char data_type[120];
    diLOG_HDF5STAT( H5Gget_num_objs(file_id, &num_obj) );
	for ( hsize_t idx=0; idx<num_obj; idx++ )
	{
		int objtype = H5Gget_objtype_by_idx( file_id, idx );  diLOG_HDF5STAT( objtype );
		if ( objtype == H5G_GROUP )
		{
			diLOG_HDF5STAT( (int)H5Gget_objname_by_idx(file_id, idx, group_name, 120) );
			if ( MString(group_name) != "Attributes" )
			{
				hid_t group_id = H5Gopen2( file_id, group_name, H5P_DEFAULT );   diLOG_HDF5STAT( group_id );
				hid_t attr_id = H5Aopen_name( group_id, "Type" );   diLOG_HDF5STAT( attr_id );
				hid_t attr_type_id = H5Aget_type( attr_id );	 diLOG_HDF5STAT( attr_type_id );
				diLOG_HDF5STAT( H5Aread(attr_id, attr_type_id, data_type) );
				diLOG_HDF5STAT( H5Tclose(attr_type_id) );
				diLOG_HDF5STAT( H5Aclose(attr_id) );
				diLOG_HDF5STAT( H5Gclose(group_id) );
				if ( MString(data_type) == "Scalar" ) scalarDataGroups.append( MString(group_name) );
				if ( MString(data_type) == "3-vector" ) vectorDataGroups.append( MString(group_name) );
			}
		}
	}

	// Close the ids.
	//
	diLOG_HDF5STAT( H5Fclose(file_id) );

	// Set the outputs.
	//
	diLOG_MSTATUS( fnScalarDataGroups.set(scalarDataGroups) );
	diLOG_MSTATUS( fnVectorDataGroups.set(vectorDataGroups) );
	hScalarDataGroups.setClean();
	hVectorDataGroups.setClean();

	//HDF5common::logEntry( (char*) "<-- HDF5file::computeKernel()" );
	return MS::kSuccess;
}


/* ==========================================================================
// HDF5file()
*/
HDF5file::HDF5file()
{
}


/* ==========================================================================
// postConstructor()
*/
void HDF5file::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType HDF5file::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// ~HDF5file()
*/
HDF5file::~HDF5file()
{
}


/* ==========================================================================
// creator()
*/
void *HDF5file::creator()
{
	return new HDF5file();
}


/* ==========================================================================
// initialize()
*/
MStatus HDF5file::initialize()
{
	MStatus				 stat, stat2;
	MFnTypedAttribute	 typedAttr;
	MFnNumericAttribute	 numericAttr;
	MFnCompoundAttribute compAttr;
	MFnStringData		 stringData;
	MFnStringArrayData	 stringArrayData;
	MFnVectorArrayData	 vectorArrayData;		

	HDF5common::logEntry( (char*) "--> HDF5file::initialize()" );


    aFilename = typedAttr.create( "filename", "filename", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

	aScalarDataGroups = typedAttr.create( "scalarDataGroups", "scalarDataGroups", MFnData::kStringArray, stringArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

	aVectorDataGroups = typedAttr.create( "vectorDataGroups", "vectorDataGroups", MFnData::kStringArray, stringArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

	// Add the attributes to the node
	diLOG_MSTATUS( addAttribute(aFilename) );
	diLOG_MSTATUS( addAttribute(aScalarDataGroups) );
	diLOG_MSTATUS( addAttribute(aVectorDataGroups) );

	// Define the attribute dependencies
	diLOG_MSTATUS( attributeAffects(aFilename, aScalarDataGroups) );
	diLOG_MSTATUS( attributeAffects(aFilename, aVectorDataGroups) );

	HDF5common::logEntry( (char*) "<-- HDF5file::initialize()" );
	return MS::kSuccess;
}
