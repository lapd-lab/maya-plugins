/* ============================================================================
 * Contours : pluginMain.cpp
 /
 / Copyright (C) 2008 Jim Bamber
 /
 */
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "contours.h"
#include <maya/MFnPlugin.h>
#include <maya/MPxNode.h>
#include <maya/MGlobal.h>


/* ============================================================================
 * initializePlugin()
 */
MStatus initializePlugin( MObject obj )
{
    MFnPlugin plugin( obj, "Jim Bamber", "1.0", "Any");
	MGlobal::displayInfo( " " );
	MGlobal::displayInfo( "plugin: Contours  " + lapdCommon::version );
	MGlobal::displayInfo( "==============================" );

	MGlobal::displayInfo( "plugin.registerNode(contours)" );
	diLOG_MSTATUS ( plugin.registerNode( "contours", contours::id, 
                         &contours::creator, &contours::initialize ) );

    return MS::kSuccess;
}


/* ============================================================================
 * uninitializePlugin()
 */
MStatus uninitializePlugin( MObject obj )
{
    MFnPlugin plugin( obj );
	diLOG_MSTATUS ( plugin.deregisterNode( contours::id ) );

    return MS::kSuccess;
}


