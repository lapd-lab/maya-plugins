/* ==========================================================================
// HDF5 : pluginMain.cpp
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "errorHandling.h"
#include "HDF5spatialGrid.h"
#include "HDF5vectorDataGrid.h"
#include "HDF5scalarDataGrid.h"
#include "HDF5vectorShadingTable.h"
#include "HDF5file.h"
#include "HDF5textureFile.h"
//#include "HDF5fileCmd.h" -turns out to be better to use createNode commands
//#include "HDF5shadingTableCmd.h"  -not sure this is worth the effort
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>


/* ==========================================================================
// initializePlugin()
*/
MStatus initializePlugin( MObject obj )
{ 
	MFnPlugin plugin( obj, "Jim Bamber", "1.0", "Any");
	MGlobal::displayInfo( " " );
	MGlobal::displayInfo( "plugin: HDF5  " + lapdCommon::version );
	MGlobal::displayInfo( "==========================" );

	//MGlobal::displayInfo( "plugin.deregisterCommand()" );
	//diLOG_MSTATUS( plugin.deregisterCommand("HDF5file") );
	//MGlobal::displayInfo( "plugin.registerCommand()" );
	//diLOG_MSTATUS( plugin.registerCommand("HDF5file", HDF5fileCmd::creator, HDF5fileCmd::newSyntax) );

	////MGlobal::displayInfo( "plugin.deregisterCommand()" );
	////diLOG_MSTATUS( plugin.deregisterCommand("HDF5shadingTable") );
	//MGlobal::displayInfo( "plugin.registerCommand()" );
	//diLOG_MSTATUS( plugin.registerCommand("HDF5shadingTable", HDF5shadingTableCmd::creator, HDF5shadingTableCmd::newSyntax) );

	MGlobal::displayInfo( "plugin.registerNode(HDF5spatialGrid)" );
	diLOG_MSTATUS( plugin.registerNode(
	  "HDF5spatialGrid",
	  HDF5spatialGrid::id,
	  HDF5spatialGrid::creator,
	  HDF5spatialGrid::initialize) );

	MGlobal::displayInfo( "plugin.registerNode(HDF5vectorDataGrid)" );
	diLOG_MSTATUS( plugin.registerNode(
	  "HDF5vectorDataGrid",
	  HDF5vectorDataGrid::id,
	  HDF5vectorDataGrid::creator,
	  HDF5vectorDataGrid::initialize) );

	MGlobal::displayInfo( "plugin.registerNode(HDF5scalarDataGrid)" );
	diLOG_MSTATUS( plugin.registerNode(
	  "HDF5scalarDataGrid",
	  HDF5scalarDataGrid::id,
	  HDF5scalarDataGrid::creator,
	  HDF5scalarDataGrid::initialize) );

	MGlobal::displayInfo( "plugin.registerNode(HDF5vectorShadingTable)" );
	diLOG_MSTATUS( plugin.registerNode(
	  "HDF5vectorShadingTable",
	  HDF5vectorShadingTable::id,
	  HDF5vectorShadingTable::creator,
	  HDF5vectorShadingTable::initialize) );

	MGlobal::displayInfo( "plugin.registerNode(HDF5file)" );
	diLOG_MSTATUS( plugin.registerNode(
	  "HDF5file",
	  HDF5file::id,
	  HDF5file::creator,
	  HDF5file::initialize) );

	MGlobal::displayInfo( "plugin.registerNode(HDF5textureFile)" );
	diLOG_MSTATUS( plugin.registerNode(
	  "HDF5textureFile",
	  HDF5textureFile::id,
	  HDF5textureFile::creator,
	  HDF5textureFile::initialize) );

	return MS::kSuccess;
}


/* ==========================================================================
// uninitializePlugin()
*/
MStatus uninitializePlugin( MObject obj)
{
	MFnPlugin plugin( obj );

	//diLOG_MSTATUS( plugin.deregisterCommand("HDF5file") );
	//diLOG_MSTATUS( plugin.deregisterCommand("HDF5shadingTable") );
	diLOG_MSTATUS( plugin.deregisterNode(HDF5spatialGrid::id) );
	diLOG_MSTATUS( plugin.deregisterNode(HDF5vectorDataGrid::id) );
	diLOG_MSTATUS( plugin.deregisterNode(HDF5scalarDataGrid::id) );
	diLOG_MSTATUS( plugin.deregisterNode(HDF5vectorShadingTable::id) );
	diLOG_MSTATUS( plugin.deregisterNode(HDF5file::id) );
	diLOG_MSTATUS( plugin.deregisterNode(HDF5textureFile::id) );

	return MS::kSuccess;
}
