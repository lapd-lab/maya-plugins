/* ============================================================================
// forceField : circles.h
//
// Copyright (C) 2007 Jim Bamber 
//
*/
#ifndef CIRCLES_H
#define CIRCLES_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class circles : public MPxNode
{
    public:

    circles();
	virtual void postConstructor();
    virtual ~circles();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	static MPointArray unitCircle;

    static MStatus createMesh(
		int curveCount,
		MIntArray startIndices,
		MIntArray CVCounts,
		MPointArray controlVertexList,
		MVectorArray tangentList,
		int circleType,
		int segmentCount,
		double tubeRadius,
		int start,
		int step,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MDoubleArray& scalarDataGrid,
		double multiplier,
		MObject& newOutputData );

	static MStatus addCircle(
		//MFnMesh& fnMesh,
		//MObject& newOutputData,
		MPoint controlVertex,
		MVector tangent,
		double scalarDatum,
		double tubeRadius,
		int segmentCount,
		MPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );

	static MStatus assignUVs(
		MFnMesh& fnMesh,
		MObject& newOutputData,
		int segmentCount );

	// Input attributes
	static MObject aCurveCount;			// Number of curves in the output array.
	static MObject aStartIndices;		// Used to index into the control vertex and tangent arrays (int array).
	static MObject aCVCounts;			// Number of CV's for each curve (int array), for example:
										//   index = startIndices[curveNum] + cvIndex, where cvIndex = {0:CVCounts[curveNum]-1}
	static MObject aControlVertexList;	// Control vertices for curves (point array).
	static MObject aTangentList;		// Tangents for curves (vector array).
	static MObject aTubeRadius;			// Radius of field line tubes in cm (double).
	static MObject aStart;				// Start CV for placement of circles (int).
	static MObject aStep;				// Size of step through CV's for placement of circles (int).
	static MObject aCircleType;			// Enum of circle types: 0 -> 2D full disks.
	static MObject aSegmentCount;		// Number of segments in the circle.

										// Spatial grid is assumed to be uniform => is not used as an input.
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aScalarDataGrid;		// This supplies the scalars for the secondary mesh (scalar array).
	static MObject aMultiplier;			// A convenience input to scale up the secondary data (double).

	// Output attributes
	static MObject aSecondaryMesh;		// The newly created mesh.
};


#endif

