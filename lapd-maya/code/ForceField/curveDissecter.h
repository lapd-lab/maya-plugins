/* ============================================================================
// forceField : curveDissecter.h
//
// Copyright (C) 2007 Jim Bamber 
//
*/
#ifndef CURVEDISSECTER_H
#define CURVEDISSECTER_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>


class curveDissecter : public MPxNode
{
    public:

    curveDissecter();
	virtual void postConstructor();
    virtual ~curveDissecter();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	// Input attributes
	static MObject aStartIndices;		// Used to index into the control vertex and tangent arrays (int array).
	static MObject aCVCounts;			// Number of CV's for each curve (int array), for example:
										//   index = startIndices[curveNum] + cvIndex, where cvIndex = {0:CVCounts[curveNum]-1}
	static MObject aControlVertexList;	// Control vertices for curves (point array).
	static MObject aTangentList;		// Tangents for curves (vector array).
	static MObject aCurveIndex;			// Index of the selected curve (int).
	static MObject aCVIndex;			// Index of the selected CV (and tangent) of the selected curve (int).

	// Output attributes
	static MObject aCVCount;			// Number of CV's in the selected curve (int).
    static MObject aCV;					// The selected CV (vector).
    static MObject aTangent;			// The selected tangent (vector).
};


#endif

