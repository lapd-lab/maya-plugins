/* ============================================================================
// Isosurface : resample.h
//
// Copyright (C) 2008 Jim Bamber 
//
*/
#ifndef RESAMPLE_H
#define RESAMPLE_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>


class resample : public MPxNode
{
    public:

    resample();
	virtual void postConstructor();
    virtual ~resample();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MStatus doResampling(
		float3& resamplingFactors,
		double halfWindow,
		int3& inDims,
		float3& gridMin,
		float3& gridMax,
		MDoubleArray& inScalarDataGrid,
		int3& outDims,
		MDoubleArray& outScalarDataGrid );

	// Input attributes
	static MObject aResamplingFactors;	// The number of grid points in each direction will be increased/decreased
										// by these factors (float3).
	static MObject aHalfWindow;			// Half-window for interpolation in grid steps (double).
	static MObject aInDims;				// Dimensions of the input spatial grid (int3).
	static MObject aGridMin;			// Minimum point of input spatial grid (float3).
	static MObject aGridMax;			// Maximum point of input spatial grid (float3).
	static MObject aInScalarDataGrid;	// Input scalar data grid (scalar array).

	// Output attributes
	static MObject aOutDims;			// Resampled dimensions (int3).
	static MObject aOutScalarDataGrid;	// Resampled scalar data grid (scalar array).

};


#endif

