/* ==========================================================================
// Texture 3D : defaultVectorShadingTable.cpp
//-
// ==========================================================================
// Copyright (C) Jim Bamber
//
// ==========================================================================
//+
*/
#include "defaultVectorShadingTable.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "texture3Dcommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFloatVector.h>
//#include <maya/MFnPlugin.h> -- this turns the piece of code into the plug-in main
#include <maya/MFnVectorArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>


// Static data
MTypeId defaultVectorShadingTable::id( 0x00402 );

// Attributes
MObject defaultVectorShadingTable::vectorCount;
MObject defaultVectorShadingTable::startIndex;
MObject defaultVectorShadingTable::endIndex;
MObject defaultVectorShadingTable::startValue;
MObject defaultVectorShadingTable::endValue;
 
MObject defaultVectorShadingTable::vectorShadingTable;


/* ==========================================================================
// postConstructor()
*/
void defaultVectorShadingTable::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType defaultVectorShadingTable::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// defaultVectorShadingTable()
*/
defaultVectorShadingTable::defaultVectorShadingTable()
{
}


/* ==========================================================================
// ~defaultVectorShadingTable()
*/
defaultVectorShadingTable::~defaultVectorShadingTable()
{
}


/* ==========================================================================
// creator()
//
// creates an instance of the node
*/
void * defaultVectorShadingTable::creator()
{
    return new defaultVectorShadingTable();
}


/* ==========================================================================
// initialize()
//
// initializes attribute information
*/
MStatus defaultVectorShadingTable::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnVectorArrayData vectorArrayData;		
	MStatus stat, stat2;

	texture3Dcommon::logEntry( (char*) "--> defaultVectorShadingTable::initialize()" );


	// Create input attributes:
	//
	// vectorCount = 256
	vectorCount = nAttr.create( "vectorCount", "nVec", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( nAttr.setDefault(256) );

	// startIndex = 0
	startIndex = nAttr.create( "startIndex", "iStart", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );

	// endIndex = 153
	endIndex = nAttr.create( "endIndex", "iEnd", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( nAttr.setDefault(153) );

	// startValue = (0.6, 0.6, 0.6)
    startValue = nAttr.create( "startValue", "vStart", MFnNumericData::k3Float, 0.0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( nAttr.setDefault(0.6, 0.6, 0.6) );

	// endValue = (0.0, 0.0, 0.0)
    endValue = nAttr.create( "endValue", "vEnd", MFnNumericData::k3Float, 0.0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );


	// Create output attributes
	//
	// vectorShadingTable
	vectorShadingTable = tAttr.create( "vectorShadingTable", "table", MFnData::kVectorArray, vectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_OUTPUT( tAttr );


    // Add the attributes
	//
    diLOG_MSTATUS( addAttribute(vectorCount) );
    diLOG_MSTATUS( addAttribute(startIndex) );
    diLOG_MSTATUS( addAttribute(endIndex) );
    diLOG_MSTATUS( addAttribute(startValue) );
    diLOG_MSTATUS( addAttribute(endValue) );

	diLOG_MSTATUS( addAttribute(vectorShadingTable) );


    // All input affect the output color and alpha
	//
    diLOG_MSTATUS( attributeAffects(vectorCount, vectorShadingTable) );
    diLOG_MSTATUS( attributeAffects(startIndex, vectorShadingTable) );
    diLOG_MSTATUS( attributeAffects(endIndex, vectorShadingTable) );
    diLOG_MSTATUS( attributeAffects(startValue, vectorShadingTable) );
    diLOG_MSTATUS( attributeAffects(endValue, vectorShadingTable) );

	texture3Dcommon::logEntry( (char*) "<-- defaultVectorShadingTable::initialize()" );
    return MS::kSuccess;
}


/* ==========================================================================
// calculateTable()
*/
MVectorArray defaultVectorShadingTable::calculateTable(
	int mVectorCount,
	int mStartIndex,
	int mEndIndex,
	MVector& mStartValue,
	MVector& mEndValue )
{
	// start at "startValue" vector at "startIndex", go to "endValue" at "endIndex" using 0.5*(1-cos) transition
	MVectorArray vectorTable;
	MVector vector;
	float xValue, yValue, zValue;
	float nTrans = (float)(mEndIndex - mStartIndex);
	float xRange = (float)(mEndValue.x - mStartValue.x);
	float yRange = (float)(mEndValue.y - mStartValue.y);
	float zRange = (float)(mEndValue.z - mStartValue.z);
	float theta;
	float factor;

	for ( int i=0; i<mVectorCount; i++ )
	{
		if ( i < mStartIndex ) vector = mStartValue;
		if ( i > mEndIndex )   vector = mEndValue;
		if ( (i >= mStartIndex) && (i <= mEndIndex) )
		{
			theta = (float)(3.1415927 * ( (i-mStartIndex) / nTrans ));
			factor = (float)(0.5 * (1.0-cos(theta)));
			xValue = (float)(mStartValue.x + factor*xRange);
			yValue = (float)(mStartValue.y + factor*yRange);
			zValue = (float)(mStartValue.z + factor*zRange);
			vector = MVector( xValue, yValue, zValue );
		}
		vectorTable.append( vector );
	}

	return vectorTable;
}


/* ==========================================================================
// compute()
//
// This function gets called by Maya to evaluate the texture.
//
*/
MStatus defaultVectorShadingTable::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//texture3Dcommon::logEntry( (char*) "--> defaultVectorShadingTable::compute()" );
	texture3Dcommon::logEntry( texture3Dcommon::eDefaultVectorShadingTable );

	// looking for vectorShadingTable
    if ( (plug != vectorShadingTable) && (plug.parent() != vectorShadingTable) )
	{
		texture3Dcommon::logEntry(
			(char*) "Abnormal exit from defaultVectorShadingTable::compute(), Maya not asking for vector shading table" );
		return MS::kUnknownParameter;
	}

	MDataHandle vectorCountHandle( block.inputValue(vectorCount, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle startIndexHandle( block.inputValue(startIndex, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle endIndexHandle( block.inputValue(endIndex, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle startValueHandle( block.inputValue(startValue, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle endValueHandle( block.inputValue(endValue, &stat) );  diLOG_MSTATUS( stat );

	int mVectorCount			= vectorCountHandle.asInt();
	int mStartIndex				= startIndexHandle.asInt();
	int mEndIndex				= endIndexHandle.asInt();
	float3& startValueAsFloat3	= startValueHandle.asFloat3();
	float3& endValueAsFloat3	= endValueHandle.asFloat3();

	double xx = startValueAsFloat3[0];
	double yy = startValueAsFloat3[1];
	double zz = startValueAsFloat3[2];
	MVector mStartValue = MVector( xx, yy, zz );

	xx = endValueAsFloat3[0];
	yy = endValueAsFloat3[1];
	zz = endValueAsFloat3[2];
	MVector mEndValue( xx, yy, zz );

	//texture3Dcommon::logEntry( (char*) "About to calculate table" );
	MVectorArray resultVectorTable;
	resultVectorTable = calculateTable( mVectorCount, mStartIndex, mEndIndex, mStartValue, mEndValue );

	// Set output vector table attribute
	MDataHandle vectorShadingTableHandle( block.outputValue(vectorShadingTable) );
	MFnVectorArrayData mVectorShadingTable( vectorShadingTableHandle.data() );
	diLOG_MSTATUS( mVectorShadingTable.set(resultVectorTable) );
	vectorShadingTableHandle.setClean();

	//texture3Dcommon::logEntry( (char*) "<-- defaultVectorShadingTable::compute()" );
    return MS::kSuccess;
}
