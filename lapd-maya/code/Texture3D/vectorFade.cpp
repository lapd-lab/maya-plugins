/* ==========================================================================
// Texture 3D : vectorFade.cpp
//-
// ==========================================================================
// Copyright (C) Jim Bamber
//
// ==========================================================================
//+
*/
#include "vectorFade.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "texture3Dcommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>


// Static data
MTypeId vectorFade::id( 0x00403 );

// Attributes
MObject vectorFade::fade;
MObject vectorFade::vectorBackground;
MObject vectorFade::vectorForeground;
 
MObject vectorFade::vectorShade;


/* ==========================================================================
// postConstructor()
*/
void vectorFade::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType vectorFade::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// vectorFade()
*/
vectorFade::vectorFade()
{
}


/* ==========================================================================
// ~vectorFade()
*/
vectorFade::~vectorFade()
{
}


/* ==========================================================================
// creator()
//
// creates an instance of the node
*/
void * vectorFade::creator()
{
    return new vectorFade();
}


/* ==========================================================================
// initialize()
//
// initializes attribute information
*/
MStatus vectorFade::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnVectorArrayData vectorArrayData;		
	MStatus stat, stat2;

	texture3Dcommon::logEntry( (char*) "--> vectorFade::initialize()" );


	// Create input attributes:
	//
	// fade
	fade = nAttr.create( "fade", "fade", MFnNumericData::k3Float, 0.0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );

	// vectorBackground
	vectorBackground = nAttr.createColor( "vectorBackground", "background", &stat ); diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );

	// vectorForeground
	vectorForeground = nAttr.createColor( "vectorForeground", "foreground", &stat ); diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );


    // Create output attributes
	//
	// vectorShade
    vectorShade = nAttr.createColor( "vectorShade", "out", &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( nAttr );


    // Add the attributes
	//
    diLOG_MSTATUS( addAttribute(fade) );
    diLOG_MSTATUS( addAttribute(vectorBackground) );
	diLOG_MSTATUS( addAttribute(vectorForeground) );

    diLOG_MSTATUS( addAttribute(vectorShade) );


    // All input affect the output color and alpha
	//
    diLOG_MSTATUS( attributeAffects(fade, vectorShade) );
    diLOG_MSTATUS( attributeAffects(vectorBackground, vectorShade) );
    diLOG_MSTATUS( attributeAffects(vectorForeground, vectorShade) );

	texture3Dcommon::logEntry( (char*) "<-- vectorFade::initialize()" );
    return MS::kSuccess;
}


/* ==========================================================================
// calculateFade()
*/
MFloatVector vectorFade::calculateFade(
	MFloatVector mFade,
	MFloatVector mVectorBackground,
	MFloatVector mVectorForeground )
{
	float mFadeX = mFade.x;
	float mFadeY = mFade.y;
	float mFadeZ = mFade.z;

	if ( mFadeX < 0.0 ) mFadeX = 0.0;
	if ( mFadeX > 1.0 ) mFadeX = 1.0;
	if ( mFadeY < 0.0 ) mFadeY = 0.0;
	if ( mFadeY > 1.0 ) mFadeY = 1.0;
	if ( mFadeZ < 0.0 ) mFadeZ = 0.0;
	if ( mFadeZ > 1.0 ) mFadeZ = 1.0;

	float resultX = (float)(mFadeX*mVectorBackground.x + (1.0-mFadeX)*mVectorForeground.x);
	float resultY = (float)(mFadeY*mVectorBackground.y + (1.0-mFadeY)*mVectorForeground.y);
	float resultZ = (float)(mFadeZ*mVectorBackground.z + (1.0-mFadeZ)*mVectorForeground.z);

	MFloatVector resultVector( resultX, resultY, resultZ );

	return resultVector;
}


/* ==========================================================================
// compute()
//
// This function gets called by Maya to evaluate the texture.
//
*/
MStatus vectorFade::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	texture3Dcommon::logEntry( texture3Dcommon::eVectorFade );

	// looking for vectorShade
    if ( (plug != vectorShade) && (plug.parent() != vectorShade) )
	{
		texture3Dcommon::logEntry(
			(char*) "Abnormal exit from vectorFade::compute(), Maya not asking for vector shade" );
		return MS::kUnknownParameter;
	}

	MDataHandle fadeHandle( block.inputValue(fade, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle vectorBackgroundHandle( block.inputValue(vectorBackground, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle vectorForegroundHandle( block.inputValue(vectorForeground, &stat) );  diLOG_MSTATUS( stat );

	MFloatVector& mFade( fadeHandle.asFloatVector() );
	MFloatVector& mVectorBackground( vectorBackgroundHandle.asFloatVector() );
	MFloatVector& mVectorForeground( vectorForegroundHandle.asFloatVector() );

	//texture3Dcommon::logEntry( (char*) "About to calculate fade" );
	MFloatVector resultVectorShade;
	resultVectorShade = calculateFade( mFade, mVectorBackground, mVectorForeground );

	// Set output color attribute
	MDataHandle vectorShadeHandle( block.outputValue(vectorShade) );
	MFloatVector& mVectorShade( vectorShadeHandle.asFloatVector() );
	mVectorShade = resultVectorShade;
	vectorShadeHandle.setClean();

	//texture3Dcommon::logEntry( (char*) "<-- vectorFade::compute()" );
    return MS::kSuccess;
}
