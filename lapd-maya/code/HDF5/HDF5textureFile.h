/* ==========================================================================
// HDF5 : HDF5textureFile.h
//
// Copyright (C) 2008 Jim Bamber 
// 
*/
#ifndef HDF5TEXTUREFILE_H
#define HDF5TEXTUREFILE_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h> 

class HDF5textureFile : public MPxNode
{
public:
    MPxNode::SchedulingType schedulingType() const;

	virtual MStatus compute( const MPlug& plug, MDataBlock& data );

    HDF5textureFile();
	virtual void postConstructor();
    virtual ~HDF5textureFile();

	static  void *creator();
	static  MStatus initialize();

public:
	static MObject aFilename;			// List texture groups in this HDF5 texture file (string)
	static MObject aTextureGroup;		// List texture tables in this texture group (string)

	static MObject aTextureGroups;		// Array of texture group names for the given filename (string array)
	static MObject aTextureTables;		// Array of texture table names for the given group name (string array)

	static	MTypeId id;

private:
	MStatus computeKernel( const MPlug& plug, MDataBlock& data );

};

#endif
