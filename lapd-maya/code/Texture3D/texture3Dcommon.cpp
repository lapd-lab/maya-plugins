/* ==========================================================================
// Texture 3D : texture3Dcommon.cpp
//-
// ==========================================================================
// Copyright (C) 2011 Jim Bamber
//
// ==========================================================================
//+
*/
#include "texture3Dcommon.h"

#include <math.h>
#include <stdlib.h>
#include <time.h>
//#include <windows.h>

#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MDoubleArray.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>

char texture3Dcommon::logFilename[DI_LOG_FILENAME_LENGTH] = "";
FILE* texture3Dcommon::logFile = NULL;
char texture3Dcommon::logString[DI_LOG_STRING_LENGTH] = "";
int texture3Dcommon::nodeCount = 5;
int texture3Dcommon::callCounters[5] = { 0, 0, 0, 0, 0 };
char* texture3Dcommon::nodeNameStrings[5] = 
{
	(char*) "Other",
    (char*) "DefaultVectorShadingTable",
    (char*) "ScalarDataInterpolation",
    (char*) "VectorFade",
    (char*) "VectorShadeLookup"
};
int texture3Dcommon::currentDay = 0;
time_t texture3Dcommon::currentRawTime = 0;
int texture3Dcommon::logTimestep = 1;  // seconds


/* ==========================================================================
// logEntry()
*/
void texture3Dcommon::logEntry(
	char* logString )
{
	/*
	// Check logging flag
	//  -if logging is not "On", return.
	//
	errno_t err;
	char* mayaLogging = NULL;
	err = _dupenv_s( &mayaLogging, NULL, "MAYA_LOGGING" );  if ( err ) return;  if ( mayaLogging == NULL ) return;
	if ( strcmp(mayaLogging, "On") != 0 )  { free( mayaLogging ); return; }
	else free( mayaLogging );


	// Get the current date and time
	//
	time_t rawtime;
	time ( &rawtime );
	struct tm timeinfo;
	err = localtime_s ( &timeinfo, &rawtime );  if ( err ) return;

	int year = (timeinfo).tm_year+1900;
	int month = (timeinfo).tm_mon+1;
	int day = (timeinfo).tm_mday;
	int hour = (timeinfo).tm_hour;
	int min = (timeinfo).tm_min;
	int sec = (timeinfo).tm_sec;


	// If logFilename is blank or day has changed or file handle is NULL, try to open new logfile
	//
	if ( (strcmp(logFilename, "") == 0) || (currentDay != day) || (logFile == NULL) )
	{
		currentDay = day;
		char* mayaLoggingHome = NULL;
		err = _dupenv_s( &mayaLoggingHome, NULL, "MAYA_LOGGING_HOME" );  if ( err ) return;  if ( mayaLoggingHome == NULL ) return;

		// Make a new folder for the day
		char folderName[1000];
		sprintf_s( folderName, 1000, "%s\\%4d-%02d-%02d", mayaLoggingHome, year, month, day );
		CreateDirectory(folderName, NULL);  // no error checking here, it is handled by checking logFile != NULL below

		// Now open the new logfile
		sprintf_s( logFilename, DI_LOG_FILENAME_LENGTH, "%s\\Texture 3D log %4d-%02d-%02d.txt", folderName, year, month, day );
		if ( logFile != NULL ) fclose( logFile );
		err = fopen_s( &logFile, logFilename, "a" );  if ( err ) return;

		// Do not delete this cout as Windows may need the delay in order to get its act together
		cout << hour << ":" << min << ":" << sec << "  texture3Dcommon::logEntry() opening " << logFilename << endl;

		if ( logFile != NULL )
		{
			fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
			fprintf( logFile, "================================\n" );
			fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
			fprintf( logFile, "Texture 3D log %4d-%02d-%02d opened\n", year, month, day );
			fflush( logFile );
		}
		else
		{
			cout << "Failed to open " << logFilename << endl;
		}
	}


	// Write out the log string
	//
	//cout << logString << endl;
	if ( logFile != NULL )
	{
		fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
		fprintf( logFile, "%s\n", logString );
		fflush( logFile );
		currentRawTime = rawtime;
	}
	*/
}


/* ==========================================================================
// logEntry()
*/
void texture3Dcommon::logEntry(
	nodeNameEnum nodeName )
{
	/*
	// Check logging flag
	//  -if logging is not "On", return.
	//
	errno_t err;
	char* mayaLogging = NULL;
	err = _dupenv_s( &mayaLogging, NULL, "MAYA_LOGGING" );  if ( err ) return;  if ( mayaLogging == NULL ) return;
	if ( strcmp(mayaLogging, "On") != 0 )  { free( mayaLogging ); return; }
	else free( mayaLogging );


	// Check node name.
	//
	if ( nodeName == eOther )
	{
		// -do nothing, the nodeName should always refer to an actual node
	}
	else
	{
		// -write out non-zero call counts for all nodes once per logTimestep
		// -first increment the appropriate call counter
        callCounters[nodeName]++;

		time_t rawTime;
		time ( &rawTime );
		if ( (rawTime-currentRawTime) >= logTimestep )
		{
			// -logTimestep has passed, form the string, write it out, then reset counters
			sprintf_s( logString, DI_LOG_STRING_LENGTH, "" );
			for ( int i=1; i<nodeCount; i++ )
				if ( callCounters[i] > 0 )
				{
					sprintf_s( logString, DI_LOG_STRING_LENGTH, "%s%s: %d  ", logString, nodeNameStrings[i], callCounters[i] );
					callCounters[i] = 0;
				}

			logEntry( logString );
		}
	}
	*/
}

