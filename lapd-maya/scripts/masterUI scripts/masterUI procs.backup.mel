//=================================================================================================
// Master UI procs 2008-02-12
//
// These are the procs which handle reloading scene data into the window and other global actions.
//


//=================================================================================================
// mConvertSceneData
//

global proc mConvertSceneData()
{
  print "mConvertSceneData()\n";

  // 0) [First, see if any old-style visualization datasets exist.]  No, first see if mMasterGlobals
  //    exists.  If not, set up the general stuff first, then do the field lines and cut planes separately.
  //
  //if ( (`objExists flDataGrid`) || (`objExists dtDataGrid`) )
  if ( (`objExists mMasterGlobals`) == 0 )
  {
    // This means that all the master UI sets needs to be created.  These sets are used to navigate through
    // the nodes, etc.
    playbackOptions -e -minTime 0;
    currentTime 0;
    createNode unknown -name mMasterGlobals;
    addAttr -longName "datasetCounter" -attributeType "long";
    addAttr -longName "frameOffset" -attributeType "long";   // this gets determined below
    addAttr -longName "selectedDataset" -dataType "string";
    addAttr -longName "selectedVizObject" -dataType "string";
    setAttr mMasterGlobals.datasetCounter 0;

    sets -empty -name mDatasetsSet;

    // Pick out frame offset from dtFrameExpression, if it exists.  Name change occurs below,
    // when dealing with old-style cut planes
    int $frameOffset = 0;
    if ( `objExists dtFrameExpression` )
    {
      string $frameExpression = `expression -q -s dtFrameExpression`;
      string $tokens[];
      tokenize $frameExpression $tokens;
      int $frameOffset = $tokens[4];
    }
    setAttr mMasterGlobals.frameOffset $frameOffset;
  }


  // 1) Look for old-style field lines dataset and convert (separate tubes and markers)
  //
  if ( `objExists flDataGrid` )
  {
    print "Old style field lines dataset detected...\n";

    setAttr mMasterGlobals.datasetCounter 1;

    rename flDataGrid mDataset1;
    rename flSpatialGrid mSpatialGrid1;
    createNode unknown -name dataset1;
    sets -add mDatasetsSet dataset1;
    setAttr mMasterGlobals.selectedDataset -type "string" "dataset1";

    // Add some convenience attributes to dataset1
    select -r dataset1;
    addAttr -longName "datasetType" -dataType "string";
    addAttr -longName "objectNumber" -attributeType "long";
    addAttr -longName "prefix" -dataType "string";
    addAttr -longName "shortFilename" -dataType "string";
    addAttr -longName "displayName" -dataType "string";
    addAttr -longName "fieldLinesCounter" -attributeType "long";
    //addAttr -longName "markersCounter" -attributeType "long";   // Markers should appear under field lines 
    // No, do not add furCounter here because fur belongs under field lines

    setAttr dataset1.datasetType -type "string" "vector";
    setAttr dataset1.objectNumber 1;
    setAttr dataset1.prefix -type "string" "ds1";

    string $filename = `getAttr mDataset1.filename`;
    string $tokens[];
    int $numTokens = `tokenize $filename "/\\" $tokens`;
    string $shortFilename = $tokens[$numTokens-1];
    setAttr dataset1.shortFilename -type "string" $shortFilename;

    string $group = `getAttr mDataset1.group`;
    string $displayName = "dataset1) " + $shortFilename + " : " + $group;
    setAttr dataset1.displayName -type "string" $displayName;

    setAttr dataset1.fieldLinesCounter 1;

    // Now a node (and the containing set) for the new field lines object
    sets -empty -name ds1VizObjectsSet;
    createNode unknown -name ds1FieldLines1;
    addAttr -longName "objectType" -dataType "string";
    addAttr -longName "objectNumber" -attributeType "long";
    addAttr -longName "parent" -dataType "string";
    addAttr -longName "visible" -attributeType "long";
    addAttr -longName "emitter" -dataType "string";
    addAttr -longName "emitterType" -dataType "string";
    addAttr -longName "hasMarkers" -attributeType "long";
    addAttr -longName "shaderInfo" -dataType "string";

    sets -add ds1VizObjectsSet ds1FieldLines1;
    setAttr mMasterGlobals.selectedVizObject -type "string" "ds1FieldLines1";

    setAttr ds1FieldLines1.objectType -type "string" "mFieldLines";
    setAttr ds1FieldLines1.objectNumber 1;
    setAttr ds1FieldLines1.parent -type "string" "dataset1";
    setAttr ds1FieldLines1.visible 1;
    setAttr ds1FieldLines1.hasMarkers 0;

    rename flCurves ds1FieldLines1FwdCurves;
    rename flBackCurves ds1FieldLines1BakCurves;

    // Figure out the emitter
    string $connections[] = `listConnections ds1FieldLines1FwdCurves.inputMesh`;
    //rename ($connections[0]) ds1FieldLines1Emitter;
    string $emitter = $connections[0];   // This turns out to be the transform.
    setAttr ds1FieldLines1.emitter -type "string" $emitter;
    string $emitterType = "custom";
    string $relatives[] = `listRelatives $emitter`;
    //string $emitterShape = $emitter + "Shape";
    string $emitterShape = $relatives[0];
    $connections = `listConnections -destination false $emitterShape`;
    string $connection = $connections[0];
    if ( $connection != "" )
    {
      string $objType = `objectType $connection`;
      if ( $objType == "polyPlane" ) $emitterType = $objType;
      // Anything but "polyPlane", in this context, is considered custom.
    }
    setAttr ds1FieldLines1.emitterType -type "string" $emitterType;

    // Turn off shading of the emitter grid
    string $shadingGroups[] = `listConnections -type shadingEngine $emitterShape`;
    string $shadingGroup = $shadingGroups[0];
    sets -remove $shadingGroup $emitterShape;

    // Bounding box
    if ( `objExists flBoundingFace1` ) delete flBoundingFace1;
    if ( `objExists flBoundingFace2` ) delete flBoundingFace2;
    if ( `objExists flBoundingFace3` ) delete flBoundingFace3;
    if ( `objExists flBoundingFace4` ) delete flBoundingFace4;

    float $min[] = `getAttr mSpatialGrid1.min`;
    float $max[] = `getAttr mSpatialGrid1.max`;

    float $scaleX = $max[0] - $min[0];
    float $scaleY = $max[1] - $min[1];
    float $scaleZ = $max[2] - $min[2];

    float $centerX = 0.5 * ($max[0]+$min[0]);
    float $centerY = 0.5 * ($max[1]+$min[1]);
    float $centerZ = 0.5 * ($max[2]+$min[2]);

    polyCube -name ds1BoundingBox;
    scale -a $scaleX $scaleY $scaleZ;
    move -a $centerX $centerY $centerZ;

    // Turn off shading of the bounding box
    string $shadingGroups[] = `listConnections -type shadingEngine ds1BoundingBoxShape`;
    string $shadingGroup = $shadingGroups[0];
    sets -remove $shadingGroup ds1BoundingBoxShape;

//    // Delete the old lines
//    print "Deleting lines...\n";
//    if ( `objExists flBaseCurve` ) delete flBaseCurve;
//    if ( `objExists flBaseCurveShape` ) delete flBaseCurveShape;
//    if ( `objExists flOffsetCurve` ) delete flOffsetCurve;
//    if ( `objExists flCurveVarGroup` ) delete flCurveVarGroup;
//    if ( `objExists flDisplayCurve` ) delete flDisplayCurve;
//    if ( `objExists flDisplayCurveShape` ) delete flDisplayCurveShape;
//
//    if ( `objExists flBaseBackCurve` ) delete flBaseBackCurve;
//    if ( `objExists flBaseBackCurveShape` ) delete flBaseBackCurveShape;
//    if ( `objExists flOffsetBackCurve` ) delete flOffsetBackCurve;
//    if ( `objExists flBackCurveVarGroup` ) delete flBackCurveVarGroup;
//    if ( `objExists flDisplayBackCurve` ) delete flDisplayBackCurve;
//    if ( `objExists flDisplayBackCurveShape` ) delete flDisplayBackCurveShape;

    // Rename the old lines
    print "Renaming lines...\n";
    rename flCurveVarGroup ds1FieldLines1FwdVarGroup;
    rename flBackCurveVarGroup ds1FieldLines1BakVarGroup;

    int $fwdLineCount = `getAttr ds1FieldLines1FwdCurves.curveCount`;
    for ( $i=0; $i<$fwdLineCount; $i++)
      rename ("flCurveVarGroup_" + $i) ("ds1FieldLines1FwdVarGroup_" + $i);

    int $bakLineCount = `getAttr ds1FieldLines1BakCurves.curveCount`;
    for ( $i=0; $i<$bakLineCount; $i++)
      rename ("flBackCurveVarGroup_" + $i) ("ds1FieldLines1BakVarGroup_" + $i);

    // Figure out the shader
    string $shader;
    string $shaderInfo;
    $shadingGroup = "";
    string $tubeShapeNames[] = `ls "flStreamTube*Shape" "flBackStreamTube*Shape"`;
    if ( size($tubeShapeNames) > 0 )
    {
      string $tubeShape = $tubeShapeNames[0];
      $shadingGroups = `listConnections -type shadingEngine $tubeShape`;
      $shadingGroup = $shadingGroups[0];
      string $shaders[] = `listConnections ($shadingGroup + ".surfaceShader")`;
      $shader = $shaders[0];
    }

    // Only allow data texturing or Blinn shading of field lines for now.  Anything other than data texturing is
    // replaced with an ordinary Blinn shader.
    //
    if ( $shader == "dtDisplayPlaneBlinn" )
    {
      // This is data texturing
      $shaderInfo = "ds2CutPlanes1";
    }
    else
    {
      // This is something other than data texturing
      $shader = "ds1FieldLines1Blinn";
      shadingNode -asShader blinn -name $shader;
      $shaderInfo = "blinn";
    }
    setAttr ds1FieldLines1.shaderInfo -type "string" $shaderInfo;

    // Create shading group for the field line tubes
    sets -renderable true -noSurfaceShader true -empty -name "ds1FieldLines1SG";
    defaultNavigation -ce -d ds1FieldLines1SG.surfaceShader -source $shader;

//    print "Deleting tubes...\n";
//    string $tubeNames[] = `ls "flStreamTube*" "flBackStreamTube*"`;
//    for ( $i=0; $i<size($tubeNames); $i++)
//    {
//      string $tubeName = $tubeNames[$i];
//      string $match = match( "Shape", $tubeName );
//      if ( $match == "" ) delete $tubeName;
//    }

    // Rename tubes and related items
    print "Renaming tubes...\n";
    rename flMasterStreamCircle ds1FieldLines1StreamCircle;
    int $extrudeCount = 0;
    for ( $i=0; $i<$fwdLineCount; $i++)
    {
      string $curveName = "ds1FieldLines1FwdVarGroup_" + $i;
      int $spans = `getAttr ($curveName + ".spans")`;
      if ( $spans > 0 )
      {
        rename ("flStreamTube" + $i) ("ds1FieldLines1FwdTube" + $i);
        rename ("ds1FieldLines1FwdTube" + $i + "Shape") ("ds1FieldLines1FwdTubeShape" + $i);
        $extrudeCount = $extrudeCount + 1;
        rename ("extrude" + $extrudeCount) ("ds1FieldLines1FwdExtrude" + $i);
        if ( $shadingGroup != "" ) sets -remove $shadingGroup ("ds1FieldLines1FwdTubeShape" + $i);
        sets -add ds1FieldLines1SG ("ds1FieldLines1FwdTubeShape" + $i);
      }
    }

    for ( $i=0; $i<$bakLineCount; $i++)
    {
      string $curveName = "ds1FieldLines1BakVarGroup_" + $i;
      int $spans = `getAttr ($curveName + ".spans")`;
      if ( $spans > 0 )
      {
        rename ("flBackStreamTube" + $i) ("ds1FieldLines1BakTube" + $i);
        rename ("ds1FieldLines1BakTube" + $i + "Shape") ("ds1FieldLines1BakTubeShape" + $i);
        $extrudeCount = $extrudeCount + 1;
        rename ("extrude" + $extrudeCount) ("ds1FieldLines1BakExtrude" + $i);
        if ( $shadingGroup != "" ) sets -remove $shadingGroup ("ds1FieldLines1BakTubeShape" + $i);
        sets -add ds1FieldLines1SG ("ds1FieldLines1BakTubeShape" + $i);
      }
    }

    //mRecreateFieldTubes();


    // Now to deal with the markers
    //
    string $fwdMarkers[] = `ls "curve*CV*"`;
    string $bakMarkers[] = `ls "backCurve*CV*"`;
    if ( (size($fwdMarkers) > 0) || (size($bakMarkers) > 0) )
    {
      print "Old-style markers detected...\n";

      // Figure out marker shape
      string $markerInstance;
      if ( size($fwdMarkers) > 0 ) $markerInstance = $fwdMarkers[0];
      else $markerInstance = $bakMarkers[0];

      string $relatives[] = `listRelatives $markerInstance`;
      string $markerShape = $relatives[0];
      string $transforms[] = `listTransforms $markerShape`;
      string $marker = $transforms[0];

      if ( $markerShape == "flConeShape" )
      {
        // This is the standard cone.  We can handle this.
        //
        float $radius = `getAttr makeNurbCone1.radius`;
        float $heightRatio = `getAttr makeNurbCone1.heightRatio`;
        float $height = $heightRatio * $radius;
        $marker = "ds1FieldLines1MarkersCone";
        polyCone -radius $radius -height $height -subdivisionsX 12 -axis 0 0 1 -name $marker;
        $markerShape = $marker + "Shape";


        setAttr ds1FieldLines1.hasMarkers 1;

        createNode unknown -name ds1FieldLines1Markers;
        addAttr -longName "objectType" -dataType "string";
        addAttr -longName "parent" -dataType "string";
        addAttr -longName "marker" -dataType "string";
        addAttr -longName "markerInfo" -dataType "string";
        addAttr -longName "shaderInfo" -dataType "string";

        setAttr ds1FieldLines1Markers.objectType -type "string" "mMarkers";
        setAttr ds1FieldLines1Markers.parent -type "string" "ds1FieldLines1";

        setAttr ds1FieldLines1Markers.marker -type "string" $marker;

        string $markerInfo = "polyCone";  // could be "polyCone" or "custom"
        setAttr ds1FieldLines1Markers.markerInfo -type "string" $markerInfo;

        // Figure out the shader
        $shadingGroups = `listConnections -type shadingEngine $markerShape`;
        $shadingGroup = $shadingGroups[0];
        if ( $shadingGroup != "" ) sets -remove $shadingGroup $markerShape;

        // Don't allow data texturing of markers for now.  Use ordinary Blinn shader.
        $shader = "ds1FieldLines1MarkersBlinn";
        shadingNode -asShader blinn -name $shader;
        $shaderInfo = "blinn";
        setAttr ds1FieldLines1Markers.shaderInfo -type "string" $shaderInfo;

        // Create shading group for the markers
        sets -renderable true -noSurfaceShader true -empty -name "ds1FieldLines1MarkersSG";
        defaultNavigation -ce -d ds1FieldLines1MarkersSG.surfaceShader -source $shader;

        // Figure out marker start, step, and reverse flag
        int $markerStart = 0;
        int $markerStep = 1;
        int $markerReverse = 0;

        string $markerList[] = `ls "curve0*"`;
        int $markerCount = size( $markerList );

        if ( $markerCount > 1 )
        { // try using the first forward curve
          int $CVnumber[];
          for ( $i=0; $i<$markerCount; $i++ )
          {
            $CVnumber[$i] = substring( $markerList[$i], 9, size($markerList[$i]) );
          }
          sort( $CVnumber );

          $markerStart = $CVnumber[0];
          $markerStep = $CVnumber[1] - $CVnumber[0];

          string $firstMarker = $markerList[0];
          int $CVindex = substring( $firstMarker, 9, size($firstMarker) );
          setAttr flCurveDissecter.curveIndex 0;
          setAttr flCurveDissecter.CVIndex $CVindex;
          float $tangent[] = `getAttr flCurveDissecter.tangent`;
          float $phi = acos($tangent[2])*180/3.1415927;
          string $rotateAttr = $firstMarker + ".rotate";
          float $rotate[] = `getAttr $rotateAttr`;
          $markerReverse = 0;
          if ( $phi-$rotate[1] > 0.1 ) $markerReverse = 1;
        }
        else
        { // no forward curves, try again with the first backward curve
          $markerList = `ls "backCurve0*"`;
          $markerCount = size( $markerList );
          if ( $markerCount > 1 )
          {
            int $CVnumber[];
            for ( $i=0; $i<$markerCount; $i++ )
            {
              $CVnumber[$i] = substring( $markerList[$i], 13, size($markerList[$i]) );
            }
            sort( $CVnumber );

            $markerStart = $CVnumber[0];
            $markerStep = $CVnumber[1] - $CVnumber[0];

            string $firstMarker = $markerList[0];
            int $CVindex = substring( $firstMarker, 13, size($firstMarker) );
            setAttr flBackCurveDissecter.curveIndex 0;
            setAttr flBackCurveDissecter.CVIndex $CVindex;
            float $tangent[] = `getAttr flBackCurveDissecter.tangent`;
            float $phi = acos($tangent[2])*180/3.1415927;
            string $rotateAttr = $firstMarker + ".rotate";
            float $rotate[] = `getAttr $rotateAttr`;
            $markerReverse = 1;
            if ( $phi-$rotate[1] > 0.1 ) $markerReverse = 0;
          }
        }

        print "Marker start, step, reverse are:\n";
        print $markerStart;
        print "\n";
        print $markerStep;
        print "\n";
        print $markerReverse;
        print "\n";

        // Delete old-style markers
        string $markerName;
        for ( $i=0; $i<size($fwdMarkers); $i++ )
        {
          $markerName = $fwdMarkers[$i];
          delete $markerName;
        }
        for ( $i=0; $i<size($bakMarkers); $i++ )
        {
          $markerName = $bakMarkers[$i];
          delete $markerName;
        }

        mRecreateMarkers( $markerShape, $markerStart, $markerStep, $markerReverse );
      } // end if it's the standard cone shape -- can't handle custom shapes yet...
    } // end dealing with markers


    // Time frame expression
    //
    int $frameOffset = `getAttr mMasterGlobals.frameOffset`;
    string $frameExpression = "mDataset1.timeIndex = frame + " + $frameOffset + ";";
    print $frameExpression;
    print "\n";
    expression -s $frameExpression -o mDataset1 -ae 1 -uc all -name ds1Expression;

  } // end dealing with old-style field lines


  // 2) Look for old-style cut planes and convert
  //
  if ( `objExists dtDataGrid` )
  {
    print "Old style data texturing dataset detected...\n";

    int $datasetCounter = `getAttr mMasterGlobals.datasetCounter`;
    $datasetCounter = $datasetCounter + 1;
    setAttr mMasterGlobals.datasetCounter $datasetCounter;

    string $datasetNode = "mDataset" + $datasetCounter;
    string $spatialGridNode = "mSpatialGrid" + $datasetCounter;
    rename dtDataGrid $datasetNode;
    rename dtSpatialGrid $spatialGridNode;

    string $dataset = "dataset" + $datasetCounter;
    createNode unknown -name $dataset;
    sets -add mDatasetsSet $dataset;
    setAttr mMasterGlobals.selectedDataset -type "string" $dataset;
    print ($dataset + "\n");

    // Add some convenience attributes to the dataset node
    select -r $dataset;
    addAttr -longName "datasetType" -dataType "string";
    addAttr -longName "objectNumber" -attributeType "long";
    addAttr -longName "prefix" -dataType "string";
    addAttr -longName "shortFilename" -dataType "string";
    addAttr -longName "displayName" -dataType "string";
    addAttr -longName "cutPlanesCounter" -attributeType "long";

    setAttr ($dataset + ".datasetType") -type "string" "scalar";
    setAttr ($dataset + ".objectNumber") $datasetCounter;
    setAttr ($dataset + ".prefix") -type "string" ("ds" + $datasetCounter);

    string $filename = `getAttr ($datasetNode + ".filename")`;
    string $tokens[];
    int $numTokens = `tokenize $filename "/\\" $tokens`;
    string $shortFilename = $tokens[$numTokens-1];
    setAttr ($dataset + ".shortFilename") -type "string" $shortFilename;

    string $group = `getAttr ($datasetNode + ".group")`;
    string $displayName = $dataset + ") " + $shortFilename + " : " + $group;
    setAttr ($dataset + ".displayName") -type "string" $displayName;

    setAttr ($dataset + ".cutPlanesCounter") 1;

    // Now a node (and the containing set) for the new cut planes object
    string $prefix = `getAttr ($dataset + ".prefix")`;
    sets -empty -name ($prefix + "VizObjectsSet");
    string $convenienceNode = $prefix + "CutPlanes1";
    createNode unknown -name $convenienceNode;
    addAttr -longName "objectType" -dataType "string";
    addAttr -longName "objectNumber" -attributeType "long";
    addAttr -longName "parent" -dataType "string";
    addAttr -longName "visible" -attributeType "long";
    addAttr -longName "shaderInfo" -dataType "string";
    print ($convenienceNode + "\n");

    sets -add ($prefix + "VizObjectsSet") $convenienceNode;
    setAttr mMasterGlobals.selectedVizObject -type "string" $convenienceNode;

    setAttr ($convenienceNode + ".objectType") -type "string" "mCutPlanes";
    setAttr ($convenienceNode + ".objectNumber") 1;
    setAttr ($convenienceNode + ".parent") -type "string" $dataset;
    int $visibility = `getAttr dtDisplayPlane.visibility`;
    if ( $visibility == 0 ) setAttr dtDisplayPlane.visibility 1;
    setAttr ($convenienceNode + ".visible") 1;

    string $displayPlanes = $convenienceNode + "Display";
    string $displayPlanesShape = $convenienceNode + "DisplayShape";
    rename dtDisplayPlane $displayPlanes;
    //rename dtDisplayPlaneShape $displayPlanesShape;  // not necessary

    // Shader related
    string $shader = $convenienceNode + "Blinn";
    rename dtDisplayPlaneBlinn $shader;
    $shadingGroups = `listConnections -type shadingEngine $displayPlanesShape`;
    $shadingGroup = $shadingGroups[0];
    if ( $shadingGroup == "initialShadingGroup" )
    {
      $shadingGroup = $convenienceNode + "SG";
      sets -renderable true -noSurfaceShader true -empty -name $shadingGroup;
      defaultNavigation -ce -d ($shadingGroup + ".surfaceShader") -source $shader;
      sets -remove initialShadingGroup $displayPlanesShape;
      sets -add $shadingGroup $displayPlanesShape;
    }
    else
    {
      $shadingGroup = $convenienceNode + "SG";
      rename dtDisplayPlaneBlinnSG $shadingGroup;
    }

    string $shaderInfo = `getAttr dtHDF5colorTable.tableName`;
    setAttr ($convenienceNode + ".shaderInfo") -type "string" $shaderInfo;

    rename dtScalarDataInterpolation ($convenienceNode + "Interpolation");
    rename dtHDF5colorTable ($convenienceNode + "ColorTable");
    rename dtPlace3dTexture ($convenienceNode + "Place3dTexture");
    rename dtFadeLookup ($convenienceNode + "FadeLookup");
    rename dtFadeTable ($convenienceNode + "FadeTable");
    rename dtTransparencyFade ($convenienceNode + "TransparencyFade");
    rename dtColorLookup ($convenienceNode + "ColorLookup");
    rename dtColorFade ($convenienceNode + "ColorFade");
    rename dtDisplayPlaneFile ($convenienceNode + "TextureFile");

    // Bounding box
    if ( `objExists dtBoundingFace1` ) delete dtBoundingFace1;
    if ( `objExists dtBoundingFace2` ) delete dtBoundingFace2;
    if ( `objExists dtBoundingFace3` ) delete dtBoundingFace3;
    if ( `objExists dtBoundingFace4` ) delete dtBoundingFace4;

    float $min[] = `getAttr ($spatialGridNode + ".min")`;
    float $max[] = `getAttr ($spatialGridNode + ".max")`;

    float $scaleX = $max[0] - $min[0];
    float $scaleY = $max[1] - $min[1];
    float $scaleZ = $max[2] - $min[2];

    float $centerX = 0.5 * ($max[0]+$min[0]);
    float $centerY = 0.5 * ($max[1]+$min[1]);
    float $centerZ = 0.5 * ($max[2]+$min[2]);

    string $boundingBox = $prefix + "BoundingBox";
    polyCube -name $boundingBox;
    scale -a $scaleX $scaleY $scaleZ;
    move -a $centerX $centerY $centerZ;

    // Turn off shading of the bounding box
    $shadingGroups = `listConnections -type shadingEngine ($boundingBox + "Shape")`;
    string $shadingGroup = $shadingGroups[0];
    sets -remove $shadingGroup ($boundingBox + "Shape");


    // Time frame expression
    //
    print "Time frame expression...\n";
    if ( `objExists dtFrameExpression` )
    {
      rename dtFrameExpression ($prefix + "Expression");
    }
    else
    {
      int $frameOffset = `getAttr mMasterGlobals.frameOffset`;
      string $frameExpression = $datasetNode + ".timeIndex = frame + " + $frameOffset + ";";
      expression -s $frameExpression -o $datasetNode -ae 1 -uc all -name ($prefix + "Expression");
    }
  }


}


//=================================================================================================
// mRecreateFieldTubes
//

global proc mRecreateFieldTubes()
{
  print "mRecreateFieldTubes()\n";

  // Create the transforms and the meshes (these are receptacles to hold the generated tube meshes).
  int $fwdLineCount = `getAttr ds1FieldLines1FwdCurves.curveCount`;
  for ( $i=0; $i<$fwdLineCount; $i++)
  {
    string $transformName = "ds1FieldLines1FwdTransform" + $i;
    string $meshName = "ds1FieldLines1FwdShape" + $i;
    createNode transform -n $transformName;
    createNode mesh -n $meshName -p $transformName;
    sets -add ds1FieldLines1SG $meshName;
  }

  int $bakLineCount = `getAttr ds1FieldLines1BakCurves.curveCount`;
  for ( $i=0; $i<$bakLineCount; $i++)
  {
    string $transformName = "ds1FieldLines1BakTransform" + $i;
    string $meshName = "ds1FieldLines1BakShape" + $i;
    createNode transform -n $transformName;
    createNode mesh -n $meshName -p $transformName;
    sets -add ds1FieldLines1SG $meshName;
  }

  // Set up the forward and backward "tube" nodes.
  //
  // But first get tube radius and segment count
  string $connections[] = `listConnections flMasterStreamCircleShape.create`;
  string $circleAttr = $connections[0] + ".radius";
  float $tubeRadius = `getAttr $circleAttr`;
  int $segmentCount = 12;

  // Forward
  print "Creating forward tube nodes...\n";
  for ( $i=0; $i<$fwdLineCount; $i++ )
  {
    string $print = "fwdCurveIndex = " + $i + "\n";
    print $print;
    string $nodeName = "ds1FieldLines1FwdTube" + $i;
    createNode tube -name $nodeName;

    connectAttr ds1FieldLines1FwdCurves.curveCount ($nodeName + ".curveCount");
    connectAttr ds1FieldLines1FwdCurves.startIndices ($nodeName + ".startIndices");
    connectAttr ds1FieldLines1FwdCurves.CVCounts ($nodeName + ".CVCounts");
    connectAttr ds1FieldLines1FwdCurves.controlVertexList ($nodeName + ".controlVertexList");
    connectAttr ds1FieldLines1FwdCurves.tangentList ($nodeName + ".tangentList");
    setAttr ($nodeName + ".curveIndex") $i;
    setAttr ($nodeName + ".tubeRadius") $tubeRadius;
    setAttr ($nodeName + ".segmentCount") $segmentCount;

    string $meshName = "ds1FieldLines1FwdShape" + $i;
    connectAttr ($nodeName + ".secondaryMesh") ($meshName + ".inMesh");
  }

  // Backward
  print "Creating backward tubes node...\n";
  for ( $i=0; $i<$bakLineCount; $i++ )
  {
    string $print = "bakCurveIndex = " + $i + "\n";
    print $print;
    string $nodeName = "ds1FieldLines1BakTube" + $i;
    createNode tube -name $nodeName;

    connectAttr ds1FieldLines1BakCurves.curveCount ($nodeName + ".curveCount");
    connectAttr ds1FieldLines1BakCurves.startIndices ($nodeName + ".startIndices");
    connectAttr ds1FieldLines1BakCurves.CVCounts ($nodeName + ".CVCounts");
    connectAttr ds1FieldLines1BakCurves.controlVertexList ($nodeName + ".controlVertexList");
    connectAttr ds1FieldLines1BakCurves.tangentList ($nodeName + ".tangentList");
    setAttr ($nodeName + ".curveIndex") $i;
    setAttr ($nodeName + ".tubeRadius") $tubeRadius;
    setAttr ($nodeName + ".segmentCount") $segmentCount;

    string $meshName = "ds1FieldLines1BakShape" + $i;
    connectAttr ($nodeName + ".secondaryMesh") ($meshName + ".inMesh");
  }

  print "Finished recreating field tubes.\n";
}


//=================================================================================================
// mRecreateMarkers
//

global proc mRecreateMarkers(
  string $markerShape,
  int $markerStart,
  int $markerStep,
  int $markerReverse )
{
  print "mRecreateMarkers()\n";

  // Create the transforms and the meshes (these are receptacles to hold the generated markers).
  int $fwdLineCount = `getAttr ds1FieldLines1FwdCurves.curveCount`;
  for ( $i=0; $i<$fwdLineCount; $i++)
  {
    string $transformName = "ds1FieldLines1MarkersFwdTransform" + $i;
    string $meshName = "ds1FieldLines1MarkersFwdShape" + $i;
    createNode transform -n $transformName;
    createNode mesh -n $meshName -p $transformName;
    sets -add ds1FieldLines1MarkersSG $meshName;
  }

  int $bakLineCount = `getAttr ds1FieldLines1BakCurves.curveCount`;
  for ( $i=0; $i<$bakLineCount; $i++)
  {
    string $transformName = "ds1FieldLines1MarkersBakTransform" + $i;
    string $meshName = "ds1FieldLines1MarkersBakShape" + $i;
    createNode transform -n $transformName;
    createNode mesh -n $meshName -p $transformName;
    sets -add ds1FieldLines1MarkersSG $meshName;
  }

  // Forward
  print "Creating forward marker nodes...\n";
  for ( $i=0; $i<$fwdLineCount; $i++ )
  {
    string $print = "fwdCurveIndex = " + $i + "\n";
    print $print;
    string $nodeName = "ds1FieldLines1MarkersFwd" + $i;
    createNode markers -name $nodeName;

    //connectAttr ($markerShape + ".outMesh") ($nodeName + ".markerMesh");
    connectAttr ($markerShape + ".worldMesh") ($nodeName + ".markerMesh");
    connectAttr ds1FieldLines1FwdCurves.curveCount ($nodeName + ".curveCount");
    connectAttr ds1FieldLines1FwdCurves.startIndices ($nodeName + ".startIndices");
    connectAttr ds1FieldLines1FwdCurves.CVCounts ($nodeName + ".CVCounts");
    connectAttr ds1FieldLines1FwdCurves.controlVertexList ($nodeName + ".controlVertexList");
    connectAttr ds1FieldLines1FwdCurves.tangentList ($nodeName + ".tangentList");
    setAttr ($nodeName + ".curveIndex") $i;
    setAttr ($nodeName + ".start") $markerStart;
    setAttr ($nodeName + ".step") $markerStep;
    setAttr ($nodeName + ".reverse") $markerReverse;

    string $meshName = "ds1FieldLines1MarkersFwdShape" + $i;
    connectAttr ($nodeName + ".secondaryMesh") ($meshName + ".inMesh");
  }

  // Backward
  print "Creating backward markers nodes...\n";
  if ( $markerStep > $markerStart ) $markerStart = $markerStep - $markerStart;
  if ( $markerReverse ) $markerReverse = 0;
  else $markerReverse = 1;
  for ( $i=0; $i<$bakLineCount; $i++ )
  {
    string $print = "bakCurveIndex = " + $i + "\n";
    print $print;
    string $nodeName = "ds1FieldLines1MarkersBak" + $i;
    createNode markers -name $nodeName;

    //connectAttr ($markerShape + ".outMesh") ($nodeName + ".markerMesh");
    connectAttr ($markerShape + ".worldMesh") ($nodeName + ".markerMesh");
    connectAttr ds1FieldLines1BakCurves.curveCount ($nodeName + ".curveCount");
    connectAttr ds1FieldLines1BakCurves.startIndices ($nodeName + ".startIndices");
    connectAttr ds1FieldLines1BakCurves.CVCounts ($nodeName + ".CVCounts");
    connectAttr ds1FieldLines1BakCurves.controlVertexList ($nodeName + ".controlVertexList");
    connectAttr ds1FieldLines1BakCurves.tangentList ($nodeName + ".tangentList");
    setAttr ($nodeName + ".curveIndex") $i;
    setAttr ($nodeName + ".start") $markerStart;
    setAttr ($nodeName + ".step") $markerStep;
    setAttr ($nodeName + ".reverse") $markerReverse;

    string $meshName = "ds1FieldLines1MarkersBakShape" + $i;
    connectAttr ($nodeName + ".secondaryMesh") ($meshName + ".inMesh");
  }

  print "Finished recreating markers.\n";
}


//=================================================================================================
// mShowVizObject
//

global proc mShowVizObject()
{
  print "mShowVizObject()\n";
  string $selectedObjects[] = `textScrollList -q -selectItem mVizObjectList`;
  string $selectedObject = $selectedObjects[0];
  setAttr mMasterGlobals.selectedVizObject -type "string" $selectedObject;

  string $objectType = "";
  string $objectNumber = 0;
  int $visibility;
  if ( `objExists $selectedObject` )
  {
    $objectType = `getAttr ($selectedObject + ".objectType")`;
    $objectNumber = `getAttr ($selectedObject + ".objectNumber")`;
    $visibility = `getAttr ($selectedObject + ".visible")`;
  }

  checkBox -edit -value $visibility mVizObjectVisibleField;
  if ( $objectType != "" )
    tabLayout -edit -selectTab ($objectType + "Frame") mVizObjectTabs;


  // Field lines tab page
  // ====================
  if ( $objectType == "mFieldLines" )
  {
    frameLayout -edit -label $selectedObject mFieldLinesFrame;

    // Emitter grid
    print "Field lines tab\n";
    string $type = `getAttr ($selectedObject + ".emitterType")`;
    if ( $type == "custom" ) $type = `getAttr ($selectedObject + ".emitter")`;
    textField -edit -text $type mEmitterGridField;
    string $emitter = `getAttr ($selectedObject + ".emitter")`;
    float $translate[] = `getAttr ($emitter + ".translate")`;
    floatField -edit -value $translate[0] mEmitterXField;
    floatField -edit -value $translate[1] mEmitterYField;
    floatField -edit -value $translate[2] mEmitterZField;

    // Shader
    $info = `getAttr ($selectedObject + ".shaderInfo")`;
    textField -edit -text $info mFieldLinesShaderField;

    $node = $selectedObject + "FwdCurves";
    float $stepSize = `getAttr ($node + ".stepDistance")`;
    int $maxFwdSteps = `getAttr ($node + ".maxSteps")`;
    $node = $selectedObject + "BakCurves";
    int $maxBakSteps = `getAttr ($node + ".maxSteps")`;

    floatField -edit -value $stepSize mStepSizeField;
    intField -edit -value $maxFwdSteps mMaxFwdStepsField;
    intField -edit -value $maxBakSteps mMaxBakStepsField;

    string $streamCircle = $selectedObject + "StreamCircle";
    float $tubeRadius = `circle -query -r $streamCircle`;
    floatField -edit -value $tubeRadius mTubeRadiusField;

    flSelectFieldLines( $selectedObject );
  } // end updating mFieldLines tab


  // Cut planes tab page
  // ===================
  if ( $objectType == "mCutPlanes" )
  {
    frameLayout -edit -label $selectedObject mCutPlanesFrame;

    // Cut planes
    print "Cut planes tab\n";
    string $displayPlanes = $selectedObject + "Display";
    float $translate[] = `getAttr ($displayPlanes + ".translate")`;
    floatField -edit -value $translate[0] mCutPlanesXField;
    floatField -edit -value $translate[1] mCutPlanesYField;
    floatField -edit -value $translate[2] mCutPlanesZField;

    // Shader
    $info = `getAttr ($selectedObject + ".shaderInfo")`;
    textField -edit -text $info mCutPlanesShaderField;

    cpSelectCutPlanes( $selectedObject );
  } // end updating mCutPlanes tab


}


//=================================================================================================
// mShowDataset
//

global proc mShowDataset()
{
  string $selectedDatasetStrings[] = `textScrollList -q -selectItem mDatasetList`;
  string $selectedDatasetString = $selectedDatasetStrings[0];

  string $tokens[];
  int $numTokens = `tokenize $selectedDatasetString ")" $tokens`;
  string $dataset = $tokens[0];
  setAttr mMasterGlobals.selectedDataset -type "string" $dataset;

  string $prefix = `getAttr ($dataset + ".prefix")`;
  string $vizObjects[] = `sets -q ($prefix + "VizObjectsSet")`;
  if ( size($vizObjects) > 0 ) setAttr mMasterGlobals.selectedVizObject -type "string" $vizObjects[0];

  string $boundingBox = $prefix + "BoundingBox";
  int $boundingBoxVisibility = `getAttr ($boundingBox + "Shape.visibility")`;
  if ( $boundingBoxVisibility == 1 )
  {
    select -r $boundingBox;
    //viewFit;
  }

  createMasterUI();
}


//=================================================================================================
// mUpdateFrameOffset
//

global proc mUpdateFrameOffset()
{
  int $frameOffset = `intField -query -value mFrameOffsetField`;
  setAttr mMasterGlobals.frameOffset $frameOffset;

  string $datasets[] = `sets -q mDatasetsSet`;
  for ( $i=0; $i<size($datasets); $i++)
  {
    string $dataset = $datasets[$i];
    int $objectNumber = `getAttr ($dataset + ".objectNumber")`;
    string $datasetNode = "mDataset" + $objectNumber;
    string $frameExpression = $datasetNode + ".timeIndex = frame + " + $frameOffset + ";";
    string $prefix = `getAttr ($dataset + ".prefix")`;
    string $expressionNode = $prefix + "Expression";
    expression -edit -s $frameExpression $expressionNode;
  }
}