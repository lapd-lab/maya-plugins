/* ==========================================================================
// Texture 3D : pluginMain.cpp
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "scalarDataInterpolation.h"
#include "vectorShadeLookup.h"
#include "defaultVectorShadingTable.h"
#include "vectorFade.h"
#include <maya/MFnPlugin.h>
#include <maya/MGlobal.h>


/* ==========================================================================
// initializePlugin()
*/
MStatus initializePlugin( MObject obj )
{
    const MString UserClassify( "texture/3d" );

    MFnPlugin plugin( obj, "Jim Bamber", "1.0", "Any");
	MGlobal::displayInfo( " " );
	MGlobal::displayInfo( "plugin: Texture 3D  " + lapdCommon::version );
	MGlobal::displayInfo( "================================" );

	MGlobal::displayInfo( "plugin.registerNode(scalarDataInterpolation)" );
	diLOG_MSTATUS ( plugin.registerNode( "scalarDataInterpolation", scalarDataInterpolation::id, 
                         &scalarDataInterpolation::creator, &scalarDataInterpolation::initialize,
                         MPxNode::kDependNode, &UserClassify ) );

	MGlobal::displayInfo( "plugin.registerNode(vectorShadeLookup)" );
	diLOG_MSTATUS ( plugin.registerNode( "vectorShadeLookup", vectorShadeLookup::id, 
                         &vectorShadeLookup::creator, &vectorShadeLookup::initialize,
                         MPxNode::kDependNode, &UserClassify ) );

	MGlobal::displayInfo( "plugin.registerNode(defaultVectorShadingTable)" );
	diLOG_MSTATUS ( plugin.registerNode( "defaultVectorShadingTable", defaultVectorShadingTable::id, 
                         &defaultVectorShadingTable::creator, &defaultVectorShadingTable::initialize,
                         MPxNode::kDependNode, &UserClassify ) );

	MGlobal::displayInfo( "plugin.registerNode(vectorFade)" );
	diLOG_MSTATUS ( plugin.registerNode( "vectorFade", vectorFade::id, 
                         &vectorFade::creator, &vectorFade::initialize,
                         MPxNode::kDependNode, &UserClassify ) );

    return MS::kSuccess;
}


/* ==========================================================================
// uninitializePlugin()
*/
MStatus uninitializePlugin( MObject obj )
{
    MFnPlugin plugin( obj );
    diLOG_MSTATUS ( plugin.deregisterNode( scalarDataInterpolation::id ) );
    diLOG_MSTATUS ( plugin.deregisterNode( vectorShadeLookup::id ) );
    diLOG_MSTATUS ( plugin.deregisterNode( defaultVectorShadingTable::id ) );
    diLOG_MSTATUS ( plugin.deregisterNode( vectorFade::id ) );

    return MS::kSuccess;
}
