/* ==========================================================================
// HDF5 : HDF5fileCmd.h
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#ifndef HDF5FILECMD_H
#define HDF5FILECMD_H

#include <maya/MPxCommand.h>
#include <maya/MDGModifier.h>
#include <maya/MGlobal.h>

class HDF5fileCmd : public MPxCommand 
{
public:
	virtual MStatus	doIt ( const MArgList& );
	virtual MStatus undoIt();
 	virtual MStatus redoIt();
	virtual bool isUndoable() const { return false; }

	static void *creator() { return new HDF5fileCmd; }
	static MSyntax newSyntax();

private:
	MDGModifier dgMod;
	static int spatialGridNodeCount;
	static int vectorDataNodeCount;
	static int scalarDataNodeCount;
};

#endif
