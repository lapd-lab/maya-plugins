/* ============================================================================
// forceField : tubesUV.cpp
//-
// ==========================================================================
// Copyright (C) 2012 Jim Bamber
//
// ==========================================================================
//+
*/
#include "tubesUV.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MFnStringData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MItDag.h>
#include <maya/MDagPath.h>
#include <maya/MPointArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshEdge.h>
#include <maya/MQuaternion.h>


// Static data
MTypeId tubesUV::id( 0x00464 );
MPointArray tubesUV::unitCircle;

// Attributes
MObject tubesUV::aUVMode;
MObject tubesUV::aFwdCurveCount;
MObject tubesUV::aFwdStartIndices;
MObject tubesUV::aFwdCVCounts;
MObject tubesUV::aFwdControlVertexList;
MObject tubesUV::aFwdTangentList;
MObject tubesUV::aBakCurveCount;
MObject tubesUV::aBakStartIndices;
MObject tubesUV::aBakCVCounts;
MObject tubesUV::aBakControlVertexList;
MObject tubesUV::aBakTangentList;
MObject tubesUV::aTubeRadius;
MObject tubesUV::aSegmentCount;
MObject tubesUV::aUCycles;
MObject tubesUV::aSpiralAngle;
MObject tubesUV::aFixedAngle;
MObject tubesUV::aStepsPerVCycle;
MObject tubesUV::aDims;				
MObject tubesUV::aGridMin;			
MObject tubesUV::aGridMax;			
MObject tubesUV::aScalarDataGrid;	
MObject tubesUV::aDataMin;			
MObject tubesUV::aDataMax;			

MObject tubesUV::aSecondaryMesh;


/* ============================================================================
// createMesh()
*/
MStatus tubesUV::createMesh(
	MFnMesh& fnMesh,
	int fwdCurveCount,
	MIntArray fwdStartIndices,
	MIntArray fwdCVCounts,
	MPointArray fwdControlVertexList,
	MVectorArray fwdTangentList,
	int bakCurveCount,
	MIntArray bakStartIndices,
	MIntArray bakCVCounts,
	MPointArray bakControlVertexList,
	MVectorArray bakTangentList,
	int segmentCount,
	double tubeRadius,
	int UCycles,
	double spiralAngle,
	double fixedAngle,
	MObject& newOutputData )
{
	MStatus stat;

	MFloatPointArray vertices;
	MIntArray polygonVertexCounts;
	MIntArray polygonVertexIndices;
	int index;
	MFloatPoint controlVertex;
	MVector tangent;

	double pi = 3.1415927;
	double fwdSpiralAngleRadians = spiralAngle * pi / 180.0;
	double bakSpiralAngleRadians = -fwdSpiralAngleRadians;
	double fixedAngleRadians = fixedAngle * pi / 180.0;


	// Do the mesh geometry for all forward curves
	//
	for ( int fwdCurveIndex=0; fwdCurveIndex<fwdCurveCount; fwdCurveIndex++ )
	{
		int fwdCVCount = fwdCVCounts[ fwdCurveIndex ];
		int fwdStartIndex = fwdStartIndices[ fwdCurveIndex ];

		for ( int iCV=0; iCV<fwdCVCount; iCV++ )
		{
			index = fwdStartIndex + iCV;
			controlVertex = MFloatPoint( fwdControlVertexList[index] );
			tangent = fwdTangentList[index];

			addSection(
				iCV,
				controlVertex,
				tangent,
				tubeRadius,
				segmentCount,
				UCycles,
				fwdSpiralAngleRadians,
				fixedAngleRadians,
				vertices,
				polygonVertexCounts,
				polygonVertexIndices );
		}  // end looping through CV's for this curve
	}  // end looping through fwd curves


	// Do the mesh geometry for all backward curves
	//
	for ( int bakCurveIndex=0; bakCurveIndex<fwdCurveCount; bakCurveIndex++ )
	{
		int bakCVCount = bakCVCounts[ bakCurveIndex ];
		int bakStartIndex = bakStartIndices[ bakCurveIndex ];

		for ( int iCV=0; iCV<bakCVCount; iCV++ )
		{
			index = bakStartIndex + iCV;
			controlVertex = MFloatPoint( bakControlVertexList[index] );
			tangent = bakTangentList[index];

			addSection(
				iCV,
				controlVertex,
				tangent,
				tubeRadius,
				segmentCount,
				UCycles,
				bakSpiralAngleRadians,
				fixedAngleRadians,
				vertices,
				polygonVertexCounts,
				polygonVertexIndices );
		}  // end looping through CV's for this curve
	}  // end looping through bak curves


	int vertexCount=vertices.length();
	int polygonCount=polygonVertexCounts.length();


	// Load up newOutputData with the mesh data.
	//
	fnMesh.create(
		vertexCount,
		polygonCount,
		vertices,
		polygonVertexCounts,
		polygonVertexIndices,
		newOutputData,
		&stat );  diLOG_MSTATUS( stat );

	return MStatus::kSuccess;
}


/* ============================================================================
// addSection()
*/
MStatus tubesUV::addSection(
	int iCV,
	MFloatPoint controlVertex,
	MVector tangent,
	double tubeRadius,
	int segmentCount,
	int UCycles,
	double spiralAngleRadians,
	double fixedAngleRadians,
	MFloatPointArray& vertices,
	MIntArray& polygonVertexCounts,
	MIntArray& polygonVertexIndices )
{
	MStatus stat;

	// This is the way to build the mesh by hand
	//
	MVector zAxis( 0.0, 0.0, 1.0 );
	MQuaternion quaternion( zAxis, tangent );
	MVector vector;
	MVector alignedVector;
	double length = tubeRadius;

	double totalAngle = fixedAngleRadians + spiralAngleRadians * (double)iCV;

	// First, the vertices
	unsigned int vertexCount = vertices.length();
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		vector = tubesUV::unitCircle[ iSegment ];
		vector = vector * length;
		vector = vector.rotateBy( MVector::kZaxis, totalAngle );
		alignedVector = vector.rotateBy( quaternion );
		vertices.append( controlVertex + alignedVector );
	}

	// Then the connectivity, as long as it's not the first CV
	if ( iCV > 0 )
	{
		int vertexIndexBase;

		// Connectivity for all but the last segment of the circle
		for ( int iSegment=0; iSegment < segmentCount-1; iSegment++ )
		{
			vertexIndexBase = vertexCount + iSegment;  // vertexCount == 0-based index of first vertex in new circle
			polygonVertexCounts.append(4);
			polygonVertexIndices.append( vertexIndexBase - segmentCount + 0 );
			polygonVertexIndices.append( vertexIndexBase                + 0 );
			polygonVertexIndices.append( vertexIndexBase                + 1 );
			polygonVertexIndices.append( vertexIndexBase - segmentCount + 1 );
		}

		// Connectivity for the last segment of the circle
		vertexIndexBase = vertexIndexBase + 1;
		polygonVertexCounts.append(4);
		polygonVertexIndices.append( vertexIndexBase - segmentCount + 0 );
		polygonVertexIndices.append( vertexIndexBase                + 0 );
		polygonVertexIndices.append( vertexIndexBase                + 1 - segmentCount );  // wraps around to beginning
		polygonVertexIndices.append( vertexIndexBase - segmentCount + 1 - segmentCount );  // of the circle
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// addCube()
*/
/*MStatus tubesUV::addCube(
	MFloatPoint controlVertex,
	MFloatPointArray& cubeVertices,
	MIntArray& cubePolygonVertexCounts,
	MIntArray& cubePolygonVertexIndices )
{
	MStatus stat;

	// This is the way to build the mesh by hand
	//

	// First, the vertices
	unsigned int vertexCount = cubeVertices.length();
	double length = 0.1;  // 1 mm cube
	for ( int iz=-1; iz<=1; iz=iz+2 )
		for ( int iy=-1; iy<=1; iy=iy+2 )
			for ( int ix=-1; ix<=1; ix=ix+2 )
			{
				double x = (double)ix * length;
				double y = (double)iy * length;
				double z = (double)iz * length;
				MVector vector = MVector( x, y, z );
				cubeVertices.append( controlVertex + vector );
			}

	// Then the connectivity for 6 faces of cube
	int vertexIndexBase = vertexCount;

	// face 1
	cubePolygonVertexCounts.append(4);
	cubePolygonVertexIndices.append( vertexIndexBase + 0 );
	cubePolygonVertexIndices.append( vertexIndexBase + 1 );
	cubePolygonVertexIndices.append( vertexIndexBase + 3 );
	cubePolygonVertexIndices.append( vertexIndexBase + 2 );

	// face 2
	cubePolygonVertexCounts.append(4);
	cubePolygonVertexIndices.append( vertexIndexBase + 4 );
	cubePolygonVertexIndices.append( vertexIndexBase + 5 );
	cubePolygonVertexIndices.append( vertexIndexBase + 7 );
	cubePolygonVertexIndices.append( vertexIndexBase + 6 );

	// face 3
	cubePolygonVertexCounts.append(4);
	cubePolygonVertexIndices.append( vertexIndexBase + 0 );
	cubePolygonVertexIndices.append( vertexIndexBase + 2 );
	cubePolygonVertexIndices.append( vertexIndexBase + 6 );
	cubePolygonVertexIndices.append( vertexIndexBase + 4 );

	// face 4
	cubePolygonVertexCounts.append(4);
	cubePolygonVertexIndices.append( vertexIndexBase + 1 );
	cubePolygonVertexIndices.append( vertexIndexBase + 3 );
	cubePolygonVertexIndices.append( vertexIndexBase + 7 );
	cubePolygonVertexIndices.append( vertexIndexBase + 5 );

	// face 5
	cubePolygonVertexCounts.append(4);
	cubePolygonVertexIndices.append( vertexIndexBase + 0 );
	cubePolygonVertexIndices.append( vertexIndexBase + 1 );
	cubePolygonVertexIndices.append( vertexIndexBase + 5 );
	cubePolygonVertexIndices.append( vertexIndexBase + 4 );

	// face 6
	cubePolygonVertexCounts.append(4);
	cubePolygonVertexIndices.append( vertexIndexBase + 2 );
	cubePolygonVertexIndices.append( vertexIndexBase + 3 );
	cubePolygonVertexIndices.append( vertexIndexBase + 7 );
	cubePolygonVertexIndices.append( vertexIndexBase + 6 );

	return MStatus::kSuccess;
}*/


/* ============================================================================
// assignUVs()
//
// Assign multiple UV sets to the given MFnMesh which refers to a newly created set of field line tubes.
//
*/
MStatus tubesUV::assignUVs(
	MFnMesh& fnMesh,
	int UVMode,
	int fwdCurveCount,
	MIntArray fwdStartIndices,
	MIntArray fwdCVCounts,
	MPointArray fwdControlVertexList,
	int bakCurveCount,
	MIntArray bakStartIndices,
	MIntArray bakCVCounts,
	MPointArray bakControlVertexList,
	int3& dims,
	float3& gridMin,
	float3& gridMax,
	MDoubleArray& scalarDataGrid,
	double dataMin,
	double dataMax,
	int segmentCount,
	int UCycles,
	int stepsPerVCycle)
{
	MStatus stat;
	double slope = 1.0;
    // Make the slope always be 1.0 so that the scaling of the dataset can be used to control the mapping
	// if ( dataMax != dataMin ) slope = 1.0 / (dataMax-dataMin);


	// U coordinate goes around the circle, equates to x coordinate of texture swatch
	// V coordinate goes along the tube, its value is:
	//   (a) scaled to the scalarDatum at the control vertex, equates to y coordinate of texture swatch, if the UVMode is "data-based"
	//   (b) cycles up and down, 0->1->0 as determined by stepsPerVCycle, if the UVMode is "geometry-based"
	//
	// Set up all possible UV pairs.  Make segmentCount+1 U values, for example:
	//   0, 0.25, 0.5, 0.75, 1, 0.75, 0.5, 0.25, 0 (9 U values for 8 segments).
	//
	// UVMode: 0=="data-based" or 1=="geometry-based" 
	// For data-based UVMode, make 1024 V values from 0 to 1. 
	// For geometry-based UVMode, make stepsPerVCycle+1 V values, as in the U case
	//
	int cycleSegmentCount = segmentCount / UCycles;
	int halfCycleSegmentCount = cycleSegmentCount / 2;
	MFloatArray uValues;
	for ( int iUCycle=0; iUCycle<UCycles; iUCycle++ )
	{
		for ( int iU=0; iU<cycleSegmentCount; iU++ )
		{
			if ( iU < halfCycleSegmentCount ) uValues.append( (float)iU/(float)halfCycleSegmentCount );
			else uValues.append( (float)(cycleSegmentCount-iU)/(float)halfCycleSegmentCount );
		}
	}
	uValues.append( 0.0 );

	int uvId=0;
	float uValue, vValue;
	MString secondaryUVSetName;
	MFloatArray uArray;
	MFloatArray vArray;


	// UVMode: 0=="data-based" or 1=="geometry-based" 
	// ==============================================

	if ( UVMode == 0 )
	{
		// Data-based UV set
		// V mapped to secondary data in 1024 steps
		//
		for ( int iScalarDatum=0; iScalarDatum<1024; iScalarDatum++ )
		{
			vValue = (float)( iScalarDatum / 1023.0 );
			for ( int iU=0; iU<(segmentCount+1); iU++ )
			{
				uValue = uValues[iU];
				uArray.append( uValue );
				vArray.append( vValue );
				//diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue, dataBasedUVSetNamePtr) );
				//uvId++;
			}
		}

		diLOG_MSTATUS( fnMesh.setUVs(uArray, vArray) );
	}


	if ( UVMode == 1 )
	{
		// Geometry-based UV set
		// V mapped to CV index
		//
		int halfCycleCVCount = stepsPerVCycle / 2;
		//float epsilon = 1.0e-2;  // this got rid of the stripes
		float epsilon = 0.0;  // try unchecking wrapV (maybe wrapU also)
		for ( int iV=0; iV<(stepsPerVCycle+1); iV++ )
		{
			if ( iV < halfCycleCVCount ) vValue = (float)iV / (float)halfCycleCVCount;
			else vValue = (float)(stepsPerVCycle-iV) / (float)halfCycleCVCount;
			if ( vValue < epsilon ) vValue = epsilon;
			if ( vValue > 1.0-epsilon ) vValue = 1.0 - epsilon;
			for ( int iU=0; iU<(segmentCount+1); iU++ )
			{
				uValue = uValues[iU];
				uArray.append( uValue );
				vArray.append( vValue );
				//diLOG_MSTATUS( fnMesh.setUV(uvId, uValue, vValue) );
				//uvId++;
			}
		}

		diLOG_MSTATUS( fnMesh.setUVs(uArray, vArray) );
	}


	// Prepare to assign the UV's
	//
	int index;
	MPoint controlVertex1, controlVertex2;
	double scalarDatum1, scalarDatum2;
	double vValue1, vValue2;
	int uvIdBase1, uvIdBase2;
	double halfWindow = 2.0;

	MIntArray geomBasedUVCounts;
	MIntArray geomBasedUVIds;
	MIntArray dataBasedUVCounts;
	MIntArray dataBasedUVIds;

	div_t divResult;
	int CVCount = 0;
	int startIndex = 0;


	// Assign UV's for the forward curves
	//
	for ( int fwdCurveIndex=0; fwdCurveIndex<fwdCurveCount; fwdCurveIndex++ )
	{
		CVCount = fwdCVCounts[ fwdCurveIndex ];
		startIndex = fwdStartIndices[ fwdCurveIndex ];

		for ( int iCV = 0; iCV < CVCount-1; iCV++ )
		{
			if ( UVMode == 0 )
			{
				// Data-based UV set
				// V mapped to secondary data in 1024 steps
				//
				index = startIndex + iCV;
				controlVertex1 = fwdControlVertexList[index];
				controlVertex2 = fwdControlVertexList[index+1];
				scalarDatum1 = lapdCommon::interpolateScalar( controlVertex1, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
				scalarDatum2 = lapdCommon::interpolateScalar( controlVertex2, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
				vValue1 = slope * (scalarDatum1-dataMin);
				vValue2 = slope * (scalarDatum2-dataMin);
				if ( vValue1 < 0.0 ) vValue1=0.0;  if ( vValue1 > 1.0 ) vValue1 = 1.0;
				if ( vValue2 < 0.0 ) vValue2=0.0;  if ( vValue2 > 1.0 ) vValue2 = 1.0;
				uvIdBase1 = (int)(vValue1*1023.0) * (segmentCount+1);
				uvIdBase2 = (int)(vValue2*1023.0) * (segmentCount+1);

				for (int iSegment=0; iSegment<segmentCount; iSegment++ )
				{
					dataBasedUVCounts.append( 4 );

					uvId = uvIdBase1 + iSegment;      dataBasedUVIds.append( uvId );
					uvId = uvIdBase2 + iSegment;      dataBasedUVIds.append( uvId );
					uvId = uvIdBase2 + iSegment + 1;  dataBasedUVIds.append( uvId );
					uvId = uvIdBase1 + iSegment + 1;  dataBasedUVIds.append( uvId );
				}
			}  // End if UVMode is "data-based"

			if ( UVMode == 1 )
			{
				// Geometry-based UV set
				// V mapped to CV index
				//
				divResult = div( iCV, stepsPerVCycle );
				uvIdBase1 = divResult.rem * (segmentCount+1);

				divResult = div( iCV+1, stepsPerVCycle );
				uvIdBase2 = divResult.rem * (segmentCount+1);

				for (int iSegment=0; iSegment<segmentCount; iSegment++ )
				{
					geomBasedUVCounts.append( 4 );

					uvId = uvIdBase1 + iSegment;      geomBasedUVIds.append( uvId );
					uvId = uvIdBase2 + iSegment;      geomBasedUVIds.append( uvId );
					uvId = uvIdBase2 + iSegment + 1;  geomBasedUVIds.append( uvId );
					uvId = uvIdBase1 + iSegment + 1;  geomBasedUVIds.append( uvId );
				}
			}  // End if UVMode is "geometry-based"
		}  // End looping through CV's for this fwd curve
	}  // End looping through fwd curves


	// Assign UV's for the backward curves
	//
	for ( int bakCurveIndex=0; bakCurveIndex<bakCurveCount; bakCurveIndex++ )
	{
		CVCount = bakCVCounts[ bakCurveIndex ];
		startIndex = bakStartIndices[ bakCurveIndex ];

		for ( int iCV = 0; iCV < CVCount-1; iCV++ )
		{
			if ( UVMode == 0 )
			{
				// Data-based UV set
				// V mapped to secondary data in 1024 steps
				//
				index = startIndex + iCV;
				controlVertex1 = bakControlVertexList[index];
				controlVertex2 = bakControlVertexList[index+1];
				scalarDatum1 = lapdCommon::interpolateScalar( controlVertex1, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
				scalarDatum2 = lapdCommon::interpolateScalar( controlVertex2, dims, gridMax, gridMin, scalarDataGrid, halfWindow );
				vValue1 = slope * (scalarDatum1-dataMin);
				vValue2 = slope * (scalarDatum2-dataMin);
				if ( vValue1 < 0.0 ) vValue1=0.0;  if ( vValue1 > 1.0 ) vValue1 = 1.0;
				if ( vValue2 < 0.0 ) vValue2=0.0;  if ( vValue2 > 1.0 ) vValue2 = 1.0;
				uvIdBase1 = (int)(vValue1*1023.0) * (segmentCount+1);
				uvIdBase2 = (int)(vValue2*1023.0) * (segmentCount+1);

				for (int iSegment=0; iSegment<segmentCount; iSegment++ )
				{
					dataBasedUVCounts.append( 4 );

					uvId = uvIdBase1 + iSegment;      dataBasedUVIds.append( uvId );
					uvId = uvIdBase2 + iSegment;      dataBasedUVIds.append( uvId );
					uvId = uvIdBase2 + iSegment + 1;  dataBasedUVIds.append( uvId );
					uvId = uvIdBase1 + iSegment + 1;  dataBasedUVIds.append( uvId );
				}
			}  // End if UVMode is data-based

			if ( UVMode == 1 )
			{
				// Geometry-based UV set
				//
				// V mapped to CV index
				index = startIndex + iCV;

				divResult = div( iCV, stepsPerVCycle );
				uvIdBase1 = divResult.rem * (segmentCount+1);

				divResult = div( iCV+1, stepsPerVCycle );
				uvIdBase2 = divResult.rem * (segmentCount+1);

				for (int iSegment=0; iSegment<segmentCount; iSegment++ )
				{
					geomBasedUVCounts.append( 4 );

					uvId = uvIdBase1 + iSegment;      geomBasedUVIds.append( uvId );
					uvId = uvIdBase2 + iSegment;      geomBasedUVIds.append( uvId );
					uvId = uvIdBase2 + iSegment + 1;  geomBasedUVIds.append( uvId );
					uvId = uvIdBase1 + iSegment + 1;  geomBasedUVIds.append( uvId );
				}
			}  // End if UVMode is geometry-based
		}  // End looping through CV's for this bak curve
	}  // End looping through bak curves


	// UVMode: 0=="data-based" or 1=="geometry-based" 
	//
	if ( UVMode == 0 ) diLOG_MSTATUS( fnMesh.assignUVs(dataBasedUVCounts, dataBasedUVIds) );
	if ( UVMode == 1 ) diLOG_MSTATUS( fnMesh.assignUVs(geomBasedUVCounts, geomBasedUVIds) );

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the UV-mapped tubes.
//
*/
MStatus tubesUV::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> tubesUV::compute()" );
	//forceFieldCommon::logEntry( forceFieldCommon::eTubeUV );
	cout << "--> tubesUV::compute()" << endl;

	if ( plug != aSecondaryMesh )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from tubesUV::compute(), Maya not asking for secondary mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hUVMode( block.inputValue(aUVMode, &stat) );   diLOG_MSTATUS( stat );
	int UVMode = hUVMode.asInt();

	MDataHandle hFwdCurveCount( block.inputValue(aFwdCurveCount, &stat) );   diLOG_MSTATUS( stat );
	int fwdCurveCount = hFwdCurveCount.asInt();

	MDataHandle hFwdStartIndices( block.inputValue(aFwdStartIndices, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnFwdStartIndices( hFwdStartIndices.data() );
	MIntArray fwdStartIndices( fnFwdStartIndices.array() );

	MDataHandle hFwdCVCounts( block.inputValue(aFwdCVCounts, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnFwdCVCounts( hFwdCVCounts.data() );
	MIntArray fwdCVCounts( fnFwdCVCounts.array() );

	MDataHandle hFwdControlVertexList( block.inputValue(aFwdControlVertexList, &stat) );   diLOG_MSTATUS( stat );
	MFnPointArrayData fnFwdControlVertexList( hFwdControlVertexList.data() );
	MPointArray fwdControlVertexList( fnFwdControlVertexList.array() );

	MDataHandle hFwdTangentList( block.inputValue(aFwdTangentList, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnFwdTangentList( hFwdTangentList.data() );
	MVectorArray fwdTangentList( fnFwdTangentList.array() );

	MDataHandle hBakCurveCount( block.inputValue(aBakCurveCount, &stat) );   diLOG_MSTATUS( stat );
	int bakCurveCount = hBakCurveCount.asInt();

	MDataHandle hBakStartIndices( block.inputValue(aBakStartIndices, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnBakStartIndices( hBakStartIndices.data() );
	MIntArray bakStartIndices( fnBakStartIndices.array() );

	MDataHandle hBakCVCounts( block.inputValue(aBakCVCounts, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnBakCVCounts( hBakCVCounts.data() );
	MIntArray bakCVCounts( fnBakCVCounts.array() );

	MDataHandle hBakControlVertexList( block.inputValue(aBakControlVertexList, &stat) );   diLOG_MSTATUS( stat );
	MFnPointArrayData fnBakControlVertexList( hBakControlVertexList.data() );
	MPointArray bakControlVertexList( fnBakControlVertexList.array() );

	MDataHandle hBakTangentList( block.inputValue(aBakTangentList, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnBakTangentList( hBakTangentList.data() );
	MVectorArray bakTangentList( fnBakTangentList.array() );

	MDataHandle hTubeRadius( block.inputValue(aTubeRadius, &stat) );   diLOG_MSTATUS( stat );
	double tubeRadius = hTubeRadius.asDouble();

	MDataHandle hSegmentCount( block.inputValue(aSegmentCount, &stat) );   diLOG_MSTATUS( stat );
	int segmentCount = hSegmentCount.asInt();

	MDataHandle hUCycles( block.inputValue(aUCycles, &stat) );   diLOG_MSTATUS( stat );
	int UCycles = hUCycles.asInt();

	MDataHandle hSpiralAngle( block.inputValue(aSpiralAngle, &stat) );   diLOG_MSTATUS( stat );
	double spiralAngle = hSpiralAngle.asDouble();

	MDataHandle hFixedAngle( block.inputValue(aFixedAngle, &stat) );   diLOG_MSTATUS( stat );
	double fixedAngle = hFixedAngle.asDouble();

	MDataHandle hStepsPerVCycle( block.inputValue(aStepsPerVCycle, &stat) );   diLOG_MSTATUS( stat );
	int stepsPerVCycle = hStepsPerVCycle.asInt();

	MDataHandle hDims( block.inputValue(aDims, &stat) );   diLOG_MSTATUS( stat );
	int3& dims = hDims.asInt3();

	MDataHandle hGridMin( block.inputValue(aGridMin, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMin = hGridMin.asFloat3();

	MDataHandle hGridMax( block.inputValue(aGridMax, &stat) );   diLOG_MSTATUS( stat );
	float3& gridMax = hGridMax.asFloat3();

	MDataHandle hScalarDataGrid( block.inputValue(aScalarDataGrid, &stat) );   diLOG_MSTATUS( stat );
	MFnDoubleArrayData fnScalarDataGrid( hScalarDataGrid.data() );
	MDoubleArray scalarDataGrid( fnScalarDataGrid.array() );

	MDataHandle hDataMin( block.inputValue(aDataMin, &stat) );   diLOG_MSTATUS( stat );
	double dataMin = hDataMin.asDouble();

	MDataHandle hDataMax( block.inputValue(aDataMax, &stat) );   diLOG_MSTATUS( stat );
	double dataMax = hDataMax.asDouble();


	// Now do the output attributes.
	//
	MDataHandle hSecondaryMesh =  block.outputValue( aSecondaryMesh, &stat );   diLOG_MSTATUS( stat );


	// Set up a few things before getting to work
	//
	// Create the data objects which will be passed out as aSecondaryMesh
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );

	// Force segmentCount to be multiple of 2*UCycles
	div_t divResult;
	divResult = div( segmentCount, 2*UCycles );
	segmentCount = segmentCount + divResult.rem; 

	// Force stepsPerVCycle to be multiple of 2
	divResult = div( stepsPerVCycle, 2 );
	stepsPerVCycle = stepsPerVCycle + divResult.rem; 

	// Unit circle
	unitCircle.clear();
	double angleFactor = (1.0/(double)segmentCount)*(2.0*3.1415927);
	double theta;
	double x, y, z = 0.0;
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		theta = (double)iSegment * angleFactor;
		x = cos( theta );
		y = sin( theta );
		unitCircle.append( x, y, z );
	}


	// Now create the mesh which will hold all the forward and back curves
	//
	MFnMesh fnMesh;
	cout << "About to call createMesh()" << endl;
	diLOG_MSTATUS(
		createMesh(
			fnMesh,
			fwdCurveCount,
			fwdStartIndices,
			fwdCVCounts,
			fwdControlVertexList,
			fwdTangentList,
			bakCurveCount,
			bakStartIndices,
			bakCVCounts,
			bakControlVertexList,
			bakTangentList,
			segmentCount,
			tubeRadius,
			UCycles,
			spiralAngle,
			fixedAngle,
			newOutputData) );

	// Make the call to assign the UV's
	diLOG_MSTATUS(
		assignUVs(
			fnMesh,
			UVMode,
			fwdCurveCount,
			fwdStartIndices,
			fwdCVCounts,
			fwdControlVertexList,
			bakCurveCount,
			bakStartIndices,
			bakCVCounts,
			bakControlVertexList,
			dims,
			gridMin,
			gridMax,
			scalarDataGrid,
			dataMin,
			dataMax,
			segmentCount,
			UCycles,
			stepsPerVCycle) );


	// Set the outputs as clean.
	hSecondaryMesh.set( newOutputData );
	block.setClean( plug );

	//forceFieldCommon::logEntry( (char*) "<-- tubesUV::compute()" );
	cout << "<-- tubesUV::compute()" << endl;
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information and the unit circle values
*/
MStatus tubesUV::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MFnStringData stringData;
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> tubesUV::initialize()" );


	// Create input attributes:
	//
	// mode
    aUVMode = enumAttr.create( "UVMode", "UVMode", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("data-based", 0) );
	diLOG_MSTATUS( enumAttr.addField("geometry-based", 1) );
	MAKE_INPUT( enumAttr );
	diLOG_MSTATUS( addAttribute(aUVMode) );

	// fwd curve count
	aFwdCurveCount = nAttr.create( "fwdCurveCount", "fwdCurveCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aFwdCurveCount) );

	// fwd start indices
	aFwdStartIndices = tAttr.create( "fwdStartIndices", "fwdStartIndices", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aFwdStartIndices) );

	// fwd CV counts
	aFwdCVCounts = tAttr.create( "fwdCVCounts", "fwdCVCounts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aFwdCVCounts) );

	// fwd control vertices
	aFwdControlVertexList = tAttr.create( "fwdControlVertexList", "fwdControlVertexList", MFnData::kPointArray, fnPointArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aFwdControlVertexList) );

	// fwd tangents
	aFwdTangentList = tAttr.create( "fwdTangentList", "fwdTangentList", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aFwdTangentList) );

	// bak curve count
	aBakCurveCount = nAttr.create( "bakCurveCount", "bakCurveCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aBakCurveCount) );

	// bak start indices
	aBakStartIndices = tAttr.create( "bakStartIndices", "bakStartIndices", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aBakStartIndices) );

	// bak CV counts
	aBakCVCounts = tAttr.create( "bakCVCounts", "bakCVCounts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aBakCVCounts) );

	// bak control vertices
	aBakControlVertexList = tAttr.create( "bakControlVertexList", "bakControlVertexList", MFnData::kPointArray, fnPointArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aBakControlVertexList) );

	// bak tangents
	aBakTangentList = tAttr.create( "bakTangentList", "bakTangentList", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aBakTangentList) );

	// tube radius
	aTubeRadius = nAttr.create( "tubeRadius", "tubeRadius", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aTubeRadius) );

	// segment count
	aSegmentCount = nAttr.create( "segmentCount", "segmentCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(24) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aSegmentCount) );

	// U cycles
	aUCycles = nAttr.create( "UCycles", "UCycles", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	diLOG_MSTATUS( nAttr.setMax(3) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aUCycles) );

	// spiral angle
	aSpiralAngle = nAttr.create( "spiralAngle", "spiralAngle", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aSpiralAngle) );

	// fixed angle
	aFixedAngle = nAttr.create( "fixedAngle", "fixedAngle", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aFixedAngle) );

	// steps per V cycle
	aStepsPerVCycle = nAttr.create( "stepsPerVCycle", "stepsPerVCycle", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(20) );
	diLOG_MSTATUS( nAttr.setMin(1) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aStepsPerVCycle) );

	// grid dimensions
	aDims = nAttr.create( "gridDims", "gridDims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	// grid min
	aGridMin = nAttr.create( "gridMin", "gridMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	// grid max
	aGridMax = nAttr.create( "gridMax", "gridMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

	// scalar data grid
	aScalarDataGrid = tAttr.create( "scalarDataGrid", "scalarDataGrid", MFnData::kDoubleArray, fnDoubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aScalarDataGrid) );

	// data min
	aDataMin = nAttr.create( "dataMin", "dataMin", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aDataMin) );

	// data max
	aDataMax = nAttr.create( "dataMax", "dataMax", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aDataMax) );


	// Create output attributes
	//
	// secondary mesh
	aSecondaryMesh = tAttr.create( "secondaryMesh", "secondaryMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aSecondaryMesh) );


	// All inputs affect all outputs
	//
    diLOG_MSTATUS( attributeAffects(aUVMode, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aFwdCurveCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aFwdStartIndices, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aFwdCVCounts, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aFwdControlVertexList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aFwdTangentList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aBakCurveCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aBakStartIndices, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aBakCVCounts, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aBakControlVertexList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aBakTangentList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTubeRadius, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aSegmentCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aUCycles, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aSpiralAngle, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aFixedAngle, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStepsPerVCycle, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aDims, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMin, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aGridMax, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aScalarDataGrid, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMin, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aDataMax, aSecondaryMesh) );

	forceFieldCommon::logEntry( (char*) "<-- tubesUV::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
// tubesUV()
*/
tubesUV::tubesUV()
{
}


/* ============================================================================
// postConstructor()
*/
void tubesUV::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType tubesUV::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * tubesUV::creator()
{
    return new tubesUV();
}


/* ============================================================================
// ~tubesUV()
*/
tubesUV::~tubesUV()
{
}


