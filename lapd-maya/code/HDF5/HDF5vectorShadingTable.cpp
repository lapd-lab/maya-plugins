/* ==========================================================================
// HDF5 : HDF5vectorShadingTable.cpp
//
// Copyright (C) 2006 Jim Bamber 
// 
*/
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "HDF5common.h"
#include "errorHandling.h"
#include "HDF5vectorShadingTable.h"
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h>
#include <maya/MArrayDataHandle.h>
#include <maya/MArrayDataBuilder.h>
#include <maya/MGlobal.h>
#include <maya/MFnStringData.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MVectorArray.h>
#include <maya/MFloatVector.h>

#include <H5public.h>
#include <H5Apublic.h>
#include <H5Fpublic.h>
#include <H5Gpublic.h>
#include <H5Ppublic.h>
#include <H5Spublic.h>
#include <H5Tpublic.h>

MTypeId  HDF5vectorShadingTable::id( 0x00340 );

MObject  HDF5vectorShadingTable::filename;        
MObject  HDF5vectorShadingTable::groupName;        
MObject  HDF5vectorShadingTable::tableName;        
MObject  HDF5vectorShadingTable::vectorShadingTable;


/* ==========================================================================
// compute()
*/
MStatus HDF5vectorShadingTable::compute( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;
	//HDF5common::logEntry( (char*) "--> HDF5vectorShadingTable::compute()" );
	HDF5common::logEntry( HDF5common::eHDF5vectorShadingTable );

	if ( ( plug == vectorShadingTable ) || ( plug.parent() == vectorShadingTable ) )
	{
		stat = computeKernel( plug, data );
		data.setClean( plug );
	} 
	else 
		stat  = MS::kUnknownParameter;

	//HDF5common::logEntry( (char*) "<-- HDF5vectorShadingTable::compute()" );
	return stat;
}


/* ==========================================================================
// computeKernel()
*/
MStatus HDF5vectorShadingTable::computeKernel( const MPlug& plug, MDataBlock& data )
{
	MStatus stat;

	MDataHandle vectorShadingTableHandle( data.outputValue(vectorShadingTable, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle filenameHandle( data.inputValue(filename, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle groupNameHandle( data.inputValue(groupName, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle tableNameHandle( data.inputValue(tableName, &stat) );  diLOG_MSTATUS( stat );

	//HDF5common::logEntry( (char*) "--> HDF5vectorShadingTable::computeKernel()" );


	MString filename( filenameHandle.asString() );
	if ( filename == "" )
	{
		HDF5common::logEntry( (char*) "Abnormal exit from HDF5vectorShadingTable::computeKernel(), filename is blank" );
		return MS::kSuccess;
	}

	// Open the file.
	//
	const char *filenameAsChar = filename.asChar();
	hid_t file_id = H5Fopen( filenameAsChar, H5F_ACC_RDONLY, H5P_DEFAULT );	  diLOG_HDF5STAT( file_id );
	if ( file_id < 0 )
	{
		sprintf_s( HDF5common::logString, DI_LOG_STRING_LENGTH, "Could not open %s", filenameAsChar );
		HDF5common::logEntry( HDF5common::logString );
		return MS::kFailure;
	}

	// This is the basic scheme for reading data:
	/*
	Define memory dataspace of read. (Optional if dataspace is H5S_SELECT_ALL)
	Open the dataset.
	Get the dataset dataspace. (If using H5S_SELECT_ALL above)

	Else define dataset dataspace of read. Define the memory datatype. (Optional)
	Define the memory buffer.
	Open the dataset.
	Read data.

	Close the datatype, dataspace, and property list. (As necessary)
	Close the dataset.
	*/

	// Open the dataset specified by groupName and tableName.
	//
	MString groupNameStr( groupNameHandle.asString() );
	if ( groupNameStr == "" )
	{
		HDF5common::logEntry( (char*) "Abnormal exit from HDF5vectorShadingTable::computeKernel(), group name is blank" );
		diLOG_HDF5STAT( H5Fclose(file_id) );
		return MS::kSuccess;
	}
	const char *groupNameAsChar = groupNameStr.asChar();
	hid_t group_id = H5Gopen2( file_id, groupNameAsChar, H5P_DEFAULT );   diLOG_HDF5STAT( group_id );

	MString tableNameStr( tableNameHandle.asString() );
	if ( tableNameStr == "" )
	{
		HDF5common::logEntry( (char*) "Abnormal exit from HDF5vectorShadingTable::computeKernel(), table name is blank" );
		diLOG_HDF5STAT( H5Gclose(group_id) );
		diLOG_HDF5STAT( H5Fclose(file_id) );
		return MS::kSuccess;
	}
	const char *tableNameAsChar = tableNameStr.asChar();
	hid_t dataset_id = H5Dopen2( group_id, tableNameAsChar, H5P_DEFAULT );   diLOG_HDF5STAT( dataset_id );

	// Check that the data type is 3-vector.
	//
	hid_t data_type_id = H5Aopen_name( dataset_id, "Type" );   diLOG_HDF5STAT( data_type_id );
	hid_t data_type_type_id = H5Aget_type( data_type_id );	   diLOG_HDF5STAT( data_type_type_id );

	char data_type[120];
	diLOG_HDF5STAT( H5Aread(data_type_id, data_type_type_id, data_type) );
	//sprintf( HDF5common::logString, "data_type: %s", data_type );
	//HDF5common::logEntry( HDF5common::logString );

	diLOG_HDF5STAT( H5Tclose(data_type_type_id) );
	diLOG_HDF5STAT( H5Aclose(data_type_id) );
	
	if ( MString(data_type) != "3-vector" )
	{
		HDF5common::logEntry( (char*) "Abnormal exit from HDF5vectorShadingTable::computeKernel(), table type is not 3-vector" );
		diLOG_HDF5STAT( H5Dclose(dataset_id) );
		diLOG_HDF5STAT( H5Gclose(group_id) );
		diLOG_HDF5STAT( H5Fclose(file_id) );
		return MStatus::kFailure;
	}

	hid_t space_id   = H5Dget_space( dataset_id );			   diLOG_HDF5STAT( space_id );
	hsize_t current_dims[1];
	hsize_t max_dims[1];
	int ndims = H5Sget_simple_extent_dims( space_id, current_dims, max_dims );   diLOG_HDF5STAT( ndims );
	unsigned long npoints = (unsigned long)current_dims[0];

	// Define the data structure and allocate memory.
	//
	typedef struct {
	  float table_x;
	  float table_y;
	  float table_z;
	} table_element;

	hid_t table_type_id = H5Tcreate( H5T_COMPOUND, sizeof(table_element) );   diLOG_HDF5STAT( table_type_id );
	diLOG_HDF5STAT( H5Tinsert(table_type_id, "table_x", HOFFSET(table_element, table_x), H5T_NATIVE_FLOAT) );
	diLOG_HDF5STAT( H5Tinsert(table_type_id, "table_y", HOFFSET(table_element, table_y), H5T_NATIVE_FLOAT) );
	diLOG_HDF5STAT( H5Tinsert(table_type_id, "table_z", HOFFSET(table_element, table_z), H5T_NATIVE_FLOAT) );

	table_element *mem_buffer = (table_element *)malloc( npoints*sizeof(table_element) );

	// Read the dataset.
	//
	diLOG_HDF5STAT( H5Dread(dataset_id, table_type_id, H5S_ALL, H5S_ALL, H5P_DEFAULT, mem_buffer) );
	float table_x = (*mem_buffer).table_x;
	float table_y = (*mem_buffer).table_y;
	float table_z = (*mem_buffer).table_z;
	//sprintf( HDF5common::logString, "table_(x,y,z): %f %f %f", table_x, table_y, table_z );
	//HDF5common::logEntry( HDF5common::logString );

	// Build the array attribute.
	//
	MFnVectorArrayData mVectorShadingTable( vectorShadingTableHandle.data() );
	MVectorArray vectorArray;
	MVector vectorElement;
    table_x = (*mem_buffer).table_x;
    table_y = (*mem_buffer).table_y;
    table_z = (*mem_buffer).table_z;
	for (int i=0; i<(int)npoints; i++)
	{

		vectorElement = MVector( table_x, table_y, table_z );
		vectorArray.append( vectorElement );
        table_x = (*mem_buffer++).table_x;
        table_y = (*mem_buffer).table_y;
        table_z = (*mem_buffer).table_z;
	}
	diLOG_MSTATUS( mVectorShadingTable.set(vectorArray) );
	vectorShadingTableHandle.setClean();

	// Close the ids.
	//
	diLOG_HDF5STAT( H5Tclose(table_type_id) );
	diLOG_HDF5STAT( H5Sclose(space_id) );
	diLOG_HDF5STAT( H5Dclose(dataset_id) );
	diLOG_HDF5STAT( H5Fclose(file_id) );

	//For some reason, freeing the mem_buffer causes a crash, maybe because it's on the heap (?)
	//free( (void *)mem_buffer );

	//HDF5common::logEntry( (char*) "<-- HDF5vectorShadingTable::computeKernel()" );
	return MS::kSuccess;
}


/* ==========================================================================
// HDF5vectorShadingTable()
*/
HDF5vectorShadingTable::HDF5vectorShadingTable()
{
}


/* ==========================================================================
// postConstructor()
*/
void HDF5vectorShadingTable::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType HDF5vectorShadingTable::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// ~HDF5vectorShadingTable()
*/
HDF5vectorShadingTable::~HDF5vectorShadingTable()
{
}


/* ==========================================================================
// creator()
*/
void *HDF5vectorShadingTable::creator()
{
	return new HDF5vectorShadingTable();
}


/* ==========================================================================
// initialize()
*/
MStatus HDF5vectorShadingTable::initialize()
{
	MStatus				 stat, stat2;
	MFnTypedAttribute	 typedAttr;
	MFnStringData		 stringData;
	MFnVectorArrayData	 vectorArrayData;		

	HDF5common::logEntry( (char*) "--> HDF5vectorShadingTable::initialize()" );


    filename = typedAttr.create( "filename", "file", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

    groupName = typedAttr.create( "groupName", "group", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

    tableName = typedAttr.create( "tableName", "tableName", MFnData::kString, stringData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

    vectorShadingTable = typedAttr.create( "vectorShadingTable", "vTable", MFnData::kVectorArray, vectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );

	diLOG_MSTATUS( addAttribute(filename) );
	diLOG_MSTATUS( addAttribute(groupName) );
	diLOG_MSTATUS( addAttribute(tableName) );
	diLOG_MSTATUS( addAttribute(vectorShadingTable) );

	diLOG_MSTATUS( attributeAffects(filename, vectorShadingTable) );
	diLOG_MSTATUS( attributeAffects(groupName, vectorShadingTable) );
	diLOG_MSTATUS( attributeAffects(tableName, vectorShadingTable) );

	HDF5common::logEntry( (char*) "<-- HDF5vectorShadingTable::initialize()" );
	return MS::kSuccess;
}
