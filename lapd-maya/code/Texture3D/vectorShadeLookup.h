/* ==========================================================================
// Texture 3D : vectorShadeLookup.h
//
// Copyright (C) 2006 Jim Bamber 
//
*/
#ifndef VECTORSHADELOOKUP_H
#define VECTORSHADELOOKUP_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnVectorArrayData.h>


class vectorShadeLookup : public MPxNode
{
    public:

    vectorShadeLookup();
	virtual void postConstructor();
    virtual ~vectorShadeLookup();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MFloatVector lookup(
		float mScaledData,
		float mDataMinMapsTo,
		float mDataMaxMapsTo,
		MFnVectorArrayData& mVectorTable,
		float3& mDefaultVector );

	// Input attributes
	static MObject scaledDatum;
	static MObject dataMinMapsTo;
	static MObject dataMaxMapsTo;
	static MObject vectorShadingTable;
	static MObject defaultVectorShade;

	// Output attributes
    static MObject vectorShade;
};


#endif

