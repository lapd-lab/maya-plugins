/* ==========================================================================
// HDF5 : HDF5common.cpp
//-
// ==========================================================================
// Copyright (C) 2011 Jim Bamber
//
// ==========================================================================
//+
*/
#include "HDF5common.h"

#include <math.h>
#include <stdlib.h>
#include <time.h>
//#include <windows.h>

#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatMatrix.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MDoubleArray.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>

char HDF5common::logFilename[1000] = "";
FILE* HDF5common::logFile = NULL;
char HDF5common::logString[5000] = "";
int HDF5common::nodeCount = 7;
int HDF5common::callCounters[7] = { 0, 0, 0, 0, 0, 0, 0 };
char* HDF5common::nodeNameStrings[7] = 
{
	(char*) "Other",
    (char*) "HDF5file",
    (char*) "HDF5scalarDataGrid",
    (char*) "HDF5spatialGrid",
    (char*) "HDF5textureFile",
    (char*) "HDF5vectorDataGrid",
    (char*) "HDF5vectorShadingTable"
};
int HDF5common::currentDay = 0;
time_t HDF5common::currentRawTime = 0;
int HDF5common::logTimestep = 1;  // seconds


/* ==========================================================================
// logEntry()
*/
void HDF5common::logEntry(
	char* logString )
{
	/*
	// Check logging flag
	//  -if logging is not "On", return.
	//
	errno_t err;
	char* mayaLogging = NULL;
	err = _dupenv_s( &mayaLogging, NULL, "MAYA_LOGGING" );  if ( err ) return;  if ( mayaLogging == NULL ) return;
	if ( strcmp(mayaLogging, "On") != 0 )  { free( mayaLogging ); return; }
	else free( mayaLogging );


	// Get the current date and time
	//
	time_t rawtime;
	time ( &rawtime );
	struct tm timeinfo;
	err = localtime_s ( &timeinfo, &rawtime );  if ( err ) return;

	int year = (timeinfo).tm_year+1900;
	int month = (timeinfo).tm_mon+1;
	int day = (timeinfo).tm_mday;
	int hour = (timeinfo).tm_hour;
	int min = (timeinfo).tm_min;
	int sec = (timeinfo).tm_sec;


	// If logFilename is blank or day has changed or file handle is NULL, try to open new logfile
	//
	if ( (strcmp(logFilename, "") == 0) || (currentDay != day) || (logFile == NULL) )
	{
		currentDay = day;
		char* mayaLoggingHome = NULL;
		err = _dupenv_s( &mayaLoggingHome, NULL, "MAYA_LOGGING_HOME" );  if ( err ) return;  if ( mayaLoggingHome == NULL ) return;

		// Make a new folder for the day
		char folderName[1000];
		sprintf_s( folderName, 1000, "%s\\%4d-%02d-%02d", mayaLoggingHome, year, month, day );
		CreateDirectory(folderName, NULL);  // no error checking here, it is handled by checking logFile != NULL below

		// Now open the new logfile
		sprintf_s( logFilename, DI_LOG_FILENAME_LENGTH, "%s\\HDF5 log %4d-%02d-%02d.txt", folderName, year, month, day );
		if ( logFile != NULL ) fclose( logFile );
		err = fopen_s( &logFile, logFilename, "a" );  if ( err ) return;

		// Do not delete this cout as Windows may need the delay in order to get its act together
		cout << hour << ":" << min << ":" << sec << "  HDF5common::logEntry() opening " << logFilename << endl;

		if ( logFile != NULL )
		{
			fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
			fprintf( logFile, "================================\n" );
			fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
			fprintf( logFile, "HDF5 log %4d-%02d-%02d opened\n", year, month, day );
			fflush( logFile );
		}
		else
		{
			cout << "Failed to open " << logFilename << endl;
		}
	}


	// Write out the log string
	//
	//cout << logString << endl;
	if ( logFile != NULL )
	{
		fprintf( logFile, "%02d:%02d:%02d  ", hour, min, sec );
		fprintf( logFile, "%s\n", logString );
		fflush( logFile );
		currentRawTime = rawtime;
	}
	*/
}


/* ==========================================================================
// logEntry()
*/
void HDF5common::logEntry(
	nodeNameEnum nodeName )
{
	/*
	// Check node name.
	//
	if ( nodeName == eOther )
	{
		// -do nothing, the nodeName should always refer to an actual node
	}
	else
	{
		// -write out non-zero call counts for all nodes once per logTimestep
		// -first increment the appropriate call counter
        callCounters[nodeName]++;

		time_t rawTime;
		time ( &rawTime );
		if ( (rawTime-currentRawTime) >= logTimestep )
		{
			// -logTimestep has passed, form the string, write it out, then reset counters
			sprintf_s( logString, DI_LOG_STRING_LENGTH, "" );
			for ( int i=1; i<nodeCount; i++ )
				if ( callCounters[i] > 0 )
				{
					sprintf_s( logString, DI_LOG_STRING_LENGTH, "%s%s: %d  ", logString, nodeNameStrings[i], callCounters[i] );
					callCounters[i] = 0;
				}

			logEntry( logString );
		}
	}
	*/
}

