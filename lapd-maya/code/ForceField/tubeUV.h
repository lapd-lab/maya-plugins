/* ============================================================================
// forceField : tubeUV.h
//
// Copyright (C) 2011 Jim Bamber 
//
*/
#ifndef TUBEUV_H
#define TUBEUV_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class tubeUV : public MPxNode
{
    public:

    tubeUV();
	virtual void postConstructor();
    virtual ~tubeUV();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

	static MPointArray unitCircle;

    static MStatus createMesh(
		//int mode,
		//MFnMesh& fnGeometryBasedMesh,
		//MFnMesh& fnDataBasedMesh,
		MObject transform,
		MFnMesh& fnMesh,
		int curveCount,
		MIntArray startIndices,
		MIntArray CVCounts,
		MPointArray controlVertexList,
		MVectorArray tangentList,
		int curveIndex,
		int segmentCount,
		double tubeRadius,
		int UCycles,
		double spiralAngle,
		double fixedAngle,
		//MObject& geometryBasedOutputData,
		//MObject& dataBasedOutputData );
		MObject& newOutputData );

	static MStatus addSection(
		int iCV,
		MFloatPoint controlVertex,
		MVector tangent,
		double tubeRadius,
		int segmentCount,
		int UCycles,
		double spiralAngleRadians,
		double fixedAngleRadians,
		MFloatPointArray& vertices,
		MIntArray& polygonVertexCounts,
		MIntArray& polygonVertexIndices );

	static MStatus assignUVs(
		//MFnMesh& fnGeometryBasedMesh,
		//MFnMesh& fnDataBasedMesh,
		MFnMesh& fnMesh,
		int curveCount,
		MIntArray startIndices,
		MIntArray CVCounts,
		MPointArray controlVertexList,
		int curveIndex,
		int3& dims,
		float3& gridMin,
		float3& gridMax,
		MDoubleArray& scalarDataGrid,
		double dataMin,
		double dataMax,
		int segmentCount,
		int UCycles,
		int stepsPerVCycle );

	// Input attributes
	/*
	static MObject aMeshName;			// This name is the target "shape", i.e. "mesh" node (string).  If mode is "create", then a new mesh node
										//   is created under the parent transform if the named mesh.  This new mesh will have to be swapped
										//   for the named mesh in MEL after this node executes.  The "create" mode should be used only once,
										//   then "update" should be used.  The "update" mode causes operations to be done directly on the
										//   named mesh.  Note that the secondary mesh output still needs to be connected to the named mesh in
										//   order to trigger execution via the dependency graph.
	static MObject aMode;				// Either "create" or "update" (enum).
	*/
	static MObject aTransform;			// Name of the parent transform (string).
	static MObject aCurveCount;			// Number of curves in the input array (int).
	static MObject aStartIndices;		// Used to index into the control vertex and tangent arrays (int array).
	static MObject aCVCounts;			// Number of CV's for each curve (int array), for example:
										//   index = startIndices[curveIndex] + cvIndex, where cvIndex = {0:CVCounts[curveIndex]-1}
	static MObject aControlVertexList;	// Control vertices for curves (point array).
	static MObject aTangentList;		// Tangents for curves (vector array).
	static MObject aCurveIndex;			// Index of curve to use as basis for output tube (int, 0-based).
	static MObject aTubeRadius;			// Radius of field line tube in cm (double).
	static MObject aSegmentCount;		// Number of segments in the circle (int).
	static MObject aUCycles;			// How many U cycles, 0->1->0, going around the circumference (int, default: 1, range: 1-3)
	static MObject aSpiralAngle;		// Rotation of each segment about the field line with respect to the previous segment in degrees (double)
	static MObject aFixedAngle;			// Rotation of entire tube about the field line in degrees (double)
	static MObject aStepsPerVCycle;		// How many steps along the tube for V to cycle once, 0->1->0 (int, default: 20, min: 1)

										// Spatial grid is assumed to be uniform => is not used as an input.
	static MObject aDims;				// Dimensions of the spatial grid (int3).
	static MObject aGridMin;			// Minimum point of spatial grid (float3).
	static MObject aGridMax;			// Maximum point of spatial grid (float3).
	static MObject aScalarDataGrid;		// This supplies the scalars for the UV values (double array).
	static MObject aDataMin;			// Minimum value of scalar data grid (double).
	static MObject aDataMax;			// Maximum value of scalar data grid (double).

	// Output attributes
	static MObject aSecondaryMesh;		// Output mesh data with both geometry-based and data-based UV's.
	/*
	static MObject aGeometryBasedMesh;	// Output mesh data with geometry-based UV's.
	static MObject aDataBasedMesh;		// Output mesh data with data-based UV's.
	*/
};


#endif

