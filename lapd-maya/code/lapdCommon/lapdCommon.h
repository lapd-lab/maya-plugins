/* ============================================================================
 * lapdCommon : lapdCommon.h
 /
 / Copyright (C) 2007 Jim Bamber
 /
 */
#ifndef LAPDCOMMON_H
#define LAPDCOMMON_H

#include <maya/MVectorArray.h>
#include <maya/MPoint.h>
#include <maya/MString.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnVectorArrayData.h>

#include "defines.h"


class lapdCommon
{
    public:

	static double interpolateScalar(
		MPoint& position,
		int3& dims,
		float3& gridMax,
		float3& gridMin,
		MDoubleArray& scalarDataGrid,
		double halfWindow );

	static MVector interpolateVector(
		MPoint& position,
		int3& dims,
		float3& gridMax,
		float3& gridMin,
		MVectorArray& vectorDataGrid,
		double halfWindow );

	static void logEntry(
		char *logString );

	static char logString[DI_LOG_STRING_LENGTH];
	static MString version;

	private:

	static char logFilename[DI_LOG_FILENAME_LENGTH];
	static FILE* logFile;
    static int day_of_month;

};


#endif

