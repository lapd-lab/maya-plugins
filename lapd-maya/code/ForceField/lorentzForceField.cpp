/* ============================================================================
// forceField : lorentzForceField.cpp
//
// Copyright (C) 2006 Jim Bamber 
// 
//  Description
//	The lorentzForceField node implements F = q(E+vxB/c) in cgs units.
//
*/

#include "lorentzForceField.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>

#include <maya/MTime.h>
#include <maya/MVectorArray.h>
#include <maya/MDoubleArray.h>
#include <maya/MMatrix.h>
#include <maya/MArrayDataBuilder.h> //STV

#include <maya/MFnDependencyNode.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnCompoundAttribute.h>
#include <maya/MFnUnitAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnMatrixData.h>


MObject lorentzForceField::aParticleType;
MObject lorentzForceField::aChargeMultiple;
MObject lorentzForceField::aMassMultiple;
MObject lorentzForceField::aSpatialGrid;
MObject lorentzForceField::aDims;
MObject lorentzForceField::aGridMin;
MObject lorentzForceField::aGridMax;
MObject lorentzForceField::aEFieldData;
MObject lorentzForceField::aBFieldData;
MObject lorentzForceField::aBackgroundEField;
MObject lorentzForceField::aBackgroundBField;

MTypeId lorentzForceField::id( 0x00451 );


/* ============================================================================
// creator()
*/
void *lorentzForceField::creator()
{
    return new lorentzForceField;
}


/* ============================================================================
// postConstructor()
*/
void lorentzForceField::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType lorentzForceField::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// initialize()
//
//	Descriptions:
//		Initialize the node, attributes.
//
*/
MStatus lorentzForceField::initialize()
{
	MStatus stat, stat2;

	MFnNumericAttribute numAttr;
	MFnTypedAttribute typedAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData vectorArrayData;

	forceFieldCommon::logEntry( (char*) "--> lorentzForceField::initialize()" );


	// create the field basic attributes.
	//
    aParticleType = enumAttr.create( "particleType", "type", 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( enumAttr.addField("electron", 0) );
	diLOG_MSTATUS( enumAttr.addField("ion", 1) );
	MAKE_INPUT( enumAttr );
	diLOG_MSTATUS( addAttribute(aParticleType) );

    aChargeMultiple = numAttr.create( "chargeMultiple", "charge", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( numAttr.setDefault(1) );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aChargeMultiple) );

    aMassMultiple = numAttr.create( "massMultiple", "mass", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( numAttr.setDefault(1) );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aMassMultiple) );

    aSpatialGrid = typedAttr.create( "spatialGrid", "grid", MFnData::kVectorArray, vectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( typedAttr );
	diLOG_MSTATUS( addAttribute(aSpatialGrid) );

	aDims = numAttr.create( "dims", "dims", MFnNumericData::k3Int, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( numAttr.setDefault(0, 0, 0) );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aDims) );

	aGridMin = numAttr.create( "gridMin", "gMin", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( numAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aGridMin) );

	aGridMax = numAttr.create( "gridMax", "gMax", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( numAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aGridMax) );

    aEFieldData = typedAttr.create( "EFieldData", "E_Data", MFnData::kVectorArray, vectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( typedAttr );
	diLOG_MSTATUS( addAttribute(aEFieldData) );

    aBFieldData = typedAttr.create( "BFieldData", "B_Data", MFnData::kVectorArray, vectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( typedAttr );
	diLOG_MSTATUS( addAttribute(aBFieldData) );

	aBackgroundEField = numAttr.create( "backgroundEField", "backgroundE", MFnNumericData::k3Double, 0.0, &stat );
	diLOG_MSTATUS( stat );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aBackgroundEField) );

	aBackgroundBField = numAttr.create( "backgroundBField", "backgroundB", MFnNumericData::k3Double, 0.0, &stat );
	diLOG_MSTATUS( stat );
	MAKE_INPUT( numAttr );
	diLOG_MSTATUS( addAttribute(aBackgroundBField) );

	// No dependencies are set -- perhaps they are taken care of by the parent class.

	forceFieldCommon::logEntry( (char*) "<-- lorentzForceField::initialize()" );
	return( MS::kSuccess );
}


/* ============================================================================
// compute()
//
//	Descriptions:
//		compute output force.
//
*/
MStatus lorentzForceField::compute(const MPlug& plug, MDataBlock& block)
{
	MStatus status;

	//forceFieldCommon::logEntry( (char*) "--> lorentzForceField::compute()" );
	forceFieldCommon::logEntry( forceFieldCommon::eLorentzForceField );
	if( !(plug == mOutputForce) )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from lorentzForceField::compute(), Maya not asking for output force" );
		return MS::kUnknownParameter;
	}

	// get the logical index of the element this plug refers to.
	//
	int multiIndex = plug.logicalIndex( &status );
	diLOG_MSTATUS( status );

	// Get input data handle, use outputArrayValue since we do not
	// want to evaluate both inputs, only the one related to the
	// requested multiIndex. Evaluating both inputs at once would cause
	// a dependency graph loop.
	//
	MArrayDataHandle hInputArray( block.outputArrayValue(mInputData, &status) );
	diLOG_MSTATUS( status );

	diLOG_MSTATUS( hInputArray.jumpToElement(multiIndex) );

	// get children of aInputData.
	//
	MDataHandle hCompound( hInputArray.inputValue(&status) );
	diLOG_MSTATUS( status );

	MDataHandle hPosition( hCompound.child(mInputPositions) );
	MObject dPosition( hPosition.data() );
	MFnVectorArrayData fnPosition( dPosition );
	MVectorArray points( fnPosition.array(&status) );
	diLOG_MSTATUS( status );

	MDataHandle hVelocity( hCompound.child(mInputVelocities) );
	MObject dVelocity( hVelocity.data() );
	MFnVectorArrayData fnVelocity( dVelocity );
	MVectorArray velocities( fnVelocity.array(&status) );
	diLOG_MSTATUS( status );

	//MDataHandle hMass = hCompound.child( mInputMass );
	//MObject dMass = hMass.data();
	//MFnDoubleArrayData fnMass( dMass );
	//MDoubleArray masses = fnMass.array( &status );
	//diLOG_MSTATUS( status );


	// Compute the output force.
	//
	//forceFieldCommon::logEntry( (char*) "About to calculate force array" );
	MVectorArray forceArray;
	calculateForceArray( block, points, velocities, forceArray );

	// get output data handle
	//
	MArrayDataHandle hOutArray( block.outputArrayValue(mOutputForce, &status) );   diLOG_MSTATUS( status );
	MArrayDataBuilder bOutArray( hOutArray.builder(&status) );   diLOG_MSTATUS( status );

	// get output force array from block.
	//
	MDataHandle hOut( bOutArray.addElement(multiIndex, &status) );   diLOG_MSTATUS( status );
	MFnVectorArrayData fnOutputForce;
	MObject dOutputForce( fnOutputForce.create(forceArray, &status) );   diLOG_MSTATUS( status );

	// update data block with new output force data.
	//
	hOut.set( dOutputForce );
	block.setClean( plug );

	//forceFieldCommon::logEntry( (char*) "<-- lorentzForceField::compute()" );
	return( MS::kSuccess );
}


/* ============================================================================
// calculateForceArray()
//
//	Descriptions:
//		Compute output force.
//
*/
void lorentzForceField::calculateForceArray
	(
		MDataBlock &block,					// block containing field params
		const MVectorArray &points,			// current positions of objects
		const MVectorArray &velocities,		// current velocities of objects
		//const MDoubleArray &/*masses*/,		// masses of objects
		MVectorArray &forceArray			// output force array
	)
{
	// points and velocities should have the same length. If not return.
	//
	int pointCount = points.length();
	if( (unsigned int)pointCount != velocities.length() )
	{
		forceFieldCommon::logEntry(
			(char*) "Abnormal exit from lorentzForceField::calculateForceArray(), length mismatch between points and velocities arrays" );
		return;
	}

	// clear the output force array.
	//
	forceArray.clear();

	// get field parameters.
	//
	//double magnitude = getMagnitudeValue( block );
	//double attenuation = getAttenuationValue( block );
	int3& dims = getDims( block );
	float3& gridMin = getGridMin( block );
	float3& gridMax = getGridMax( block );
	MVectorArray EFieldData( getEFieldData(block) );
	MVectorArray BFieldData( getBFieldData(block) );
	MVector backgroundEField( getBackgroundEField(block) );
	MVector backgroundBField( getBackgroundBField(block) );
	double halfWindow = 2.0;

	// get owner's data. posArray may have only one point which is the centroid
	// (if this has owner) or field position(if without owner). Or it may have
	// a list of points if with owner and applyPerVertex.
	//
	//MVectorArray posArray;
	//posArray.clear();
	//ownerPosition( block, posArray );

	//int fieldPosCount = posArray.length();
	MVector point;
	MVector velocity;
	double particleCharge;
	double particleMass;
	short particleType = getParticleType( block );

	if ( particleType == 0 ) 
	{ // electron
		particleCharge = -4.8032e-10;
		particleMass = 9.1094e-28;
	}
	else
	{ // ion
		int chargeMultiple = getChargeMultiple( block );
		int massMultiple = getMassMultiple( block );
		particleCharge = chargeMultiple * 4.8032e-10;
		particleMass = massMultiple * 1.6726e-24;
	}
	double q_over_m = particleCharge / particleMass;   // charge to mass ratio in esu/g
	//
	// Note that mass is divided out so that a default mass of 1 gram can be used in the Maya calculations
	//
	double EForceFactor = q_over_m / 300.0;   // converts from V/cm to statVolts/cm
	double c = 3.0e10;   // speed of light in cm/sec
	double BForceFactor = q_over_m / c;

	// With this model, where the fields are supplied, max distance attribute doesn't do anything.
	// Also attenuation is used as if it were drag.
	//
	MVector EField, BField;
	MVector forceV;
	for (int ptIndex = 0; ptIndex < pointCount; ptIndex ++ )
	{
		point = points[ptIndex];
		velocity = velocities[ptIndex];
		if ( (ptIndex >= 0) && (ptIndex <= 0) )
		{
			double speed = velocity.length();
			//sprintf( forceFieldCommon::logString,
			//	"speed[%d]: %f  velocity[%d]: (%f, %f, %f)", ptIndex, speed, ptIndex, velocity.x, velocity.y, velocity.z );
			//forceFieldCommon::logEntry( forceFieldCommon::logString );
		}

		calculateFields( point, dims, gridMax, gridMin, EFieldData, BFieldData,
			backgroundEField, backgroundBField, halfWindow, EField, BField );

		forceV = calculateForce( velocity, EField, BField, EForceFactor, BForceFactor );

		if ( (ptIndex >= 0) && (ptIndex <= 0) )
		{
			//sprintf( forceFieldCommon::logString,
			//	"BField[%d]: (%f, %f, %f)", ptIndex, BField.x, BField.y, BField.z );
			//forceFieldCommon::logEntry( forceFieldCommon::logString );
			//sprintf( forceFieldCommon::logString,
			//	"EField[%d]: (%f, %f, %f)", ptIndex, EField.x, EField.y, EField.z );
			//forceFieldCommon::logEntry( forceFieldCommon::logString );
			//sprintf( forceFieldCommon::logString,
			//	"forceV[%d]: (%f, %f, %f)", ptIndex, forceV.x, forceV.y, forceV.z );
			//forceFieldCommon::logEntry( forceFieldCommon::logString );
		}

		forceArray.append( forceV );
	}
}


/* ============================================================================
// calculateFields()
*/
void lorentzForceField::calculateFields(
	MVector& point, int3& dims, float3& gridMax, float3& gridMin,
	MVectorArray& EFieldData, MVectorArray& BFieldData, MVector& backgroundEField, MVector& backgroundBField,
	double halfWindow, MVector& EField, MVector& BField )
{
	EField = backgroundEField;
	BField = backgroundBField;

	int EDataLength = EFieldData.length();
	int BDataLength = BFieldData.length();

	if ( (EDataLength == 0) && (BDataLength == 0) )
	{
		forceFieldCommon::logEntry(
			(char*) "Abnormal exit from lorentzForceField::calculateFields(), E field or B field missing" );
		return;
	}

	if ( ( ( point[0] >= gridMin[0] ) && ( point[0] < gridMax[0]-0.001 ) ) &&
		 ( ( point[1] >= gridMin[1] ) && ( point[1] < gridMax[1]-0.001 ) ) &&
		 ( ( point[2] >= gridMin[2] ) && ( point[2] < gridMax[2]-0.001 ) ) )
	{
		MVector weightedE( 0.0, 0.0, 0.0 );
		MVector weightedB( 0.0, 0.0, 0.0 );

		int nx = dims[0];
		int ny = dims[1];
		int nz = dims[2];

		// distances between grid points
		float xmin = gridMin[0];
		float ymin = gridMin[1];
		float zmin = gridMin[2];
		float dx = (gridMax[0]-xmin) / (nx-1);
		float dy = (gridMax[1]-ymin) / (ny-1);
		float dz = (gridMax[2]-zmin) / (nz-1);

		// central point
		float x0 = (float)(point[0]);
		float y0 = (float)(point[1]);
		float z0 = (float)(point[2]);

		// floating point indices of central point
		float fx0 = (x0-xmin) / dx;
		float fy0 = (y0-ymin) / dy;
		float fz0 = (z0-zmin) / dz;

		// number of steps to either side, based on smoothing radius
		int ixStart = (int)floor( fx0 - halfWindow );  if ( ixStart < 0  ) ixStart = 0;
		int iyStart = (int)floor( fy0 - halfWindow );  if ( iyStart < 0  ) iyStart = 0;
		int izStart = (int)floor( fz0 - halfWindow );  if ( izStart < 0  ) izStart = 0;
		int ixEnd   = (int)ceil( fx0 + halfWindow );   if ( ixEnd > nx-1 ) ixEnd   = nx-1;
		int iyEnd   = (int)ceil( fy0 + halfWindow );   if ( iyEnd > ny-1 ) iyEnd   = ny-1;
		int izEnd   = (int)ceil( fz0 + halfWindow );   if ( izEnd > nz-1 ) izEnd   = nz-1;

		// loop through the bounding "cube", total up values within the window
		int nplane = ny * nx;
		int iplane, irow, index;
		int iz, iy, ix;
		float rz, ry, rx;
		float rz2, ry2, rx2;
		float rz2_ry2;
		float r2;
		float limit = (float)(halfWindow * halfWindow);
		float weight;
		float totalWeight = 0.0;

		for ( iz=izStart; iz<=izEnd; iz++ )
		{
			rz = (float)iz - fz0;
			rz2 = rz * rz;
			iplane = iz * nplane;
			for ( iy=iyStart; iy<=iyEnd; iy++ )
			{
				ry = (float)iy - fy0;
				ry2 = ry * ry;
				rz2_ry2 = rz2 + ry2;
				irow = iplane + iy * nx;
				for ( ix=ixStart; ix<=ixEnd; ix++ )
				{
					rx = (float)ix - fx0;
					rx2 = rx * rx;
					r2 = rz2_ry2 + rx2;
					index = irow + ix;
					if ( r2 < limit )
					{
						weight = limit - r2;
						if ( EDataLength > 0 ) weightedE = weightedE + weight*EFieldData[index];
						if ( BDataLength > 0 ) weightedB = weightedB + weight*BFieldData[index];
						totalWeight = totalWeight + weight;
					} // end if within the smoothing radius
				} // end x loop
			} // end y loop
		} // end z loop

		// Divide out the total weight
		weightedE = weightedE/totalWeight;
		weightedB = weightedB/totalWeight;

		EField = EField + weightedE;
		BField = BField + weightedB;
	}
}


/* ============================================================================
// calculateForce()
*/
MVector lorentzForceField::calculateForce(
	MVector& velocity, MVector& EField, MVector& BField, double EForceFactor, double BForceFactor )
{
	MVector forceVector;

	velocity = velocity * 1.0e9;   // this converts from cm/nsec to cm/sec

	// F = q(E+vxB/c)
	forceVector[0] = velocity[1]*BField[2] - velocity[2]*BField[1];
	forceVector[1] = velocity[2]*BField[0] - velocity[0]*BField[2];
	forceVector[2] = velocity[0]*BField[1] - velocity[1]*BField[0];
	forceVector = BForceFactor * forceVector;

	forceVector = EForceFactor * EField + forceVector;

	forceVector = forceVector / 1.0e18;  // this converts from g cm / sec^2 to g cm / nsec^2

	//forceVector = forceVector / particleMass;

	return forceVector;
}


/* ============================================================================
// ownerPosition()
//
//	Descriptions:
//		If this field has an owner, get the owner's position array or
//		centroid, then assign it to the ownerPosArray.
//		If it does not have owner, get the field position in the world
//		space, and assign it to the given array, ownerPosArray.
//
*/
void lorentzForceField::ownerPosition
	(
		MDataBlock& block,
		MVectorArray &ownerPosArray
	)
{
	MStatus status;

	if( getApplyPerVertexValue(block) )
	{
		MDataHandle hOwnerPos( block.inputValue(mOwnerPosData, &status) );
		if( status == MS::kSuccess )
		{
			MObject dOwnerPos( hOwnerPos.data() );
			MFnVectorArrayData fnOwnerPos( dOwnerPos );
			MVectorArray posArray( fnOwnerPos.array(&status) );
			if( status == MS::kSuccess )
			{
				// assign vectors from block to ownerPosArray.
				//
				for( unsigned int i = 0; i < posArray.length(); i ++ )
					ownerPosArray.append( posArray[i] );
			}
			else
			{
				MVector worldPos(0.0, 0.0, 0.0);
				//status = getWorldPosition( block, worldPos );
				status = getWorldPosition( worldPos );
				ownerPosArray.append( worldPos );
			}
		}
		else
		{
			// get the field position in the world space
			// and add it into ownerPosArray.
			//
			MVector worldPos(0.0, 0.0, 0.0);
			//status = getWorldPosition( block, worldPos );
			status = getWorldPosition( worldPos );
			ownerPosArray.append( worldPos );
		}
	}
	else
	{
		MVector centroidV(0.0, 0.0, 0.0);
		status = getOwnerCentroidValue( block, centroidV );
		if( status == MS::kSuccess )
		{
			// assign centroid vector to ownerPosArray.
			//
			ownerPosArray.append( centroidV );
		}
		else
		{
			// get the field position in the world space.
			//
			MVector worldPos(0.0, 0.0, 0.0);
			//status = getWorldPosition( block, worldPos );
			status = getWorldPosition( worldPos );
			ownerPosArray.append( worldPos );
		}
	}
}


/* ============================================================================
// getWorldPosition()
//
//	Descriptions:
//		get the field position in the world space.
//		The position value is from inherited attribute, aWorldMatrix.
//
*/
MStatus lorentzForceField::getWorldPosition( MVector &vector )
{
	MStatus status;

	MObject thisNode( thisMObject() );
	MFnDependencyNode fnThisNode( thisNode );

	// get worldMatrix attribute.
	//
	MObject worldMatrixAttr( fnThisNode.attribute("worldMatrix") );

	// build worldMatrix plug, and specify which element the plug refers to.
	// We use the first element(the first dagPath of this field).
	//
	MPlug matrixPlug( thisNode, worldMatrixAttr );
	matrixPlug = matrixPlug.elementByLogicalIndex( 0 );

	// Get the value of the 'worldMatrix' attribute
	//
	MObject matrixObject;
	status = matrixPlug.getValue( matrixObject );
	if( !status )
	{
		status.perror("lorentzForceField::getWorldPosition(): get matrixObject");
		forceFieldCommon::logEntry( (char*) "Abnormal exit from lorentzForceField::getWorldPosition(): get matrixObject" );
		diLOG_MSTATUS( status );
		return( status );
	}

	MFnMatrixData worldMatrixData( matrixObject, &status );
	if( !status )
	{
		status.perror("lorentzForceField::getWorldPosition(): get worldMatrixData");
		forceFieldCommon::logEntry( (char*) "Abnormal exit from lorentzForceField::getWorldPosition(): get worldMatrixData" );
		diLOG_MSTATUS( status );
		return( status );
	}

	MMatrix worldMatrix = worldMatrixData.matrix( &status );
	if( !status )
	{
		status.perror("lorentzForceField::getWorldPosition(): get worldMatrix");
		forceFieldCommon::logEntry( (char*) "Abnormal exit from lorentzForceField::getWorldPosition(): get worldMatrix" );
		diLOG_MSTATUS( status );
		return( status );
	}

	// assign the translate to the given vector.
	//
	vector[0] = worldMatrix( 3, 0 );
	vector[1] = worldMatrix( 3, 1 );
	vector[2] = worldMatrix( 3, 2 );

    return( status );
}


/* ============================================================================
// getWorldPosition()
//
//	Descriptions:
//		Find the field position in the world space.
//
*/
MStatus lorentzForceField::getWorldPosition( MDataBlock& block, MVector &vector )
{
    MStatus status;

	MObject thisNode( thisMObject() );
	MFnDependencyNode fnThisNode( thisNode );

	// get worldMatrix attribute.
	//
	MObject worldMatrixAttr( fnThisNode.attribute("worldMatrix") );

	// build worldMatrix plug, and specify which element the plug refers to.
	// We use the first element(the first dagPath of this field).
	//
	MPlug matrixPlug( thisNode, worldMatrixAttr );
	matrixPlug = matrixPlug.elementByLogicalIndex( 0 );

    //MDataHandle hWMatrix = block.inputValue( worldMatrix, &status );
    MDataHandle hWMatrix( block.inputValue(matrixPlug, &status) );   diLOG_MSTATUS( status );

    if( status == MS::kSuccess )
    {
        MMatrix wMatrix = hWMatrix.asMatrix();
        vector[0] = wMatrix(3, 0);
        vector[1] = wMatrix(3, 1);
        vector[2] = wMatrix(3, 2);
    }
    return( status );
}


/* ============================================================================
// getForceAtPoint()
//
//    This method is not required to be overridden, it is only necessary
//    for compatibility with the MFnField function set.
//
*/
MStatus lorentzForceField::getForceAtPoint(const MVectorArray&	points,
                                  const MVectorArray&	velocities,
                                  const MDoubleArray&	masses,
                                  MVectorArray&	forceArray,
                                  double	/*deltaTime*/)
{
	MDataBlock block( forceCache() );

	//calculateForceArray( block, points, velocities, masses, forceArray );
	calculateForceArray( block, points, velocities, forceArray );

    return MS::kSuccess;
}


/* ============================================================================
// iconSizeAndOrigin()
*/
MStatus lorentzForceField::iconSizeAndOrigin(	GLuint& width,
					GLuint& height,
					GLuint& xbo,
					GLuint& ybo   )
//
//	This method is not required to be overridden.  It should be overridden
//	if the plug-in has custom icon.
//
//	The width and height have to be a multiple of 32 on Windows and 16 on 
//	other platform.
//
//	Define an 8x8 icon at the lower left corner of the 32x32 grid. 
//	(xbo, ybo) of (4,4) makes sure the icon is center at origin.
//
{
	width = 32;
	height = 32;
	xbo = 4;
	ybo = 4;
	return MS::kSuccess;
}


/* ============================================================================
// iconBitMap()
//
//	This method is not required to be overridden.  It should be overridden
//	if the plug-in has custom icon.
//
*/
MStatus lorentzForceField::iconBitmap(GLubyte* bitmap)
//	Define an 8x8 icon at the lower left corner of the 32x32 grid. 
//	(xbo, ybo) of (4,4) makes sure the icon is center at origin.
{
	bitmap[0] = 0x18;
	bitmap[4] = 0x66;
	bitmap[8] = 0xC3;
	bitmap[12] = 0x81;
	bitmap[16] = 0x81;
	bitmap[20] = 0xC3;
	bitmap[24] = 0x66;
	bitmap[28] = 0x18;

	return MS::kSuccess;
}


