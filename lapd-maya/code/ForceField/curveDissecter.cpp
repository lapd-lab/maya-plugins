/* ============================================================================
// forceField : curveDissecter.cpp
//
// ==========================================================================
// Copyright (C) 2007 Jim Bamber
//
// ==========================================================================
//+
*/
#include "curveDissecter.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>


// Static data
MTypeId curveDissecter::id( 0x00454 );

// Attributes
MObject curveDissecter::aStartIndices;
MObject curveDissecter::aCVCounts;
MObject curveDissecter::aControlVertexList;
MObject curveDissecter::aTangentList;
MObject curveDissecter::aCurveIndex;
MObject curveDissecter::aCVIndex;

MObject curveDissecter::aCVCount;
MObject curveDissecter::aCV;
MObject curveDissecter::aTangent;


/* ============================================================================
// postConstructor()
*/
void curveDissecter::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType curveDissecter::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// curveDissecter()
*/
curveDissecter::curveDissecter()
{
}


/* ============================================================================
// ~curveDissecter()
*/
curveDissecter::~curveDissecter()
{
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * curveDissecter::creator()
{
    return new curveDissecter();
}


/* ============================================================================
// initialize()
//
// initializes attribute information
*/
MStatus curveDissecter::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> curveDissecter::initialize()" );


	// Create input attributes:
	//
	// start indices
	aStartIndices = tAttr.create( "startIndices", "starts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aStartIndices) );

	// CV counts
	aCVCounts = tAttr.create( "CVCounts", "counts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aCVCounts) );

	// control vertices
	aControlVertexList = tAttr.create( "controlVertexList", "cvs", MFnData::kPointArray, fnPointArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aControlVertexList) );

	// tangents
	aTangentList = tAttr.create( "tangentList", "tangents", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aTangentList) );

	// curve index
	aCurveIndex = nAttr.create( "curveIndex", "curve", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveIndex) );

	// CV index
	aCVIndex = nAttr.create( "CVIndex", "CVIndex", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( addAttribute(aCVIndex) );


	// Create output attributes
	//
	// CV count
	aCVCount = nAttr.create( "CVCount", "CVCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_OUTPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCVCount) );

	aCV = nAttr.create( "CV", "CV", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );
	MAKE_OUTPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCV) );

	aTangent = nAttr.create( "tangent", "tangent", MFnNumericData::k3Float, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 1.0) );
	MAKE_OUTPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aTangent) );


	// All inputs affect all outputs
	//
	// CV count
    diLOG_MSTATUS( attributeAffects(aStartIndices, aCVCount) );
    diLOG_MSTATUS( attributeAffects(aCVCounts, aCVCount) );
    diLOG_MSTATUS( attributeAffects(aControlVertexList, aCVCount) );
    diLOG_MSTATUS( attributeAffects(aTangentList, aCVCount) );
    diLOG_MSTATUS( attributeAffects(aCurveIndex, aCVCount) );
    diLOG_MSTATUS( attributeAffects(aCVIndex, aCVCount) );

	// CV
    diLOG_MSTATUS( attributeAffects(aStartIndices, aCV) );
    diLOG_MSTATUS( attributeAffects(aCVCounts, aCV) );
    diLOG_MSTATUS( attributeAffects(aControlVertexList, aCV) );
    diLOG_MSTATUS( attributeAffects(aTangentList, aCV) );
    diLOG_MSTATUS( attributeAffects(aCurveIndex, aCV) );
    diLOG_MSTATUS( attributeAffects(aCVIndex, aCV) );

	// Tangent
    diLOG_MSTATUS( attributeAffects(aStartIndices, aTangent) );
    diLOG_MSTATUS( attributeAffects(aCVCounts, aTangent) );
    diLOG_MSTATUS( attributeAffects(aControlVertexList, aTangent) );
    diLOG_MSTATUS( attributeAffects(aTangentList, aTangent) );
    diLOG_MSTATUS( attributeAffects(aCurveIndex, aTangent) );
    diLOG_MSTATUS( attributeAffects(aCVIndex, aTangent) );


	forceFieldCommon::logEntry( (char*) "<-- curveDissecter::initialize()" );
    return MS::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the curves.
//
*/
MStatus curveDissecter::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> curveDissecter::compute()" );
	forceFieldCommon::logEntry( forceFieldCommon::eCurveDissecter );

	if ( (plug != aCVCount ) && (plug != aCV ) && (plug != aTangent ) )
	{
		forceFieldCommon::logEntry(
			(char*) "Abnormal exit from curveDissecter::compute(), Maya not asking for CV count, CV, or tangent" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hStartIndices( block.inputValue(aStartIndices, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnStartIndices( hStartIndices.data() );
	MIntArray startIndices( fnStartIndices.array() );

	MDataHandle hCVCounts( block.inputValue(aCVCounts, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnCVCounts( hCVCounts.data() );
	MIntArray CVCounts( fnCVCounts.array() );

	MDataHandle hControlVertexList( block.inputValue(aControlVertexList, &stat) );   diLOG_MSTATUS( stat );
	MFnPointArrayData fnControlVertexList( hControlVertexList.data() );
	MPointArray controlVertexList( fnControlVertexList.array() );

	MDataHandle hTangentList( block.inputValue(aTangentList, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnTangentList( hTangentList.data() );
	MVectorArray tangentList( fnTangentList.array() );

	MDataHandle hCurveIndex( block.inputValue(aCurveIndex, &stat) );   diLOG_MSTATUS( stat );
	int curveIndex = hCurveIndex.asInt();

	MDataHandle hCVIndex( block.inputValue(aCVIndex, &stat) );   diLOG_MSTATUS( stat );
	int CVIndex = hCVIndex.asInt();


	// Now do the output attributes.
	//
	MDataHandle hCVCount( block.outputValue(aCVCount, &stat) );   diLOG_MSTATUS( stat );
	int& CVCount = hCVCount.asInt();

	MDataHandle hCV( block.outputValue(aCV, &stat) );   diLOG_MSTATUS( stat );
	float3& CV = hCV.asFloat3();

	MDataHandle hTangent( block.outputValue(aTangent, &stat) );   diLOG_MSTATUS( stat );
	float3& tangent = hTangent.asFloat3();

    CVCount = CVCounts[curveIndex];
	int index = startIndices[curveIndex] + CVIndex;
	MPoint controlVertex( controlVertexList[index] );
	CV[0] = (float)(controlVertex.x);
	CV[1] = (float)(controlVertex.y);
	CV[2] = (float)(controlVertex.z);
	MVector tangentVector( tangentList[index] );
	tangent[0] = (float)(tangentVector.x);
	tangent[1] = (float)(tangentVector.y);
	tangent[2] = (float)(tangentVector.z);

	//hCVCount.set( CVCount );
	//hCV.set( CV );
	//hTangent.set( tangent );

	hCVCount.setClean();
	hCV.setClean();
	hTangent.setClean();

	//forceFieldCommon::logEntry( (char*) "<-- curveDissecter::compute()" );
	return stat;
}
