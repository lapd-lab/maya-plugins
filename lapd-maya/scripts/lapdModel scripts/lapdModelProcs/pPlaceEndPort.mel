// PLACE END PORT (PROC)
// *** places a new port instance on the end flange, at the specified location in cylindrical coordinates,
// *** that is, the port normal points along z:
// $basePort     -- name of port object to instance (or create)
// $portDiameter -- diameter of port (only used when creating original)
// $z            -- distance along z from main chamber port 1, toward cathode
// $radius       -- distance from chamber center line 
// $angle        -- angle around chamber from East port location (degrees) 

global proc placeEndPort(
  string $basePort,
  float $portDiameter,
  float $z,
  float $radius,
  float $angle,
  int $portType)
{
  float $angle_rad = $angle * 3.1415927/180.0;
  float $x = $radius * cos($angle_rad);
  float $y = $radius * sin($angle_rad);

  if (`objExists $basePort`)
  {
    string $newPort = $basePort + "_" + floor(abs($z)) + "_" + floor(abs($radius)) + "_" + floor(abs($angle));
    instance -name $newPort $basePort;
    select -r $newPort;
    sets -add lapdSet;
    move -a $x $y $z;
  }
  else
  {
    port($basePort, $portDiameter, $portType);
    select -r $basePort;
    sets -add lapdSet;
    move -r $x $y $z;
  }
}

