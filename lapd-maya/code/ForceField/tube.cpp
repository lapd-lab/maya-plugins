/* ============================================================================
// forceField : tube.cpp
//
// 2011-11-28.  Uses addPolygon() method which is very slow for large meshes.  See tubeUV instead.
//-
// ==========================================================================
// Copyright (C) 2007 Jim Bamber
//
// ==========================================================================
//+
*/
#include "tube.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "forceFieldCommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnEnumAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MFnPointArrayData.h>
#include <maya/MFnIntArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>
#include <maya/MFnNurbsCurveData.h>
#include <maya/MFnMesh.h>
#include <maya/MFnMeshData.h>
#include <maya/MPointArray.h>
#include <maya/MIntArray.h>
#include <maya/MFloatArray.h>
#include <maya/MItMeshPolygon.h>
#include <maya/MItMeshVertex.h>
#include <maya/MItMeshEdge.h>
#include <maya/MQuaternion.h>


// Static data
MTypeId tube::id( 0x00459 );
MPointArray tube::unitCircle;

// Attributes
MObject tube::aCurveCount;
MObject tube::aStartIndices;
MObject tube::aCVCounts;
MObject tube::aControlVertexList;
MObject tube::aTangentList;
MObject tube::aCurveIndex;
MObject tube::aTubeRadius;
MObject tube::aSegmentCount;

MObject tube::aSecondaryMesh;


/* ============================================================================
// createMesh()
*/
MStatus tube::createMesh(
	int curveCount,
	MIntArray startIndices,
	MIntArray CVCounts,
	MPointArray controlVertexList,
	MVectorArray tangentList,
	int curveIndex,
	int segmentCount,
	double tubeRadius,
	MObject& newOutputData )
{
	MStatus stat;

	MFnMesh fnMesh;
	MPointArray polygonVertices;
	MVector translation;
	int index;
	MPoint controlVertex1, controlVertex2;
	MVector tangent1, tangent2;

	int CVCount = 0;
	int startIndex = 0;
	if ( curveIndex < curveCount )
	{
		CVCount = CVCounts[ curveIndex ];
		startIndex = startIndices[ curveIndex ];
	}

	if ( CVCount > 1 )
	{
		for ( int iCV = 0; iCV < CVCount-1; iCV++ )
		{
			index = startIndex + iCV;
			controlVertex1 = controlVertexList[index];
			controlVertex2 = controlVertexList[index+1];
			tangent1 = tangentList[index];
			tangent2 = tangentList[index+1];

			addSection(
				fnMesh,
				newOutputData,
				controlVertex1,
				controlVertex2,
				tangent1,
				tangent2,
				tubeRadius,
				segmentCount );
		}
	}
	else if ( CVCount == 1 )
	{
		index = startIndex;
		controlVertex1 = controlVertexList[index];
		controlVertex2 = controlVertex1 + MPoint(0.1, 0.1, 0.1);
		tangent1 = tangentList[index];
		tangent2 = tangent1;

		addSection(
			fnMesh,
			newOutputData,
			controlVertex1,
			controlVertex2,
			tangent1,
			tangent2,
			tubeRadius,
			segmentCount );
	}
	else
	{
		// do nothing
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// addSection()
*/
MStatus tube::addSection(
	MFnMesh& fnMesh,
	MObject& newOutputData,
	MPoint controlVertex1,
	MPoint controlVertex2,
	MVector tangent1,
	MVector tangent2,
	double tubeRadius,
	int segmentCount )
{
	MStatus stat;

	// There are two circles, "1" and "2".  The polygon joins one segment from each circle.  The ends of
	// each segment are labelled "a" and "b".

	MVector zAxis( 0.0, 0.0, 1.0 );
	MQuaternion quaternion1( zAxis, tangent1 );
	MQuaternion quaternion2( zAxis, tangent2 );
	MVector vector1a, vector1b, vector2a, vector2b;
	MVector rVector1a, rVector1b, rVector2a, rVector2b;
	MVector vector;
	MVector rotatedVector;
	double length1 = tubeRadius;
	double length2 = tubeRadius;
	MPointArray vertexArray;

	vertexArray.append( controlVertex1 );
	vertexArray.append( controlVertex2 );
	vertexArray.append( controlVertex2 );
	vertexArray.append( controlVertex1 );   // to make sure there are 4 entries in the array
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		vector1a = tube::unitCircle[ iSegment ];
		vector2a = vector1a;
		if ( iSegment == segmentCount-1 ) vector1b = tube::unitCircle[ 0 ];
		else vector1b = tube::unitCircle[ iSegment + 1 ];
		vector2b = vector1b;

		vector1a = vector1a * length1;
		vector1b = vector1b * length1;
		vector2a = vector2a * length2;
		vector2b = vector2b * length2;

		rVector1a = vector1a.rotateBy( quaternion1 );
		rVector1b = vector1b.rotateBy( quaternion1 );
		rVector2a = vector2a.rotateBy( quaternion2 );
		rVector2b = vector2b.rotateBy( quaternion2 );

		vertexArray[ 0 ] = controlVertex1 + rVector1a;
		vertexArray[ 1 ] = controlVertex2 + rVector2a;
		vertexArray[ 2 ] = controlVertex2 + rVector2b;
		vertexArray[ 3 ] = controlVertex1 + rVector1b;
		fnMesh.addPolygon( vertexArray, true, 1.0e-4, newOutputData, &stat );   diLOG_MSTATUS( stat );
	}

	return MStatus::kSuccess;
}


/* ============================================================================
// compute()
//
// This function gets called by Maya to calculate the curves.
//
*/
MStatus tube::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	//forceFieldCommon::logEntry( (char*) "--> tube::compute()" );
	forceFieldCommon::logEntry( forceFieldCommon::eTube );

	if ( plug != aSecondaryMesh )
	{
		forceFieldCommon::logEntry( (char*) "Abnormal exit from tube::compute(), Maya not asking for secondary mesh" );
		return MS::kUnknownParameter;
	}


	// Extract the input attributes.
	//
	MDataHandle hCurveCount( block.inputValue(aCurveCount, &stat) );   diLOG_MSTATUS( stat );
	int curveCount = hCurveCount.asInt();

	MDataHandle hStartIndices( block.inputValue(aStartIndices, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnStartIndices( hStartIndices.data() );
	MIntArray startIndices( fnStartIndices.array() );

	MDataHandle hCVCounts( block.inputValue(aCVCounts, &stat) );   diLOG_MSTATUS( stat );
	MFnIntArrayData fnCVCounts( hCVCounts.data() );
	MIntArray CVCounts( fnCVCounts.array() );

	MDataHandle hControlVertexList( block.inputValue(aControlVertexList, &stat) );   diLOG_MSTATUS( stat );
	MFnPointArrayData fnControlVertexList( hControlVertexList.data() );
	MPointArray controlVertexList( fnControlVertexList.array() );

	MDataHandle hTangentList( block.inputValue(aTangentList, &stat) );   diLOG_MSTATUS( stat );
	MFnVectorArrayData fnTangentList( hTangentList.data() );
	MVectorArray tangentList( fnTangentList.array() );

	MDataHandle hCurveIndex( block.inputValue(aCurveIndex, &stat) );   diLOG_MSTATUS( stat );
	int curveIndex = hCurveIndex.asInt();

	MDataHandle hTubeRadius( block.inputValue(aTubeRadius, &stat) );   diLOG_MSTATUS( stat );
	double tubeRadius = hTubeRadius.asDouble();

	MDataHandle hSegmentCount( block.inputValue(aSegmentCount, &stat) );   diLOG_MSTATUS( stat );
	int segmentCount = hSegmentCount.asInt();


	// Now do the output attributes.
	//
	MDataHandle hSecondaryMesh( block.outputValue(aSecondaryMesh, &stat) );   diLOG_MSTATUS( stat );


	// Create the data object which underlies the output mesh
	MFnMeshData fnMeshData;
	MObject newOutputData( fnMeshData.create(&stat) );   diLOG_MSTATUS( stat );

	// Unit circle
	unitCircle.clear();
	double angleFactor = (1.0/(double)segmentCount)*(2.0*3.1415927);
	double theta;
	double x, y, z = 0.0;
	for ( int iSegment=0; iSegment < segmentCount; iSegment++ )
	{
		theta = (double)iSegment * angleFactor;
		x = cos( theta );
		y = sin( theta );
		unitCircle.append( x, y, z );
	}

	// Make the call to create the secondary mesh
	//forceFieldCommon::logEntry( (char*) "About to create mesh" );
	diLOG_MSTATUS(
		createMesh(
			curveCount,
			startIndices,
			CVCounts,
			controlVertexList,
			tangentList,
			curveIndex,
			segmentCount,
			tubeRadius,
			newOutputData) );


	// Set the outputs as clean.
	hSecondaryMesh.set( newOutputData );
	block.setClean( plug );

	//forceFieldCommon::logEntry( (char*) "<-- tube::compute()" );
	return stat;
}


/* ============================================================================
// initialize()
//
// initializes attribute information and the unit circle values
*/
MStatus tube::initialize()
{
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnMatrixAttribute mAttr;
	MFnEnumAttribute enumAttr;
	MFnVectorArrayData fnVectorArrayData;	
	MFnDoubleArrayData fnDoubleArrayData;	
	MFnPointArrayData fnPointArrayData;	
	MFnIntArrayData fnIntArrayData;	
	MStatus stat, stat2;

	forceFieldCommon::logEntry( (char*) "--> tube::initialize()" );


	// Create input attributes:
	//
	// curve count
	aCurveCount = nAttr.create( "curveCount", "curveCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveCount) );

	// start indices
	aStartIndices = tAttr.create( "startIndices", "startIndices", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aStartIndices) );

	// CV counts
	aCVCounts = tAttr.create( "CVCounts", "CVCounts", MFnData::kIntArray, fnIntArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aCVCounts) );

	// control vertices
	aControlVertexList = tAttr.create( "controlVertexList", "controlVertexList", MFnData::kPointArray, fnPointArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aControlVertexList) );

	// tangents
	aTangentList = tAttr.create( "tangentList", "tangentList", MFnData::kVectorArray, fnVectorArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );
	diLOG_MSTATUS( addAttribute(aTangentList) );

	// curve index
	aCurveIndex = nAttr.create( "curveIndex", "curveIndex", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aCurveIndex) );

	// tube radius
	aTubeRadius = nAttr.create( "tubeRadius", "tubeRadius", MFnNumericData::kDouble, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(1.0) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aTubeRadius) );

	// segment count
	aSegmentCount = nAttr.create( "segmentCount", "segmentCount", MFnNumericData::kInt, 0, &stat );   diLOG_MSTATUS( stat );
	diLOG_MSTATUS( nAttr.setDefault(18) );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( addAttribute(aSegmentCount) );


	// Create output attributes
	//
	// secondary mesh
	aSecondaryMesh = tAttr.create( "secondaryMesh", "secondaryMesh", MFnData::kMesh, MObject::kNullObj, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( tAttr );
    diLOG_MSTATUS( addAttribute(aSecondaryMesh) );


	// All inputs affect all outputs
	//
	// secondary mesh
    diLOG_MSTATUS( attributeAffects(aCurveCount, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aStartIndices, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCVCounts, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aControlVertexList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTangentList, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aCurveIndex, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aTubeRadius, aSecondaryMesh) );
    diLOG_MSTATUS( attributeAffects(aSegmentCount, aSecondaryMesh) );

	forceFieldCommon::logEntry( (char*) "<-- tube::initialize()" );
	return MS::kSuccess;
}


/* ============================================================================
// tube()
*/
tube::tube()
{
}


/* ============================================================================
// postConstructor()
*/
void tube::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType tube::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ============================================================================
// creator()
//
// creates an instance of the node
*/
void * tube::creator()
{
    return new tube();
}


/* ============================================================================
// ~tube()
*/
tube::~tube()
{
}


