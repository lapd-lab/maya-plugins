/* ==========================================================================
// Texture 3D : scalarDataInterpolation.cpp
//-
// ==========================================================================
// Copyright (C) Jim Bamber
//
// ==========================================================================
//+
*/
#include "scalarDataInterpolation.h"
#include "../lapdCommon/defines.h"
#include "../lapdCommon/lapdCommon.h"
#include "texture3Dcommon.h"

#include <math.h>
#include <stdlib.h>

#include <maya/MGlobal.h>
#include <maya/MPxNode.h>
#include <maya/MIOStream.h>
#include <maya/MString.h>
#include <maya/MTypeId.h>
#include <maya/MPlug.h>
#include <maya/MDataBlock.h>
#include <maya/MDataHandle.h> 
#include <maya/MFnNumericAttribute.h>
#include <maya/MFnMatrixAttribute.h>
#include <maya/MFnTypedAttribute.h>
#include <maya/MFloatPoint.h>
#include <maya/MFloatVector.h>
#include <maya/MFloatMatrix.h>
//#include <maya/MFnPlugin.h> -- this turns the piece of code into the plug-in main
#include <maya/MFnVectorArrayData.h>
#include <maya/MFnDoubleArrayData.h>
#include <maya/MVector.h>
#include <maya/MVectorArray.h>


// Static data
MTypeId scalarDataInterpolation::id( 0x00400 );

// Attributes
MObject scalarDataInterpolation::halfWindow;

MObject scalarDataInterpolation::aPlaceMat;
MObject scalarDataInterpolation::aPointWorld;
 
//MObject scalarDataInterpolation::grid;
MObject scalarDataInterpolation::scalarDataGrid;
MObject scalarDataInterpolation::dims;
MObject scalarDataInterpolation::gridMax;
MObject scalarDataInterpolation::gridMin;
MObject scalarDataInterpolation::dataMax;
MObject scalarDataInterpolation::dataMin;
 
MObject scalarDataInterpolation::scaledDatum;


/* ==========================================================================
// postConstructor()
*/
void scalarDataInterpolation::postConstructor( )
{

}


/* ============================================================================
 * schedulingType()
 */
MPxNode::SchedulingType scalarDataInterpolation::schedulingType( ) const
{
    return MPxNode::SchedulingType::kUntrusted;
}


/* ==========================================================================
// scalarDataInterpolation()
*/
scalarDataInterpolation::scalarDataInterpolation()
{
}


/* ==========================================================================
// ~scalarDataInterpolation()
*/
scalarDataInterpolation::~scalarDataInterpolation()
{
}


/* ==========================================================================
// creator()
//
// creates an instance of the node
*/
void * scalarDataInterpolation::creator()
{
    return new scalarDataInterpolation();
}


/* ==========================================================================
// initialize()
//
// initializes attribute information
*/
MStatus scalarDataInterpolation::initialize()
{
    MFnMatrixAttribute mAttr; 
    MFnNumericAttribute nAttr;
	MFnTypedAttribute tAttr;
	MFnVectorArrayData vectorArrayData;		
	MFnDoubleArrayData doubleArrayData;		
	MStatus stat, stat2;

	texture3Dcommon::logEntry( (char*) "--> scalarDataInterpolation::initialize()" );


	// Create input attributes:
	//
	// halfWindow = 2.0 points
	halfWindow = nAttr.create( "halfWindow", "win", MFnNumericData::kFloat, 0.0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( nAttr.setDefault(2.0) );
    diLOG_MSTATUS( nAttr.setMin(1.0) );

	// aPlaceMat
    aPlaceMat = mAttr.create( "placementMatrix", "pm", MFnMatrixAttribute::kFloat, &stat );   diLOG_MSTATUS( stat );
    MAKE_INPUT( mAttr );

	// grid
	//grid = tAttr.create( "grid", "grid", MFnData::kVectorArray, vectorArrayData.create(&stat2), &stat );
	//diLOG_MSTATUS( stat );
	//diLOG_MSTATUS( stat2 );
	//MAKE_INPUT( tAttr );

	// scalarDataGrid
	scalarDataGrid = tAttr.create( "scalarDataGrid", "sData", MFnData::kDoubleArray, doubleArrayData.create(&stat2), &stat );
	diLOG_MSTATUS( stat );
	diLOG_MSTATUS( stat2 );
	MAKE_INPUT( tAttr );

	// dims = (1,1,1)
    dims = nAttr.create( "dimensions", "dims", MFnNumericData::k3Int, 1, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( nAttr.setDefault(1, 1, 1) );

	// gridMax = (0.0, 0.0, 0.0)
    gridMax = nAttr.create( "gridMaximums", "gMax", MFnNumericData::k3Float, 0.0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );

	// gridMin = (0.0, 0.0, 0.0)
    gridMin = nAttr.create( "gridMinimums", "gMin", MFnNumericData::k3Float, 0.0, &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
	diLOG_MSTATUS( nAttr.setDefault(0.0, 0.0, 0.0) );

	// dataMax = 0.0
    dataMax = nAttr.create( "dataMaximum", "dMax", MFnNumericData::kFloat, 0.0, &stat );   diLOG_MSTATUS( stat );
	//MAKE_INPUT( nAttr );

	// dataMin = 0.0
    dataMin = nAttr.create( "dataMinimum", "dMin", MFnNumericData::kFloat, 0.0, &stat );   diLOG_MSTATUS( stat );
	//MAKE_INPUT( nAttr );


	// Implicit shading network attributes
	//
	// aPointWorld
    aPointWorld = nAttr.createPoint( "pointWorld", "pw", &stat );   diLOG_MSTATUS( stat );
	MAKE_INPUT( nAttr );
    diLOG_MSTATUS( nAttr.setHidden(true) );


    // Create output attributes
	//
	// scaledDatum
    scaledDatum = nAttr.create( "scaledDatum", "scaled", MFnNumericData::kFloat, 0.0, &stat );   diLOG_MSTATUS( stat );
	MAKE_OUTPUT( nAttr );


    // Add the attributes
	//
    diLOG_MSTATUS( addAttribute(halfWindow) );
    diLOG_MSTATUS( addAttribute(aPointWorld) );
    diLOG_MSTATUS( addAttribute(aPlaceMat) );

	//diLOG_MSTATUS( addAttribute(grid) );
	diLOG_MSTATUS( addAttribute(scalarDataGrid) );
	diLOG_MSTATUS( addAttribute(dims) );
	diLOG_MSTATUS( addAttribute(gridMax) );
	diLOG_MSTATUS( addAttribute(gridMin) );
	diLOG_MSTATUS( addAttribute(dataMax) );
	diLOG_MSTATUS( addAttribute(dataMin) );

    diLOG_MSTATUS( addAttribute(scaledDatum) );


    // All inputs affect the output scaledDatum
	//
    diLOG_MSTATUS( attributeAffects(halfWindow, scaledDatum) );
    diLOG_MSTATUS( attributeAffects(aPointWorld, scaledDatum) );
    diLOG_MSTATUS( attributeAffects(aPlaceMat, scaledDatum) );
    //diLOG_MSTATUS( attributeAffects(grid, scaledDatum) );
    diLOG_MSTATUS( attributeAffects(scalarDataGrid, scaledDatum) );
    diLOG_MSTATUS( attributeAffects(dims, scaledDatum) );
    diLOG_MSTATUS( attributeAffects(gridMax, scaledDatum) );
    diLOG_MSTATUS( attributeAffects(gridMin, scaledDatum) );
    diLOG_MSTATUS( attributeAffects(dataMax, scaledDatum) );
    diLOG_MSTATUS( attributeAffects(dataMin, scaledDatum) );

	texture3Dcommon::logEntry( (char*) "<-- scalarDataInterpolation::initialize()" );
    return MS::kSuccess;
}


/* ==========================================================================
// calculateScaledDatum()
*/
float scalarDataInterpolation::calculateScaledDatum(
	MFloatPoint& solidPos,
	int3& mDims,
	float3& mGridMax,
	float3& mGridMin,
	float mDataMax,
	float mDataMin,
	MFnDoubleArrayData& mScalarDataGrid,
	double mHalfWindow )
{
	int nx = mDims[0];
	int ny = mDims[1];
	int nz = mDims[2];

	// distances between grid points
	float xmin = mGridMin[0];
	float ymin = mGridMin[1];
	float zmin = mGridMin[2];
	float dx = (mGridMax[0]-xmin) / (nx-1);
	float dy = (mGridMax[1]-ymin) / (ny-1);
	float dz = (mGridMax[2]-zmin) / (nz-1);

	// central point
	float x0 = solidPos[0];
	float y0 = solidPos[1];
	float z0 = solidPos[2];

	// indices of central point, both floating point and rounded down to nearest integer
	float fx0 = (x0-xmin) / dx;
	float fy0 = (y0-ymin) / dy;
	float fz0 = (z0-zmin) / dz;
	//int ix0 = (int)floor( fx0 );
	//int iy0 = (int)floor( fy0 );
	//int iz0 = (int)floor( fz0 );

	// number of steps to either side, based on smoothing radius
	int ixStart = (int)floor( fx0 - mHalfWindow );  if ( ixStart < 0  ) ixStart = 0;
	int iyStart = (int)floor( fy0 - mHalfWindow );  if ( iyStart < 0  ) iyStart = 0;
	int izStart = (int)floor( fz0 - mHalfWindow );  if ( izStart < 0  ) izStart = 0;
	int ixEnd   = (int)ceil( fx0 + mHalfWindow );   if ( ixEnd > nx-1 ) ixEnd   = nx-1;
	int iyEnd   = (int)ceil( fy0 + mHalfWindow );   if ( iyEnd > ny-1 ) iyEnd   = ny-1;
	int izEnd   = (int)ceil( fz0 + mHalfWindow );   if ( izEnd > nz-1 ) izEnd   = nz-1;

	// loop through the bounding "cube", total up values within the window
	int nplane = ny * nx;
	int iplane, irow, index;
	int iz, iy, ix;
	float rz, ry, rx;
	float rz2, ry2, rx2;
	float rz2_ry2;
	float r2;
	float limit = (float)(mHalfWindow * mHalfWindow);
	float weight;
	float weightedData = 0.0;
	float totalWeight = 0.0;
	for ( iz=izStart; iz<=izEnd; iz++ )
	{
		rz = (float)iz - fz0;
		rz2 = rz * rz;
		iplane = iz * nplane;
		for ( iy=iyStart; iy<=iyEnd; iy++ )
		{
			ry = (float)iy - fy0;
			ry2 = ry * ry;
			rz2_ry2 = rz2 + ry2;
			irow = iplane + iy * nx;
			for ( ix=ixStart; ix<=ixEnd; ix++ )
			{
				rx = (float)ix - fx0;
				rx2 = rx * rx;
				r2 = rz2_ry2 + rx2;
				index = irow + ix;
				if ( r2 < limit )
				{
					weight = limit - r2;
					weightedData = weightedData + weight*(float)(mScalarDataGrid[index]);
					totalWeight = totalWeight + weight;
				} // end if within the smoothing radius
			} // end x loop
		} // end y loop
	} // end z loop

	weightedData = weightedData/totalWeight;
	float scaledDatum = (weightedData-mDataMin) / (mDataMax-mDataMin);
	if ( scaledDatum < 0.0 ) scaledDatum = 0.0;
	if ( scaledDatum > 1.0 ) scaledDatum = 1.0;
	return scaledDatum;
}


/* ==========================================================================
// compute()
//
// This function gets called by Maya to evaluate the texture.
//
*/
MStatus scalarDataInterpolation::compute(const MPlug& plug, MDataBlock& block) 
{
	MStatus stat;
	texture3Dcommon::logEntry( texture3Dcommon::eScalarDataInterpolation );

	// looking for scaledDatum
    if ( plug != scaledDatum )
	{
		texture3Dcommon::logEntry(
			(char*) "Abnormal exit from scalarDataInterpolation::compute(), Maya not asking for scaled datum" );
		return MS::kUnknownParameter;
	}

	MDataHandle halfWindowHandle( block.inputValue(halfWindow, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle worldPosHandle( block.inputValue(aPointWorld, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle placementMatrixHandle( block.inputValue(aPlaceMat, &stat) );  diLOG_MSTATUS( stat );
	//MDataHandle gridHandle( block.inputValue(grid, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle scalarDataGridHandle( block.inputValue(scalarDataGrid, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle dimsHandle( block.inputValue(dims, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle gridMaxHandle( block.inputValue(gridMax, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle gridMinHandle( block.inputValue(gridMin, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle dataMaxHandle( block.inputValue(dataMax, &stat) );  diLOG_MSTATUS( stat );
	MDataHandle dataMinHandle( block.inputValue(dataMin, &stat) );  diLOG_MSTATUS( stat );

	float mHalfWindow  = halfWindowHandle.asFloat();
	float3 & worldPos  = worldPosHandle.asFloat3();
	MFloatMatrix& mat( placementMatrixHandle.asFloatMatrix() );
	int3 & mDims	   = dimsHandle.asInt3();
	float3 & mGridMax  = gridMaxHandle.asFloat3();
	float3 & mGridMin  = gridMinHandle.asFloat3();
	float mDataMax	   = dataMaxHandle.asFloat();
	float mDataMin	   = dataMinHandle.asFloat();

	//MFnVectorArrayData mGrid = gridHandle.data();   // not used now but needed for irregular grids
	MFnDoubleArrayData mScalarDataGrid( scalarDataGridHandle.data() );

	MFloatPoint solidPos( worldPos[0], worldPos[1], worldPos[2] );
	solidPos *= mat;   // Convert into solid space

	//float resultScaledDatum = -1.0;   // Flag with negative value for points outside the data volume
	float resultScaledDatum = 0.0;   // Set to zero for points outside the data volume

	//texture3Dcommon::logEntry( (char*) "About to calculate scaled datum" );
	if ( ( ( solidPos[0] >= mGridMin[0] ) && ( solidPos[0] < mGridMax[0]-0.001 ) ) &&
		 ( ( solidPos[1] >= mGridMin[1] ) && ( solidPos[1] < mGridMax[1]-0.001 ) ) &&
		 ( ( solidPos[2] >= mGridMin[2] ) && ( solidPos[2] < mGridMax[2]-0.001 ) ) )
	{
		resultScaledDatum = calculateScaledDatum(
		  solidPos, mDims, mGridMax, mGridMin, mDataMax, mDataMin, mScalarDataGrid, mHalfWindow );
	}

	// Set scaledDatum attribute
	MDataHandle scaledDatumHandle( block.outputValue(scaledDatum) );
	float& mScaledDatum = scaledDatumHandle.asFloat();
	mScaledDatum = resultScaledDatum;
	scaledDatumHandle.setClean();

	//texture3Dcommon::logEntry( (char*) "<-- scalarDataInterpolation::compute()" );
    return MS::kSuccess;
}
