#-
# ==========================================================================
# Copyright (c) 2018 Autodesk, Inc.
# All rights reserved.
# 
# These coded instructions, statements, and computer programs contain
# unpublished proprietary information written by Autodesk, Inc., and are
# protected by Federal copyright law. They may not be disclosed to third
# parties or copied or duplicated in any form, in whole or in part, without
# the prior written consent of Autodesk, Inc.
# ==========================================================================
#+


cmake_minimum_required(VERSION 2.8)

# include the project setting file
include($ENV{DEVKIT_LOCATION}/cmake/pluginEntry.cmake)

# specify project name
set(PROJECT_NAME ForceField)

# HDF5
set(HDF5_INCLUDE_DIR $ENV{HDF5_INCLUDE_DIR})
include_directories(${HDF5_INCLUDE_DIR})

# set SOURCE_FILES
set(SOURCE_FILES
    circles.cpp curveDissecter.cpp curves.cpp envelopes.cpp fieldTracer.cpp fins.cpp forceFieldCommon.cpp linTrans.cpp linTransScalar.cpp lorentzForceField.cpp markers.cpp pluginMain.cpp secondaryData.cpp secondaryData2.cpp squares.cpp tube.cpp tubeUV.cpp tubesUV.cpp
    circles.h curveDissecter.h curves.h envelopes.h fieldTracer.h fins.h forceFieldCommon.h linTrans.h linTransScalar.h lorentzForceField.h markers.h secondaryData.h secondaryData2.h squares.h tube.h tubeUV.h tubesUV.h
    ../lapdCommon/lapdCommon.cpp
    ../lapdCommon/lapdCommon.h ../lapdCommon/defines.h
    ${HDF5_INCLUDE_DIR}/H5public.h
)

# set linking libraries
set(LIBRARIES
     OpenMaya Foundation OpenMayaAnim OpenMayaFX OpenMayaRender OpenMayaUI
)

# find z
find_zlib()

# find Alembic
find_alembic()

# Build plugin
build_plugin()

