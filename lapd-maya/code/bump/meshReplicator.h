/* ============================================================================
// bump : meshReplicator.h
//
// Copyright (C) 2007 Jim Bamber 
//
*/
#ifndef MESHREPLICATOR_H
#define MESHREPLICATOR_H

#include <maya/MPxNode.h>
#include <maya/MTypeId.h>
#include <maya/MFnNurbsCurve.h>
#include <maya/MFloatPoint.h>
#include <maya/MVectorArray.h>
#include <maya/MMatrix.h>
#include <maya/MFnMesh.h>
#include <maya/MItMeshEdge.h>


class meshReplicator : public MPxNode
{
    public:

    meshReplicator();
	virtual void postConstructor();
    virtual ~meshReplicator();

    static  void *  creator();
    static  MStatus initialize();

    MPxNode::SchedulingType schedulingType() const;

    virtual MStatus compute( const MPlug&, MDataBlock& );

	//  Id tag for use with binary file format
    static MTypeId id;

	private:

    static MStatus replicateMesh(
		MPointArray& inVertices,
		MIntArray& inPolygonVertexCounts,
		MIntArray& inPolygonVertexIndices,
		int nx, int ny, int nz,
		double dx, double dy, double dz,
		MPointArray& outVertices,
		MIntArray& outPolygonVertexCounts,
		MIntArray& outPolygonVertexIndices );

	// Input attributes
	static MObject aInputMesh;			// The mesh of the base object to replicate.
	static MObject aNx;					// Number of copies of the mesh in the x direction.
	static MObject aNy;					// Number of copies of the mesh in the y direction.
	static MObject aNz;					// Number of copies of the mesh in the z direction.
	static MObject aDx;					// Offset of the mesh in the x direction.
	static MObject aDy;					// Offset of the mesh in the y direction.
	static MObject aDz;					// Offset of the mesh in the z direction.

	// Output attributes
	static MObject aOutputMesh;			// The replicated mesh.
};


#endif

